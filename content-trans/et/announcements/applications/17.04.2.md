---
aliases:
- ../announce-applications-17.04.2
changelog: true
date: 2017-06-08
description: KDE toob välja KDE rakendused 17.04.2
layout: application
title: KDE toob välja KDE rakendused 17.04.2
version: 17.04.2
---
June 8, 2017. Today KDE released the second stability update for <a href='../17.04.0'>KDE Applications 17.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Rohkem kui 15 teadaoleva veaparanduse hulka kuuluvad Kdepimi, Arki, Dolphini, Gwenview, Kdenlive'i ja teiste rakenduste täiustused.

This release also includes Long Term Support version of KDE Development Platform 4.14.33.
