---
aliases:
- ../announce-applications-15.12.0
changelog: true
date: 2015-12-16
description: KDE Ships Applications 15.12.
layout: application
release: applications-15.12.0
title: KDE toob välja KDE rakendused 15.12.0
version: 15.12.0
---
16. detsember 2015. KDE laskis täna välja KDE rakendused 15.12.

KDE annab vaimustusega teada KDE rakenduste 15.12, KDE rakenduste 2015. aasta detsembri uuenduste väljalaskest. See väljalase sisaldab üht uut rakendust ning veaparandusi ja lisandunud omadusi mitmetes senistes rakendustes. Meeskond püüdleb jätkuvalt töölaua ja rakenduste parima kvaliteedi poole, mistõttu ootame ärevalt kõigi kasutajate tagasisidet.

In this release, we've updated a whole host of applications to use the new <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>, including <a href='https://edu.kde.org/applications/all/artikulate/'>Artikulate</a>, <a href='https://www.kde.org/applications/system/krfb/'>Krfb</a>, <a href='https://www.kde.org/applications/system/ksystemlog/'>KSystemLog</a>, <a href='https://games.kde.org/game.php?game=klickety'>Klickety</a> and more of the KDE Games, apart from the KDE Image Plugin Interface and its support libraries. This brings the total number of applications that use KDE Frameworks 5 to 126.

### Imetabane uus rakendus

Pärast 14 aastat KDE koosseisu kuulumist lasti ekraani pildistamise rakendus KSnapshot viimaks puhkusele ja selle asemel astus uhiuus Spectacle.

Uute omaduste ja täielikult uue kasutajaliidesega Spectacle lubab ekraanipilte teha hõlpsamalt ja vähema vaevaga kui kunagi varem. Lisaks juba KSnapshotis olnud võimalustele võimaldab Spectacle nüüd teha liitpilte, millel on näha nii rakendus ise kui ka selle hüpik- või lihtsalt avatud menüüd. Samuti saab tervest ekraanist (või parajasti aktiivsest aknast) pilti teha isegi Spectacle'it käivitamata, kasutades lihtsalt kiirklahve (vastavalt tõstuklahv+PrintScreen ja Meta+PrintScreen).

Rakenduse käivitusaega on jõuliselt optimeeritud, et ajaline viivitus rakenduse käivitamise ja pildi tegemise vahel oleks võimalikult tilluke.

### Viimistlustööd kõikjal

Paljusid rakendusi on lisaks stabiilsuse tagamisele ja vigade parandamisele oluliselt viimistletud.

<a href='https://userbase.kde.org/Kdenlive'>Kdenlive</a>, the non-linear video editor, has seen major fixes to its User Interface. You can now copy and paste items on your timeline, and easily toggle transparency in a given track. The icon colours automatically adjust to the main UI theme, making them easier to see.

{{<figure src="/announcements/applications/15.12.0/ark1512.png" class="text-center" width="600px" caption=`Ark suudab nüüd näidata ZIP-failide kommentaare`>}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, the archive manager, can now display comments embedded in ZIP and RAR archives. We've improved support for Unicode characters in file names in ZIP archives, and you can now detect corrupt archives and recover as much data as possible from them. You can also open archived files in their default application, and we've fixed drag-and-drop to the desktop as well previews for XML files.

### Mängulust ja ei mingit vaeva

{{<figure src="/announcements/applications/15.12.0/ksudoku1512.png" class="text-center" width="600px" caption=`KSudoku uus avaekraan (Ma OS X peal)`>}}

KDE mängude arendajad on viimastel kuudel näinud suurt vaeva mängude optimeerimisel, et tagada nende sujuvam ja rahuldavam kasutamine, samuti on selles vallas tublisti uusi ja kasutajatele küllap meeltmööda lisandusi.

In <a href='https://www.kde.org/applications/games/kgoldrunner/'>KGoldrunner</a>, we've added two new sets of levels, one allowing digging while falling and one not. We've added solutions for several existing sets of levels. For an added challenge, we now disallow digging while falling in some older sets of levels.

In <a href='https://www.kde.org/applications/games/ksudoku/'>KSudoku</a>, you can now print Mathdoku and Killer Sudoku puzzles. The new multi-column layout in KSudoku's Welcome Screen makes it easier to see more of the many puzzle types available, and the columns adjust themselves automatically as the window is resized. We've done the same to Palapeli, making it easier to see more of your jigsaw puzzle collection at once.

We've also included stability fixes for games like KNavalBattle, Klickety, <a href='https://www.kde.org/applications/games/kshisen/'>KShisen</a> and <a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a>, and the overall experience is now greatly improved. KTuberling, Klickety and KNavalBattle have also been updated to use the new KDE Frameworks 5.

### Tähtsad parandused

Don't you simply hate it when your favorite application crashes at the most inconvinient time? We do too, and to remedy that we've worked very hard at fixing lots of bugs for you, but probably we've left some sneak, so don't forget to <a href='https://bugs.kde.org'>report them</a>!

In our file manager <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, we've included various fixes for stability and some fixes to make scrolling smoother. In <a href='https://www.kde.org/applications/education/kanagram/'>Kanagram</a>, we've fixed a bothersome issue with white text on white backgrounds. In <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, we've attempted to fix a crash that was occurring on shutdown, in addition to cleaning up the UI and adding in a new cache cleaner.

The <a href='https://userbase.kde.org/Kontact'>Kontact Suite</a> has seen tons of added features, big fixes and performance optimisations. In fact, there's been so much development this cycle that we've bumped the version number to 5.1. The team is hard at work, and is looking forward to all your feedback.

### Edasi tulevikku

Meie pakkumiste kaasajastamise huvides oleme loobunud mõningatest KDE rakendustest, mida KDE rakenduste 15.12 koosseisust enam ei leia.

Me loobusime selles väljalaskes neljast rakendusest: Amor, Tux, KSnapshot ja SuperKaramba. Nagu eespool öeldud, astus KSnapshoti asemele Spectacle ning SuperKarambat kui vidinamootorit asendab tõhusalt Plasma ise. Loobusime ka autonoomsetest ekraanisäästjatest, sest nüüdisaja töölaudadel käib ekraani lukustamine sootuks teistmoodi kui varem.

Ühtlasi loobusime kolmest kunsti pakkuvast paketist (kde-base-artwork, kde-wallpapers ja kdeartwork), mille sisu ei olnud keegi juba pikka aega puudutanud.

### Täielik muudatuste logi
