---
aliases:
- ../announce-applications-17.12.1
changelog: true
date: 2018-01-11
description: KDE toob välja KDE rakendused 17.12.1
layout: application
title: KDE toob välja KDE rakendused 17.12.1
version: 17.12.1
---
January 11, 2018. Today KDE released the first stability update for <a href='../17.12.0'>KDE Applications 17.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Umbes 20 teadaoleva veaparanduse hulka kuuluvad Kontacti, Dolphini, Filelighti, Gwenview, KGeti, Okteta, Umbrello ja teiste rakenduste täiustused.

Täiustused sisaldavad muu hulgas:

- Kirjade saatmine Kontactis on mõne SMTP-serveri puhul parandatud
- Gwenview ajatelge ja sildiotsingut on täiustatud
- UML-skeemide rakenduses Umbrello tehti korda JAVA import
