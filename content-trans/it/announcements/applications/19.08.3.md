---
aliases:
- ../announce-applications-19.08.3
changelog: true
date: 2019-11-07
description: KDE rilascia Applications 19.08.
layout: application
major_version: '19.08'
release: applications-19.08.3
title: KDE rilascia Applications 19.08.3
version: 19.08.3
---
{{% i18n_date %}}

Oggi KDE ha rilasciato il terzo aggiornamento di stabilizzazione per <a href='../19.08.0'>KDE Applications 19.08</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Più di una dozzina di errori corretti includono, tra gli altri, miglioramenti a Kontact, Ark, Cantor, K3b, Kdenlive, Konsole, Okular, Spectacle e Umbrello.

I miglioramenti includono:

- Nell'editor video Kdenlive, le composizioni non scompaiono più quando si riapre un progetto con tracce bloccate
- La vista delle note di Okular ora mostra il tempo di creazione in ora locale anziché in UTC
- È stato migliorato il controllo della tastiera nell'accessorio per la cattura delle schermate Spectacle
