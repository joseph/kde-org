---
aliases:
- ../announce-applications-16.08.3
changelog: true
date: 2016-11-10
description: KDE veröffentlicht die KDE-Anwendungen 16.04.3
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 16.04.3
version: 16.08.3
---
10 November 2016. Heute veröffentlicht KDE die dritte Aktualisierung der <a href='../16.08.0'>KDE-Anwendungen 16.08</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 240 aufgezeichnete Fehlerkorrekturen enthalten Verbesserungen für KDEPim, Ark, Okteta, Umbrello und KMines.

Diese Veröffentlichung enthält die Version der KDE Development Platform %1 mit langfristige Unterstützung.
