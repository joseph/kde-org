---
aliases:
- ../announce-applications-18.08.2
changelog: true
date: 2018-10-11
description: KDE lanza las Aplicaciones de KDE 18.08.2
layout: application
title: KDE lanza las Aplicaciones de KDE 18.08.2
version: 18.08.2
---
Hoy, 11 de octubre de 2018, KDE ha lanzado la segunda actualización de estabilización para las <a href='../18.08.0'>Aplicaciones 18.08</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 12 correcciones de errores registradas, se incluyen mejoras en Kontact, Dolphin, Gwenview, KCalc y Umbrello, entre otras aplicaciones.

Las mejoras incluyen:

- Al arrastrar un archivo a Dolphin ya no se lanza por accidente un cambio de nombre en línea.
- KCalc vuelve a permitir el uso de las teclas del punto y de la coma para introducir decimales.
- Se ha corregido un defecto visual en la baraja de cartas «Paris» para los juegos de cartas de KDE.
