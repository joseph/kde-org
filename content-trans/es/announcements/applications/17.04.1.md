---
aliases:
- ../announce-applications-17.04.1
changelog: true
date: 2017-05-11
description: KDE lanza las Aplicaciones de KDE 17.04.1
layout: application
title: KDE lanza las Aplicaciones de KDE 17.04.1
version: 17.04.1
---
Hoy, 11 de mayo de 2017, KDE ha lanzado la primera actualización de estabilización para las <a href='../17.04.0'>Aplicaciones 17.04</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 20 correcciones de errores registradas, se incluyen mejoras en kdepim, dolphin, gwenview, kate y kdenlive, entre otras aplicaciones.

También se incluye la versión de la Plataforma de desarrollo de KDE 4.14.32 que contará con asistencia a largo plazo.
