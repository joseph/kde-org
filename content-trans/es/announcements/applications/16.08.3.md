---
aliases:
- ../announce-applications-16.08.3
changelog: true
date: 2016-11-10
description: KDE lanza las Aplicaciones de KDE 16.08.3
layout: application
title: KDE lanza las Aplicaciones de KDE 16.08.3
version: 16.08.3
---
Hoy, 10 de noviembre de 2016, KDE ha lanzado la tercera actualización de estabilización para las <a href='../16.08.0'>Aplicaciones 16.08</a>. Esta versión solo contiene correcciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 20 correcciones de errores registradas, se incluyen mejoras en kdepim, ark, okteta, umbrello y kmines, entre otras aplicaciones.

También se incluye la versión de la Plataforma de desarrollo de KDE 4.14.26 que contará con asistencia a largo plazo.
