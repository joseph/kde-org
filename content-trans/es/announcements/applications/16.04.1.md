---
aliases:
- ../announce-applications-16.04.1
changelog: true
date: 2016-05-10
description: KDE lanza las Aplicaciones de KDE 16.04.1
layout: application
title: KDE lanza las Aplicaciones de KDE 16.04.1
version: 16.04.1
---
Hoy, 16 de mayo de 2016, KDE ha lanzado la primera actualización de estabilización para las <a href='../16.04.0'>Aplicaciones 16.04</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 25 correcciones de errores registradas, se incluyen mejoras en Kdepim, Ark, Kate, Dolphin, Kdenlive, Lokalize y Spectacle, entre otras aplicaciones.

También se incluye la versión de la Plataforma de desarrollo de KDE 4.14.20 que contará con asistencia a largo plazo.
