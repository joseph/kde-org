---
aliases:
- ../../kde-frameworks-5.35.0
date: 2017-06-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Mellorar a notificación de erro

### BluezQt

- Pasar unha lista explícita de argumentos. Isto impide que QProcess intente xestionar a nosa ruta que contén un espazo mediante un intérprete de ordes
- Corrixir que os cambios de propiedade se perdan inmediatamente tras engadir un obxecto (fallo 377405)

### Iconas de Breeze

- actualizar o tipo MIME de awk dado que é unha linguaxe de scripting (fallo 376211)

### Módulos adicionais de CMake

- restaurar a proba de visibilidade agochada con Xcode 6.2
- ecm_qt_declare_logging_category(): protección de inclusión máis única para a cabeceira
- Engadir ou mellorar as mensaxes «Xerado. Non editar» e facelas consistentes
- Engadir un novo módulo de FindGperf
- Cambiar a ruta de instalación predeterminada de pkgconfig para FreeBSD

### KActivitiesStats

- Corrixir kactivities-stats en tier3

### Ferramentas de Doxygen de KDE

- Non considerar a palabra clave Q_REQUIRED_RESULT

### KAuth

- Verificar que quen chama é quen di ser

### KCodecs

- Xerar saída de gperf en tempo de construción

### KCoreAddons

- Asegurarse de que KRandom usa unha semente axeitada por fío
- Non vixiar as rutas de QRC (fallo 374075)

### KDBusAddons

- Non incluír o identificador de proceso na ruta de D-Bus ao usar Flatpak

### KDeclarative

- Emitir o sinal MouseEventListener::pressed de maneira consistente
- Non manter un obxecto MimeData en memoria (fallo 380270)

### Compatibilidade coa versión 4 de KDELibs

- Xestionar os espazos na ruta de CMAKE_SOURCE_DIR

### KEmoticons

- Corrección: Qt5::DBus só se usa de maneira privada

### KFileMetaData

- usar /usr/bin/env para atopar python2

### KHTML

- Xerar saída de gperf de kentities en tempo de construción
- Xerar a saída de gperf de doctypes en tempo de construción

### KI18n

- Estender a guía do programados con notas sobre a influencia de setlocale()

### KIO

- Solucionouse un problema polo que certos elementos de aplicacións (p. ex. a vista de ficheiros de Dolphin) volveríanse inaccesíbeis en configuracións de varias pantallas con DPI alto (fallo 363548)
- [RenameDialog] Forzar o formato de texto simple
- Identificar os binarios PIE (application/x-sharedlib) como ficheiros executábeis (fallo 350018)
- core: expoñer GETMNTINFO_USES_STATVFS na cabeceira de configuración
- PreviewJob: saltar os directorios remotos. Xerar unha vista previa require moitos recursos (fallo 208625)
- PreviewJob: limpar o ficheiro temporal baleiro cando get() falle (fallo 208625)
- Acelerar a visualización da vista de árbore de detalles evitando demasiados cambios de tamaño de columnas

### KNewStuff

- Usar un único QNAM (e unha caché de disco) para traballos de HTTP
- Caché interna para os datos do fornecedor ao inicializar

### KNotification

- Corrixir que KSNIs non puidese rexistrar o servizo desde Flatpak
- Usar o nome da aplicación en vez de o identificador de proceso ao crear o servidor de D-Bus de SNI

### KPeople

- Non exportar símbolos de bibliotecas privadas
- Corrixir a exportación de símbolos de KF5PeopleWidgets e KF5PeopleBackend
- limitar #warning a GCC

### Infraestrutura de KWallet

- Substituír kwalletd4 unha vez rematada a migración
- Completado de sinal de axente de migración
- Só iniciar o temporizador do axente de migración se fai falla
- Comprobar se hai unha instancia única da aplicación o antes posíbel

### KWayland

- Engadir requestToggleKeepAbove/below
- Manter QIcon::fromTheme no fío principal
- Retirar changedSignal de pid en Client::PlasmaWindow
- Engadir pid ao protocolo de xestión de xanelas de Plasma

### KWidgetsAddons

- KViewStateSerializer: Corrixir a quebra cando a vista se destrúe antes do serializador do estado (fallo 353380)

### KWindowSystem

- Unha corrección mellor para NetRootInfoTestWM das rutas con espazos

### KXMLGUI

- Definir a xanela principal como pai dos menús emerxentes independentes
- Ao construír as xerarquías de menús, subordinar os menús aos seus contedores

### Infraestrutura de Plasma

- Engadir unha icona da área de notificacións para VLC
- Modelos de plasmoide: usar a imaxe que forma parte do paquete (de novo)
- Engadir un modelo para miniaplicativo de QML de Plasma con extensión de QML
- Recrear plasmashellsurf ao expoñerse, destruírse ou agocharse

### Realce da sintaxe

- Haskell: realzar o quasi-entrecomiñador «julius» coas regras Normal##Javascript
- Haskell: activar o realce de hamlet tamén para o quasi-entrecomiñador «shamlet»

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
