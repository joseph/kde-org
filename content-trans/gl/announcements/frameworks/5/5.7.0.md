---
aliases:
- ../../kde-frameworks-5.7.0
date: '2015-02-14'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Xeral

- Varias correccións para compilar coa vindeira Qt 5.5

### KActivities

- Corrixiuse o inicio e a parada de actividades
- Corrixiuse que a vista previa da actividade mostrase ás veces o fondo de pantalla incorrecto

### KArchive

- Crear os ficheiros temporais no directorio temporal en vez de no directorio actual

### KAuth

- Corrixiuse a xeración de ficheiros de servizo de asistencia de D-Bus de KAuth

### KCMUtils

- Corrixir unha aserción cando as rutas de D-Bus conteñen un «.»

### KCodecs

- Engadiuse compatibilidade con CP949 a KCharsets

### KConfig

- kconf_update xa non procesa o ficheiro *.upd da versión 4 da colección de software de KDE. Engadir «Version=5» ao principio do ficheiro upd para actualizacións que deberían aplicarse a aplicacións de Qt 5 e a versión 5 das infraestruturas de KDE.
- Corrixir KCoreConfigSkeleton ao conmutar un valor con gardas entremedias

### KConfigWidgets

- KRecentFilesAction: corrixiuse a orde das entrada de menú (para que coincida coa orde de kdelibs4)

### KCoreAddons

- KAboutData: Chamar a addHelpOption e addVersionOption automaticamente, por comodidade e consistencia
- KAboutData: Recuperar «Use http://bugs.kde.org para informar de fallos.» cando non se definise outro correo electrónico ou URL
- KAutoSaveFile: agora allStaleFiles() funciona como se espera con ficheiros locais, tamén se corrixiu staleFiles()
- Agora KRandomSequence usa enteiros internamente e expón unha API de enteiros para evitar calquera ambigüidade con 64 bits
- Definicións de tipos MIME: os ficheiros *.qmltypes e *.qmlproject tamén teñen o tipo MIME text/x-qml
- KShell: facer que quoteArgs cite os URL con QChar::isSpace(), os caracteres de espazo menos habituais non se estaban xestionando de maneira axeitada
- KSharedDataCache: corrixir a creación do directorio que contén a caché (fallo de migración)

### KDBusAddons

- Engadiuse o método de asistencia KDEDModule::moduleForMessage para escribir máis servizos como kded, como kiod

### KDeclarative

- Engadiuse un compoñente de gráfico
- Engadiuse un método de sobreposición a Formats::formatDuration que acepta int
- Engadíronse as propiedades paintedWidth e paintedHeight a QPixmapItem e QImageItem
- Corrixiuse o pintado de QImageItem e QPixmapItem

### Kded

- Engadir a posibilidade de cargar módulos de kded con metadatos de JSON

### KGlobalAccel

- Agora inclúe o compoñente de tempo de execución, converténdoa nunha infraestrutura de nivel 3
- Facer que a infraestrutura de Windows funcione de novo
- Activar de novo a infraestrutura de Mac
- Corrixir unha quebra no apagado en tempo de execución de X11 de KGlobalAccel

### KI18n

- Marcar os resultados como necesarios para avisar cando a API se usa mal
- Engadiuse a opción BUILD_WITH_QTSCRIPT do sistema de construción para permitir un conxunto de funcionalidades reducido nos sistemas incrustados

### KInit

- OSX: cargar as bibliotecas compartidas correctas en tempo de execución
- Correccións de compilación de Mingw

### KIO

- Corrixir unha quebra nos traballos ao ligar con KIOWidgets pero só se usa un QCoreApplication
- Corrixiuse a edición de atallos web
- Engadiuse a opción KIOCORE_ONLY para compilar só KIOCore e os seus programas de asistencia, pero KIOWidgets e KIOFileWidgets non, polo que se reducen en gran medida as dependencias necesarias
- Engadiuse a clase KFileCopyToMenu que engade os menús de contexto «Copiar a» e «Mover a»
- Protocolos compatíbeis con SSL: engadiuse compatibilidade cos protocolos TLSv1.1 e TLSv1.2, retirar SSLv3
- Corrixíronse negotiatedSslVersion e negotiatedSslVersionName para devolver o protocolo negociado real
- Aplicar o URL escrito á vista ao premer o botón que cambia o navegador de URL de volta ao modo de ronsel
- Corrixíronse dúas barras e diálogos de progreso que aparecían para traballos de copiar e mover
- Agora KIO usa un servizo de seu, kiod, para servizos fóra de proceso que antes se executaban en kded, para reducir as dependencias; de momento só substitúe a kssld
- Corrixiuse o erro «Non se puido escribir en &lt;ruta&gt;» ao causarse kioexec
- Corrixíronse os avisos de «QFileInfo::absolutePath: Construído con nome de ficheiro baleiro» ao usar KFilePlacesModel

### KItemModels

- Corrixiuse KRecursiveFilterProxyModel para Qt ≥ 5.5.0, dado que QSortFilterProxyModel pasou a usar o parámetro de roles para o sinal dataChanged

### KNewStuff

- Sempre cargar de novo os datos XML de URL remotas

### KNotifications

- Documentación: mencionáronse os requisitos de nome de ficheiro dos ficheiros .notifyrc
- Corrixiuse un punteiro solto a KNotification
- Corrixiuse unha fuga en knotifyconfig
- Instalar unha cabeceira de knotifyconfig que faltaba

### KPackage

- Cambiouse o nome da páxina de manual de kpackagetool a kpackagetool5
- Corrixiuse a instalación en sistemas de ficheiros que non distinguen maiúsculas

### Kross

- Corrixiuse Kross::MetaFunction para que funcione co sistema de metaobxecto de Qt 5

### KService

- Incluír propiedades descoñecidas ao converter KPluginInfo desde KService
- KPluginInfo: corrixiuse que as propiedades non se copiasen de KService::Ptr
- OS X: corrección de rendemento en kbuildsycoca4 (saltar os paquetes de aplicación)

### KTextEditor

- Corrixiuse o desprazamento de área táctil de alta precisión
- Non emitir documentUrlChanged durante recargas
- Non romper a posición do cursor ao cargar de novo o documento en liñas con separadores
- Non (des)pregar de novo a primeira liña se se (des)pregou manualmente
- vimode: historial de ordes mediante as teclas de frecha
- Non intentar crear un resumo ao recibir o sinal KDirWatch::deleted()
- Rendemento: retirar inicializacións globais

### KUnitConversion

- Corrixiuse unha recursión infinita en Unit::setUnitMultiplier

### KWallet

- Detectar e converter automaticamente carteiras vellas de ECB a CBC
- Corrixiuse o algoritmo de cifrado CBC
- Asegurouse de que a lista de carteiras se actualiza ao retirar un ficheiro de carteira do disco
- Retirar un &lt;/p&gt; innecesario en texto visíbel por usuarios

### KWidgetsAddons

- Usar kstyleextensions para indicar un elemento de control personalizado para renderizar a barra de kcapacity cando se permita, isto permite aplicar o estilo axeitado ao trebello
- Fornecer un nome accesíbel para KLed

### KWindowSystem

- Corrixiuse que NETRootInfo::setShowingDesktop(bool) non funcionase en Openbox
- Engadiuse o método de comodidade KWindowSystem::setShowingDesktop(bool)
- Correccións na xestión do formato das iconas
- O método NETWinInfo::icccmIconPixmap engadido fornece mapa de píxeles de icona a partir da propiedade WM_HINTS
- Engadir unha sobreposición a KWindowSystem::icon que reduza as voltas ao servidor de X
- Engadiuse compatibilidade con _NET_WM_OPAQUE_REGION

### NetworkManagerQt

- Non imprimir unha mensaxe sobre a propiedade sen xestionar «AccessPoints»
- Engadiuse compatibilidade con NetworkManager 1.0.0 (non requirido)
- Corrixiuse a xestión de segredos de VpnSetting
- Engadiuse a clase GenericSetting para conexións que non están xestionadas por NetworkManager
- Engadiuse a propiedade AutoconnectPriority a ConnectionSettings

#### Infraestrutura de Plasma

- Corrixiuse que se abrise accidentalmente un menú contextual roto ao usar o clic centrar na xanela emerxente de Plasma
- Facer que a roda do rato cause un cambio de botón
- Nunca cambiar de tamaño un diálogo que sexa máis grande que a pantalla
- Recuperar os paneis cando se recuperen os miniaplicativos
- Corrixíronse os atallos de teclado
- Restaurar a compatibilidade con hint-apply-color-scheme
- Cargar de novo a configuración cando plasmarc cambie
- …

### Solid

- Engadir energyFull e energyFullDesign a Battery

### Cambios do sistema de construción (extra-cmake-modules)

- Novo módulo de ECMUninstallTarget para crear un obxectivo de desinstalación
- Facer que KDECMakeSettings importe ECMUninstallTarget de maneira predeterminada
- KDEInstallDirs: avisar sobre a mestura de rutas de instalación relativas e absolutas na liña de ordes
- Engadiuse o módulo ECMAddAppIcon para engadir iconas aos destinos de executábeis en Windows e Mac OS X
- Corrixiuse o aviso CMP0053 con CMake 3.1
- Non retirar a definición de variábeis da caché en KDEInstallDirs

### Frameworkintegration

- Corrixiuse a actualización da opción de clic único en tempo de execución
- Varias correccións da integración coa área de notificacións
- Só instalar esquemas de cores nos trebellos raíz (para corrixir QQuickWidgets)
- Actualizar a configuración de XCursor na plataforma X11

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
