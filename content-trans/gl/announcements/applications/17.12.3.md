---
aliases:
- ../announce-applications-17.12.3
changelog: true
date: 2018-03-08
description: KDE publica a versión 17.12.3 das aplicacións de KDE
layout: application
title: KDE publica a versión 17.12.3 das aplicacións de KDE
version: 17.12.3
---
March 8, 2018. Today KDE released the third stability update for <a href='../17.12.0'>KDE Applications 17.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Arredor de 25 correccións de erros inclúen melloras en, entre outros, Kontact, Dolphin, Gwenview, JuK, KGet, Okular e Umbrello.

Entre as melloras están:

- Akregator xa non borra a lista de fontes feeds.opml tras un erro
- O modo a pantalla completa de Gwenview agora opera co nome de ficheiro correcto tras un cambio de nome
- Identificáronse e corrixíronse varios casos estraños de quebra en Okular
