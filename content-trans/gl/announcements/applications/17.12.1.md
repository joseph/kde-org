---
aliases:
- ../announce-applications-17.12.1
changelog: true
date: 2018-01-11
description: KDE publica a versión 17.12.1 das aplicacións de KDE
layout: application
title: KDE publica a versión 17.12.1 das aplicacións de KDE
version: 17.12.1
---
January 11, 2018. Today KDE released the first stability update for <a href='../17.12.0'>KDE Applications 17.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Arredor de 20 correccións de erros inclúen melloras en, entre outros, Kontact, Dolphin, Filelight, Gwenview, KGet, Okteta e Umbrello.

Entre as melloras están:

- Corrixiuse o envío de mensaxes en Kontact para certos servidores de SMTP
- Melloráronse a liña de tempo de Gwenview e as buscas de etiquetas
- Na ferramenta de diagrama de UML de Umbrello corrixiuse a importación de JAVA
