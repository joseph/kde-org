---
aliases:
- ../announce-applications-18.04.3
changelog: true
date: 2018-07-12
description: KDE publica a versión 18.04.3 das aplicacións de KDE
layout: application
title: KDE publica a versión 18.04.3 das aplicacións de KDE
version: 18.04.3
---
July 12, 2018. Today KDE released the third stability update for <a href='../18.04.0'>KDE Applications 18.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Arredor de 20 correccións de erros inclúen melloras en, entre outros, Kontact, Ark, Cantor, Dolphin, Gwenview e KMag.

Entre as melloras están:

- Restaurouse a compatibilidade con servidores de IMAP que non anuncian as súas funcionalidades
- Agora Ark pode extraer arquivos ZIP que non teñen as entradas axeitadas para cartafoles
- As notas en pantalla de KNotes volven seguir o punteiro do rato mentres se moven
