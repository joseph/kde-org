---
aliases:
- ../announce-applications-19.08.1
changelog: true
date: 2019-09-05
description: KDE Uygulamalar 19.08.1'i Gönderdi.
layout: application
major_version: '19.08'
release: applications-19.08.1
title: KDE Uygulamalar 19.08.1'i Gönderdi.
version: 19.08.1
---
{{% i18n_date %}}

Bugün KDE <a href='../19.08.0'>KDE Uygulamaları 19.08</a> için ilk kararlılık güncellemesini yayınladı. Bu sürüm yalnızca hata düzeltmeleri ve çeviri güncellemelerini içerir, herkes için güvenli ve hoş bir güncelleme sağlar.

More than twenty recorded bugfixes include improvements to Kontact, Dolphin, Kdenlive, Konsole, Step, among others.

İyileştirmeler şunları içerir:

- Several regressions in Konsole's tab handling have been fixed
- Dolphin again starts correctly when in split-view mode
- Deleting a soft body in the Step physics simulator no longer causes a crash
