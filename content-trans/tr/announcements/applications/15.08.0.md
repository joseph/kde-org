---
aliases:
- ../announce-applications-15.08.0
changelog: true
date: 2015-08-19
description: KDE Uygulamalar 15.08.0.'ı Gönderdi
layout: application
release: applications-15.08.0
title: KDE, KDE Uygulamalar 15.08.0'ı Gönderdi
version: 15.08.0
---
{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/dolphin15.08_0.png" alt=`Dolphin in the new look - now KDE Frameworks 5 based` class="text-center" width="600px" caption=`Dolphin in the new look - now KDE Frameworks 5 based`>}}

19 Ağustos 2015. Bugün KDE, KDE Uygulamaları 15.08'i yayınladı.

With this release a total of 107 applications have been ported to <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>. The team is striving to bring the best quality to your desktop and these applications. So we're counting on you to send your feedback.

With this release there are several new additions to the KDE Frameworks 5-based applications list, including <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, <a href='https://www.kde.org/applications/office/kontact/'>the Kontact Suite</a>, <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> <a href='https://games.kde.org/game.php?game=picmi'>Picmi</a>, etc.

### Kontak Takımı teknik önizlemesi

Geçtiğimiz birkaç ay içinde KDE PIM ekibi, Kontak'ı Qt 5 ve KDE Framework 5'e taşımak için çok çaba sarf etti. Ek olarak, veri erişim performansı optimize edilmiş bir iletişim katmanı ile önemli ölçüde iyileştirildi. KDE PIM ekibi Kontak Suite'i daha da geliştirmek için çok çalışıyor ve geri bildirimlerinizi bekliyor. KDE PIM'de nelerin değiştiği hakkında daha fazla ve ayrıntılı bilgi için <a href='http://www.aegiap.eu/kdeblog/'>Laurent Montels bloğuna</a> bakın.

### Kdenlive ve Okular

This release of Kdenlive includes lots of fixes in the DVD wizard, along with a large number of bug-fixes and other features which includes the integration of some bigger refactorings. More information about Kdenlive's changes can be seen in its <a href='https://kdenlive.org/discover/15.08.0'>extensive changelog</a>. And Okular now supports Fade transition in the presentation mode.

{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/ksudoku_small_0.png" alt=`KSudoku with a character puzzle` class="text-center" width="600px" caption=`KSudoku with a character puzzle`>}}

### Dolphin, Edu and Games

Dolphin was as well ported to KDE Frameworks 5. Marble got improved <a href='https://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system'>UTM</a> support as well as better support for annotations, editing and KML overlays.

Ark has had an astonishing number of commits including many small fixes. Kstars received a large number of commits, including improving the flat ADU algorithm and checking for out of bound values, saving Meridian Flip, Guide Deviation, and Autofocus HFR limit in the sequence file, and adding telescope rate and unpark support. KSudoku just got better. Commits include: add GUI and engine for entering in Mathdoku and Killer Sudoku puzzles, and add a new solver based on Donald Knuth's Dancing Links (DLX) algorithm.

### Diğer Sürümler

Bu sürümle birlikte Plasma 4, 4.11.22 sürümü olarak son kez LTS sürümünde yayınlanacaktır.
