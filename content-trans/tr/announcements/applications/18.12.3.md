---
aliases:
- ../announce-applications-18.12.3
changelog: true
date: 2019-03-07
description: KDE Uygulamalar 18.12.2.'yi Gönderdi
layout: application
major_version: '18.12'
release: applications-18.12.3
title: KDE, KDE Uygulamaları 18.12.3'ü Gönderdi
version: 18.12.3
---
{{% i18n_date %}}

Bugün KDE  <a href='../18.12.0'>KDE Uygulamaları 18.12</a>için üçüncü kararlılık güncellemesini yayınladı. Bu sürüm yalnızca hata düzeltmeleri ve çeviri güncellemelerini içerir, herkes için güvenli ve hoş bir güncelleme sağlar.

More than twenty recorded bugfixes include improvements to Kontact, Ark, Cantor, Dolphin, Filelight, JuK, Lokalize, Umbrello, among others.

İyileştirmeler şunları içerir:

- Ark'a .tar.zstd arşivlerinin yüklenmesi düzeltildi
- Dolphin artık bir Plasma aktivitesini durdururken çökmüyor
- Farklı bir bölüme geçmek artık Dosya Feneri'ı çökertemez
