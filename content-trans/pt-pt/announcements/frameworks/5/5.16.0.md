---
aliases:
- ../../kde-frameworks-5.16.0
date: 2015-11-13
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Biblioteca do Monitor: Uso do Kformat::spelloutDuration para formatar o texto dos tempos
- Uso do KDE_INSTALL_DBUSINTERFACEDIR para instalar as interfaces de D-Bus
- UnindexedFileIndexer: Tratamento dos ficheiros que foram movidos quando o baloo_file não estava em execução
- Remoção do Transaction::renameFilePath e adição de uma DocumentOperation para ele.
- Tornar os construtores com um único parâmetro explícitos
- UnindexedFileIndexer: só indexar as partes necessárias do ficheiro
- Transaction: adição de método para devolver uma estrutura timeInfo
- Adição da exclusão de tipos MIME à configuração do Balooctl
- Databases: Uso do QByteArray::fromRawData ao passar os dados para um codificador
- Balooctl: Passagem do comando 'status' para a sua própria classe
- Balooctl: Apresentação do menu de ajuda se o comando não for reconhecido
- Balooshow: Permitir a pesquisa dos ficheiros pelo seu inode + ID de dispositivo
- Monitor do Balooctl: parar se o Baloo morrer
- MonitorCommand: Uso dos sinais 'started' e 'finished'
- Monitor do Balooctl: Passagem para uma classe de comando adequada
- Adição de notificação do D-Bus quando se inicia/termina a indexação de um ficheiro
- FileIndexScheduler: Matar as tarefas à força na saída
- Gravação da WriteTransaction: Evitar a leitura da 'positionList' a menos que necessário
- WriteTransaction: Verificações extra no replaceDocument

### BluezQt

- O 'isBluetoothOperational' agora também depende do 'rfkill' desbloqueado
- Correcção da análise do estado global do botão 'rfkill'
- API QML: Marcação das propriedades sem sinal de notificação como constantes

### Módulos Extra do CMake

- Aviso em vez de erro se o 'ecm_install_icons' não encontrar ícones. (erro 354610)
- possibilidade de compilação das Plataformas do KDE 5 com um Qt 5.5.x simples e instalado a partir do instalador normal do qt.io no Mac OS
- Não limpar as variáveis de 'cache' no KDEInstallDirs (erro 342717)

### Integração da Plataforma

- Configuração do valor por omissão no WheelScrollLines
- Correcção da configuração do WheelScrollLines com Qt &gt;= 5.5 (erro 291144)
- Mudança para o tipo de letra Noto no Plasma 5.5

### KActivities

- Correcção da compilação no Qt 5.3
- Passagem da inclusão 'boost.optional' para o local que a usa
- Substituição da utilização do 'boost.optional' nas continuações com uma estrutura 'optional_view' mais reduzida
- Adição do suporte para uma ordenação personalizada dos resultados ligados
- Permissão ao QML para invocar o KCM das actividades
- Adição do suporte para a remoção de actividades no KCM de actividades
- Nova UI de configuração da actividade
- Nova UI de configuração que suporta a adição de uma descrição e um papel de parede
- A UI de configuração está agora devidamente modularizada

### KArchive

- Correcção do KArchive para a mudança de comportamento no Qt 5.6
- Correcção de fugas de memória - menos uso de memória

### KAuth

- Tratamento das mensagens 'qInfo' indirectas
- Espera pelo fim do utilitário da chamada assíncrona antes de verificar a resposta (erro 345234)
- Correcção do nome da variável, caso contrário não existe forma de funcionar a inclusão

### KConfig

- Correcção do uso do ecm_create_qm_loader.
- Correcção da variável de inclusão.
- Uso da variante KDE*INSTALL*FULL*, de forma a não existir ambiguidade
- Permissão do KConfig para usar alguns recursos, como os ficheiros de configuração de contingência

### KConfigWidgets

- Tornar o KConfigWidgets autónomo, fornecendo um ficheiro global num recurso
- Tornar o 'doctools' opcional

### KCoreAddons

- KAboutData: correcção do "is is" -&gt; "is" addCredit(): ocsUserName -&gt; ocsUsername
- O KJob::kill(Quiet) também deverá sair do ciclo de eventos
- Adição do suporte para o nome do ficheiro 'desktop' no KAboutData
- Uso dos caracteres de escape correctos
- Redução de algumas alocações
- Simplificação do KAboutData::translators/setTranslators
- Correcção do código de exemplo do 'setTranslator'
- desktopparser: correcção da chave Encoding=
- desktopfileparser: Comentários de revisão do endereço
- Permitir a definição de tipos de serviços no kcoreaddons_desktop_to_json()
- desktopparser: Correcção do processamento de valores de precisão dupla e booleanos
- Adição do KPluginMetaData::fromDesktopFile()
- desktopparser: Permissão da passagem de locais relativos para os ficheiros de tipo de serviço
- desktopparser: Uso de mais registo de eventos por categorias
- O QCommandLineParser usa o '-v' para '--version', pelo que só é permitido o '--verbose'
- Remoção de diverso código duplicado no desktop{tojson,fileparser}.cpp
- Processamento dos ficheiros ServiceType ao ler os ficheiros '.desktop'
- Passagem do SharedMimeInfo para um requisito opcional
- Remoção de chamada ao QString::squeeze()
- desktopparser: evitar a descodificação desnecessária de UTF-8
- desktopparser: Não adicionar outro elemento se o mesmo terminar com um separador
- KPluginMetaData: Aviso quando um elemento da lista não é uma lista em JSON
- Adição do mimeTypes() ao KPluginMetaData

### KCrash

- Melhoria na pesquisa do Drkonqui e mantê-lo silencioso por omissão, caso não seja encontrado

### KDeclarative

- O ConfigPropertyMap poderá agora ser pesquisado por opções de configuração imutáveis, usando o método isImmutable(chave)
- Desdobragem do QJSValue no mapa de propriedades de configuração
- EventGenerator: Adição do suporte para o envio de eventos da roda
- Correcção do 'initialSize' do QuickViewSharedEngine perdido na inicialização.
- Correcção de uma regressão crítica do QuickViewSharedEngine pela modificação 3792923639b1c480fd622f7d4d31f6f888c925b9
- Tornar o tamanho da janela pré-determinado precedente face ao tamanho inicial do objecto no QuickViewSharedEngine

### KDED

- Tornar o 'doctools' opcional

### Suporte para a KDELibs 4

- Não tentar guardar um QDateTime na memória mapeada com 'mmap'
- Sincronização e adopção do 'uriencode.cmake' a partir do kdoctools.

### KDesignerPlugin

- Adição do KCollapsibleGroupBox

### KDocTools

- Actualização das entidades pt_BR

### KGlobalAccel

- Não efectuar um XOR do Shift para o KP_Enter (erro 128982)
- Captura de todas as teclas de um símbolo (erro 351198)
- Não obter duas vezes os 'keysyms' por cada tecla pressionada

### KHTML

- Correcção da impressão no KHTMLPart, definindo o item-pai do 'printSetting' correctamente

### KIconThemes

- O kiconthemes agora suporta temas incorporados nos recursos do Qt dentro do prefixo ':/icons', tal como o Qt faz ele próprio para o QIcon::fromTheme
- Adição de dependências obrigatórias em falta

### KImageFormats

- Reconhecimento do tipo image/vnd.adobe.photoshop em vez do image/x-psd
- Reversão parcial do 'd7f457a' para evitar um estoiro ao sair da aplicação

### KInit

- Tornar o 'doctools' opcional

### KIO

- Gravação do URL do 'proxy' com o esquema correcto
- Emissão dos "modelos de novos ficheiros" na biblioteca 'kiofilewidgets', usando um .qrc (erro 353642)
- Tratamento adequado do botão do meio do rato no menu de navegação
- Tornar o kio_http_cache_cleaner instalável através dos instaladores/pacotes de aplicações
- KOpenWithDialog: Correcção da criação de ficheiros 'desktop' com um tipo MIME vazio
- Leitura da informação do protocolo a partir dos meta-dados do 'plugin'
- Permissão da instalação de um 'kioslave' local
- Adição de um '.protocol' convertido para JSON
- Correcção da dupla emissão do resultado e do aviso em falta quando a listagem aceder a uma pasta inacessível (erro 333436)
- Preservação dos destinos das ligações relativas quando se copiam ligações simbólicas. (erro 352927)
- Uso de ícones adequados para as pastas predefinidas na área do utilizador (erro 352498)
- Adição de uma interface que permite ao 'plugin' mostrar ícones sobrepostos personalizados
- Mudança da dependência do KIO no KNotifications (kpac) para opcional
- Mudança do doctools + carteira para opcional
- Protecção contra estoiros do KIO se o servidor de D-Bus não estiver em execução
- Adição do KUriFilterSearchProviderActions para mostrar uma lista de acções quando se procura por algum texto com os atalhos da Web
- Passagem dos elementos do menu "Criar um Novo" da pasta kde-baseapps/lib/konq paa a kio (erro 349654)
- Passagem do konqpopupmenuplugin.desktop do kde-baseapps para o kio (erro 350769)

### KJS

- Usar a variável global "_timezone" no MSVC em vez da "timezone". Corrige a compilação com o MSVC 2015.

### KNewStuff

- Correcção do ficheiro .desktop do 'Gestor de Partições do KDE' e o URL da página Web

### KNotification

- Agora que o KParts não precisa mais do KNotifications, só realmente as coisas que precisam mesmo de notificações estão realmente dependentes dessa plataforma
- Adição de descrição + objectivo para a fala + Phonon
- Mudança da dependência do Phonon para opcional - mudança puramente interna, como é feito na fala.

### KParts

- Usar o 'deleteLater' no Part::slotWidgetDestroyed().
- Remoção da dependência do KParts no KNotifications
- Usar uma função para consultar a localização do 'ui_standards.rc' em vez de a fixar no código, permitindo assim que resulte com uma contingência de recursos

### KRunner

- RunnerManager: Simplificação do código de carregamento dos 'plugins'

### KService

- KBuildSycoca: gravar sempre, mesmo que não tenham sido detectadas mudanças no ficheiro .desktop. (erro 353203)
- Tornar o 'doctools' opcional
- kbuildsycoca: processamento de todos os ficheiros 'mimeapps.list' mencionados na nova especificação.
- Usar a data mais recente na sub-pasta como data da pasta de recursos.
- Manutenção dos tipos MIME em separado ao converter o KPluginInfo no KPluginMetaData

### KTextEditor

- realce: gnuplot: adição da extensão .plt
- correcção da sugestão de validação, graças a "Thomas Jarosch" &lt;thomas.jarosch@intra2net.com&gt;, adição de sugestão também sobre a validação do tempo de compilação
- Não estoirar quando o comando não está disponível.
- Correcção do erro #307107
- Realce da variáveis de Haskell que começam por _
- Simplificação da inicialização do git2, dado que é necessária uma versão suficientemente recente (erro 353947)
- envio das configurações predefinidas no recurso
- realce de sintaxe (d-g): uso dos estilos predefinidos em vez de cores fixas em código
- melhor pesquisa dos programas, em primeiro nos locais do utilizador, depois nos nossos recursos e depois em todos os outros programas; desta forma, o utilizador poderá substituir os programas enviados por nós por versões locais
- colocação num pacote de todas as coisas em JS nos recursos também; assim só 3 ficheiros de configuração em falta e o ktexteditor também poderia passar a ser usado como uma biblioteca sem quaisquer ficheiros empacotados
- próxima tentativa: colocação de todos os ficheiros de sintaxe em XML num recurso
- adição de um atalho de mudança de modo de introdução de dados (erro 347769)
- colocação dos ficheiros XML num recurso
- realce de sintaxe (a-c): migração para novos estilos predefinidos, remoção de cores fixas em código
- realce de sintaxe: remoção de cores fixas no código e uso dos estilos predefinidos como alternativa
- realce de sintaxe: usar os novos estilos predefinidos (remoção de cores fixas)
- Melhor estilo predefinido de "Importação"
- Introdução da opção "Gravar com a Codificação" para gravar um dado ficheiro com uma codificação diferente, usando o menu de codificação devidamente agrupado que existe e substituindo todas as janelas de gravação com as correctas do sistema operativo, sem que se perca esta funcionalidade importante.
- colocação do ficheiro UI numa biblioteca, usando a extensão do 'xmlgui'
- A impressão volta a respeitar o esquema de tipos de letra &amp; cores (erro 344976)
- Uso das cores do Brisa para as linhas gravadas e modificadas
- Melhoria das cores predefinidas do contornos dos ícones do esquema "Normal"
- autobrace: só inserir parêntesis quando a letra seguinte for vazia ou não-alfanumérica
- autobrace: se remover o parêntesis inicial com o Backspace, remover o final também
- autobrace: Só estabelecer a ligação uma vez
- autobrace: remoção dos parêntesis de fecho em algumas circunstâncias
- Correcção da substituição de atalhos não ser propagada para a janela principal
- Erro 342659 - A cor de "realce de parêntesis" é imperceptível (correcção do esquema Normal) (erro 342659)
- Adição de cores predefinidas adequadas para a cor "Número de Linha Actual"
- correspondência de parêntesis &amp; parêntesis automáticos: partilha de código
- correspondência de parêntesis: protecção contra 'maxLines' negativos
- correspondência de parêntesis: só porque o novo intervalo corresponde ao antigo, não significa que não é necessária a actualização
- Adição da largura de meio-espaço para permitir a pintura do cursor no fim-da-linha
- Correcção de alguns problemas com o HiDPI no contorno dos ícones
- Correcção do erro #310712: remoção dos espaços finais também na linha com o cursor (erro 310712)
- Só mostrar a mensagem "marcação definida" quando o modo de entrada do VI estiver activo
- Remoção de &amp; do texto do botão (erro 345937)
- Correcção da actualização da cor do número da linha actual (erro 340363)
- Implementação da inserção de parêntesis ao escrever um parêntesis sobre uma selecção (erro 350317)
- Parêntesis automáticos (erro 350317)
- Correcção de alerta no realce de sintaxe (erro 344442)
- Não deslocar a coluna com a mudança de linha dinâmica ligada
- Recordação se o realce foi configurado pelo utilizador nas sessões para não o perder na gravação após a reposição (erro 332605)
- Correcção da dobragem no TeX (erro 328348)
- Correcção do erro #327842: A detecção do fim do comentário com o estilo do C não funcionava (erro 327842)
- Gravação/reposição da mudança de linha dinâmica na gravação/reposição da sessão (erro 284250)

### KTextWidgets

- Adição de um novo submenu ao KTextEdit para mudar de língua na verificação ortográfica
- Correcção do carregamento das predefinições do Sonnet

### Plataforma da KWallet

- Uso do KDE_INSTALL_DBUSINTERFACEDIR para instalar as interfaces de D-Bus
- Correcção dos avisos do ficheiro de configuração da KWallet no início da sessão  (erro 351805)
- Prefixo adequado do resultado do kwallet-pam

### KWidgetsAddons

- Adição de um contentor colapsável: KCollapsibleGroupBox
- KNewPasswordWidget: inicialização de cores em falta
- Introdução do KNewPasswordWidget

### KXMLGUI

- kmainwindow: Pré-preenchimento da informação do tradutor quando disponível. (erro 345320)
- Possibilidade de associar a tecla do menu de contexto (inferior direita) aos atalhos (erro 165542)
- Adição de função para consultar a localização do ficheiro XML 'standards'
- Possibilidade de a plataforma kxmlgui ser usada sem quaisquer ficheiros instalados
- Adição de dependências obrigatórias em falta

### Plataforma do Plasma

- Correcção do TabBar não ficar disposto adequadamente a título inicial, o que se verificava, por exemplo, no Kickoff após o arranque do Plasma
- Correcção da largada de ficheiros no ecrã/painel não oferecer uma selecção das acções a executar
- Consideração do QApplication::wheelScrollLines no ScrollView
- Uso do BypassWindowManagerHint apenas na plataforma X11
- Remoção do fundo antigo do painel
- Campo incremental mais legível com tamanhos pequenos
- Histórico de visualização colorido
- Calendário: Possibilidade de carregar com o cursor sobre toda a área do cabeçalho
- Calendário: Não usar o número do dia actual no 'goToMonth'
- Calendário: Correcção da visão de actualização de décadas
- Ícones do tema Brisa, quando carregados através do IconItem
- Correcção da propriedade 'minimumWidth' do objecto Button (erro 353584)
- Introdução do sinal 'appletCreated'
- Ícone Brisa do Plasma: Adição de elementos 'id' do SVG no Touchpad
- Ícone Brisa do Plasma: mudança do Touchpad para um tamanho 22x22px
- Ícone Brisa: adição do ícone do elemento às notas
- Um programa para substituir cores fixas por folhas de estilo
- Aplicação do SkipTaskbar no ExposeEvent
- Não definir o SkipTaskbar em cada evento

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
