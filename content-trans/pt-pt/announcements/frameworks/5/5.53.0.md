---
aliases:
- ../../kde-frameworks-5.53.0
date: 2018-12-09
layout: framework
libCount: 70
---
### Baloo

- Correcção das pesquisas pela classificação (5 estrelas) (erro 357960)
- Evitar escreve dados inalterados nas BD's de termos
- Não adicionar o Type::Document/Presentation/Spreadsheet duas vezes para os documentos do MS Office
- Inicialização adequada do KCrash
- Validação de que só existe um MTime por documento na MTimeDB
- [Extracção] Uso da serialização do QDataStream em vez da versão incorporada
- [Extracção] Substituição do tratamento de E/S incorporado pelo QDataStream, tratamento do HUP

### Ícones do Brisa

- Adição de ícones para o tipo application-vnd.appimage/x-iso9660-appimage
- Adição do ícone 'dialog-warning' de 22px (erro 397983)
- Correcção do ângulo e margem da imagem 'dialog-ok-apply' a 32px (erro 393608)
- Mudança das cores principais dos ícones monocromáticos para corresponder às novas cores do HIG
- Mudança dos ícones de acção 'archive-*' para representar pacotes (erro 399253)
- Adição de ligação simbólica 'help-browser' às pastas de 16px e 22px (erro 400852)
- Adição de novos ícones genéricos de ordenação; mudança de nome dos ícones existentes
- Adição da versão da raiz para o 'drive-harddisk' (erro 399307)

### Módulos Extra do CMake

- Novo módulo: FindLibExiv2.cmake

### Integração da Plataforma

- Adição da opção BUILD_KPACKAGE_INSTALL_HANDLERS para ignorar a compilação das rotinas de tratamento da instalação

### Ferramentas de Doxygen do KDE

- Adição de indicador de ocupado durante a pesquisa e tornar a pesquisa assíncrona (erro 379281)
- Normalização de todos os locais de entrada com a função 'os.path.normpath' (erro 392428)

### KCMUtils

- Alinhamento perfeito entre os títulos do KCM com QWidget e o QML
- Adição de contexto para a ligação ao 'kcmodule' para os lambdas (erro 397894)

### KCoreAddons

- Possibilidade de uso do KAboutData/License/Person do QML
- Correcção de estoiro se a pasta XDG_CACHE_HOME é demasiado pequena ou ficar sem espaço (erro 339829)

### DNS-SD do KDE

- agora instala o kdnssd_version.h para verificar a versão da biblioteca
- não deixas fugas na resolução do 'remoteservice'
- evitar um erro de sincronização do sinal do Avahi
- correcção do macOS

### KFileMetaData

- Reactivação da propriedade 'Description' para os meta-dados do DublinCore
- adição de propriedade 'description' no KFileMetaData
- [KFileMetaData] Adição de módulo de extracção para PostScript (Encapsulado) em conformidade com o DSC
- [ExtractorCollection] Eliminação de dependência do 'kcoreaddons' para os testes de CI
- Adição do suporte para os ficheiros do 'speex' ao 'taglibextractor'
- adição de mais duas fontes da Internet respeitantes à informação de marcas
- simplificação do tratamento de marcas ID3
- [XmlExtractor] Uso do QXmlStreamReader para melhor performance

### KIO

- Correcção de validação ao limpar as ligações simbólicas no PreviewJob
- Adição da possibilidade de ter um atalho de teclado para criar um ficheiro
- [KUrlNavigator] Reactivação ao carregar com o botão do meio do rato (erro 386453)
- Remoção de motores de busca descontinuados
- Migração de mais motores de busca para HTTPS
- Exportação de novo do KFilePlaceEditDialog (erro 376619)
- Reposição do suporte do 'sendfile'
- [ioslaves/trash] Tratamento de ligações simbólicas quebradas nas sub-pastas apagadas (erro 400990)
- [RenameDialog] Correcção do formato ao usar a marca NoRename
- Adição de '@since' em falta para o KFilePlacesModel::TagsType
- [KDirOperator] Uso do novo ícone <code>view-sort</code> para a selecção do sentido da ordenação
- Uso de HTTPS para todos os motores de busca que o suportem
- Desactivação da opção de desmontagem para o '/' ou o '/home' (erro 399659)
- [KFilePlaceEditDialog] Correcção de guarda de inclusão
- [Painel de Locais] Uso do novo ícone <code>folder-root</code> do item de Raiz
- [KSambaShare] Tornar o processamento do "net usershare info" capaz de ser testado
- Atribuição às janelas de ficheiros de um botão de menu "Ordenar por" na barra de ferramentas

### Kirigami

- DelegateRecycler: Não criar um novo 'propertiesTracker' para todos os métodos delegados
- Passagem da página Acerca do Discover para o Kirigami
- Ocultação da área de contexto quando existe uma barra de ferramentas global
- garantia de que todos os itens estão dispostos (erro 400671)
- mudança do índice ao pressionar e não ao carregar (pressionar e ligar) (erro 400518)
- novos tamanhos de texto para os Cabeçalhos
- as áreas da barra lateral não movem os cabeçalhos/rodapés globais

### KNewStuff

- Adição de um tratamento de erros útil de forma programática

### KNotification

- Mudança de nome de NotifyByFlatpak para NotifyByPortal
- Portal de notificações: suporte de imagens nas notificações

### Plataforma KPackage

- Não gerar dados do AppStream para os ficheiros que não tenham uma descrição (erro 400431)
- Captura dos meta-dados do pacotes antes do início da instalação

### KRunner

- Quando reutilizar os módulos de execução ao recarregar, volta a carregar a configuração dos mesmos (erro 399621)

### KTextEditor

- Possibilidade de prioridade negativas na definição da sintaxe
- Exposição da funcionalidade "Comutar o Comentário" através do menu de ferramentas e do atalho predefinido (erro 387654)
- Correcção das linguagens escondidas no menu de modo
- SpellCheckBar: Uso do DictionaryComboBox em vez de uma QComboBox simples
- KTextEditor::ViewPrivate: Evitar o aviso "Text requested for invalid range" (Pedido de texto para um intervalo inválido)
- Android: Não é necessário definir mais o 'log2'
- desligar o menu de contexto de todos os receptores do aboutToXXContextMenu (erro 401069)
- Introdução do AbstractAnnotationItemDelegate para mais controlo pelo consumidor

### KUnitConversion

- Actualização com as unidades industriais do petróleo (erro 388074)

### KWayland

- Auto-geração do ficheiro de registo + correcção do ficheiro de categorias
- Adição de VirtualDesktops ao PlasmaWindowModel
- Actualização do teste do PlasmaWindowModel para reflectir as mudanças do VirtualDesktop
- Limpeza do 'windowInterface' nos testes antes de destruir o 'windowManagement'
- Eliminação do item correcto no 'removeDesktop'
- Limpeza do elemento da lista do Gestor de Ecrãs Virtuais no destrutor do PlasmaVirtualDesktop
- Corrigir a versão da nova interface PlasmaVirtualDesktop
- [servidor] Sugestão de conteúdo da introdução de texto e objectivo por versão do protocolo
- [servidor] Colocação de rotinas '(de-)activate', 'en-/disable' nas classes-filhas de introdução de texto
- [servidor] Colocação da rotina de texto envolvente com um 'uint' na classe v0
- [servidor] Colocação de algumas rotinas exclusivas do v0 na classe v0

### KWidgetsAddons

- Adição da API do 'level' no Kirigami.Heading

### KXMLGUI

- Actualização do texto "Acerca do KDE"

### NetworkManagerQt

- Correcção de um erro (?) nas configurações de IPv4 &amp; IPv6
- Adição da configuração do 'ovs-bridge' e 'ovs-interface'
- Actualização das configurações do túnel IP
- Adição da configuração do 'proxy' e utilizador
- Adição da configuração IpTunnel
- É possível criar um teste de configuração do 'tun' a toda a hora
- Adição das opções do IPV6 em falta
- Atendimento das interfaces de DBus adicionadas em vez dos serviços registados (erro 400359)

### Plataforma do Plasma

- funcionalidade de paridade do Menu com o estilo do Ecrã
- O Qt 5.9 é agora a versão mínima obrigatório
- Adição de linha removida (por acidente?) no CMakeLists.txt
- 100% consistência com o tamanho dos cabeçalhos do Kirigami
- visual mais homogéneo com os cabeçalhos do Kirigami
- instalação da versão processada das importações privadas
- Controlos de selecção de texto em dispositivos móveis
- Actualização dos esquemas de cores 'breeze-light' e 'breeze-dark'
- Correcção de um conjunto de fugas de memória (graças ao ASAN)

### Purpose

- 'Plugin' do Phabricator: uso da ordem 'diff.rev' do Arcanist (erro 401565)
- Atribuição de um título à JobDialog
- Possibilidade de o JobDialog obter um tamanho inicial mais adequado (erro 400873)
- Possibilidade de o 'menudemo' partilhar URL's diferentes
- Uso do QQC2 para o JobDialog (erro 400997)

### QQC2StyleBridge

- dimensionamento consistente do item em comparação com os QWidgets
- correcção do dimensionamento do Menu
- validação de que os 'flickables' estão 'pixelAligned'
- Suporte para as aplicações baseadas no QGuiApplication (erro 396287)
- controlos de texto de ecrãs tácteis
- Tamanho de acordo com a largura e altura indicadas dos ícones
- Respeito da propriedade 'flat' dos botões
- Correcção de um problema onde só existe um elemento do menu (erro 400517)

### Solid

- Correcção da mudança do ícone do disco de raiz, de forma que não mude os outros ícones de forma errada

### Sonnet

- DictionaryComboBoxTest: Adição de esticamento para evitar expandir o botão 'Exportar'

### Realce de Sintaxe

- BrightScript: Permitir um 'sub' sem nome
- Adição do realce de sintaxe para os Registos de Depuração do Wayland
- Adição do realce de sintaxe para o TypeScript &amp; TypeScript React
- Rust &amp; Yacc/Bison: melhoria dos comentários
- Prolog &amp; Lua: correcção do 'shebang'
- Correcção do carregamento das linguagens após incluir palavras-chave desta linguagem noutro ficheiro
- Adição da sintaxe de BrightScript
- debchangelog: adição do Disco Dingo

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
