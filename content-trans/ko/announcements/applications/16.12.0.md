---
aliases:
- ../announce-applications-16.12.0
changelog: true
date: 2016-12-15
description: KDE에서 KDE 프로그램 16.12.0 출시
layout: application
title: KDE에서 KDE 프로그램 16.12.0 출시
version: 16.12.0
---
2016년 12월 15일. KDE에서는 KDE 프로그램 16.12을 출시했습니다. 이 릴리스에는 사용 편의성 개선, 새로운 기능 추가, 사소한 버그 수정 등이 있었으며 KDE 프로그램을 통해서 장치를 더 완벽하게 사용하는 것에 한 걸음 더 다가갔습니다.

<a href='https://okular.kde.org/'>Okular</a>, <a href='https://konqueror.org/'>Konqueror</a>, <a href='https://www.kde.org/applications/utilities/kgpg/'>KGpg</a>, <a href='https://www.kde.org/applications/education/ktouch/'>KTouch</a>, <a href='https://www.kde.org/applications/education/kalzium/'>Kalzium</a> and more (<a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>Release Notes</a>) have now been ported to KDE Frameworks 5. We look forward to your feedback and insight into the newest features introduced with this release.

In the continued effort to make applications easier to build standalone, we have split the kde-baseapps, kdepim and kdewebdev tarballs. You can find the newly created tarballs at <a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_we_have_split'>the Release Notes document</a>

다음 패키지의 지원을 중단했습니다: kdgantt2, gpgmepp 및 kuser. 나머지 코드에 더 집중할 예정입니다.

### Kwave 소리 편집기가 KDE 프로그램에 합류했습니다!

{{<figure src="https://www.kde.org/images/screenshots/kwave.png" width="600px" >}}

<a href='http://kwave.sourceforge.net/'>Kwave</a> is a sound editor, it can record, play back, import and edit many sorts of audio files including multi channel files. Kwave includes some plugins to transform audio files in several ways and presents a graphical view with a complete zoom and scroll capability.

### 세상을 배경 화면으로

{{<figure src="https://frinring.files.wordpress.com/2016/08/screenshot_20160804_171642.png" width="600px" >}}

Marble에 Plasma 배경 그림 및 위젯이 추가되었습니다. 현재 시간의 지구를 위성으로 볼 수 있으며 실시간으로 주/야간이 업데이트됩니다. Plasma 4에서 사용 가능했던 기능을 Plasma 5에서 작동하도록 갱신했습니다.

You can find more information on <a href='https://frinring.wordpress.com/2016/08/04/wip-plasma-world-map-wallpaper-world-clock-applet-powered-by-marble/'>Friedrich W. H. Kossebau's blog</a>.

### 화려한 이모티콘!

{{<figure src="/announcements/applications/16.12.0/kcharselect1612.png" width="600px" >}}

KCharSelect has gained the ability to show the Unicode Emoticons block (and other SMP symbol blocks).

책갈피 메뉴가 추가되어 자주 사용하는 글자를 관리할 수 있습니다.

### Julia를 통한 향상된 수학

{{<figure src="https://2.bp.blogspot.com/-BzJNpF5SXZQ/V7skrKcQttI/AAAAAAAAAA8/7KD8g356FfAd9-ipPcWYi6QX5_nCQJFKgCLcB/s640/promo.png" width="600px" >}}

Cantor에 새로운 Julia 백엔드가 추가되었습니다. 과학 컴퓨팅의 새로운 기능을 사용할 수 있습니다.

You can find more information on <a href='https://juliacantor.blogspot.com/2016/08/cantor-gets-support-of-julia-language.html'>Ivan Lakhtanov's blog</a>.

### 고급 압축 파일

{{<figure src="https://rthomsen6.files.wordpress.com/2016/11/blog-1612-comp-method.png" width="600px" >}}

Ark에 몇 가지 새로운 기능을 추가했습니다:

- 압축 파일 내에서 파일 및 폴더의 이름을 바꾸거나 복사하거나 이동할 수 있습니다.
- 압축 파일을 만들 때 압축 및 암호화 알고리즘을 선택할 수 있습니다.
- Ark can now open AR files (e.g. Linux \*.a static libraries)

You can find more information on <a href='https://rthomsen6.wordpress.com/2016/11/26/new-features-in-ark-16-12/'>Ragnar Thomsen's blog</a>.

### 그리고 더 있습니다!

Kopete의 Jabber 프로토콜에서 X-OAUTH2 SASL 인증을 지원하고 OTR 암호화 플러그인의 몇 가지 문제를 해결했습니다.

Kdenlive has a new Rotoscoping effect, support for downloadable content and an updated Motion Tracker. It also provides <a href='https://kdenlive.org/download/'>Snap and AppImage</a> files for easier installation.

KMail과 Akregator에서 Google 안전 브라우징을 사용하여 링크의 유해성 여부를 확인할 수 있습니다. 인쇄 지원이 다시 추가되었습니다(Qt 5.8 필요).

### 공격적인 버그 잡기

Dolphin, Akonadi, KAddressBook, KNotes, Akregator, Cantor, Ark, Kdenlive 등 프로그램에서 130개 이상의 버그가 수정되었습니다!

### 전체 변경 기록
