---
aliases:
- ../announce-applications-15.08.0
changelog: true
date: 2015-08-19
description: KDE випущено збірку програм 15.08.0.
layout: application
release: applications-15.08.0
title: KDE випущено збірку програм KDE 15.08.0
version: 15.08.0
---
{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/dolphin15.08_0.png" alt=`Новий вигляд Dolphin — тепер на основі KDE Frameworks 5` class="text-center" width="600px" caption=`Новий вигляд Dolphin — тепер на основі KDE Frameworks 5`>}}

19 серпня 2015 року. Сьогодні KDE випущено збірку програм KDE 15.08.

У цьому випуску портовано на <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a> 107 програм. Метою команди розробників цього випуску було забезпечення найкращої якості середовища і цих програм. Отже, ми розраховуємо на ваші відгуки.

До цього випуску включено перші засновані на KDE Frameworks 5 версії, зокрема <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, <a href='https://www.kde.org/applications/office/kontact/'>комплекс програм Kontact</a>, <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, <a href='https://games.kde.org/game.php?game=picmi'>Picmi</a>.

### Технологічна демонстрація комплексу Kontact

Протягом останніх декількох місяців командою розробників KDE PIM виконано значний обсяг роботи щодо портування Kontact на Qt 5 та KDE Frameworks 5. Крім того, значно поліпшено швидкодію засобів доступу до даних шляхом оптимізації шару обміну даними. Команда розробників KDE PIM інтенсивно працює над подальшим удосконаленням комплексу програм Kontact та очікує на ваші відгуки. Докладніший опис змін у KDE PIM наведено у <a href='http://www.aegiap.eu/kdeblog/'>блозі Лорана Монтеля</a>.

### Kdenlive і Okular

До нового випуску Kdenlive включено багато виправлень у майстрі створення DVD та інших інструментах, зокрема інтеграцію декількох значних латок з переформатування програми. Докладніший опис змін у Kdenlive наведено у <a href='https://kdenlive.org/discover/15.08.0'>розширеному списку змін</a>. У новій версії Okular передбачено перехід між слайдами зі згасанням у режимі презентації.

{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/ksudoku_small_0.png" alt=`KSudoku із символьною головоломкою` class="text-center" width="600px" caption=`KSudoku із символьною головоломкою`>}}

### Dolphin, освітні програми та ігри

Dolphin було остаточно портовано на KDE Frameworks 5. У Marble поліпшено підтримку <a href='https://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system'>UTM</a>, а також анотацій, редагування та накладок KML.

До Ark внесено багато дрібних виправлень. Значну роботу виконано над Kstars, зокрема поліпшено алгоритм спрощення ADU та перевірки граничних значень, збереження віддзеркалення меридіанів, відхилення спрямування телескопа та обмеження автофокусування у файлі послідовності знімків, додано швидкість обертання телескопа та підтримку зняття з паркування. Гра KSudoku стала просто кращою. Серед змін: додано графічний інтерфейс та рушій для введення матдоку та судоку-кілер, додано новий засіб розв'язування на основі алгоритму танцюючих зв'язків (DLX) Дональда Кнута.

### Інші випуски

Разом із набором програм востаннє випущено Плазму 4 у версії з подовженим періодом підтримки, версію 4.11.22.
