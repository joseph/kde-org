---
aliases:
- ../../kde-frameworks-5.68.0
date: 2020-03-07
layout: framework
libCount: 70
---
### Baloo

- [ModifiedFileIndexer] Виправлено перевірки часу для нових файлів
- [ModifiedFileIndexer] Усунено запуск BasicIndexingJob, якщо у цьому немає потреби
- Реалізовано синхронізацію IndexerConfig при виході (виправлено ваду 417127)
- [FileIndexScheduler] Реалізовано примусову оцінку indexerState після призупинення і поновлення роботи системи

### BluezQt

- Виправлено помилки у внеску з портування на QRegularExpression

### Піктограми Breeze

- Додано піктограму network-wireless-hotspot
- Піктограми панелі telegram пересунуто до категорії станів
- [Піктограми breeze] Додано піктограми лотка telegram-desktop (виправлено ваду 417583)
- [Піктограми breeze] Нова піктограма telegram розміром 48 пікселів
- Додано піктограми rss до дій
- Вилучено піктограми telegram розміру 48 пікселів
- Виправлення, яке забезпечує те, що перевірка не виконуватиметься паралельно до створення
- Новий логотип і піктограма yakuake
- Виправлено неоднорідності та дублікати у піктограмах дротових і бездротових мереж
- Виправлено застарілі текстові значення кольорів для піктограм osd-*
- Реалізовано встановлення створених піктограм, лише якщо їх створено
- Реалізовано екранування усіх шляхів для забезпечення роботи системи CI
- Встановлено -e у скрипті створення, щоб він виводив помилки належним чином
- Збирання: виправлено збирання, якщо префікс для встановлення не є придатним до запису для користувача
- Виправлено новий засіб створення піктограм розміром 24 пікселі — слід використовувати bash, а не sh
- Реалізовано автоматичне створення символічних посилань для сумісності із 24@2x
- Реалізовано автоматичне створення монохромних піктограм розміром 24 пікселі
- До actions/22 додано піктограми, які були лише у actions/24
- Реалізовано встановлення масштабу документа 1.0 для усіх піктограм у actions/22
- Додано нові піктограми <code>smiley-add</code>
- Піктограми shapes і shape-choose зроблено однорідними із іншими піктограмами -shape
- Піктограму smiley-shape зроблено однорідною із іншими піктограмами -shape
- Піктограми flower-shape та hexagon-shape зроблено однорідними із іншими піктограмами -shape
- &lt;use/&gt; у muondiscover.svg замінено на &lt;path/&gt;
- Додано піктограми станів: data-error, data-warning, data-information
- Додано піктограму org.kde.Ikona
- Додано піктограму vvave
- Додано піктограму puremaps
- Уніфіковано вигляд усіх піктограм, що містять 🚫 (символ заперечення)
- Нова піктограма KTimeTracker (виправлено ваду 410708)
- Оптимізовано піктограми KTrip і KDE Itinerary
- Оновлено піктограми сімейства travel

### Додаткові модулі CMake

- Реалізовано підтримку NDK r20 та Qt 5.14
- Реалізовано завантаження файлів QM із даними: адреси на Android
- Додано ecm_qt_install_logging_categories і ecm_qt_export_logging_category
- ECMGeneratePriFile: уможливлено використання із назвою призначення, відмінною від LIB_NAME
- ECMGeneratePriFile: виправлено статичні налаштування

### Інтеграція бібліотек

- [KStyle] Реалізовано встановлення належне значення кольору KMessageWidgets за поточною схемою кольорів

### KActivities

- Виправлено помилку із пошуком каталогів включення Boost
- Використано відкриті методи DBus для перемикання просторів дій із командного рядка

### KAuth

- [KAuth] Додано підтримку для подробиць дій у модулі Polkit1
- [policy-gen] Виправлено код для справжнього використання належної групи захоплення
- Відкинуто модуль Policykit
- [polkit-1] Спрощено пошук дій, які існують, у Polkit1Backend
- [polkit-1] Реалізовано повернення стану помилки у actionStatus, якщо станеться помилка
- Реалізовано обчислення KAuthAction::isValid на вимогу

### KBookmarks

- З метою забезпечення однорідності назв перейменовано дії

### KCalendarCore

- Реалізовано оновлення кешу видимості, якщо змінюється видимість нотатника

### KCMUtils

- Реалізовано перевірку існування activeModule, перш ніж його буде використано (виправлено ваду 417396)

### KConfig

- [KConfigGui] Реалізовано зняття властивості шрифту styleName для стилів звичайного шрифту (виправлено ваду 378523)
- Виправлено створення коду для записів із min/max (виправлено ваду 418146)
- KConfigSkeletonItem: уможливлено встановлення KconfigGroup для читання і запису об'єктів у вкладені групи
- Виправлено створені властивості для is&lt;НазваВластивості&gt;Immutable
- До KPropertySkeletonItem додано setNotifyFunction
- Додано is&lt;НазваВластивості&gt;Immutable для визначення того, чи є властивість незмінною

### KConfigWidgets

- «Redisplay» замінено на «Refresh»

### KCoreAddons

- Додано підказку щодо того, що QIcon можна використовувати як логотип програми

### KDBusAddons

- Визнано застарілим KDBusConnectionPool

### KDeclarative

- Відкрито сигнал захоплення у KeySequenceItem
- Виправлено розмір заголовка у GridViewKCM (виправлено ваду 417347)
- Похідному класу ManagedConfigModule дозволено реєструвати KCoreConfigSkeleton явним чином
- У ManagedConfigModule дозволено використання KPropertySkeletonItem

### KDED

- До kded5 додано параметр --replace

### Додатки графічного інтерфейсу KDE

- [UrlHandler] Реалізовано обробку відкриття інтернет-документації до модулів KCM
- [KColorUtils] Діапазон відтінків getHcy() змінено на [0.0, 1.0)

### KHolidays

- Оновлено японські свята
- holiday_jp_ja — виправлено написання національного для заснування (виправлено ваду 417498)

### KI18n

- Реалізовано підтримку Qt 5.14 на Android

### KInit

- Реалізовано запуск у kwrapper/kshell klauncher5, якщо потрібно

### KIO

- [KFileFilterCombo] Усунено можливість додавання некоректного QMimeType до фільтра MIME (виправлено ваду 417355)
- [src/kcms/*] Замінено foreach (застаріле) на for на основі діапазонів та індексів
- KIO::iconNameForUrl(): реалізовано випадок файла або теки у trash:/
- [krun] Використано спільну реалізацію runService і runApplication
- [krun] З KRun::runService викинуто підтримку KToolInvocation
- Удосконалено KDirModel, щоб на панелі не було показано «+», якщо немає підкаталогів
- Виправлено запуск konsole у Wayland (виправлено ваду 408497)
- KIO::iconNameForUrl: виправлено пошук піктограм протоколів KDE (виправлено ваду 417069)
- Реалізовано належне використання регістру літер для запису «базове посилання»
- «AutoSkip» замінено на «Skip All» (виправлено ваду 416964)
- Усунено витік пам'яті у KUrlNavigatorPlacesSelector::updateMenu
- Допоміжний засіб введення-виведення file: реалізовано зупинення копіювання, щойно буде перервано роботу допоміжного засобу введення-виведення
- [KOpenWithDialog] Реалізовано автоматичний вибір результату, якщо у фільтрі моделі виявлено лише один відповідник (виправлено ваду 400725)

### Kirigami

- Реалізовано показ панелі підказки із повною адресою для кнопки адреси із перевизначеним текстом
- Реалізовано висувні панелі інструментів для нижніх колонтитулах на сторінках із смужками гортання
- Виправлено поведінку PrivateActionToolButton із showText замість IconOnly
- Виправлено ActionToolBar/PrivateActionToolButton у поєднанні із дією QQC2
- Реалізовано безумовне пересування позначеного пункту меню завжди у діапазон
- Реалізовано спостереження за подіями зміни мови і передавання даних до рушія QML
- Реалізовано підтримку Qt 5.14 на Android
- Усунено накладання аркушів під заголовком сторінки
- Реалізовано використання резервного варіанта, якщо піктограму не вдалося завантажити
- Додано пропущені посилання на початкові файли pagepool
- Піктограма: виправлено обробку адрес image: при високій роздільній здатності (виправлено ваду 417647)
- Усунено причини аварійного завершення роботи для піктограм зі значеннями ширини або висоти 0 (виправлено ваду 417844)
- Виправлено поля у OverlaySheet
- [examples/simplechatapp] Реалізовано безумовне встановлення для isMenu значення true
- [RFC] Розмір заголовків першого рівня зменшено, збільшено розмір фаски на заголовках сторінок
- Реалізовано належну синхронізацію розмірів із скінченним автоматом (виправлено ваду 417351)
- Додано підтримку для статичних додатків platformtheme
- Реалізовано належне вирівнювання headerParent, якщо у вікні є смужка гортання
- Виправлено обчислення ширини панелі вкладок
- До файла QRC додано PagePoolAction
- Уможливлено стиль панелей інструментів у мобільному інтерфейсі
- У документації до програмного інтерфейсу описано те, що Kirigami призначено не лише для набору інструментів для мобільних інтерфейсах

### KItemModels

- KRearrangeColumnsProxyModel: тимчасово вимкнено перевірку умови через ваду у QTreeView
- KRearrangeColumnsProxyModel: реалізовано скидання у setSourceColumns()
- SortFilterProxyModel Плазми пересунуто до додатка QML KItemModel

### KJS

- Відкрито функції керування часом очікування на обчислення у загальнодоступному програмному інтерфейсі

### KNewStuff

- Виправлено клацання на делегаті (виправлено ваду 418368)
- Виправлено гортання на сторінці EntryDetails (виправлено ваду 418191)
- Усунено подвійне вилучення CommentsModel (виправлено ваду 417802)
- Реалізовано обробку додатка qtquick у встановленому файлі категорій
- Реалізовано використання належного каталогу перекладів для показу перекладів
- Виправлено заголовок закриття вікна KNSQuick і базове компонування (виправлено ваду 414682)

### KNotification

- Реалізовано доступ до kstatusnotifieritem без DBus
- Нумерацію дій в Android адаптовано до роботи у спосіб, подібний до KNotifications
- Кая-Уве вписано як супровідника knotifications
- Реалізовано безумовне спрощення HTML, якщо у сервері не передбачено його підтримки
- [android] Реалізовано видання сигналу defaultActivated при натисканні сповіщення

### KPeople

- Виправлено створення файлів pri

### KQuickCharts

- Усунено виведення повідомлень щодо некоректних ролей, якщо roleName не встановлено
- Реалізовано використання позаекранної платформи для тестів у Windows
- Зі скрипту перевірки вилучено отримання засобу перевірки glsl
- Виправлено помилку перевірки у шейдері лінійчастої діаграми
- Оновлено шейдер профілю ядра лінійчастої діаграми для сумісності
- Додано коментар щодо перевірки меж
- Лінійчаста діаграма: додано підтримку мінімального і максимального значення меж за y
- До бібліотеки sd додано функцію sdf_rectangle
- [Лінійчаста діаграма] Реалізовано захист від ділення на 0
- Лінійчасті діаграми: зменшено кількість точок на сегмент
- Усунено можливість втрати точок наприкінці лінійчастої діаграми

### Kross

- Враховано те, що Qt5::UiTools не є необов'язковим у цьому модулі

### KService

- Новий механізм опитування для програм: KApplicationTrader

### KTextEditor

- Додано параметр для керування динамічним перенесенням у словах
- KateModeMenuList: усунено можливість накладання на смужку гортання

### KWayland

- Додано шляхи DBus до інтерфейс org_kde_plasma_window у меню програми
- Регістр: усунено знищення зворотного виклику globalsync
- [surface] Виправлено відступ буфера при долученні буферів до поверхонь

### KWidgetsAddons

- [KMessageWidget] Уможливлено стиль для зміни нашої палітри
- [KMessageWidget] Реалізовано малювання за допомогою QPainter замість використання таблиці стилів
- Дещо зменшено розмір заголовка першого рівня

### ModemManagerQt

- Усунено створення і встановлення файла pri — у поточній версії не працює

### NetworkManagerQt

- Реалізовано підтримку SAE у securityTypeFromConnectionSetting
- Усунено створення і встановлення файла pri — у поточній версії не працює

### Піктограми Oxygen

- Реалізовано підтримку data-error/warning/information і у розмірах 32, 46, 64, 128
- Додано пункт дії «plugins» для відповідності піктограмам Breeze
- Додано піктограми станів: data-error, data-warning, data-information

### Бібліотеки Plasma

- Кнопки: уможливлено збільшення масштабу кнопок
- Реалізовано спробу застосувати схему кольорів поточної теми до QIcon (виправлено ваду 417780)
- Діалогове вікно: реалізовано від'єднання від сигналів QWindow у деструкторі
- Виправлено витік пам'яті у ConfigView і Dialog
- Виправлено підказку щодо розміру компонування для міток кнопок
- Реалізовано перевірку підказок щодо розмірів — мають бути цілими і парними
- Реалізовано підтримку icon.width/height (виправлено ваду 417514)
- Вилучено жорстке визначення кольорів (виправлено ваду 417511)
- Реалізовано побудову NullEngine за допомогою KPluginMetaData() (виправлено ваду 417548)
- Дещо зменшено розмір заголовка першого рівня
- Реалізовано вертикальне центрування піктограми або зображення підказки
- Реалізовано підтримку властивості display для кнопок
- Усунено попередження щодо некоректних метаданих додатка (виправлено ваду 412464)
- Реалізовано безумовне використання нормальної групи кольорів для панелей підказок
- [Tests] Реалізовано використання PC3 у radiobutton3.qml
- Оптимізовано код дій при скиданні файлів на стільницю (виправлено ваду 415917)

### Prison

- Виправлено файл pri так, щоб він працював із включеннями у форматі «верблюжого» регістру
- Виправлено файл pri так, щоб назва qmake QtGui належала до залежностей

### Purpose

- Переписано додаток nextcloud
- Усунено підтримку twitter

### QQC2StyleBridge

- ScrollView: реалізовано використання висоти смужки гортання як нижньої фаски замість ширини

### Solid

- Виправлено інверсію логіки у IOKitStorage::isRemovable

### Sonnet

- Усунено помилку сегментування при виході

### Підсвічування синтаксису

- Виправлено вихід за межі пам'яті через надто великі стоси контекстів
- Загальне оновлення підсвічування синтаксичних конструкцій CartoCSS
- Додано підсвічування синтаксичних конструкцій для властивостей Java
- TypeScript: додано закриті (private) поля та типізовані імпортування та експортування, деякі виправлення
- Додано розширення FCMacro FreeCAD до визначення підсвічування синтаксичних конструкцій Python
- Оновлення до CMake 3.17
- C++: ключове слово constinit і синтаксис std::format для рядків. Поліпшення у форматі printf.
- Специфікації RPM: різноманітні удосконалення
- Підсвічування Makefile: виправлення назв змінних в умовах «else» (виправлено ваду 417379)
- Додано підсвічування синтаксичних конструкцій для Solidity
- Незначні поліпшення у деяких файлах XML
- Підсвічування Makefile: додано замінники (виправлено ваду 416685)

### Відомості щодо безпеки

Випущений код підписано за допомогою GPG з використанням такого ключа: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Відбиток основного ключа: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
