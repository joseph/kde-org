---
aliases:
- ../announce-applications-18.04.2
changelog: true
date: 2018-06-07
description: O KDE disponibiliza o KDE Applications 18.04.2
layout: application
title: O KDE disponibiliza o KDE Applications 18.04.2
version: 18.04.2
---
7 de Junho de 2018. Hoje o KDE lançou a segunda actualização de estabilidade para as <a href='../18.04.0'>Aplicações do KDE 18.04</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 25 correcções de erros registadas incluem as melhorias no Kontact, no Cantor, no Dolphin, no Gwenview, no KGpg, no Kig, no Konsole, no Lokalize, no Okular, entre outros.

As melhorias incluem:

- As operações com imagens no Gwenview agora podem ser refeitas depois de as desfazer
- O KGpg já não falha a descodificar as mensagem sem cabeçalhos da versão
- A exportação das folhas de trabalho do Cantor para LaTeX foi corrigida para as matrizes do Maxima
