---
aliases:
- ../announce-applications-16.08.1
changelog: true
date: 2016-09-08
description: O KDE disponibiliza o KDE Applications 16.08.1
layout: application
title: O KDE disponibiliza o KDE Applications 16.08.1
version: 16.08.1
---
8 de Setembro de 2016. Hoje o KDE lançou a primeira actualização de estabilidade para as <a href='../16.08.0'>Aplicações do KDE 16.08</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Mais de 45 correções de erros registradas incluem melhorias no kdepim, kate, kdenlive, konsole, marble, kajongg, kopete, umbrello, dentre outras.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.24.
