---
aliases:
- ../announce-applications-18.04.3
changelog: true
date: 2018-07-12
description: KDE publie les applications de KDE en version 18.04.3
layout: application
title: KDE publie les applications de KDE en version 18.04.3
version: 18.04.3
---
12 Juillet 2018. Aujourd'hui, KDE a publié la troisième mise à jour de consolidation pour les <a href='../18.04.0'>applications 18.04 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 20 corrections de bogues apportent des améliorations à Kontact, Ark, Cantor, Dolphin, Gwenview, KMag et bien d'autres.

Les améliorations incluses sont :

- La compatibilité avec les serveurs « IMAP » n'annonçant pas leurs capacités a été restaurée.
- Ark peut maintenant extraire des archives « zip » avec des entrées appropriées manquantes pour les dossiers.
- Les notes sur l'écran de KNotes suivent de nouveau le pointeur de souris lors de leurs déplacements.
