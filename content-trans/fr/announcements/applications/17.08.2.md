---
aliases:
- ../announce-applications-17.08.2
changelog: true
date: 2017-10-12
description: KDE publie les applications de KDE en version 17.08.2
layout: application
title: KDE publie les applications de KDE en version 17.08.2
version: 17.08.2
---
12 Octobre 2017. Aujourd'hui, KDE a publié la seconde mise à jour de consolidation pour les <a href='../17.08.0'> applications 17.08 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 25 corrections de bogues apportent des améliorations à Kontact, Dolphin, Gwenview, Kdenlive, Marble, Okular et bien d'autres.

Cette mise à jour inclut aussi une version de prise en charge à long terme pour la plate-forme de développement 4.14.37 de KDE.

Les améliorations incluses sont :

- Une fuite de mémoire et un plantage dans la configuration du module externe d'évènements ont été corrigés.
- Les messages lus ne sont plus supprimés immédiatement à partir du filtre « Non lus » dans Akregator.
- L'outil d'importation de Gwenview utilise maintenant la date et l'heure « EXIF ».
