---
aliases:
- ../announce-applications-15.12.0
changelog: true
date: 2015-12-16
description: KDE publie la version 15.12 des applications de KDE.
layout: application
release: applications-15.12.0
title: KDE publie les applications de KDE en version 15.12.0
version: 15.12.0
---
16 Décembre 2015. Aujourd'hui, KDE publie les applications KDE 15.12.

KDE est heureux d'annoncer la publication de la version 15.12 des applications KDE,en Décembre 2015. Cette mise à jour apporte une nouvelle application, des ajouts de fonctionnalités et des correction de bogues dans l'ensemble des applications existantes. L'équipe s'efforce de toujours apporter la meilleures qualité à votre bureau et aux applications. Aussi, votre retour d'expérience est important pour l'équipe KDE.

Dans cette version, nous avons mis à jour toute une série d'applications pour utiliser le nouveau <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>, notamment <a href='https://edu.kde.org/applications/all/artikulate/'>Artikulate</a>, <a href='https://www.kde.org/applications/system/krfb/'>Krfb</a>, <a href='https://www.kde.org/applications/system/ksystemlog/'>KSystemLog</a>, <a href='https://games. kde.org/game.php?game=klickety'>Klickety</a> et d'autres jeux KDE, en dehors de l'interface plugin image KDE et de ses bibliothèques de support. Cela porte le nombre total d'applications qui utilisent KDE Frameworks 5 à 126.

### Une nouvelle addition, spectaculaire

Après 14 années comme partie de KDE, KSnapshot a été mis à la retraite et remplacé avec une nouvelle application de copie d'écran, Spectacle.

Grâce à de nouvelles fonctionnalités et à une interface utilisateur entièrement repensée, Spectacle rend les copies d'écran aussi faciles et discrètes que possible. En plus de ce que vous pouviez faire avec KSnapshot, avec Spectacle, vous pouvez maintenant prendre des copies d'écran composées de menus contextuels et de leurs fenêtres parentes, ou prendre des copies d'écran de l'écran entier (ou de la fenêtre actuellement active) sans même démarrer Spectacle, simplement en utilisant les raccourcis clavier « Maj » + « Impécr » et « Meta » + « Impécr » respectivement.

Les applications ont été optimisées agressivement concernant leurs temps de démarrage, pour minimiser l'écart de temps entre le lancement de l'application et la prise de la première image.

### Polir partout

Plusieurs applications ont entrepris des efforts significatifs de polissage dans ce cycle ainsi que des améliorations de stabilité et des corrections de bogues.

<a href='https://userbase.kde.org/Kdenlive'>Kdenlive</a>, l'éditeur vidéo non-linéaire, a bénéficié de corrections majeurs dans son interface utilisateur. Vous pouvez maintenant copier et coller les éléments dans votre ligne de temps et facilement jouer avec la transparence pour une piste donnée. L'icône « Couleurs » ajuste automatiquement le thème principal de l'interface utilisateur, les rendant plus facile à voir.

{{<figure src="/announcements/applications/15.12.0/ark1512.png" class="text-center" width="600px" caption=`Ark peut maintenant afficher les commentaires des fichiers « zip »`>}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, le gestionnaire d'archives, peut désormais afficher les commentaires intégrés dans les archives ZIP et RAR. Nous avons amélioré la prise en charge des caractères Unicode dans les noms de fichiers des archives ZIP, et vous pouvez désormais détecter les archives corrompues et récupérer autant de données que possible à partir de celles-ci. Vous pouvez également ouvrir les fichiers archivés dans leur application par défaut, et nous avons corrigé le glisser-déposer sur le bureau ainsi que les aperçus des fichiers XML.

### Tout lire et aucun travail

{{<figure src="/announcements/applications/15.12.0/ksudoku1512.png" class="text-center" width="600px" caption=`Nouvel écran de bienvenue de KSudoku (sur Mac OS X)`>}}

Les développeurs de jeu de KDE ont travaillé dur durant les derniers mois pour optimiser vos jeux pour une expérience plus facile et plus riche. Beaucoup de nouveaux sujets sont disponibles dans ce domaine pour réjouir nos utilisateurs.

<a href='https://www.kde.org/applications/games/kgoldrunner/'>KGoldrunner</a> reçoit deux nouveaux ensembles de niveaux, un permettant de creuser pendant la chute et un autre non. Des solutions ont été ajoutées pour les multiples niveaux existants. Pour un challenge supplémentaire, creuser est maintenant interdit durant une chute dans certains anciens ensembles de niveaux.

Dans <a href='https://www.kde.org/applications/games/ksudoku/'>KSudoku</a>, vous pouvez désormais imprimer les puzzles Mathdoku et Killer Sudoku. La nouvelle disposition en plusieurs colonnes dans l'écran d'accueil de KSudoku permet de voir plus facilement les nombreux types de puzzle disponibles, et les colonnes s'ajustent automatiquement lorsque la fenêtre est redimensionnée. Nous avons fait de même pour Palapeli, ce qui vous permet de voir plus facilement votre collection de puzzles en une seule fois.

Des corrections de stabilité ont été également apportées pour des jeux KNavalBattle, Klickety, <a href='https://www.kde.org/applications/games/kshisen/'>KShisen</a> et <a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a>, de façon à améliorer grandement le plaisir de jouer. KTuberling, Klickety et KNavalBattle ont également été mis à jour pour utiliser le nouvel environnement de développement de KDE 5.

### Corrections importantes

Ne détestez-vous pas lorsque votre application préférée plante au moment le plus inopportun ? Nous aussi, et pour y remédier, nous avons travaillé très dur pour corriger de nombreux bogues pour vous, mais nous en avons probablement laissé quelques-uns en douce, alors n'oubliez pas de <a href='https://bugs.kde.org'>les signaler</a> !

Dans notre gestionnaire de fichiers <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, nous avons inclus diverses corrections pour la stabilité et quelques corrections pour rendre le défilement plus fluide. Dans <a href='https://www.kde.org/applications/education/kanagram/'>Kanagram</a>, nous avons corrigé un problème gênant de texte blanc sur fond blanc. Dans <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, nous avons tenté de corriger un crash qui se produisait à l'arrêt, en plus de nettoyer l'interface utilisateur et d'ajouter un nouveau nettoyeur de cache.

La <a href='https://userbase.kde.org/Kontact'>Suite Kontact</a> a reçu des ajouts massifs de fonctionnalités, a reçu de grosses corrections et a été optimisée au niveau des performances. En fait, il y a eu tellement de développement durant ce cycle que nous avons fait passer le numéro de version à 5.1. L'équipe travaille intensément et attend vos commentaires avec impatience.

### Déplacement en avant

Comme partie de notre effort pour moderniser nos offres, certaines applications de KDE ont été abandonnées et ne sont plus mises à jours comme des applications de KDE 15.12.

Nous avons supprimé 4 applications de la version - Amor, KTux, KSnapshot et SuperKaramba. Comme indiqué ci-dessus, KSnapshot a été remplacé par Spectacle, et Plasma remplace essentiellement SuperKaramba comme moteur de composants graphiques. Les économiseurs d'écran autonomes ont été abandonnés car la gestion du verrouillage d'écran est trop variée dans les différents bureau actuels.

Les trois paquets « kde-base-artwork », « kde-wallpapers » et « kdeartwork » ont été abandonnés et leurs contenus n'ont pas évolué depuis longtemps.

### Journal complet des changements
