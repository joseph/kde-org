---
aliases:
- ../announce-applications-17.12.2
changelog: true
date: 2018-02-08
description: KDE publie les applications de KDE en version 17.12.2
layout: application
title: KDE publie les applications de KDE en version 17.12.2
version: 17.12.2
---
08 Septembre 2018. Aujourd'hui, KDE a publié la seconde mise à jour de stabilisation des <a href='../17.12.0'>applications 17.12 de KDE</a>. Cette mise à jour ne contient que des corrections de bogues et des mises à jour de traduction, elle sera sûre et appréciable pour tout le monde.

Plus de 20 corrections de bogues apportent des améliorations à Kontact, Dolphin, Gwenview, KGet, Okular et bien d'autres.
