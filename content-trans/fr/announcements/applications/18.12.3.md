---
aliases:
- ../announce-applications-18.12.3
changelog: true
date: 2019-03-07
description: KDE publie la version 18.12.2 des applications.
layout: application
major_version: '18.12'
release: applications-18.12.3
title: KDE publie la version 18.12.3 des applications.
version: 18.12.3
---
{{% i18n_date %}}

Aujourd'hui, KDE a publié la troisième mise à jour de consolidation pour les <a href='../18.12.0'>applications de KDE %[2 ]s</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traductions, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 20 corrections de bogues apportent des améliorations à Kontact, Ark, Cantor, Dolphin, Filelight, JuK, Lokalize, Umbrello et bien d'autres.

Les améliorations incluses sont :

- Le chargement des archives « .tar.zstd » dans Ark a été corrigé.
- Dolphin ne se plante plus lors de l'arrêt d'une activité Plasma.
- Le basculement vers une partition différente ne peut plus faire planter Filelight.
