---
aliases:
- ../../kde-frameworks-5.49.0
date: 2018-08-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Don't instantiate a QStringRef into a QString only to search in a QStringList
- Définition des éléments au moment de leurs déclarations

### Baloo

- [tags_kio] Correction pour les copies multiples de noms de fichiers
- Revert "[tags_kio] Use UDS_URL instead of UDS_TARGET_URL."
- [tags_kio] Use UDS_URL instead of UDS_TARGET_URL
- [tags_kio] Query target filepaths instead of appending paths to the file UDS entry
- Support special URLs for finding files of a certain type
- Avoid manipulation of lists with quadratic complexity
- Utilisation de la méthode conseillée « fastInsert » dans « baloo »

### Icônes « Breeze »

- Ajout d'une icône <code>drive-optical</code> (bogue 396432)

### Modules additionnels « CMake »

- Android: Don't hardcode a random version of the Android SDK
- ECMOptionalAddSubdirectory: Provide a bit more detail
- Correction de la vérification de définition des variables
- Modifier la version « Depuis »
- Amélioration de « ECMAddAppIconMacro »

### Intégration avec l'environnement de développement

- Privilégier BUILD_TESTING

### KActivities

- Privilégier BUILD_TESTING

### KArchive

- Privilégier BUILD_TESTING

### KAuth

- Suppression des alarmes pour les en-têtes de « PolkitQt5-1 »
- Privilégier BUILD_TESTING

### KBookmarks

- Privilégier BUILD_TESTING

### KCodecs

- Privilégier BUILD_TESTING

### KCompletion

- Privilégier BUILD_TESTING

### KConfig

- Privilégier BUILD_TESTING

### KConfigWidgets

- Privilégier BUILD_TESTING

### KCoreAddons

- Fix overflow in rounding code (bug 397008)
- API dox: remove not-to-be-there ":"s behind "@note"
- API dox : se référer à « nullptr » et non 0
- KFormat: Replace unicode literal with unicode codepoint to fix MSVC build
- KFormat: correct @since tag for new KFormat::formatValue
- KFormat: Allow usage of quantities beyond bytes and seconds
- Correction des exemples « KFormat::formatBytes »
- Privilégier BUILD_TESTING

### KCrash

- Privilégier BUILD_TESTING

### KDBusAddons

- Don't block forever in ensureKdeinitRunning
- Privilégier BUILD_TESTING

### KDeclarative

- ensure we are always writing in the engine's root context
- Meilleure lisibilité
- Petite amélioration des documentations des « API »
- Privilégier BUILD_TESTING

### Prise en charge de « KDELibs 4 »

- Correction de « qtplugins » dans « KStandardDirs »

### KDocTools

- Privilégier BUILD_TESTING

### KEmoticons

- Privilégier BUILD_TESTING

### KFileMetaData

- API dox: add @file to functions-only header to have doxygen cover those

### KGlobalAccel

- Privilégier BUILD_TESTING

### Modules externes pour l'interface graphique utilisateur de KDE

- Privilégier BUILD_TESTING

### KHolidays

- Install the sunrise/sunset computation header
- Added leap year day as (cultural) holiday for Norway
- Added ‘name’ entry for Norwegian holiday files
- Ajout des descriptions pour les fichiers de jours fériés de Norvège
- more Japanese holiday updates from phanect
- holiday_jp_ja, holiday_jp-en_us - updated (bug 365241)

### KI18n

- Ré-utiliser une fonction faisant déjà la même chose
- Fix the catalog handling and locale detection on Android
- Lisibilité. Ignorer les déclarations « no-op »
- Fix KCatalog::translate when translation is same as original text
- Un fichier a été renommé
- Let ki18n macro file name follow style of other find_package related files
- Fix the configure check for _nl_msg_cat_cntr
- Ne pas générer de fichiers dans le dossier des sources
- libintl: Determine if _nl_msg_cat_cntr exists before use (bug 365917)
- Correction des compilations binaires de développement

### KIconThemes

- Privilégier BUILD_TESTING

### KIO

- Install kio related kdebugsettings category file
- Renommer l'en-tête privé en « _p.h »
- Remove custom icon selection for trash (bug 391200)
- Aligner sur le haut les étiquettes dans la boîte de dialogue de propriétés
- Present error dialog when user tries to create directory named "." or ".." (bug 387449)
- API dox : se référer à « nullptr » et non 0
- Mesure de performances de « lstItems » pour « kcoredirlister »
- [KSambaShare] Check file that's changed before reloading
- [KDirOperator] Use alternating background colors for multi-column views
- avoid memory leak in slave jobs (bug 396651)
- SlaveInterface: deprecate setConnection/connection, nobody can use them anyway
- Accélérer légèrement le constructeur « UDS »
- [KFilePlacesModel] Support pretty baloosearch URLs
- Suppression du raccourci Internet vers « projects.kde.org »
- Switch KIO::convertSize() to KFormat::formatByteSize()
- Use non deprecated fastInsert in file.cpp (first of many to come)
- Remplacer un raccourci Internet « Gitorious » par « GitLab »
- Don't show confirmation dialog for Trash action by default (bug 385492)
- Ne pas demander de mots de passe dans « kfilewidgettest »

### Kirigami

- support dynamically adding and removing title (bug 396417)
- Introduction de « actionsVisible »(bogue 396413)
- adapt margins when scrollbar appears/disappear
- better management of the size (bug 396983)
- Optimisation du paramétrage de la palette
- AbstractApplciationItem shouldn't have its own size, only implicit
- Nouveaux signaux pour « pagePushed » / « pageRemoved »
- Correction de logique
- Ajout de l'élément « ScenePosition » (bogue 396877)
- No need to emit the intermediary palette for every state
- Cacher -&gt; afficher
- Mode de la barre de coté repliable
- kirigami_package_breeze_icons: don't treat lists as elements (bug 396626)
- Correction de l'expression rationnelle pour la recherche et le remplacement (bogue 396294)
- animating a color produces a rather unpleasant effect (bug 389534)
- Colorer l'élément avec le focus pour une navigation par clavier
- Supprimer le raccourci de fermeture
- Remove long-time deprecated Encoding=UTF-8 from desktop format file
- Correction de la taille de la barre d'outils (bogue 396521)
- Correction du dimensionnement des poignées
- Privilégier BUILD_TESTING
- Show icons for actions that have an icon source rather than an icon name

### KItemViews

- Privilégier BUILD_TESTING

### KJobWidgets

- Privilégier BUILD_TESTING

### KJS

- Privilégier BUILD_TESTING

### KMediaPlayer

- Privilégier BUILD_TESTING

### KNewStuff

- Remove long-time deprecated Encoding=UTF-8 from desktop format files
- Modification de l'ordre de tri par défaut dans la boîte de dialogue de téléchargement « Par note »
- Fix DownloadDialog window margins to meet general theme margins
- Restauration de la suppression accidentelle de « qCDebug »
- Utilisation d'un « API » correct pour « QSharedPointer »
- Gérer des listes vides d'aperçus

### KNotification

- Privilégier BUILD_TESTING

### Environnement de développement « KPackage »

- Privilégier BUILD_TESTING

### KParts

- API dox : se référer à « nullptr » et non 0

### KPeople

- Privilégier BUILD_TESTING

### KPlotting

- Privilégier BUILD_TESTING

### KPty

- Privilégier BUILD_TESTING

### KRunner

- Privilégier BUILD_TESTING

### KService

- API dox : se référer à « nullptr » et non 0
- Demander une compilation de sources externes
- Add subseq operator to match sub-sequences

### KTextEditor

- proper fix for the raw string indenting auto-quoting
- fix indenter to cope with new syntax file in syntaxhighlighting framework
- adjust test to new state in syntax-highlighting repository
- Show "Search wrapped" message in center of view for better visibility
- Correction de l'avertissement. Simple utilisation de « isNull() »
- Étendre l'« API » de langage de scripts
- fix segfault on rare cases where empty vector occurs for word count
- enforce clear of scrollbar preview on document clear (bug 374630)

### KTextWidgets

- API docs: partial revert of earlier commit, didn't really work
- KFindDialog: give the lineedit focus when showing a reused dialog
- KFind: reset count when changing the pattern (e.g. in the find dialog)
- Privilégier BUILD_TESTING

### KUnitConversion

- Privilégier BUILD_TESTING

### Environnement de développement « KWallet »

- Privilégier BUILD_TESTING

### KWayland

- Cleanup RemoteAccess buffers on aboutToBeUnbound instead of object destruction
- Prise en charge des indications de curseurs sur un pointeur verrouillé
- Reduce unnecessary long wait times on failing signal spies
- Correction de la sélection et installation des auto-tests
- Replace remaining V5 compat global includes
- Ajout de la prise en charge de « XDG WM » à notre API « XDGShell »
- Make XDGShellV5 co-compilable with XDGWMBase

### KWidgetsAddons

- Fix KTimeComboBox input mask for AM/PM times (bug 361764)
- Privilégier BUILD_TESTING

### KWindowSystem

- Privilégier BUILD_TESTING

### KXMLGUI

- Fix KMainWindow saving incorrect widget settings (bug 395988)
- Privilégier BUILD_TESTING

### KXmlRpcClient

- Privilégier BUILD_TESTING

### Environnement de développement de Plasma

- if an applet is invalid, it has immediately UiReadyConstraint
- [Plasma PluginLoader] Cache plugins during startup
- Fix fading node when one textured is atlassed
- [Containment] Don't load containment actions with plasma/containment_actions KIOSK restriction
- Privilégier BUILD_TESTING

### Prison

- Fix Mixed to Upper mode latching in Aztec code generation

### Motif

- Make sure debugging for kf5.kio.core.copyjob is disabled for the test
- Revert "test: A more "atomic" way of checking for the signal to happen"
- test: A more "atomic" way of checking for the signal to happen
- Ajout du module externe « bluetooth »
- [Telegram] Don't wait for Telegram to be closed
- Prepare to use Arc's status colours in the revision drop-down list
- Privilégier BUILD_TESTING

### QQC2StyleBridge

- Amélioration du dimensionnement des menus (bogue 396841)
- Supprimer la double comparaison
- Retirer le portage par les connexions reposant sur des chaînes
- Vérifier la présence d'une icône valable

### Opaque

- Privilégier BUILD_TESTING

### Sonnet

- Sonnet: setLanguage should schedule a rehighlight if highlight is enabled
- Utilisation de l'« API » courant « hunspell »

### Coloration syntaxique

- CoffeeScript: fix templates in embedded JavaScript code &amp; add escapes
- Exclude this in Definition::includedDefinitions()
- Use in-class member initialization where possible
- Ajout des fonctions pour accéder aux mots clé
- Ajoute de « Definition::::formats() »
- Ajout de « QVector&lt;Definition&gt; Definition::includedDefinitions() const »
- Ajout de « Theme::TextStyle Format::textStyle() const; »
- C++: fix standard floating-point literals (bug 389693)
- CSS : mise à jour de la syntaxe et correction de quelques erreurs
- C++: update for c++20 and fix some syntax errors
- CoffeeScript &amp; JavaScript: fix member objects. Add .ts extension in JS (bug 366797)
- Lua: fix multi-line string (bug 395515)
- Spécifications « RPM » : ajout d'un type « MIME »
- Python: fix escapes in quoted-comments (bug 386685)
- haskell.xml: don't highlight Prelude data constructors differently from others
- haskell.xml: remove types from "prelude function" section
- haskell.xml: highlight promoted data constructors
- haskell.xml: add keywords family, forall, pattern
- Privilégier BUILD_TESTING

### ThreadWeaver

- Privilégier BUILD_TESTING

### Informations sur la sécurité

Le code publié a été signé en « GPG » avec la clé suivante  pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empreinte de la clé primaire : 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Vous pouvez discuter et partager vos idées sur cette version dans la section des commentaires de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article</a>.
