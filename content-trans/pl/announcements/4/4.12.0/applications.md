---
date: '2013-12-18'
hidden: true
title: Aplikacje KDE 4.12 są olbrzymim krokiem naprzód w zarządzaniu informacją osobistą
  i dostarczają wiele ogólnych ulepszeń
---
Społeczność KDE jest dumna z tego, że może ogłosić znaczne uaktualnienia do Aplikacji KDE dostarczające nowych funkcji i poprawek. Wydanie to oznacza znaczne ulepszenia w stosie ZIO KDE, z dużo lepszą wydajnością i mnogością nowych funkcji. Kate gładko zintegrowało wtyczki Pythona i dodało wstępną obsługę makr Vim, a gry i aplikacje edukacyjne zyskały nowe funkcje.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/kate.png" width="600px">}}

Najbardziej zaawansowany graficzny edytor tekstowy na Linuksa znów został poddany pracom nad uzupełnianiem kodu, tym razem wprowadzono<a href='http://scummos.blogspot.com/2013/10/advanced-code-completion-filtering-in.html'>zaawansowane dopasowywanie kodu, obsługę skróceń i częściowe dopasowywanie w klasach</a>. Na przykład, wpisanie 'QualIdent' uzupełni się w nowym kodzie do 'QualifiedIdentifier'. Kate zyskało także<a href='http://dot.kde.org/2013/09/09/kde-commit-digest-18th-august-2013'>wstępną obsługę makr Vim</a>. Najlepszym z tego wszystkiego jest to, że te ulepszenia skapują do KDevelop i innych aplikacji używających technologii Kate.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/okular.png" width="600px">}}

Przeglądarka dokumentów Okular <a href='http://tsdgeos.blogspot.com/2013/10/changes-in-okular-printing-for-412.html'>od teraz bierze pod uwagę sprzętowe marginesy drukarki </a>, zyskał obsługę dźwięku i ruchomego obrazu dla epub, lepsze znajdywanie oraz obsługę transformacji uwzględniając te związane z metadanymi obrazów Exif. W narzędziu do diagramów UML Umbrello od teraz można rysować skojarzenia<a href='http://dot.kde.org/2013/09/20/kde-commit-digest-1st-september-2013'>przy użyciu różnych układów</a> a sam Umbrello <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-25th-august-2013'>wizualnie pokazuje czy element interfejsu jest  udokumentowany</a>.

Strażnik prywatności KGpg pokazuje użytkownikom i Zarządcy Portfeli KDE więcej informacji, a narzędzie do zapisywania haseł od teraz może <a href='http://www.rusu.info/wp/?p=248'>przechowywać je w postaci GPG</a>. W Konsoli wprowadzono nową funkcję: Ctrl-kliknięcie do bezpośredniego uruchamiania adresów URL z wyjścia konsoli. Teraz może ona także <a href='http://martinsandsmark.wordpress.com/2013/11/02/mangonel-1-1-and-more/'>wyświetlać procesy, które przestrzegają przed wyjściem</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/dolphin.png" width="600px">}}

KWebKit dodaje możliwość do <a href='http://dot.kde.org/2013/08/09/kde-commit-digest-7th-july-2013'>samoczynnego przeskalowania treści celem dopasowania do rozdzielczości pulpitu</a>. Dla zarządcy plików Dolphin wzrosła wydajność sortowania i pokazywania plików, czemu towarzyszyło zmniejszenie użycia pamięci i przyspieszenie działania rzeczy. W KRDC wprowadzono samoczynne ponowne połączenia w VNC, a KDialog od teraz zapewnia dostęp do pól wiadomości 'detailedsorry' i 'detailederror' celem bardziej informacyjnych skryptów konsoli. W Kopete uaktualniono wtyczkę OTR, a protokół  Jabber zyskał obsługę dla XEP-0264: Miniatury przesyłanych plików. Poza tymi funkcjami, skupiano się głównie na porządkowaniu kodu i pozbywaniu się ostrzeżeń kompilatora.

### Gry i oprogramowanie edukacyjne

Można zauważyć wiele pracy w różnych obszarach gier KDE. KReversi jest od teraz <a href='http://tsdgeos.blogspot.ch/2013/10/kreversi-master-is-now-qt-quick-based.html'>oparty na QML i Qt Quick</a>, czyniąc rozgrywkę milszą oku i bardziej płynną. KNetWalk został również <a href='http://tsdgeos.blogspot.ch/2013/08/knetwalk-portedx-to-qtquick.html'>przystosowany</a> z tymi samymi korzyściami , a także zyskał możliwość ustawienia siatki z własną szerokością i wysokością. . Konquest zyskał nowego wymagającego gracza o sztucznej inteligencji o nazwie 'Becai'.

W aplikacjach edukacyjnych nastąpiły pewne znaczące zmiany. KTouch <a href='http://blog.sebasgo.net/blog/2013/11/12/what-is-new-for-ktouch-in-kde-sc-4-dot-12/'>wprowadza obsługę własnych lekcji i kilka nowych kursów</a>; KStars posiada nowy dokładniejszy <a href='http://knro.blogspot.ch/2013/10/demo-of-ekos-alignment-module.html'>moduł wyrównywania dla teleskopów</a>, tu znajdziesz <a href='http://www.youtube.com/watch?v=7Dcn5aFI-vA'> film na youtube </a> na temat innych funkcji. Cantor, który oferuje prosty i zaawansowany interfejs użytkownika dla różnych silników matematycznych, od teraz dysponuje silnikami <a href='http://blog.filipesaraiva.info/?p=1171'>dla Python2 oraz Scilab</a>. Przeczytaj więcej o zaawansowanym silniku Scilab <a href='http://blog.filipesaraiva.info/?p=1159'>here</a>. W Marble dodano integrację z ownCloud (ustawienia są dostępne w Ustawieniach) i dodaje obsługę wyświetlania nakładki. W KAlgebra możliwym stało się eksportowanie wykresów 3D do PDF, będąc wspaniałym sposobem na udostępnianie twojej pracy. Ostatnie lecz nie mniej ważne, naprawiono wiele błędów w różnych programach edukacyjnych KDE.

### Poczta, kalendarz i informacje osobiste

ZIO KDE, zestaw aplikacji KDE do obsługi poczty, kalendarza i innych informacji osobistych, na nimi wszystkimi dużo pracowano.

Zaczynając od klienta pocztowego KMail, zyskał on <a href='http://dot.kde.org/2013/10/11/kde-commit-digest-29th-september-2013'>obsługę AdBlock</a> (gdy włączony jest HTML) i ulepszone wykrywanie oszustw poprzez rozwijanie skróconych adresów URL. Nowy agent Akonadi o nazwie  AgentKataloguArchiwum pozwala użytkownikom na zarchiwizowanie przeczytanych wiadomości pocztowych do określonych katalogów, a graficzny interfejs użytkownika funkcjonalności Wyślij Później został posprzątany. KMail korzysta także z ulepszonej obsługi filtrów Sieve. Sieve pozwala na filtrowanie wiadomości pocztowych po stronie serwera, a od teraz możesz <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-script-parsing-22/'>tworzyć i modyfikować filtry na serwerach</a> oraz <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-12/'>przekształcać istniejące filtry KMail na filtry serwera</a>. Ulepszona została także  <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-mboximporter/'>obsługaskrzynki pocztowej</a>.

W innych aplikacjach, kilka ze zmian czynią pracę łatwiejszą i przyjemniejszą. Wprowadzono nowe narzędzie, <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>EdytorMotywówKontaktów</a>, które pozwala na tworzenie motywów Grantlee dla Książki Adresowej KDE do wyświetlania kontaktów. Książka adresowa może od teraz pokazać podgląd przed wydrukowaniem danych. W KNotes <a href='http://www.aegiap.eu/kdeblog/2013/11/what-news-in-kdepim-4-12-knotes/'>poważnie popracowano nad naprawianiem błędów</a>. Narzędzie do blogowania Blogilo radzi sobie teraz z tłumaczeniami. Jak zawsze pojawiło się wiele poprawek i ulepszeń w przestrzeni wszystkich aplikacji ZIO KDE.

Z korzyścią dla wszystkich aplikacji <a href='http://ltinkl.blogspot.ch/2013/11/this-month-october-in-red-hat-kde.html'>dużo pracowano nad wydajnością, stabilnością i skalowalnością</a> wewnętrznego składu danych ZIO KDE, naprawiając<a href='http://www.progdan.cz/2013/10/akonadi-1-10-3-with-postgresql-fix/'>obsługę dla PostgreSQL w najświeższym Qt 4.8.5</a>. Pojawiło się nowe narzędzie wiersza poleceń, WoźnyKalendarza, który poszukuje błędnych wystąpień we wszystkich danych kalendarza wyświetlając przy tym okno diagnostyczne. Specjalnie podziękowania dla Laurent Montel za pracę jaką wkłada w funkcje ZIO KDE!

#### Instalowanie aplikacji KDE

Oprogramowanie KDE, włączając w to wszystkie jego biblioteki i aplikacje, jest dostępne na warunkach Wolnego Oprogramowania. Oprogramowanie KDE działa na rozmaitych konfiguracjach sprzętowych i architekturach procesora, takich jak ARM i x86, systemach operacyjnych oraz działa z każdym rodzajem menadżera okien czy otoczeniem pulpitu. Poza Linuksem i innymi, opartymi o UNIKSA, systemami operacyjnymi można też znaleźć wersję na Microsoft Windows na stronie <a href='http://windows.kde.org'>Oprogramowanie KDE na Windows</a> i wersję dla Apple Mac OS X na stronie <a href='http://mac.kde.org/'>Oprogramowanie KDE na Mac</a>. Eksperymentalne wydania aplikacji KDE, dla różnych platform przenośnych, takich jak MeeGo, MS Windows Mobile i Symbian można znaleźć w sieci, aczkolwiek są one teraz niewspierane. <a href='http://plasma-active.org'>Plasma Active</a> jest środowiskiem użytkownika dla szerokiego zakresu urządzeń, takich jak komputery typu tablet i inne przenośne.

Oprogramowanie KDE można pobrać w postaci źródła i różnych formatów binarnych z <a href='http://download.kde.org/stable/Szkieletów KDE %1'> download.kde.org</a>, ale można je także uzyskać na  <a href='/download'>CD-ROM</a>lub w każdym <a href='/distributions'>znaczącym systemie operacyjnym GNU/Linuks i UNIX</a> obecnie dostępnym.

##### Pakiety

Niektórzy wydawcy Linux/UNIX OS uprzejmie dostarczyli pakiety binarne Szkieletów KDE %[1] dla niektórych wersji ich dystrybucji, a w niektórych przypadkach dokonali tego wolontariusze społeczności.

##### Położenie pakietów

Po bieżącą listę dostępnych pakietów binarnych, o których został poinformowany Zespół wydawniczy KDE, proszę zajrzyj na stronę <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.12.0'> Wiki Społeczności</a>.

Pełny kod źródłowy dla Szkieletów KDE %[1] można <a href='/info/4/4.12.0'> pobrać za darmo</a>. Instrukcje dotyczące kompilowania i wgrywania oprogramowania KDE Szkieletów KDE %[1] są dostępne na <a href='/info/4/4.12.0#binary'>Stronie informacyjnej Szkieletów KDE %[1]</a>.

#### Wymagania systemowe

Aby uzyskać najwięcej z tych wydań, zalecamy użycie najnowszej wersji Qt, takiej jak 4.8.4. Jest to potrzebne, aby zapewnić odczucie stabilności i wydajności, jako iż pewne ulepszenia poczynione w oprogramowaniu KDE, w rzeczywistości zostały dokonane w strukturze programistycznej Qt.

Aby w pełni wykorzystać możliwości oprogramowania KDE, zalecamy  także użycie najnowszych sterowników graficznych dla twojego systemu,  jako iż może to znacznie polepszyć odczucia użytkownika, zarówno w  opcjonalnej funkcjonalności, jak i w ogólnej wydajności i stabilności.
