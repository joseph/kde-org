---
aliases:
- ../4.11
custom_about: true
custom_contact: true
date: 2013-08-14
description: KDE wydało Przestrzenie Pracy Plazmy, Aplikacje i Platformę 4.11.
title: Zbiór Oprogramowania KDE 4.11
---
{{<figure src="/announcements/4/4.11.0/screenshots/plasma-4.11.png" class="text-center" caption=`Przestrzenie Pracy Plazmy KDE 4.11` >}} <br />

Sierpień 14, 2013. Społeczność KDE z dumą ogłasza informacje o najnowszych znaczących uaktualnieniach do: Przestrzeni Roboczych Plazmy, Aplikacji oraz Platformy Programistycznej; dostarczając nowe funkcje i poprawki, przygotowują w tym czasie platformę do dalszej ewolucji. Przestrzenie Robocze Plazmy 4.11 otrzymają wsparcie w okresie długoterminowym, podczas gdy zespół skupi się na technicznym przejściu na Platformę 5. Wydanie to prezentuje ostatnie połączone wydanie Przestrzeni Roboczych, Aplikacji i Platformy pod tym samym numerem wersji.<br />

Wydanie to jest poświęcone pamięci <a href='http://en.wikipedia.org/wiki/Atul_Chitnis'>Atul 'toolz' Chitnis</a>, wielkiego mistrza Darmowego i Wolnego Oprogramowania z Indii. Atul prowadził konferencje Linux Bangalore oraz FOSS.IN od 2001 i oba były wydarzeniami znaczącymi na indyjskiej scenie FOSS. Indyjskie KDE zrodziło się na pierwszym FOSS.in w grudniu 2005. Wielu indyjskich współtwórców KDE rozpoczęło swoją przygodę z KDE od tych wydarzeń. Dzień projektu KDE na FOSS.IN był zawsze wielkim sukcesem tylko dzięki zaangażowaniu Atula.Atul opuścił nas 3. lipca po walce z rakiem. Niech jego dusza spoczywa w spokoju. Jesteśmy wdzięczni za jego wkład w lepszy świat.

Wszystkie te wydania są przetłumaczone na 54 języków; oczekujemy, że zostanie dodanych więcej języków w kolejnych comiesięcznych wydaniach KDE z poprawkami błędów. Zespół Dokumentacji uaktualnił 91 podręczników aplikacji dla tego wydania.

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="Przestrzenie Pracy Plazmy od KDE, w wersji 4.11" width="64" height="64" /> Przestrzenie Pracy Plazmy kontynuują udoskonalanie wrażeń użytkowników</a>

Przygotowując się do zapewnienia wsparcia w długim czasie, Przestrzenie Robocze Plazmy dostarczają dalszych ulepszeń do podstawowej funkcjonalności z płynniejszym paskiem zadań, inteligentniejszym elementem interfejsu baterii i ulepszonym mikserem dźwięku. Wprowadzenie KEkran zapewnia inteligentną obsługę wielu monitorów na Przestrzeniach Roboczych, a zakrojone na dużą skalę ulepszenia w wydajności w połączeniu z małymi dostosowaniami używalności dają przyjemniejsze odczucie.

## <a href="./applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="Aplikacje KDE w wersji 4.11"/> Aplikacje KDE 4.11 są olbrzymim krokiem naprzód w zarządzaniu informacją osobistą i dostarczają wiele ogólnych ulepszeń</a>

Wydanie to oznacza znaczne ulepszenia w stosie ZIO KDE, z dużo lepszą wydajnością i wiele nowych funkcji. Kate poprawia produktywność programistów Pythona oraz Javascript dzięki nowym wtyczkom, Dolphin stał się szybszy, a aplikacje edukacyjne niosą ze sobą wiele nowych funkcji.

## <a href="./platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="KDE Development Platform w wersji 4.11"/>  Platforma KDE 4.11 dostarcza wiele ogólnych ulepszeń </a>

Wydanie Platformy KDE 4.11 kontynuuje prace skupione na stabilności. Dla naszego przyszłego wydania Frameworks KDE 5.0, implementowane są nowe funkcje, lecz dla stabilnego wydania daliśmy radę upchnąć optymalizacje dla naszego modułu Nepomuka.

<br />
W czasie uaktualniania, proszę obejrzeć <a href='http://community.kde.org/KDE_SC/4.11_Release_Notes'>uwagi o wydaniu</a>.<br />

## Powiadom innych i obserwuj co się dzieje: oznacz jako &quot;KDE&quot;

KDE zachęca ludzi do rozpowszechniania informacji w sieciach społecznościowych. Wysyłaj historie na strony z nowinkami, używaj kanałów takich jak delicious, digg, reddit, twitter, identi.ca. Wysyłaj zrzuty ekranu na usługi takie jak Facebook, Flickr, ipernity oraz Picasa, i przesyłaj je do odpowiednich grup. Nagrywaj obraz na ekranie i wysyłaj go na YouTube, Blip.tv, oraz Vimeo. Proszę oznaczaj posty i wysyłaj materiały z &quot;KDE&quot;. To czyni je łatwiejszymi do znalezienia i daje Zespołowi ds. Promocji KDE możliwości do przeanalizowania rozgłosu wydań 4.11 oprogramowania KDE.

## Imprezy z okazji wydania

Jak zwykle, członkowie społeczności KDE organizują imprezy z okazji wydania na całym świecie. Kilka z nich już zostało zaplanowanych, a plany na inne nadejdą później. Znajdź <a href='http://community.kde.org/Promo/Events/Release_Parties/4.11'>listę imprez</a>. Każdy jest mile widziany! Na imprezach zawsze jest kombinacja interesującego towarzystwa i inspirujące rozmowy, ale także jedzenie i napoje. Jest to wspaniała szansa, aby dowiedzieć się więcej o tym co się dzieje w KDE, zaangażować się lub po prostu spotkać się z użytkownikami i współtwórcami.

Zachęcamy ludzi do organizowania ich własnych imprez. Fajnie się je organizuje i są otwarte dla każdego! Sprawdź <a href='http://community.kde.org/Promo/Events/Release_Parties'>rady przy organizowaniu imprezy</a>.

## O ogłoszeniach tych wydań

Te ogłoszenia o wydaniach zostały przygotowane przez zespół: Jos Poortvliet, Sebastian Kügler, Markus Slopianka, Burkhard Lück, Valorie Zimmerman, Maarten De Meyer, Frank Reininghaus, Michael Pyne, Martin Gräßlin i innych członków Zespołu ds. Promocji KDE oraz szerszą społeczność KDE. Ogłoszenia traktują o wielu zmianach poczynionych w oprogramowaniu KDE na przestrzeni minionych sześciu miesięcy.

#### Wesprzyj KDE

<a href="http://jointhegame.kde.org/"> <img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3" alt="Przyłącz się  do gry"/> </a>

Nowy <a href='http://jointhegame.kde.org/'>Wspierający Program Członkowski</a> na rzecz KDE e.V. uznaje się za otwarty.Za &euro;25  kwartalnie możesz zapewnić przetrwanie i wzrost międzynarodowej  społeczności, tworzącej Wolne Oprogramowanie światowej klasy.
