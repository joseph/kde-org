---
date: '2014-04-16'
description: KDE wydało Aplikacje i Platformę 4.13.
hidden: true
title: Aplikacje KDE 4.13 korzystają z nowego wyszukiwania semantycznego, wprowadzają
  nowe funkcje
---
Społeczność KDE z dumą ogłasza wydanie najświeższych głównych uaktualnień do Aplikacji KDE, które dostarczają nowych funkcji i poprawiają błędy. Kontact (program do zarządzania informacjami osobistymi) został poddany wzmożonym pracom skupionym na wykorzystaniu ulepszeń w technologi semantycznego wyszukiwania KDE oraz dodawaniu nowych funkcji. Przeglądarka dokumentów Okular i zaawansowany edytor tekstu Kate zyskały nowe ulepszenia do interfejsu i w obszarze funkcji. W obszarach edukacji i gier, wprowadziliśmy nowego trenera wymowy języków obcych Artikulate; Marble (biurkowy globus) zyskał obsługę Słońca, Księżyca, planet, wytyczanie trasy dla rowerów i morskie mile. Palapeli (aplikacja puzzli jigsaw) weszła w niespotykane wymiary i zyskała niespotykane możliwości.

{{<figure src="/announcements/4/4.13.0/screenshots/applications.png" class="text-center" width="600px">}}

## KDE Kontact przyspiesza, a także wprowadza nowe funkcje

Pakiet Kontact dla KDE wprowadza szereg nowych funkcji w jego różnych częściach składowych. KMail wprowadza przechowywane w chmurze i ulepszoną obsługę sieve dla filtrowania po stronie serwera, KNotes od teraz może generować alarmy i umożliwia wyszukiwanie, poza tym pojawiło się wiele ulepszeń w warstwie danych podręcznych w Kontact, co przyspiesza niemalże wszystkie czynności.

### Obsługa przechowywania w chmurze

KMail wprowadza obsługę usług przechowywania, tak więc duże załączniki można teraz przechowywać w chmurach i załączać je jako odsyłacze w wiadomościach pocztowych. Obsługiwane usługi przechowywania uwzględniają: Dropbox, Box, KolabServer, YouSendIt, UbuntuOne, Hubic, a na dodatek istnieje opcja zwykłego WebDav. Narzędzie <em>storageservicemanager</em> pomaga w zarządzaniu plikami tych usług.

{{<figure src="/announcements/4/4.13.0/screenshots/CloudStorageSupport.png" class="text-center" width="600px">}}

### Znacznie ulepszona obsługa Sieve

Filtry Sieve, technologia pozwalająca KMail na obsługę filtrów na serwerze, może teraz obsłużyć dni wolne na wielu serwerach. Narzędzie KSieveEditor pozwala użytkownikom na edytowanie filtrów sieve bez potrzeby dodawania serwera do Kontact.

### Inne zmiany

Pasek szybkiego filtrowania zyskał małe ulepszenia w interfejsie i korzysta znacznie z ulepszonych możliwości wyszukiwania wprowadzonych w wydaniu Platformy Programistycznej 4.13. Wyszukiwanie stało się znacznie szybsze i bardziej niezawodne. Program do komponowania wprowadza skracacz adresów URL, lepiej wykorzystując istniejące tłumaczenia i narzędzia wstawek tekstowych.

Znaczniki i przypisy do danych ZIO są teraz przechowywane w Akonadi. W przyszłych wersjach będą także przechowywane na serwerach (na IMAP lub Kolab), umożliwiając współdzielenie znaczników między wieloma komputerami. Dodana została obsługa API Dysku Google. Istnieje obsługa wyszukiwania przy użyciu wtyczek stron trzecich (co oznacza, że wyniki można otrzymać bardzo szybko) i obsługa wyszukiwania serwerowego (szukanie elementów niezaindeksowanych przez lokalną usługę indeksowania).

### KNotes, KAddressbook

Wiele pracowano nad KNotes, poprawiając szereg błędów i małych niedogodności. Możliwość ustawienia alarmów na notatkach i nowe wyszukiwanie wśród notatek. Przeczytaj więcej <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>tutaj</a>. Książka adresowa zyskała obsługę wydruku: więcej szczegółów <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>tutaj</a>.

### Poprawa wydajności

Wydajność Kontact została znacznie poprawiona w tej wersji. Niektóre ulepszenia są wynikiem integracji z nową wersją infrastruktury <a href='http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search'>Semantycznego Wyszukiwania</a> KDE i warstwą danych podręcznych oraz samą w sobie pracą nad wczytywaniem danych do KMail. Podjęto znaczące prace nad usprawnieniem obsługi bazy danych PostgreSQL. Więcej informacji i szczegółów dotyczących zmian poprawiających wydajność można znaleźć po naciśnięciu na te odnośniki:

- Udoskonalenia pamięci: <a href='http://www.progdan.cz/2013/11/kde-pim-sprint-report/'>raport z biegu</a>
- przyspieszenie i zmniejszenie rozmiaru bazy danych: <a href='http://lists.kde.org/?l=kde-pim&amp;m=138496023016075&amp;w=2'>lista dyskusyjna</a>
- usprawnienie w dostępie do katalogów: <a href='https://git.reviewboard.kde.org/r/113918/'>rada opiniująca</a>

### KNotes, KAddressbook

Wiele pracowano nad KNotes, poprawiając szereg błędów i małych niedogodności. Możliwość ustawienia alarmów na notatkach i nowe wyszukiwanie wśród notatek. Przeczytaj więcej <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>tutaj</a>. Książka adresowa zyskała obsługę wydruku: więcej szczegółów <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>tutaj</a>.

## Okular udoskonala interfejs użytkownika

To wydanie czytnika dokumentów Okular przynosi liczne ulepszenia. Od teraz, dzięki obsłudze kart, można otwierać wiele plików PDF w jednym wystąpieniu Okular. Wprowadzono now tryb powiększania z myszą, a także zaczęto używać DPI bieżącego ekranu do wyświetlania PDF, tym samym poprawiając wygląd dokumentów. Dodano nowy przycisk odtwarzania w trybie prezentacji, a także ulepszono działania znajdowania oraz cofnij/powtórz.

{{<figure src="/announcements/4/4.13.0/screenshots/okular.png" class="text-center" width="600px">}}

## Kate wprowadza ulepszony pasek stanu, animowane dopasowywanie nawiasów, ulepszone wtyczki

Najświeższa wersja edytora Kate wprowadza <a href='http://kate-editor.org/2013/11/06/animated-bracket-matching-in-kate-part/'>animowane dopasowywanie nawiasów</a>, zmiany umożliwiają <a href='http://dot.kde.org/2014/01/20/kde-commit-digest-5th-january-2014'>klawiaturom AltGr na działanie w trybie vim</a> oraz wnoszą szereg usprawnień we wtyczkach Kate, szczególnie w obszarze obsługi Pythona i <a href='http://kate-editor.org/2014/03/16/coming-in-4-13-improvements-in-the-build-plugin/'>wtyczce budowania</a>. Powstał nowy bardzo<a href='http://kate-editor.org/2014/01/23/katekdevelop-sprint-status-bar-take-2/'>ulepszony pasek stanu</a>, który pozwala na bezpośrednie działania takie jak zmiana ustawień wcinania, kodowania i podświetlania, nowy pasek kart w każdym widoku, obsługę uzupełniania kodu dla <a href='http://kate-editor.org/2014/02/20/lumen-a-code-completion-plugin-for-the-d-programming-language/'>języka programistycznego D</a> i <a href='http://kate-editor.org/2014/02/02/katekdevelop-sprint-wrap-up/'>dużo więcej</a>. Zespół <a href='http://kate-editor.org/2014/03/18/kate-whats-cool-and-what-should-be-improved/'>czeka na odzew na temat tego co ulepszyć w Kate</a> i przesuwa swoją uwagę na przystosowanie do Szkieletów 5.

## Różne rozproszone funkcje

Konsole staje się bardziej elastyczny pozwalając na własne arkusze stylów w paskach kart sterowania. Profile mogą teraz przechowywać żądane rozmiary kolumn i wierszy. Po więcej zajrzyj <a href='http://blogs.kde.org/2014/03/16/konsole-new-features-213'>tutaj</a>.

Umbrello umożliwia powielanie diagramów i wprowadza inteligentne menu podręczne, które dopasowują swoją treść do wybranych elementów interfejsów. Usprawniono także obsługę cofania i właściwości. Gwenview <a href='http://agateau.com/2013/12/12/whats-new-in-gwenview-4.12/'> wprowadza obsługę podglądu obrazów RAW</a>.

{{<figure src="/announcements/4/4.13.0/screenshots/marble.png" class="text-center" width="600px">}}

W mikserze dźwięku KMix wprowadzono zdalne sterowanie przez protokół porozumiewania się między procesami DBUS (<a href='http://kmix5.wordpress.com/2013/12/28/kmix-dbus-remote-control/'>szczegóły</a>), dodatki do menu dźwiękowego i nowy dialog konfiguracji (<a href='http://kmix5.wordpress.com/2013/12/23/352/'>szczegóły</a>), oraz szereg poprawek błędów i mniejszych usprawnień.

Interfejs wyszukiwania Dolphin został zmieniony, tak aby zyskać na nowej infrastrukturze wyszukiwania, a także otrzymał dalsze poprawki do wydajności. Szczegółowo możesz o nich przeczytać w tym <a href='http://freininghaus.wordpress.com/2013/12/12/a-brief-history-of-dolphins-performance-and-memory-usage'>przeglądzie prac optymalizacyjnych w czasie ostatniego roku</a>.

KHelpcenter dodaje porządkowanie alfabetyczne dla modułów i reorganizacje kategorii, aby uczynić go prostszym w użyciu.

## Aplikacje edukacyjne i gry

Uaktualniono aplikacje edukacyjne i gry w tym wydaniu KDE. Aplikacja puzzli jigsaw KDE, Palapeli, zyskała <a href='http://techbase.kde.org/Schedules/KDE4/4.13_Feature_Plan#kdegames'>świetne nowe funkcje</a>, które czynią układanie dużych puzzli (do 10,000 kawałków) znacznie łatwiejszym dla tych, którzy podejmują takie wyzwania. KNavalBattle pokazuje położenia wrogich statków po skończeniu gry, tak aby można było zobaczyć gdzie poszło się źle.

{{<figure src="/announcements/4/4.13.0/screenshots/palapeli.png" class="text-center" width="600px">}}

Aplikacje edukacyjne KDE zyskały nowe funkcje. KStars otrzymał interfejs dla skryptów przez D-BUS i może korzystać z API usług sieciowych astrometry.net aby zoptymalizować wykorzystanie pamięci. Cantor zyskał podświetlanie składni w swoim edytorze skryptów i w swoich silnikach dla Scilab i Python 2. Narzędzie edukacyjne z mapami i do nawigacji Marble od teraz posiada położenie <a href='http://kovalevskyy.tumblr.com/post/71835769570/news-from-marble-introducing-sun-and-the-moon'>Słońca, Księżyca</a> i <a href='http://kovalevskyy.tumblr.com/post/72073986685/news-from-marble-planets'>planet</a> oraz oferuje <a href='http://ematirov.blogspot.ch/2014/01/tours-and-movie-capture-in-marble.html'>capturing movies during wirtualne wycieczki</a>. Wytyczanie trasy rowerów zostało ulepszone wraz z dodaną obsługą cyclestreets.net. Od teraz obsługiwane są mile morskie, a naciśnięcie na <a href='http://en.wikipedia.org/wiki/Geo_URI'>Geo URI</a> otworzy Marble.

#### Instalowanie aplikacji KDE

Oprogramowanie KDE, włączając w to wszystkie jego biblioteki i aplikacje, jest dostępne na warunkach Wolnego Oprogramowania. Oprogramowanie KDE działa na rozmaitych konfiguracjach sprzętowych i architekturach procesora, takich jak ARM i x86, systemach operacyjnych oraz działa z każdym rodzajem menadżera okien czy otoczeniem pulpitu. Poza Linuksem i innymi, opartymi o UNIKSA, systemami operacyjnymi można też znaleźć wersję na Microsoft Windows na stronie <a href='http://windows.kde.org'>Oprogramowanie KDE na Windows</a> i wersję dla Apple Mac OS X na stronie <a href='http://mac.kde.org/'>Oprogramowanie KDE na Mac</a>. Eksperymentalne wydania aplikacji KDE, dla różnych platform przenośnych, takich jak MeeGo, MS Windows Mobile i Symbian można znaleźć w sieci, aczkolwiek są one teraz niewspierane. <a href='http://plasma-active.org'>Plasma Active</a> jest środowiskiem użytkownika dla szerokiego zakresu urządzeń, takich jak komputery typu tablet i inne przenośne.

Oprogramowanie KDE można pobrać w postaci źródła i różnych formatów binarnych z <a href='http://download.kde.org/stable/Szkieletów KDE %1'> download.kde.org</a>, ale można je także uzyskać na  <a href='/download'>CD-ROM</a>lub w każdym <a href='/distributions'>znaczącym systemie operacyjnym GNU/Linuks i UNIX</a> obecnie dostępnym.

##### Pakiety

Niektórzy wydawcy Linux/UNIX OS uprzejmie dostarczyli pakiety binarne Szkieletów KDE %[1] dla niektórych wersji ich dystrybucji, a w niektórych przypadkach dokonali tego wolontariusze społeczności.

##### Położenie pakietów

Po bieżącą listę dostępnych pakietów binarnych, o których został poinformowany Zespół wydawniczy KDE, proszę zajrzyj na stronę <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.13.0'> Wiki Społeczności</a>.

Pełny kod źródłowy dla Szkieletów KDE %[1] można <a href='/info/4/4.13.0'> pobrać za darmo</a>. Instrukcje dotyczące kompilowania i wgrywania oprogramowania KDE Szkieletów KDE %[1] są dostępne na <a href='/info/4/4.13.0#binary'>Stronie informacyjnej Szkieletów KDE %[1]</a>.

#### Wymagania systemowe

Aby uzyskać najwięcej z tych wydań, zalecamy użycie najnowszej wersji Qt, takiej jak 4.8.4. Jest to potrzebne, aby zapewnić odczucie stabilności i wydajności, jako iż pewne ulepszenia poczynione w oprogramowaniu KDE, w rzeczywistości zostały dokonane w strukturze programistycznej Qt.

Aby w pełni wykorzystać możliwości oprogramowania KDE, zalecamy  także użycie najnowszych sterowników graficznych dla twojego systemu,  jako iż może to znacznie polepszyć odczucia użytkownika, zarówno w  opcjonalnej funkcjonalności, jak i w ogólnej wydajności i stabilności.
