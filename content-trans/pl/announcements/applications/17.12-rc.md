---
aliases:
- ../announce-applications-17.12-rc
custom_spread_install: true
date: 2017-12-01
description: KDE wydało Aplikacje 17.12 (kandydat do wydania).
layout: application
release: applications-17.11.90
title: KDE wydało kandydata do wydania Aplikacji KDE 17.12
---
1 grudnia 2017. Dzisiaj KDE wydało kandydata do wydania nowej wersji Aplikacji KDE. Wersja ta zamraża wszelkie zmiany w zależnościach i funkcjonalności, a zespół KDE będzie się skupiał jedynie na naprawianiu w niej błędów i dalszym ulepszaniu.

Sprawdź <a href='https://community.kde.org/Applications/17.12_Release_Notes'>listę zmian społeczności</a>, aby uzyskać informację dotyczącą archiwów, obecnie opartych na KF5 oraz ich znanych błędów. Bardziej kompletny komunikat będzie dostępny po wydaniu wersji ostatecznej

Aplikacje KDE, wydanie 17.12 potrzebuje wypróbowania go w szerokim zakresie, aby utrzymać i ulepszyć jakość i odczucia użytkownika. Obecnie użytkownicy są znaczącym czynnikiem przy utrzymywaniu wysokiej jakości KDE, bo programiści po prostu nie mogą wypróbować każdej możliwej konfiguracji. Liczymy, że wcześnie znajdziesz błędy, tak aby mogły zostać poprawione przed wydaniem końcowym. Proszę rozważyć dołączenie do zespołu poprzez zainstalowanie kandydata do wydania i <a href='https://bugs.kde.org/'> zgłaszanie wszystkich błędów</a>.

#### Instalowanie pakietów binarnych Aplikacji KDE 17.12  kandydata do wydania

<em> Pakiety</em>Niektórzy wydawcy systemów operacyjnych Linuks/UNIX  uprzejmie dostarczyli pakiety binarne Aplikacji KDE 17.12 kandydata do  wydania (wewnętrznie 17.11.90) dla niektórych wersji ich dystrybucji, a w niektórych przypadkach dokonali tego wolontariusze społeczności. Dodatkowe pakiety binarne, tak samo jak uaktualnienia do pakietów już dostępnych, mogą stać się dostępne w przeciągu nadchodzących tygodni.

<em>Położenie Pakietów</em>. Po bieżącą listę dostępnych pakietów  binarnych, o których został poinformowany Projekt KDE, zajrzyj na  stronę <a href='http://community.kde.org/Binary_Packages'> Społeczności Wiki</a>.

#### Kompilowanie Aplikacji KDE 17.12 kandydata do wydania

Pełny kod źródłowy dla Aplikacji KDE 17.12 kandydata do wydania można <a href='http://download.kde.org/unstable/applications/17.11.90/src/'>pobrać bez opłaty</a>.Instrukcje na temat kompilowania i instalowania są dostępne na <a href='/info/applications/applications-17.11.90.php'>Stronie informacyjnej Aplikacji KDE kandydata do wydania 17.12</a>.
