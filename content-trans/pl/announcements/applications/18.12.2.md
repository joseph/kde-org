---
aliases:
- ../announce-applications-18.12.2
changelog: true
date: 2019-02-07
description: KDE wydało Aplikacje 18.12.2.
layout: application
major_version: '18.12'
release: applications-18.12.2
title: KDE wydało Aplikacje KDE 18.12.2
version: 18.12.2
---
{{% i18n_date %}}

Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../18.12.0'>Aplikacji KDE 18.12</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

More than a dozen recorded bugfixes include improvements to Kontact, Ark, Konsole, Lokalize, Umbrello, among others.

Wśród ulepszeń znajdują się:

- Ark no longer deletes files saved from inside the embedded viewer</li>
- The address book now remembers birthdays when merging contacts</li>
- Several missing diagram display updates were fixed in Umbrello</li>
