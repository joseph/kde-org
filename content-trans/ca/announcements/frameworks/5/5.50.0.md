---
aliases:
- ../../kde-frameworks-5.50.0
date: 2018-09-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Afegeix el suport per a l'addició de les etiquetes proposades a l'OCS 1.7

### Baloo

- Soluciona un error de tecleig a la sortida de la mida de l'índex (error 397843)
- Elimina l'URL de destí en lloc de «src» quan un URL no és indexable novament
- [tags_kio] Simplifica la coincidència de consulta del camí del nom de fitxer usant un grup de captura
- Reverteix «Omet la posada en cua dels fitxers nous que no es poden indexar i els elimina immediatament de l'índex.»
- [tags_kio] Simplifica la cerca del camí de fitxer mentre està en un bucle

### Icones Brisa

- Afegeix la icona de fitxer de projecte de LabPlot
- ScalableTest, afegeix un «plasma-browser-integration» «escalable» (error 393999)

### Mòduls extres del CMake

- Vinculacions: Comprova si les vinculacions es poden generar per a una versió específica del Python
- Vinculacions: Fa el generador compatible enrere amb el Python 3
- Desactiva l'alteració de QT_PLUGIN_PATH per l'ECM en executar les proves
- Vinculacions: Afegeix el suport per a les enumeracions amb àmbit (error 397154)
- Fa possible que l'ECM detecti fitxers «po» en temps de configuració

### Integració del marc de treball

- [KStyle] Usa «dialog-question» per a la icona de pregunta

### KArchive

- Gestiona codificacions no ASCII dels noms de fitxer en arxius tar (error 266141)
- KCompressionDevice: no crida l'escriptura després de WriteError (error 397545)
- Afegeix les macros «Q_OBJECT» que manquen a les subclasses de «QIODevice»
- KCompressionDevice: propaga els errors des de QIODevice::close() (error 397545)
- Esmena la pàgina principal del «bzip»

### KCMUtils

- Usa una QScrollArea personalitzada amb la mida de consell no limitada per la mida del tipus de lletra (error 389585)

### KConfig

- Elimina un avís quant a una funcionalitat antiga del «kiosk» que ja no aplica
- Estableix la drecera predeterminada del sistema Ctrl+0 per a l'acció «Mida real»

### KCoreAddons

- No elimina l'espai entre dos URL quan la línia comença per " (error del KMail)
- KPluginLoader: usa «/» inclús al Windows, «libraryPaths()» retorna els camins amb «/»
- KPluginMetaData: converteix una cadena buida a una llista de cadenes buida

### KDeclarative

- Reverteix «S'assegura que sempre estem escrivint en el context arrel del motor»
- Adjunta la propietat a «delegate» (error 397367)
- [KCM GridDelegate] Usa l'efecte de capa només al dorsal OpenGL (error 397220)

### KDocTools

- Afegeix l'acrònim ASCII a «general.entities»
- Afegeix JSON a «general.entities»
- Permet que el «meinproc5» sigui més explícit a la prova automàtica «install»
- Usa camins absoluts a la prova «kdoctools_install» per cercar fitxers instal·lats

### KFileMetaData

- Afegeix l'àlies d'enumeració Property::Language per a l'error tipogràfic Property::Langauge

### KHolidays

- Implementa l'algorisme adequat de càlcul dels equinoccis i solsticis (error 396750)
- src/CMakeLists.txt - instal·la les capçaleres a la manera dels Frameworks

### KI18n

- Fa una asserció si intenta usar un KCatalog sense una QCoreApplication
- Adapta «ki18n» del QtScript al QtQml
- Verifica també el directori de construcció per «po/»

### KIconThemes

- Defineix Breeze com a tema d'icones de reserva

### KIO

- [KSambaShareData] Accepta espais als noms de servidors de l'ACL
- [KFileItemListProperties] Usa «mostLocalUrl» per a les capacitats
- [KMountPoint] Comprova també si «smb-share» és un muntatge SMB (error 344146)
- [KMountPoint] Resol els muntatges «gvfsd» (error 344146)
- [KMountPoint] Elimina els traçats del «supermount»
- [KMountPoint] Elimina el suport de l'AIX i el Windows CE
- Mostra el tipus de sistema de fitxers muntat i si està muntat a partir dels camps al diàleg de propietats (error 220976)
- «kdirlistertest» no falla aleatòriament
- [KUrlComboBox] Esmena un error d'adaptació del KIcon
- Adapta el pedaç del KPATH_SEPARATOR al QDir::listSeparator, afegit a les Qt 5.6
- Soluciona una fuita de memòria a KUrlComboBox::setUrl
- [KFileItem] No llegeix el comentari del directori en muntatges lents
- En el seu lloc usa QDir::canonicalPath
- Ignora l'indicador ocult del NTFS per al volum arrel (error 392913)
- Proporciona un botó de cancel·lació al diàleg «nom de directori no vàlid»
- KPropertiesDialog: commuta a una etiqueta a «setFileNameReadOnly(true)»
- Refina el text quan no es pot crear una carpeta amb un nom no vàlid
- Usa la icona adequada per al botó de cancel·lació que pregunti per un nom nou
- Fa seleccionables els noms de fitxer de només lectura
- Usa la majúscula inicial de títol per a diverses etiquetes de botons
- Usa KLineEdit per al nom de carpeta si la carpeta té accés d'escriptura, en cas contrari usa QLabel
- KCookieJar: soluciona una conversió errònia de zona horària

### Kirigami

- Admet «fillWidth» per als elements
- Es protegeix contra la supressió externa de pàgines
- Mostrar sempre la capçalera quan està en mode reduïble
- Soluciona el comportament de «showContentWhenCollapsed»
- Soluciona els forats als menús de l'estil Material
- «actionsmenu» estàndard per la pàgina del menú contextual
- Requereix explícitament el QtQuick de les Qt 5.7 per fer ús de Connections.enabled
- Usa el color de la finestra en lluc d'un element de fons
- S'assegura que el calaix es tanca inclús en publicar una notícia
- Exporta «separatorvisible» a la «globaltoolbar»
- Soluciona la generació del connector estàtic QRC del Kirigami
- Soluciona la construcció en mode estàtic de LTO
- Assegura que la propietat «drawerOpen» se sincronitza correctament (error 394867)
- Calaix: mostra el contingut del giny en arrossegar
- Permet que els actius del «qrc» s'usin a les icones de les accions
- «ld» en un «gcc» antic (error 395156)

### KItemViews

- Fa obsolet KFilterProxySearchLine

### KNewStuff

- Memòria cau de «providerId»

### KNotification

- Implementa «libcanberra» per a les notificacions d'àudio

### KService

- KBuildSycoca: sempre processa els fitxers «desktop» d'aplicació

### KTextEditor

- Canvia l'enumeració Kate::ScriptType a una classe d'enumeració
- Corregeix la gestió d'errors a QFileDevice i KCompressedDevice
- InlineNotes: no imprimeix les notes incloses
- Elimina QSaveFile afavorint el desament antic a un fitxer normal
- InlineNotes: usa les coordenades globals de pantalla per tot arreu
- InlineNote: inicialitza la posició amb Cursor::invalid()
- InlineNote: dades de notes incloses «pimpl» sense assignacions
- Afegeix una interfície de notes incloses
- Mostra una vista prèvia del text només si la finestra principal és activa (error 392396)
- Soluciona una fallada en ocultar el giny TextPreview (error 397266)
- Fusiona ssh://git.kde.org/ktexteditor
- Millora de la renderització HiDPI de les vores de les icones
- Millora el tema de color del Vim (error 361127)
- Cerca: afegeix una solució temporal per a les icones que manquen al tema d'icones del Gnome
- Soluciona el sobrepintat de «_» o lletres com la «j» a l'última línia (error 390665)
- Amplia l'API de creació de scripts per permetre executar ordres
- Script de sagnat per a l'R
- Soluciona una fallada en substituir «n» a les línies buides (error 381080)
- Elimina el diàleg de baixada de ressaltat
- No cal un resum nou/suprimit per cada «doHighlight», netejant-ho ja és prou bo
- Assegura que es poden gestionar índexs d'atributs no vàlids que poden donar-se com a resultat després d'un canvi de ressaltat en un document
- Permet que un apuntador intel·ligent gestioni la supressió d'objectes, menys feina manual a fer
- Elimina el mapa per cercar propietats addicionals de ressaltat
- El KTextEditor usa el framework KSyntaxHighlighting per a tot
- Usa les codificacions de caràcters tal com s'han proporcionat a les definicions
- Fusiona la branca «master» a «syntax-highlighting»
- El text no negreta ja no es renderitza amb el tipus de lletra de tipus fi (error 393861)
- Usa «foldingEnabled»
- Elimina EncodedCharaterInsertionPolicy
- Impressió: respecta el tipus de lletra del peu, soluciona la posició vertical del peu, fa visualment més fina la línia de separació de la capçalera/peu
- Fusiona la branca «master» a «syntax-highlighting»
- Permet que el marc de treball «syntax-highlighting» gestioni tota la manipulació de la definició ara que hi ha la definició «None» al repositori
- Giny de compleció: esmena la mida mínima de la secció de capçalera
- Esmena: desplaçament de les línies visualitzades en lloc de les línies reals per al desplaçament de la roda o del ratolí tàctil (error 256561)
- Elimina la prova de sintaxi, que ara es prova al mateix framework de ressaltat de sintaxi
- La configuració del KTextEditor ara torna a ser una aplicació local, la configuració global antiga s'importarà en el primer ús
- Usa KSyntaxHighlighting::CommentPosition en lloc de KateHighlighting::CSLPos
- Usa «isWordWrapDelimiter()» des de KSyntaxHighlighting
- Reanomena «isDelimiter()» a «isWordDelimiter()»
- Implementa més material de cerca via format -&gt; enllaç de definició
- Ara s'obtenen sempre formats vàlids
- Una manera més fina d'aconseguir el nom d'atribut
- Esmena la prova de sagnat del Python, un mètode d'accés més segur a les bosses de propietats
- Torna a afegir la definició correcta de prefixos
- Fusiona la branca «syntax-highlighting» de git://anongit.kde.org/ktexteditor dins el «syntax-highlighting»
- Intenta retornar les llistes necessàries per fer la configuració per esquema
- Usa KSyntaxHighlighting::Definition::isDelimiter()
- El «make» pot trencar el «bit» més com en el codi de paraula
- No fa una llista enllaçada sense cap motiu
- Neteja les propietats d'inici
- Esmena l'ordre dels formats, recorda la definició a la bossa de ressaltat
- Gestiona formats no vàlid / formats de longitud zero
- Elimina més parts d'implementacions antigues, esmena diversos mètodes d'accés per usar el material dels formats
- Esmena el sagnat basat en el plegat
- Elimina l'exposició de la pila de context a «doHighlight» + esmena a «ctxChanged»
- Comença a emmagatzemar el material plegat
- Eliminació dels ajudants de ressaltat, ja no es necessiten més
- Elimina la necessitat de «contextNum», afegeix el marcador FIXME-SYNTAX a allò que cal esmenar adequadament
- Adaptació als canvis de «includedDefinitions», elimina «contextForLocation», només calen paraules clau per la ubicació o verificació de sintaxi per la ubicació. es pot implementar més endavant
- Elimina parts de ressaltat de sintaxi que ja no es necessiten
- Esmena la «m_additionalData» i una mica el seu mapatge, hauria de funcionar per atributs, no per context
- Crea els atributs inicials, encara sense valors reals d'atribut, només una llista de quelcom
- Crida el ressaltat
- Deriva del ressaltador abstracte, estableix la definició

### Framework del KWallet

- Mou l'exemple des de Techbase al repositori propi

### KWayland

- Sincronitza els mètodes «set»/«send»/«update»
- Afegeix els números de sèrie i l'ID d'EISA a la interfície OutputDevice
- Correcció de corbes de color del dispositiu de sortida
- Esmena la gestió de memòria a WaylandOutputManagement
- Aïlla cada prova a WaylandOutputManagement
- Escalat fraccionari a OutputManagement

### KWidgetsAddons

- Crea un primer exemple de l'ús de KMessageBox
- Soluciona dos errors a KMessageWidget
- [KMessageBox] Crida l'estil per a la icona
- Afegeix una solució temporal per a etiquetes amb ajust de paraules (error 396450)

### KXMLGUI

- Fa que el Konqi aparenti millor en HiDPI
- Afegeix els parèntesis que mancaven

### NetworkManagerQt

- Requereix el NetworkManager 1.4.0 o posterior
- Gestor: afegeix la implementació per llegir/escriure la propietat GlobalDnsConfiguration
- Permet realment definir la freqüència d'actualització per a les estadístiques de dispositiu

### Frameworks del Plasma

- Solució temporal d'error amb la renderització nativa i opacitat al text de TextField (error 396813)
- [Icon Item] Controla el canvi d'icona del KIconLoader en usar QIcon (error 397109)
- [Icon Item] Usa ItemEnabledHasChanged
- Desfer-se d'un ús obsolet de QWeakPointer
- Esmenar el full d'estil per a «22-22-system-suspend» (error 397441)
- Millora l'eliminació de ginys i la configuració de text

### Solid

- Solid/udisks2: afegeix la implementació per a l'enregistrament categoritzat
- [Windows Device] Mostra l'etiqueta de dispositiu només si n'hi ha
- Força la reavaluació dels Predicates si s'eliminen les interfícies (error 394348)

### Sonnet

- Hunspell: restaura la construcció amb el Hunspell &lt;=v1.5.0
- Inclou les capçaleres del Hunspell com a inclusions del sistema

### Sindicació

Mòdul nou

### Ressaltat de la sintaxi

- Ressalta 20.000 línies per cada cas de prova
- Fa que les proves de referència del ressaltat siguin més reproduïbles, sempre es vol mesurar aquesta execució amb perfils de fora, per exemple
- Afina la cerca de KeywordList i evita assignacions per als grups de captura implícits
- Elimina les captures per «Int», no s'ha implementat mai
- Iteració determinística de proves per a una millor comparativa dels resultats
- Gestiona els atributs inclosos imbricats
- Actualitza el ressaltat del Modula-2 (error 397801)
- Precalcula el format dels atributs pel context i les regles
- Evita la verificació del delimitador de paraula a l'inici de paraula clau (error 397719)
- Afegeix el ressaltat de sintaxi per al llenguatge de política del nucli de SELinux
- Oculta «bestCandidate», pot ser una funció estàtica dins un fitxer
- Afegeix diverses millores al «kate-syntax-highlighter» per a ús en la creació de scripts
- S'ha afegit := com a part vàlida d'un identificador
- Usa les nostres dades d'entrada per les proves de referència
- Intenta solucionar el problema de final de línia en comparar resultats
- Intenta una sortida de diferència trivial per al Windows
- Torna a afegir «defData» per a la verificació d'estat vàlid
- Redueix l'espai de StateData en més d'un 50% i a la meitat del nombre de «mallocs» necessaris
- Millora el rendiment de Rule::isWordDelimiter i KeywordListRule::doMatch
- Millora la gestió de salt de desplaçament, permet saltar a una línia completa si no coincideix
- Verifica la llista de comodins de les extensions
- Més ressaltat de l'Asterisk, s'han provat diverses configuracions de l'Asterisk, només són en estil «ini», usa «.conf» com a final d'«ini»
- Soluciona el ressaltat per les parts #ifdef _xxx (error 397766)
- Esmena els comodins en fitxers
- S'ha fet el rellicenciament a MIT del KSyntaxHighlighting
- JavaScript: afegeix binaris, esmena els octals, millora les escapades i permet identificadors no ASCII (error 393633)
- Permet desactivar les cerques de QStandardPaths
- Permet instal·lar fitxers de sintaxi en lloc de tenir-los en un recurs
- Gestiona el canvi de context dels atributs a partir dels mateixos contexts
- Canvia de biblioteca estàtica a biblioteca objecte amb una configuració correcta del PIC, que hauria de funcionar amb construccions compartides i estàtiques
- Evita qualsevol assignació de memòria en monticles per a un Format() construït de manera predeterminada i usat com a «invalid»
- Respecta la variable del CMake per a biblioteques estàtiques i dinàmiques, com p. ex. el Karchive
- Rellicenciament a MIT, https://phabricator.kde.org/T9455
- Elimina l'antic script «add_license», ja no caldrà més
- Esmena «includedDefinitions», gestiona el canvi de definicions en canvis de context (error 397659)
- SQL: diverses millores i esmenes de detecció «if»/«case»/«loop»/«end» amb l'SQL (Oracle)
- Esmena els fitxers de referència
- SCSS: actualització de sintaxi. CSS: esmena el ressaltat de les etiquetes Operator i Selector
- Debchangelog: afegeix Bookworm
- Torna a llicenciar el Dockerfile a la llicència MIT
- Elimina la part de la configuració de la verificació ortogràfica que ja no està admesa i que sempre havia estat en un mode que ara s'ha posat al codi font
- Afegeix un fitxer d'implementació de ressaltat de sintaxi per a Stan
- Afegeix un sagnat cap enrere
- Optimitza molts fitxers de ressaltat de sintaxi i esmena al caràcter «/» de l'SQL
- Modelines: afegeix la «byte-order-mark» i esmenes petites
- Torna a llicenciar el «modelines.xml» a la llicència MIT (error 198540)
- Afegeix QVector&lt;QPair&lt;QChar, QString&gt;&gt; Definition::characterEncodings() const
- Afegeix el booleà Definition::foldingEnabled() const
- Afegeix el ressaltat «None» al repositori com a predeterminat
- Actualitza la implementació del llenguatge Logtalk
- Afegeix el format de fitxer «sch» i «brd» de l'EAGLE Autodesk a la categoria XML
- Ressaltat C#: Es prefereix el sagnat C-Style
- AppArmor: actualitza la sintaxi i diverses millores/esmenes
- Java: afegeix binaris i hex-float, i admet caràcters de subratllat als nombres (error 386391)
- Neteja: el sagnat s'ha mogut de la secció general a la de llenguatge
- Definició: exposa els marcadors d'ordre
- Afegeix el ressaltat del JavaScript React
- YAML: esmena de claus, afegeix nombres i altres millores (error 389636)
- Afegeix el booleà Definition::isWordWrapDelimiter(QChar)
- Definició: reanomena «isDelimiter()» a «isWordDelimiter()»
- Nota d'idees de millora per l'API de KF6 a partir de l'adaptació del KTE
- Proporciona un format vàlid també per a les línies buides
- Fa que Definition::isDelimiter() també funcioni per a les definicions no vàlides
- Definició: exposa el booleà «isDelimiter() const»
- Ordena per ID els formats retornats a Definition::formats()

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
