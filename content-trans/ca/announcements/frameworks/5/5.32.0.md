---
aliases:
- ../../kde-frameworks-5.32.0
date: 2017-03-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Implementa etiquetes imbricades

### Icones Brisa

- Afegeix icones d'aplicació per a la Caixa forta del Plasma
- Icones reanomenades per a carpetes encriptades i desencriptades
- Afegeix icones de Torrents de 22px
- Afegeix icones de nm-tray (error 374672)
- Color-management: s'han eliminat enllaços no definits (error 374843)
- System-run ara és una acció fins &lt;= 32px i 48px una icona d'aplicació (error 375970)

### Mòduls extres del CMake

- Detecta «inotify»
- Reverteix «Marca automàticament les classes amb funcions virtuals pures com Abstract/»

### KActivitiesStats

- Permet planificació avançada i definir l'ordre d'un element que no estigui encara a la llista

### KArchive

- Soluciona una fuita de memòria potencial informada per «limitedDev»

### KCMUtils

- Soluciona una fallada potencial als KCM escrits en QML quan es canvia la paleta de l'aplicació

### KConfig

- KConfig: atura l'exportació i instal·lació del KConfigBackend

### KConfigWidgets

- KColorScheme: el valor predeterminat per l'esquema d'aplicació es defineix a KColorSchemeManager (error 373764)
- KConfigDialogManager: obté el canvi del senyal del «metaObject» o d'una propietat especial
- Soluciona la comprovació d'error del «KCModule::setAuthAction»

### KCoreAddons

- Exclou (6) del reconeixement de les emoticones
- KDirWatch: soluciona una fuita de memòria en destruir

### Compatibilitat amb les KDELibs 4

- Soluciona un error en el kfiledialog.cpp que provoca una fallada en utilitzar ginys natius

### KDocTools

- Meinproc5: enllaça als fitxers, no a la biblioteca (error 377406)
- Elimina la biblioteca estàtica KF5::XsltKde
- Exporta la biblioteca compartida adequada per les KDocTools
- Adaptat a un enregistrament més categoritzat i neteja d'inclusions
- Afegeix una funció per extreure un únic fitxer
- Falla aviat a la construcció si «xmllint» no és disponible (error 376246)

### KFileMetaData

- Mantenidor nou per «kfilemetadata»
- [ExtractorCollection] Usa l'herència dels tipus MIME per retornar connectors
- Afegeix la propietat nova DiscNumber per als fitxers d'àudio d'àlbums multidisc

### KIO

- KCM de Galetes: desactiva el botó «Suprimeix» si no hi ha cap element actual
- kio_help: usa la nova biblioteca compartida exportada per les KDocTools
- kpac: saneja els URL abans de passar-los a FindProxyForURL (esmena de seguretat)
- Importa els «ioslave» remots des del plasma-workspace
- kio_trash: implementa el canvi de nom dels fitxers de primer nivell i directoris
- PreviewJob: elimina la mida màxima predeterminada per als fitxers locals
- DropJob: permet afegir accions d'aplicació en un menú obert
- ThumbCreator: fa obsolet DrawFrame, tal com es va debatre a https://git.reviewboard.kde.org/r/129921/

### KNotification

- Afegeix la implementació per als portals «flatpak»
- Envia «desktopfilename» com a part dels consells «notifyByPopup»
- [KStatusNotifierItem] Restaura la finestra minimitzada com a normal

### Framework del KPackage

- Finalitza la implementació per obrir paquets comprimits

### KTextEditor

- Recorda el tipus de fitxer definit per l'usuari entre les sessions
- Reinicia el tipus de fitxer en obrir un URL
- Afegeix un «getter» per al valor de la configuració del comptador de paraules
- Conversió coherent des del/al cursor a/des de les coordenades
- Actualitza el tipus de fitxer en desar només si canvia el camí
- Admet fitxers de configuració EditorConfig (per als detalls: http://editorconfig.org/)
- Afegeix FindEditorConfig al «ktexteditor»
- Soluciona: l'acció «emmetToggleComment» no funciona (error 375159)
- Usa l'estil de sentència de posar en majúscula la primera lletra dels textos d'etiqueta de camps d'edició
- Inverteix el significat de :split, :vsplit per coincidir amb les accions del vi i el Kate
- Usa log2() del C++11en lloc de log() / log(2)
- KateSaveConfigTab: situa l'espaiador després del darrer grup a la pestanya Avançat, no a dins
- KateSaveConfigTab: elimina el marge erroni al voltant del contingut a la pestanya Avançat
- Configuració de les vores de la subpàgina: soluciona la visibilitat de la barra de desplaçament quan el quadre combinat està fora de lloc

### KWidgetsAddons

- KToolTipWidget: oculta el consell d'eina a «enterEvent» si «hideDelay» és zero
- Soluciona la pèrdua del focus a KEditListWidget en clicar els botons
- Afegeix la descomposició de les síl·labes Hangul en Hangul Jamo
- KMessageWidget: soluciona el comportament en la superposició de crides «animatedShow»/«animatedHide»

### KXMLGUI

- No usa tecles del KConfig amb barres inverses

### NetworkManagerQt

- Sincronitza les introspeccions i els fitxers generats amb el NM 1.6.0
- Gestor: soluciona l'emissió de dues «deviceAdded» quan es reinicia el NM

### Frameworks del Plasma

- Estableix els consells predeterminats quan «repr» no exporta Layout.* (error 377153)
- Fa possible establir «expanded=false» per a un contenidor
- [Menú] Millora la correcció de l'espai disponible a «openRelative»
- Mou la lògica de «setImagePath» a «updateFrameData()» (error 376754)
- IconItem: afegeix la propietat «roundToIconSize»
- [SliderStyle] Permet proporcionar un element «hint-handle-size»
- Connecta totes les connexions a l'acció en el QMenuItem::setAction
- [ConfigView] Respecta les restriccions del mòdul de control KIOSK
- Soluciona la desactivació l'animació de l'indicador rotatiu quan l'indicador d'ocupat no té opacitat
- [FrameSvgItemMargins] No actualitza per «repaintNeeded»
- Icones de miniaplicació per la Caixa forta del Plasma
- Migra AppearAnimation i DisappearAnimation a Animators
- Alinea la vora inferior a la vora superior del «visualParent» en el cas TopPosedLeftAlignedPopup
- [ConfigModel] Emet «dataChanged» quan canvia una ConfigCategory
- [ScrollViewStyle] Avalua la propietat «frameVisible»
- [Estils de botó] Usa Layout.fillHeight en lloc de «parent.height» a un Layout (error 375911)
- [ContainmentInterface] També alinea el menú contextual del contenidor al plafó

### Prison

- Esmena la versió mínima de les Qt

### Solid

- Els disquets ara mostren «Disquets» en lloc de «0 B suport extraïble»

### Ressaltat de la sintaxi

- Afegeix més paraules clau. Desactiva la verificació ortogràfica per a les paraules clau
- Afegeix més paraules clau
- Afegeix l'extensió de fitxer *.RHTML al ressaltat Ruby on Rails (error 375266)
- Actualitza el ressaltat de sintaxi de SCSS i CSS (error 376005)
- Menys ressaltat: esmena els comentaris de línia única que comencen regions noves
- Ressaltat del LaTeX: soluciona l'alineació a l'entorn (error 373286)

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
