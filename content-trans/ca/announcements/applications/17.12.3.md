---
aliases:
- ../announce-applications-17.12.3
changelog: true
date: 2018-03-08
description: KDE distribueix les aplicacions 17.12.3 del KDE
layout: application
title: KDE distribueix les aplicacions 17.12.3 del KDE
version: 17.12.3
---
8 de març de 2018. Avui KDE distribueix la tercera actualització d'estabilització per a les <a href='../17.12.0'>aplicacions 17.12 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha unes 25 esmenes registrades d'errors que inclouen millores al Kontact, Dolphin, Gwenview, JuK, KGet, Okular i Umbrello, entre d'altres.

Les millores inclouen:

- L'Akregator ja no esborra la llista de fonts «feeds.opml» després d'un error
- El mode de pantalla completa del Gwenview ara funciona amb el nom de fitxer correcte després de reanomenar
- S'han identificat i esmenat diverses fallades rares a l'Okular
