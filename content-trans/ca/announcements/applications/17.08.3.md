---
aliases:
- ../announce-applications-17.08.3
changelog: true
date: 2017-11-09
description: KDE distribueix les aplicacions 17.08.3 del KDE
layout: application
title: KDE distribueix les aplicacions 17.08.3 del KDE
version: 17.08.3
---
9 de novembre de 2017. Avui KDE distribueix la tercera actualització d'estabilització per a les <a href='../17.08.0'>aplicacions 17.08 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha una dotzena d'esmenes registrades d'errors que inclouen millores al Kontact, Ark, Gwenview, KGpg, KWave, Okular, i Spectacle entre d'altres.

Aquest llançament també inclou la darrera versió de la plataforma de desenvolupament KDE 4.14.38.

Les millores inclouen:

- Soluciona temporalment una regressió del Samba 4.7 amb les comparticions SMB protegides amb contrasenya
- L'Okular ja no torna a fallar després de tasques concretes de rotació
- L'Ark manté les dates de modificació dels fitxers en extreure arxius ZIP
