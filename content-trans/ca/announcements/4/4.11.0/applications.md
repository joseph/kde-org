---
date: 2013-08-14
hidden: true
title: Les aplicacions del KDE 4.11 fan un pas endavant en la gestió d'informació
  personal i millores globals
---
El gestor de fitxers Dolphin ha tingut moltes esmenes petites i optimitzacions en aquest llançament. La càrrega de carpetes grans s'ha accelerat i requereix fins a un 30&#37; menys de memòria. S'evita la càrrega pesada de disc i l'activitat de la CPU carregant només la vista prèvia dels elements visibles. Hi ha moltes millores més: per exemple, s'han esmenat molts errors que afectaven les carpetes ampliables en la vista de detalls, no es tornaran a mostrar icones de substitució &quot;desconegudes&quot; en entrar a una carpeta, i clicant amb el botó del mig en un arxiu ara s'obrirà una pestanya nova amb el contingut de l'arxiu, creant una experiència global més compatible.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`El nou flux de treball «envia més tard» en el Kontact` width="600px">}}

## Millores en el paquet Kontact

El paquet Kontact s'ha centrat de nou en l'estabilitat, el rendiment i l'ús de la memòria. La importació de carpetes, commutació entre mapes, recuperació del correu, marcant o movent gran nombre de missatges i el temps d'inici s'han millorat en els darrers 6 mesos. Vegeu <a href='http://blogs.kde.org/2013/07/18/memory-usage-improvements-411'>aquest blog</a> per als detalls. La <a href='http://www.aegiap.eu/kdeblog/2013/07/news-in-kdepim-4-11-archive-mail-agent/'>funcionalitat d'arxiu ha rebut moltes esmenes d'errors</a> i també s'han efectuat millores en l'ImportWizard, permetent la importació de la configuració des del client de correu Trojitá i la millora de la importació d'altres aplicacions. Trobeu més informació <a href='http://www.progdan.cz/2013/07/whats-new-in-the-akonadi-world/'>aquí</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kmail-archive-agent.png" caption=`L'agent d'arxiu gestiona l'emmagatzematge de correu electrònic en forma comprimida` width="600px">}}

Aquest llançament també aporta algunes funcionalitats noves significatives. Hi ha un <a href='http://www.aegiap.eu/kdeblog/2013/05/news-in-kdepim-4-11-header-theme-33-grantlee-theme-generator-headerthemeeditor/'>editor de temes nou per les capçaleres dels correus electrònics</a> i les imatges dels correus es poden redimensionar al vol. La <a href='http://www.aegiap.eu/kdeblog/2013/07/new-in-kdepim-4-11-send-later-agent/'>funcionalitat «envia més tard»</a> permet planificar l'enviament de correus en una data i hores determinades, amb la funcionalitat afegida d'enviament repetit d'acord amb un interval determinat. S'ha millorat la implementació del filtre Sieve del KMail (una funcionalitat de l'IMAP que permet el filtratge en el servidor), els usuaris poden generar scripts de filtratge de sieve <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-improve-sieve-support-22/'>amb una interfície d'ús senzill</a>. En l'àrea de la seguretat, el KMail introdueix la detecció automàtica de frau, <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-scam-detection/'>mostrant un avís</a> quan els correus contenen els típics estratagemes de pesca. Ara rebreu una <a href='http://www.aegiap.eu/kdeblog/2013/06/news-in-kdepim-4-11-new-mail-notifier/'>notificació informativa</a> quan arribi correu nou. I per acabar, l'aplicació d'edició de blogs Blogilo té un editor HTML basat en QtWebKit molt millorat.

## Ampliació de la implementació de llenguatges per al Kate

L'editor de text avançat Kate introdueix connectors nous: Python (2 i 3), JavaScript i JQuery, Django i XML. S'introdueixen funcionalitats com l'autocompleció estàtica i dinàmica, verificació sintàctica, inserció de retalls de codi i la possibilitat de sagnat automàtic de l'XML amb una drecera. Però n'hi ha més per als amics de Python: una consola python que proporciona informació en profunditat d'un fitxer de codi font obert. També s'han efectuat algunes petites millores de la IU, incloses <a href='http://kate-editor.org/2013/04/02/kate-search-replace-highlighting-in-kde-4-11/'>unes noves notificacions passives per a la funcionalitat cercada</a>, <a href='http://kate-editor.org/2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff/'>optimitzacions en el mode VIM</a> i <a href='http://kate-editor.org/2013/03/27/new-text-folding-in-kate-git-master/'>una funcionalitat nova de plegat de text</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kstars.png" caption=`El KStars mostra els pròxims esdeveniments interessants visibles des de la vostra ubicació` width="600px">}}

## Altres millores d'aplicacions

En l'àrea dels jocs i l'educació han arribat diverses funcionalitats noves petites i més grans i optimitzacions. Els mecanògrafs prospectius poden gaudir la implementació de dreta a esquerra en el KTouch mentre que l'amic dels observadors d'estrelles, el KStars, ara té una eina que mostra els pròxims esdeveniments interessants de la vostra àrea. Les eines matemàtiques Rocs, Kig, Cantor i KAlgebra criden l'atenció, implementant més dorsals i càlculs. I el joc KJumpingCube té funcionalitats com mides de taulell més grans, nivells d'intel·ligència nous, respostes més ràpides i una interfície d'usuari millorada.

L'aplicació de dibuix senzill Kolourpaint pot tractar el format d'imatge WebP i el visualitzador universal de documents Okular té eines de revisió que es poden configurar i introdueix la implementació de desfer i refer en formularis i anotacions. El reproductor/etiquetador d'àudio JuK implementa la reproducció i edició de metadades del format d'àudio nou Ogg Opus (això requereix que el controlador d'àudio i la TagLib també implementin Ogg Opus).

#### Instal·lació d'aplicacions del KDE

El programari KDE, incloses totes les seves biblioteques i les seves aplicacions, és disponible de franc d'acord amb les llicències de codi font obert (Open Source). El programari KDE s'executa en diverses configuracions de maquinari i arquitectures de CPU com ARM i x86, sistemes operatius i treballa amb qualsevol classe de gestor de finestres o entorn d'escriptori. A banda del Linux i altres sistemes operatius basats en UNIX, podeu trobar versions per al Windows de Microsoft de la majoria d'aplicacions del KDE al lloc web <a href='http://windows.kde.org'>programari de Windows</a> i versions per al Mac OS X d'Apple en el <a href='http://mac.kde.org/'>lloc web del programari KDE per al Mac</a>. Les compilacions experimentals d'aplicacions del KDE per a diverses plataformes mòbils com MeeGo, MS Windows Mobile i Symbian es poden trobar en la web, però actualment no estan suportades. El <a href='http://plasma-active.org'>Plasma Active</a> és una experiència d'usuari per a un espectre ampli de dispositius, com tauletes i altre maquinari mòbil.

El programari KDE es pot obtenir en codi font i en diversos formats executables des de <a href='https://download.kde.org/stable/4.11.0'>download.kde.org</a> i també es pot obtenir en <a href='/download'>CD-ROM</a> o amb qualsevol dels <a href='/distributions'>principals sistemes GNU/Linux i UNIX</a> que es distribueixen avui en dia.

##### Paquets

Alguns venedors de Linux/UNIX OS han proporcionat gentilment paquets executables de 4.11.0 per a algunes versions de la seva distribució, i en altres casos ho han fet els voluntaris de la comunitat. <br />

##### Ubicació dels paquets

Per a una llista actual de paquets executables disponibles dels quals ha informat l'equip de llançament del KDE, si us plau, visiteu el <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>wiki de la comunitat</a>.

El codi font complet per a 4.11.0 es pot <a href='/info/4/4.11.0'>descarregar de franc</a>. Les instruccions per a compilar i instal·lar el programari KDE, versió 4.11.0, estan disponibles a la <a href='/info/4/4.11.0#binary'>pàgina d'informació de la versió 4.11.0</a>.

#### Requisits del sistema

Per tal d'aprofitar al màxim aquestes versions, es recomana utilitzar una versió recent de les Qt, com la 4.8.4. Això és necessari per a assegurar una experiència estable i funcional, ja que algunes millores efectuades en el programari KDE s'ha efectuat realment en la infraestructura subjacent de les Qt.<br /> Per tal de fer un ús complet de les capacitats del programari KDE, també es recomana l'ús dels darrers controladors gràfics per al vostre sistema, ja que aquest pot millorar substancialment l'experiència d'usuari, tant per a les funcionalitats opcionals, com el rendiment i l'estabilitat en general.

## També s'ha anunciat avui:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Els espais de treball del Plasma 4.11 continuen refinant l'experiència d'usuari</a>

Preparat per un manteniment a llarg termini, els espais de treball Plasma proporcionen més millores a les funcionalitats bàsiques amb una barra de tasques més suau, un giny de bateria més intel·ligent, i un mesclador de so millorat. La introducció del KScreen aporta una gestió intel·ligent del multimonitor als espais de treball, i les millores de rendiment a gran escala combinades amb petits retocs d'usabilitat milloren l'experiència global de manera agradable.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> La plataforma KDE 4.11 proporciona un rendiment millor</a>

Aquest llançament de la plataforma KDE 4.11 continua enfocant-se en l'estabilitat. Les funcionalitats noves s'estan implementant amb el nostre futur llançament dels Frameworks 5.0 de KDE, però per al llançament estable s'ha restringit a optimitzar la infraestructura del Nepomuk.
