---
aliases:
- ../../kde-frameworks-5.79.0
date: 2021-02-13
layout: framework
libCount: 83
qtversion: 5.14
---
### Attica

* Konvertera från QNetworkRequest::FollowRedirectsAttribute till QNetworkRequest::RedirectPolicyAttribute

### Baloo

* [SearchStore] Ta bort filsystemberoenden från egenskapen includeFolder
* [FileIndexerConfig] Rätta kontroll för dolda explicit inkluderade kataloger
* [FilteredDirIterator] Begär dolda filer från QDirIterator

### Breeze-ikoner

* nya ikoner för telegram-panel
* Använd rätt stil för align-horizontal-left-out ikoner (fel 432273)
* Lägg till nya ikoner för kickoff (fel 431883)
* Lägg till halvt betyg, 100 % ogenomskinlighet för utan betyg. Textfärg för betyg i mörkt tema
* Ta bort ikoner för KeePassXC (fel 431593)
* Rätta @3x ikoner (fel 431475)
* lägg till ikon för neochat

### Extra CMake-moduler

* Aktivera bara GNU_TAR_FOUND när --sort=name är tillgängligt
* Ta bort metadatagenerering för fastlane från ett givet APK
* KDEFrameworksCompilerSettings: definiera normalt -DQT_NO_KEYWORDS och -DQT_NO_FOREACH
* [KDEGitCommitHooks] Skapa kopia av skript i källkatalog
* Stöd också den nya filändelsen för Appstream
* fetch-translations: Lös upp webbadress innan den skickas till fetchpo.rb
* Ta hänsyn till Appstream bidragswebbadresser för att skapa F-Droid metadata
* Rätta rättigheter för skript (fel 431768)
* ECMQtDeclareLoggingCategory: skapa filen .categories vid bygge, inte konfigurering
* Lägg till cmake-funktion för att ställa in git förarkiveringshakar

### Integrering med ramverk

* Rätta att fönsterdekorationer inte kan avinstalleras (fel 414570)

### KDE Doxygen-verktyg

* Säkerställ att vi inte använder doxygen standardteckensnitt
* Förbättra återgivning med QDoc och rätta några fel i det mörka temat
* Uppdatera underhållsinformation
* Helt nytt tema likformigt med develop.kde.org/docs

### KCalendarCore

* Avregistrera MemoryCalendar som observatör när en förekomst tas bort
* Använd recurrenceId för att ta bort rätt förekomst
* Rensa också associationer till anteckningsbok när en MemoryCalendar stängs

### KCMUtils

* Säkerställ enkolumnersläge

### KCodecs

* Ta bort användning av icke-UTF-8 strängliteraler

### KCompletion

* Rätta regression orsakad av konvertering från operator+ till operator|

### KConfig

* Omstrukturera koden för spara/återställ fönstergeometri så den är mindre ömtålig
* Rätta återställning av fönsterstorlek när det stängs maximerat (fel 430521)
* KConfig: bevara komponenten millisekunder från QDateTime

### KCoreAddons

* Lägg till KFuzzyMatcher för inexakt filtrering av strängar
* KJob::infoMessage: dokumentera att argumentet richtext kommer att tas bort, oanvänt
* KJobUiDelegate::showErrorMessage: implementera med qWarning()
* Avråd från metoder relaterade till X-KDE-PluginInfo-Depends
* Kasta X-KDE-PluginInfo-Depends nycklar

### KDeclarative

* Tillåt enstaka kolumnobjekt
* KeySequenceItem: Tilldela tom sträng vid rensning istället för odefinierad (fel 432106)
* Skilj mellan markerade tillstånd och tillstånd när muspekaren hålls över för GridDelegate (fel 406914)
* Använd normalt enkelt läge

### KFileMetaData

* ffmpegextractor: Använd av_find_default_stream_index för att hitta videoström

### KHolidays

* Uppdatera helgdagar för Mauritius för 2021
* Uppdatera Taiwanesiska helger

### KI18n

* Ställ inte in kodare för textström vid bygge med Qt5

### KImageFormats

* Förenkla delar av NCLX färgprofilkoden
* [imagedump] Lägg till alternativet "lista MIME-typ" (-m)
* Rätta krasch med felformande filer
* ani: Säkerställ att riffSizeData har korrekt storlek innan quint32_le typkonverteringsdansen görs
* Lägg till insticksprogram för animerade Windows-pekare (ANI)

### KIO

* Använd makrot Q_LOGGING_CATEGORY istället för explicit QLoggingCategory (fel 432406)
* Rätta att standardkodare ställs in till "US-ASCII" i KIO-program (fel 432406)
* CopyJob: rätta krasch med hoppa över/försök igen (fel 431731)
* KCoreDirLister: ta bort överlagring av signalerna canceled() och completed()
* KFilePreviewGenerator: modernisera kodbasen
* KCoreDirLister: ta bort överlagring av signalen clear()
* MultiGetJob: ta bort överlagring av signaler
* FileJob: ta bort överlagring av signalen close()
* SkipDialog: avråd från signalen result(SkipDialog *_this, int _button)
* Rätta låsning när namnet på en fil  ändras i egenskapsdialogrutan (fel 431902)
* Avråd från addServiceActionsTo och addPluginActionsTo
* [KFilePlacesView] Lägre ogenomskinlighet för dolda objekt i "visa alla"
* Ändra inte kataloger när icke-listningsbara webbadresser öppnas
* Finjustera logiken i KFileWidget::slotOk när fil + katalogläge används
* FileUndoManager: rätta ångra vid kopiering av tom katalog
* FileUndoManager: skriv inte över filer vid ångra
* FileUndoManager: avråd från metoden undoAvailable()
* ExecutableFileOpenDialog: gör textetiketten mer generell
* KProcessRunner: skicka bara signalen processStarted() en gång
* Återställ "kio_trash: rätta logiken när ingen storleksgräns är inställt"

### Kirigami

* Använd icke-symbolisk ikon för avslutningsåtgärd
* Rätta nedtryckning av menyknappar på verktygsraden korrekt
* Använd delsektion istället för sektion
* [controls/BasicListItem]: Lägg till egenskapen reserveSpaceForSubtitle
* gör implicit höjd för navigationsknappar explicit
* Rätta vertikal justering av BasicListItem
* [basiclistitem] Säkerställ att ikoner är kvadratiska
* [controls/ListItem]: Ta bort indenteringsavskiljare för inledande objekt
* Lägg till höger avskiljare för listobjekt igen när det finns ett inledande objekt
* Anropa inte reverseTwinsChanged när FormLayout förstörs (fel 428461)
* [org.kde.desktop/Units] Gör så att tidslängder motsvarar controls/Units
* [Units] Reducera veryLongDuration till 400 ms
* [Units] Reducera shortDuration och longDuration med 50 ms
* Betrakta inte syntetiserade mushändelser som mus (fel 431542)
* använd implicitHeight aggressivare istället för preferredHeight
* Använd atlasstruktur för ikoner
* controls/AbstractApplicationHeader: centrera underliggande objekt vertikalt
* [actiontextfield] Rätta marginaler och storlek för åtgärder på plats
* uppdatera huvudstorlek korrekt (fel 429235)
* [controls/OverlaySheet]: Respektera layoutens maximala bredd (fel 431089)
* [controls/PageRouter]: Exponera inställda parametrar när currentRoutes dumpas
* [controls/PageRouter]: Exponera parametrar på toppnivå i egenskapsavbildningen 'params'
* flytta exempel relaterade till pagerouter till en delkatalog
* Möjligt att dra fönstret med icke-interaktiva områden
* AbstractApplicationWindow: Använd breda fönster på skrivbordssystem för alla stilar
* Dölj inte avskiljare för listobjekt när musen hålls över och bakgrunden är genomskinlig
* [controls/ListItemDragHandle] Rätta felaktigt arrangemang för fallet utan rullning (fel 431214)
* [controls/applicationWindow]: Ta hänsyn till lådbredder när wideScreen beräknas

### KNewStuff

* Omstrukturera KNSQuick sidhuvud och sidfot för Kirigami-likhet
* Lägg till nummerbeteckning för betygskomponenten för enklare läsbarhet
* Reducera minimal storlek för QML GHNS dialogfönstret
* Matcha ljusare utseende när muspekaren hålls över för inställningsmodulernas rutnätsdelegater
* Säkerställ att minimal bredd för QML-dialogrutan är skärmbredden eller mindre
* Rätta BigPreview delegatens layout
* Validera om cacheposter innan dialogruta visas
* Lägg till stöd för kns:/ webbadresser i dialogverktyget knewstuff (fel 430812)
* filecopyworker: Öppna filer innan läsning eller skrivning
* Nollställ post till uppdateringsbar när ingen nyttolast identifieras för uppdatering (fel 430812)
* Rätta enstaka krasch på grund av att felaktigt behålla en pekare
* Avråd från klassen DownloadManager
* Avråd från egenskapen AcceptHtmlDownloads
* Avråd från egenskaperna ChecksumPolicy och SignaturePolicy
* Avråd från egenskapen Scope
* Avråd från egenskapen CustomName

### KNotification

* Skicka NewMenu när en ny sammanhangsberoende meny används (fel 383202)
* Avråd från KPassivePopup
* Säkerställ att alla gränssnitt refererar till underrättelsen innan arbete utförs
* Gör så att exempelprogrammet för underrättelser byggs och fungerar på Android
* Flytta hantering av underrättelse-id till klassen KNotification
* Rätta borttagning av väntande underrättelser från kö (fel 423757)

### Ramverket KPackage

* Dokumentera ägarskap av PackageStructure när PackageLoader används

### KPty

* Rätta generering av fullständig sökväg till kgrantpty i koden för ! HAVE_OPENPTY

### KQuickCharts

* Lägg till metoden "first" i ChartDataSource och använd den i beteckningar (fel 432426)

### Kör program

* Kontrollera vald åtgärd i fallet med informationsbaserad matchning
* Rätta tom resultatsträng för aktuell aktivitet
* Avråd från överladdning för QueryMatch-identifierare
* [DBus körprogram] Testa RemoteImage

### KService

* Avråd från KPluginInfo::dependencies()
* CMake: Ange beroenden för add_custom_command()
* Avråd explicit från överladdning av KToolInvocation::invokeTerminal
* Lägg till metod för att hämta KServicePtr för standardterminalprogrammet
* KService: lägg till metod för att ställa in workingDirectory

### KTextEditor

* [Vi-läge] Byt inte vy när skiftläge ändras (kommandot ~) (fel 432056)
* öka maximal indenteringsbredd till 200 (fel 432283)
* säkerställ att vi uppdaterar intervallavbildningen t.ex. vid invalidering av nu tomma intervall
* Visa bara bokmärkesteckenfel i vi-läge (fel 424172)
* [vi-läge] Rätta rörelse så att den matchar objektet med plus-minus-ett fel
* Behåll ersättningstexten under förutsättning att den avancerade sökraden inte är stängd (fel 338111)
* KateBookMarks: modernisera kodbasen
* Ignorera inte alfakanalen när det finns en markör
* Rätta att alfakanalen ignoreras vid läsning från inställningsgränssnittet
* Förhindra att förhandsgranskning av parentesmatchning från överförlängning utanför vyn
* Förhindra förhandsgranskning av parentesmatchning från att ibland dröja sig kvar efter att ha bytt till en annan flik
* Gör förhandsgranskning av parentesmatchning kompaktare
* Visa inte förhandsgranskning av parentesmatchning om den täcker markören
* Maximera bredd av förhandsgranskning av parentesmatchning
* Dölj förhandsgranskning av parentesmatchning vid rullning
* undvik duplicerade färgläggningsintervall som förstör ARGB-återgivning
* exponera global KSyntaxHighlighting::Repository skrivskyddat
* Rätta indenteringsfel när raden innehåller "for" eller "else"
* Rätta ett indenteringsfel
* ta bort specialfallet med tagLine, den orsakade slumpmässiga uppdateringsfel i t.ex.
* rätta återgivning av ordbrytningsmarkörer + markering
* Rätta indentering när returtangenten trycks och funktionsparametern har ett kommatecken i slutet
* rita det lilla gapet i markeringsfärg, om föregående rad slutar med en markering
* förenkla kod + rätta kommentarer
* återställ klipp ut fel, för mycket kod togs bort för extra återgivningar
* undvik att rita hela markerade rader, mer i still med andra editorer
* anpassa indentering till ändrade hl-filer
* [Vi-läge] Konvertera Command till QRegularExpression
* [Vi-läge] Konvertera findPrevWordEnd och findSurroundingBrackets till QRegularExpression
* [Vi-läge] Konvertera QRegExp::lastIndexIn till QRegularExpression och QString::lastIndexOf
* [Vi-läge] Konvertera ModeBase::addToNumberUnderCursor till QRegularExpression
* [Vi-läge] Konvertera enkla användningar av QRegExp::indexIn till QRegularExpression och QString::indexOf
* Använd rgba(r,g,b,aF) för att hantera alfakanalen vid HTML-export
* Respektera alfafärger vid export av HTML
* Introducera en hjälpmetod för att hämta färgnamnet korrekt
* Stöd för alfafärger: anpassa lager
* undvik att radändringsmarkörer förstör markering av aktuell rad
* rita markering av aktuell rad över ikonkant också
* Aktivera alfakanal för editorfärger
* Rätta att färgläggning av aktuell linje har ett litet gap i början för dynamiskt radbrutna rader
* Utför inte beräkningar med nålar, jämför bara värden direkt
* Rätta att färgläggning av aktuell linje har ett litet gap i början
* [Vimode] Hoppa inte över vikta intervall när rörelsen hamnar inne i dem
* Använd range-for snurra över m_matchingItems istället för att använda QHash::keys()
* Konvertera normalvimode till QRegularExpression
* exportera rätt beroenden riktigt
* exponera temat för KSyntaxHighlighting
* lägg till också till configChanged i KTextEditor::Editor, för globala inställningsändringar
* ordna om inställningsgrejorna lite grand
* rätta nyckelhantering
* lägg till dokument/visningsparametrar till nya signaler, rätta gamla anslutningar
* Tillåt att komma åt och ändra det aktuella temat via inställningsgränssnittet
* Ta bort const kvalificering när LineRanges skickas omkring
* Använd .toString() eftersom QStringView saknar .toInt() i äldre versioner av Qt
* Deklarera toLineRange() constexpr inline där det är möjligt
* Återanvänd versionen av QStringView från QStringRev, ta bort const kvalificering
* Flytta kommentaren TODO KF6 från doxygen-kommentar
* Cursor, Range: Add fromString(QStringView) överlagring
* Tillåt att "Justeringsindentering för dynamisk radbrytning" att inaktiveras (fel 430987)
* LineRange::toString(): Undvik '-' tecken, eftersom det är förvirrande för negativa tal
* Använd mekanismen KTextEditor::LineRange in notifyAboutRangeChange()
* Konvertera tagLines(), checkValidity() och fixLookup() till LineRanges
* Lägg till KTextEditor::LineRange
* Flytta KateTextBuffer::rangesForLine() implementering till KateTextBlock och undvik onödiga konstruktioner med behållare
* [Vimode] Rätta sökning inne i vikta intervall (fel 376934)
* [Vimode] Rätta uppspelning av makrokomplettering (fel 334032)

### KTextWidgets

* Låt fler privata klasser ärva från de i överliggande klasser

### KUnitConversion

* Definiera variabel innan den används

### KWidgetsAddons

* Använd AUTORCC
* Låt fler privata klasser ärva från de i överliggande klasser
* Inkludera QStringList explicit

### KWindowSystem

* Lägg till bekvämlighetshjälp för bråkdels ogenomskinlighet
* Rätta verkligen inkluderingar
* Rätta inkluderingar
* xcb: Arbeta med den aktiva skärmen som rapporterad av QX11Info::appScreen()

### KXMLGUI

* Rätta inkluderingar
* Lägg till signalen KXMLGUIFactory::shortcutsSaved
* Använd korrekt webbadress för engagera dig i KDE (fel 430796)

### Plasma ramverk

* [plasmoidheading] Använd avsedd färg för sidfot
* [plasmacomponents3/spinbox] Rätta markerad textfärg
* widgets>lineedit.svg: rätta sorgliga feljusteringar av bildpunkter (fel 432422)
* Rätta inkonsekvent vänster- och högervaddering i PlasmoidHeading
* Uppdatera färger för Breeze mörk och Breeze ljus
* Ångra "[SpinBox] Rätta logikfel i rullningsriktning"
* [calendar] rätta saknade importnamn
* [ExpandableListItem] Gör så expanderade vyer av åtgärdslistor respekterar teckensnitt
* DaysCalendar: konvertera till PC3/QQC2 där möjligt
* Ta bort animering vid håll över för platta knappar, kalendrar, listobjekt, knappar
* Dumpa fel på Plasmoider i terminalen
* Lös cykliskt beroende på Units.qml
* [PlasmaComponents MenuItem] Skapa tom åtgärd när en åtgärd förstörs
* gör inte bildpunktsavbildningar större än vad som behövs
* Lägg till projektfiler från jetBrains integrerade utvecklingsmiljö som ignorerade
* Rätta anslutningsvarningar
* Lägg till RESET i egenskapen globalShortcut (fel 431006)

### Syfte

* [nextcloud] Omkonstruerat användargränssnitt för inställning
* Utvärdera ursprunglig inställning
* Beskär listvy i inställning av KDE anslut och Blåtand
* Ta bort onödiga bilagda egenskaper till layout
* [plugins/nextcloud] Använd Nextcloud-ikon
* [cmake] Flytta find_package till toppnivåns CMakeLists.txt

### QQC2StyleBridge

* [combobox] Rätta rullningshastighet för tryckplatta (fel 400258)
* qw kan vara null
* Stöd QQuickWidget (fel 428737)
* tillåt att fönster dras från tomma områden

### Solid

* CMake: använd configure_file() för att säkerställa noop inkrementella byggen
* [Fstab] Ignorera docker överlagringsmonteringar (fel 422385)

### Sonnet

* Gör inte flera uppslagningar när en är tillräckligt

### Syntaxfärgläggning

* lägg till saknad ökning av version för context.xml
* Stöd officiella filändelser, se https://mailman.ntg.nl/pipermail/ntg-context/2020/096906.html
* Uppdatera referensresultat
* mindre iögonenfallande operatorfärg för Breeze-teman
* rätta syntax för ny hugo
* Rätta syntaxfel
* Förbättra läsbarhet för solariserat mörkt tema
* ta bort onödiga infångningar med en dynamisk regel
* sammanfoga färgläggning av operatorer => uppdatera referenser
* sammanfoga färgläggning av operatorer
* lägg till överlagring av const för vissa åtkomstoperatorer
* Bash, Zsh: rätta cmd;; i ett fall (fel 430668)
* Atom ljus, Breeze mörk/ljus: ny färg för operator ; Breeze ljus: ny färg för kontrollflöde
* email.xml: Detektera nästlade kommentarer och undantagna tecken (fel 425345)
* Uppdatera atom ljus att använda alfafärger
* Ändra om vissa symbol- och operatorstilar till dsOperator
* Bash: rätta } i ${!xy*} och fler parameterexpansionsoperatorer (# i ${#xy} ; !,*,@,[*],[@] i ${!xy*}) (fel 430668)
* Uppdatera temaversioner
* Uppdatera färgläggning av kconfig till Linux 5.9
* Använd rgba (r, g, b, aF) för att hantera alfakanalen för Qt-bläddrare korrekt
* Använd inte rgba inne i themeForPalette
* Säkerställ att rgba respekteras i html och format
* Rättningar av atom-ett-mörk
* Några ytterligare uppdateringar av Atom Ett Mörk
* Kontrollera inte rgba vid kontroll av bästa träff
* Tillåt användning av alfakanal i temafärger
* Uppdatera monokai-färger och rätta felaktig blå
* C++: rätta us suffix
* Bash: rätta #5: $ i slutet på en sträng med dubbla citationstecken
* VHDL: rätta funktion, procedur, typ intervall/enheter och andra förbättringar
* Uppdatera temat Breeze mörk
* Raku: nr. 7: Rätta symboler som börjar med Q
* snabbfix saknar kontrast för utökning
* Lägg till färgschemat Oblivion från GtkSourceView/Pluma/gEdit

### ThreadWeaver

* Rätta map-iteratorer vid bygge med Qt6
* Initiera inte mutexes explicit som NonRecursive

### Säkerhetsinformation

Den utgivna koden har signerats med GPG genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org> Fingeravtryck för primär nyckel: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
