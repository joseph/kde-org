---
aliases:
- ../../kde-frameworks-5.54.0
date: 2019-01-12
layout: framework
libCount: 70
---
### Attica

- Underrätta om en förvald leverantör misslyckades laddas ner

### Baloo

- Flytta typesForMimeType hjälpfunktion från BasicIndexingJob till anonym namnrymd
- Lägg till "image/svg" som Type::Image i BasicIndexingJob
- Använd kompakt Json-formatering för att lagra dokumentmetadata

### Breeze-ikoner

- Ändra preferences-system-network till symbolisk länk
- Använd en Kolf-ikon med snygg bakgrund och skuggor
- Kopiera några ändrade ikoner från Breeze till Breeze mörk
- Lägg till YaST och nya preferensikoner
- Lägg till en riktig python-bytecode ikon, använd konsekventa färger i Python-ikoner (fel 381051)
- Ta bort Python bytecode symboliska länkar som förberedelse för att de blir egna ikoner.
- Gör Python Mime-typikonen till bas, och Python bytecode-ikonen en länk till den.
- Lägg till enhetsikoner för RJ11- och RJ45-portar.
- Lägg till saknad katalogavskiljare (fel 401836)
- Använd korrekt ikon för Python 3-skript (fel 402367)
- Ändra färgikoner för nätverks och webb till konsekvent stil
- Lägg till nytt namn för sqlite-filer, så att ikonerna faktiskt visas (fel 402095)
- Lägg till drive-* ikoner för YaST-partitioner
- Lägg till view-private ikon (fel 401646)
- Lägg till åtgärdsikoner för ficklampor
- Förbättra symboler för statusikonerna av och tyst (fel 398975)

### Extra CMake-moduler

- Lägg till hitta modul för Googles libphonenumber

### KActivities

- Rätta version i filen pkgconfig

### KDE Doxygen-verktyg

- Rätta återgivning a Markdown i doxygen

### KConfig

- Undanta byte som är större än eller lika med 127 i inställningsfiler

### KCoreAddons

- cmake-makron: Konvertera från användning av ECM variabel in kcoreaddons_add_plugin som avråds från (fel 401888)
- Gör enheter och prefix i formatValue översättningsbara

### KDeclarative

- Visa inte avskiljare på mobilenheter
- Använd root.contentItem istället för bara contentItem
- Lägg till saknat programmeringsgränssnitt för flernivåers inställningsmoduler för att styra kolumnerna

### KFileMetaData

- Låt skrivtest misslyckas om Mime-typen inte stöds av extraheringen
- Rätta extrahering av ape disknummer
- Implementera omslagsextrahering för asf-filer
- Utöka lista över Mime-typer som stöds för extrahering av inbäddade bilder
- Omstrukturera extrahering av inbäddade bilder för bättre utökningsbarhet
- Lägg till saknad Mime-type för wav
- Extrahera fler taggar från EXIF-metadata (fel 341162)
- Rätta extrahering av GPS-elevation från EXIF-data

### KGlobalAccel

- Rätta byggning av KGlobalAccel med förutgåva av Qt 5.13

### KHolidays

- README.md: Lägg till grundläggande instruktioner för att testa helgfiler
- Diverse kalendrar: Rätta syntaxfel

### KIconThemes

- ksvg2icns : Använd programmeringsgränssnittet Qt 5.9+ QTemporaryDir

### KIdleTime

- [xscreensaverpoller] Flush efter återställning av skärmsläckare

### KInit

- Använd mjuk gräns för antal öppna grepp. Det rättar mycket långsam start av Plasma med senaste systemd.

### KIO

- Återställ "Dölj förhandsgranskning av fil när ikonen är för liten"
- Visa fel istället för att misslyckas tyst vid begäran om att skapa katalog som redan finns (fel 400423)
- Ändra sökväg för varje objekt i underkatalogerna för namnbyte av katalog (fel 401552)
- Utöka filtrering med reguljära uttryck av getExtensionFromPatternList
- [KRun] Vid begäran om att öppna länk i en extern webbläsare, återgå till mimeapps.list om ingenting är inställt i kdeglobals (fel 100016)
- Gör funktionen att öppna webbadress i en flik lättare att upptäcka (fel 402073)
- [kfilewidget] Returnera redigerbar webbadressnavigering till länkstigsläge om den har fokus och allting markeras och när Ctrl+L skrivs in
- [KFileItem] Rätta kontroll av isLocal i checkDesktopFile (fel 401947)
- SlaveInterface: Stoppa speed_timer efter ett jobb har dödats
- Varna användare innan kopierings- eller förflyttningsjobb om filstorleken överskrider den maximalt möjliga filstorleken i FAT32-filsystem (4 GiB) (fel 198772)
- Undvik att konstant öka Qt-händelsekö i I/O-slavar
- Stöd för TLS 1.3 (del av Qt 5.12)
- [KUrlNavigator] Rätta firstChildUrl vid återgång från arkiv

### Kirigami

- Säkerställ att vi inte överskrider QIcon::themeName när vi inte ska det
- Introducera ett bilagt objekt för DelegateRecycler
- Rätta rutnätsmarginaler med hänsyn till rullningslister
- Låt bakgrund för AbstractCard reagera när AbstractCard markeras
- Förenkla kod i MnemonicAttached
- SwipeListItem: Visa alltid ikoner om !supportsMouseEvents
- Ta hänsyn till om needToUpdateWidth är enligt widthFromItem, inte höjd
- Ta hänsyn till rullningslist för marginalen i ScrollablePage (fel 401972)
- Försök inte positionera om ScrollView när vi får en felaktig höjd (fel 401960)

### KNewStuff

- Ändra förvald sorteringsordning i nerladdningsdialogrutan till "Flest nerladdningar" (fel 399163)
- Underrätta om att leverantören inte laddas

### KNotification

- [Android] Misslyckas på ett snyggare sätt vid byggning med programmeringsgränssnitt  &lt; 23
- Lägg till gränssnitt för underrättelse i Android
- Bygg utan Phonon och D-Bus på Android

### KService

- applications.menu: Ta bort oanvänd kategori X-KDE-Edu-Teaching
- applications.menu: Ta bort &lt;KDELegacyDirs/&gt;

### KTextEditor

- Rätta skriptanvändning för Qt 5.12
- Rätta emmet-skript genom att använda hexadecimala istället för oktala tal i strängar (fel 386151)
- Rätta felaktig Emmet (fel 386151)
- ViewConfig: Lägg till alternativet 'Dynamisk radbrytning vid statisk markör'
- Rätta vikområdets slut, lägg till slutsymbol i intervallet
- Undvik ful övermålning med alfa
- Markera inte om tillagda eller ignorerade ord i ordlistan som felstavade (fel 387729)
- KTextEditor: Lägg till åtgärd för statisk radbrytning (fel 141946)
- Dölj inte alternativet 'Rensa intervall i ordlistan'
- Fråga inte efter bekräftelse vid återinläsning (fel 401376)
- Klassen Message: Använd medlemsinitiering i klassen
- Exponera KTextEditor::ViewPrivate:setInputMode(InputMode) för KTextEditor::View
- Förbättra prestanda av små redigeringsåtgärder, t.ex. rätta stora åtgärder för att ersätta allt (fel 333517)
- Anropa bara updateView() i visibleRange() när endPos() är ogiltig

### Kwayland

- Lägg till förtydligande om användning av både KDE:s ServerDecoration och XdgDecoration
- Stöd för XDG-dekorering
- Rätta installation av XDGForeign klientdeklarationsfiler
- [server] Berörings- och dragstöd
- [server] Tillåt flera beröringsgränssnitt per klient

### KWidgetsAddons

- [KMessageBox] Rätta minimal dialogstorlek när detaljinformation begärs (fel 401466)

### NetworkManagerQt

- Tillägg av inställningar för DCB, macsrc, match, tc, ovs-patch och ovs-port

### Plasma ramverk

- [Calendar] Exponera firstDayOfWeek i MonthView (fel 390330)
- Lägg till preferences-system-bluetooth-battery i preferences.svgz

### Syfte

- Lägg till typ av insticksprogram för att dela webbadresser

### QQC2StyleBridge

- Rätta bredd på menyalternativ när delegaten överskrids (fel 401792)
- Rotera upptagetindikering medurs
- Tvinga kryssrutor och alternativknappar att vara fyrkantiga

### Solid

- [UDisks2] Använd MediaRemovable för att avgöra om media kan matas ut
- Stöd Blåtandsbatterier

### Sonnet

- Lägg till metod i BackgroundChecker för att lägga till ord i session
- DictionaryComboBox: Behåll ordlistor föredragna av användaren överst (fel 302689)

### Syntaxfärgläggning

- Uppdatera stöd för PHP-syntax
- WML: Rätta inbäddad Lua-kod och använd nya standardstilar
- Färglägg CUDA .cu och .cuh filer som C++
- Typescript och TS/JS React: Förbättra detektering av typer, rätta flyttal och andra förbättringar/rättningar
- Haskell: Färglägg tomma kommentarer efter 'import'
- WML: Rätta oändlig upprepning i kontextbyte och färglägg bara etiketter med giltiga namn (fel 402720)
- BrightScript: Lägg till provisorisk lösning för färgläggning av QtCreator 'endsub', lägg till vikning av funktion och sub
- Stöd fler varianter av C numeriska litteraler (fel 402002)

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
