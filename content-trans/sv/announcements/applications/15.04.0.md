---
aliases:
- ../announce-applications-15.04.0
changelog: true
date: '2015-04-15'
description: KDE levererar Program 15.04.
layout: application
title: KDE levererar KDE-program 15.04.0
version: 15.04.0
---
15:e april, 2014. Idag ger KDE ut KDE-program 15.04. I den här utgåvan har totalt 72 program konverterats till <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Ramverk 5</a>. Gruppen strävar efter att ge skrivbordet och dessa program den bästa möjliga kvalitet. Alltså räknar vi med att du skickar oss dina kommentarer.

I den här utgåvan finns flera nytillskott till listan över KDE Ramverk 5-baserade program, inklusive <a href='https://www.kde.org/applications/education/khangman/'>Hänga gubben</a>, <a href='https://www.kde.org/applications/education/rocs/'>Rocs</a>, <a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, <a href='https://www.kde.org/applications/development/kompare'>Kompare</a>, <a href='https://kdenlive.org/'>Kdenlive</a>, <a href='https://userbase.kde.org/Telepathy'>KDE Telepathy</a> och <a href='https://games.kde.org/'>några KDE-spel</a>.

Kdenlive är en av de bästa programvaror för icke-linjära videoredigering tillgängliga. Det har nyligen avslutat sin <a href='https://community.kde.org/Incubator'>inkubationsprocess</a> för att bli ett officiellt KDE-projekt och har konverterats till KDE Ramverk 5. Gruppen bakom detta mästerverk har bestämt att Kdenlive ska ges ut tillsammans med KDE-program. Några nya funktioner är att spara nya projekt automatisk och fast klippstabilisering.

KDE Telepathy är verktyget för direktmeddelanden. Det konverterades till KDE Ramverk 5 och Qt5 och är en ny medlem i utgåvorna av KDE-program. Det är i huvudsak komplett, utom att gränssnittet för ljud- och videosamtal fortfarande saknas.

Om möjligt använder KDE befintlig teknologi, vilket har skett med det nya Kaccouts, som också används i SailfishOS and Canonicals Unity. I KDE-program används det för närvarande bara av KDE Telepathy, men i framtiden kommer det att få bredare användning av program såsom Kontact och Akonadi.

<a href='https://edu.kde.org/'>KDE:s utbildningsmodul</a>, har Cantor fått några nya funktioner i Python-stödet: ett nytt Python 3-bakgrundsprogram och nya kategorier i Hämta heta nyheter. Rocs har vänts upp-och-ner: kärnan för grafteori har skrivits om, separationen av datastrukturer har tagits bort och ett mer generellt grafdokument som centralt grafobjekt  har introducerats, samt en större inspektion har gjorts av programmeringsgränssnittet för grafalgoritmer som nu endast tillhandahåller ett förenat programmeringsgränssnitt. Hänga gubben har konverterats till QtQuick, och fått ett nytt lager färg på köpet. Och Kanagram har fått ett nytt tvåspelarläge och bokstäverna är nu klickbara knappar och kan skrivas in som tidigare.

Förutom de vanliga felrättningarna har <a href='https://www.kde.org/applications/development/umbrello/'>Umbrello</a> denna gång fått några förbättringar av användbarhet och stabilitet. Dessutom kan sökfunktionen nu begränsas enligt kategori: klass, gränssnitt, paket, operationer eller egenskaper.
