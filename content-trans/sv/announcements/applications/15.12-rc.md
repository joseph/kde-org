---
aliases:
- ../announce-applications-15.12-rc
date: 2015-12-03
description: KDE levererar Program 15.12 leveranskandidat.
layout: application
release: applications-15.11.90
title: KDE levererar leveranskandidat av KDE-program 15.12
---
3:e december, 2015. Idag ger KDE ut leveranskandidaten av de nya versionerna av KDE-program. Med beroenden och funktioner frysta, fokuserar KDE-grupperna nu på att rätta fel och ytterligare finputsning.

Med de olika programmen baserade på KDE Ramverk 5, behöver KDE-program utgåva 15.12 omfattande utprovning för att behålla och förbättra kvaliteten och användarupplevelsen. Verkliga användare är väsentliga för att upprätthålla hög kvalitet i KDE, eftersom utvecklare helt enkelt inte kan prova varje möjlig konfiguration. Vi räknar med dig för att hjälpa oss hitta fel tidigt, så att de kan krossas innan den slutliga utgåvan. Överväg gärna att gå med i gruppen genom att installera utgåvan och <a href='https://bugs.kde.org/'>rapportera eventuella fel</a>.
