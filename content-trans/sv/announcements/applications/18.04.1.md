---
aliases:
- ../announce-applications-18.04.1
changelog: true
date: 2018-05-10
description: KDE levererar KDE-program 18.04.1
layout: application
title: KDE levererar KDE-program 18.04.1
version: 18.04.1
---
10:e maj, 2018. Idag ger KDE ut den första stabilitetsuppdateringen av <a href='../18.04.0'>KDE-program 18.04</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Omkring 20 registrerade felrättningar omfattar förbättringar av bland annat Kontact, Cantor, Dolphin, Gwenview, JuK, Okular och Umbrello.

Förbättringar omfattar:

- Duplicerade poster i Dolphins platspanel orsakar inte längre krascher.
- Ett gammalt fel med återinläsning av SVG-filer i Gwenview har rättats.
- Umbrellos C++ import förstår nu nyckelordet 'explicit'
