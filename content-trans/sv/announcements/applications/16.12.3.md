---
aliases:
- ../announce-applications-16.12.3
changelog: true
date: 2017-03-09
description: KDE levererar KDE-program 16.12.3
layout: application
title: KDE levererar KDE-program 16.12.3
version: 16.12.3
---
9:e mars, 2017. Idag ger KDE ut den tredje stabilitetsuppdateringen av <a href='../16.12.0'>KDE-program 16.12</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 20 registrerade felrättningar omfattar förbättringar av bland annat kdepim, ark, filelight, gwenview, kate, kdenlive och okular.

Utgåvan inkluderar också versioner för långtidsunderhåll av KDE:s utvecklingsplattform 4.14.30.
