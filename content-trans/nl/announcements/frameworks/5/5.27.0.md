---
aliases:
- ../../kde-frameworks-5.27.0
date: 2016-10-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Nieuwe MIME-type pictogrammen.

{{<figure src="/announcements/frameworks/5/5.27.0/kf-5.27-mimetypes-icons.png" >}}

### Baloo

- Juiste configuratie-item in autostart-conditie gebruiken
- Gesorteerd invoegen repareren (aka flat_map like insert) (bug 367991)
- Ontbrekend sluiten van omgeving toevoegen, zoals aangegeven door Loïc Yhuel (bug 353783)
- Transactie niet aangemaakt =&gt; probeer deze niet af te breken
- ontbrekende m_env = nullptr toekenning repareren
- Maak bijv. Baloo::Query veilig in threads
- Op 64-bit systemen staat baloo nu indexgeheugen toe &gt; 5 GB (bug 364475)
- ctime/mtime == 0 toestaan (bug 355238)
- Behandel corruptie van indexdatabase voor baloo_file, probeer de database opnieuw te maken of breek af als dat mislukt

### BluezQt

- Crash repareren bij poging om een apparaat toe te voegen aan een onbekende adapter (bug 364416)

### Breeze pictogrammen

- Nieuwe mimetype pictogrammen
- Enkele pictogrammen van kstars bijwerken (bug 364981)
- Foute stijl acties/24/formaat-rand-set (bug 368980)
- Wayland app-pictogram toevoegen
- xorg-app pictogram toevoegen (bug 368813)
- Draai distribute-randomize en view-calendar terug + pas de transformatie reparatie opnieuw toe (bug 367082)
- wijzig pictogram van map in map Documents van een éen bestand naar de meerdere bestanden omdat in een map meer dan één bestand kan zitten (bug 368224)

### Extra CMake-modules

- Ga na dat we de appstreamtest niet twee keer toepassen

### KActivities

- Activiteiten worden in de cache alfabetisch op naam gesorteerd (bug 362774)

### KDE Doxygen hulpmiddelen

- Veel wijzigingen aan de algehele indeling van de gegenereerde API docs
- Tagspad corrigeren, afhankelijk van of de bibliotheek onderdeel is van een groep of niet
- Zoeken: href van bibliotheken die geen onderdeel zijn van een groep

### KArchive

- Geheugenlek in KCompressionDevice van KTar repareren
- KArchive: geheugenlek repareren wanneer een item met dezelfde naam al bestaat
- Geheugenlek repareren in KZip bij behandelen van lege mappen
- K7Zip: geheugenlekken bij fout repareren
- Geheugenlek repareren bij detectie door ASAN wanneer open() mislukt op het onderliggende apparaat
- Slechte detectie van hoofd-/kleine letter naar KFilterDev, gedetecteerd door ASAN

### KCodecs

- Ontbrekende exportmacro's toevoegen op decodering and codering van klassen

### KConfig

- Geheugenlek in SignalsTestNoSingletonDpointer, gevonden door ASAN

### KCoreAddons

- QPair&lt;QString,QString&gt; registreren als metatype in KJobTrackerInterface
- Converteer geen een url als een url wanneer deze een aanhalingsteken bevat
- Reparatie bij compileren van Windows
- Zeer oude bug repareren wanneer we witruimte verwijderen in een url als "foo &lt;&lt;url&gt; &lt;url&gt;&gt;"

### KCrash

- Optie KCRASH_CORE_PATTERN_RAISE van cmake om door te sturen naar kernel
- Standaard logniveau wijzigen van waarschuwing naar info

### Ondersteuning van KDELibs 4

- Opschonen. Geen includes installeren die verwijzen naar niet-bestaande includes en verwijder die bestanden ook
- Meer correcte en met c++11 beschikbare std::remove_pointer gebruiken

### KDocTools

- 'checkXML5 voert gegenereerde html naar stdout voor geldige docbooks' repareren (bug 369415)
- Bug over niet in staat om native hulpmiddelen in pakket uit te voeren met kruislings gecompileerde kdoctools repareren
- Doelen opzetten voor kruislings compileren met uitvoeren van kdoctools uit andere pakketten
- Ondersteuning voor kruislings compileren voor docbookl10nhelper toevoegen
- Ondersteuning voor kruislings compileren voor meinproc5 toevoegen
- checkxml5 converteren in een qt uitvoerbaar programma voor ondersteuning van een kruislings platform

### KFileMetaData

- Epub extractor verbeteren, minder segfaults (bug 361727)
- Odf-indexer minder foutgevoelig maken, controleer of de bestanden er zijn (en echt bestanden zijn) (meta.xml + content.xml)

### KIO

- KIO slaves repareren met alleen tls1.0
- ABI break in kio repareren
- KFileItemActions: addPluginActionsTo(QMenu *) toevoegen
- Kopieerknopen alleen tonen nadat de controlesom is berekend
- Ontbrekende terugkoppeling toevoegen bij berekenen van een controlesom (bug 368520)
- KFileItem::overlays repareren die lege waarde van een tekenreeks teruggeeft
- Starten van .desktop bestanden met konsole repareren
- Classificeer nfs4 aankoppelen als mogelijk traag, zoals nfs/cifs/..
- KNewFileMenu: sneltoets van actie nieuwe map tonen (bug 366075)

### KItemViews

- In modus lijstweergave de standaard implementatie van moveCursor gebruiken

### KNewStuff

- Controles met KAuthorized toevoegen om uitschakelen van ghns in kdeglobals toe te staan (bug 368240)

### Pakket Framework

- Geen appstream-bestanden genereren voor componenten die niet in rdn zijn
- Zorg dat kpackage_install_package werkt met  KDE_INSTALL_DIRS_NO_DEPRECATED
- Ongebruikte var KPACKAGE_DATA_INSTALL_DIR verwijderen

### KParts

- URL's met een slash aan het eind waarvan altijd wordt aangenomen mappen te zijn repareren

### KPeople

- Bouwen van ASAN repareren (duplicates.cpp gebruikt KPeople::AbstractContact die in KF5PeopleBackend zit)

### KPty

- ECM-pad gebruiken om utempter binair programma te vinden, betrouwbaarder dan eenvoudige cmake prefix
- Het utempter helper uitvoerbare programma handmatig aanroepen (bug 364779)

### KTextEditor

- XML-bestanden: verwijder hard gecodeerde kleur voor waarden
- XML: verwijder hard gecodeerde kleur voor waarden
- XML-schema-definitie: Maak van 'version' een xs:integer
- Accentueerdefinitiebestanden: maak van versie het volgende gehele getal
- multi-teken captures alleen ondersteunen in {xxx} om regressies te vermijden
- Reguliere expressies vervangen door captures ondersteunen &gt; 9, bijv. 111 (bug 365124)
- Weergeven van tekens die overlopen naar de volgende regel, bijv. onderstreping, worden niet langer afgesneden bij sommige lettertypen/lettergrootten repareren (bug 335079)
- Crash repareren: ga na dat de cursor op het scherm geldig is na afbreken van tekst naar de volgende regel (bug 367466)
- KateNormalInputMode moet invoermethoden van SearchBar opnieuw uitvoeren
- probeer weergeven van onderstrepen en soortgelijke zaken te verbeteren (bug 335079)
- Alleen knop "Verschil weergeven" tonen, als 'diff' is geïnstalleerd
- Niet-modale berichtenwidget gebruiken voor extern gewijzigde bestandsmeldingen (bug 353712)
- regressie repareren: testNormal werkte alleen omdat uitvoering van test direct ging
- de test op indentatie opsplitsen in aparte runs
- Actie "Topniveau nodes uitvouwen" opnieuw ondersteunen (bug 335590)
- Crash repareren bij meerdere keren tonen van berichten boven en onderaan
- eol-instelling in modusregel repareren (bug 365705)
- Accentueer .nix bestanden als bash, raden kan geen kwaad (bug 365006)

### KWallet Framework

- Controleer of kwallet is ingeschakeld in Wallet::isOpen(name) (bug 358260)
- Ontbrekende boost-header toevoegen
- Dubbel zoeken naar KF5DocTools verwijderen

### KWayland

- [server] toetsvrijgave niet verzenden voor niet ingedrukte toetsen en geen dubbel drukken op een toets (bug 366625)
- [server] bij vervangen van de klembordselectie moet de eerdere DataSource geannuleerd te worden (bug 368391)
- Ondersteuning voor gebeurtenissen oppervlak ingaan en verlaten toevoegen
- [client] alle aangemaakte Outputs volgen en statische ophaalmethode toevoegen

### KXmlRpcClient

- Categorieën converteren naar org.kde.pim.*

### NetworkManagerQt

- Het is nodig de status gedurende initialisatie in te stellen
- Alle blokkeringsaanroepen voor initialisatie vervangen door slechts één blokkeringsaanroep
- Standaard o.f.DBus.Properties interface gebruiken voor signaal PropertiesChanged voor NM 1.4.0+ (bug 367938)

### Oxygen-pictogrammen

- Ongeldige map uit index.theme verwijderen
- Dupe-test uit breeze-pictogrammen introduceren
- Alle gedupliceerde pictogrammen converteren naar symbolische koppelingen

### Plasma Framework

- Uitvoer van tijdvolger verbeteren
- [ToolButtonStyle] menupijl repareren
- i18n: behandel tekenreeksen in kdevtemplate bestanden
- i18n: tekenreeksen herzien in kdevtemplate bestanden
- removeMenuItem aan PlasmaComponents.ContextMenu toevoegen
- pictogram van ktorrent bijwerken (bug 369302)
- [WindowThumbnail] pixmap weggooien bij overeenkomst-gebeurtenis
- kdeglobals niet invoegen bij werken met een cache configuratie
- Plasma::knownLanguages repareren
- de grootte van het beeld wijzigen direct na instelling van de container
- Maken van een KPluginInfo uit een KPluginMetaData exemplaar vermijden
- actieve taken moeten een indicatie hebben
- taakbalkregels volgens RR 128802 marco geeft verscheep het
- [AppletQuickItem] uit loop breken bij een gevonden indeling

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
