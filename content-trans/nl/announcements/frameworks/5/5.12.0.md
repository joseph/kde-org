---
aliases:
- ../../kde-frameworks-5.12.0
date: 2015-07-10
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Extra CMake-modules

- Rapporteren van fouten van macro query_qmake verbeteren

### BluezQt

- Alle apparaten van de adapter verwijderen alvorens de adapter verwijderen (bug 349363)
- Koppelingen in README.md bijwerken

### KActivities

- De optie toevoegen om de gebruiker niet te volgen bij specifieke activiteiten (overeenkomstig de modus 'privé browsing' in een webbrowser)

### KArchive

- Executierechten behouden bij bestanden in copyTo()
- Uitleg bij ~KArchive bij verwijderen van dode code.

### KAuth

- Het mogelijk maken om kauth-policy-gen te gebruiken vanuit verschillende bronnen

### KBookmarks

- Geen bladwijzer toevoegen bij een leeg URL-adres en de tekst is leeg
- KBookmark URL coderen om compatibiliteit te repareren met KDE4 toepassingen

### KCodecs

- x-euc-tw prober verwijderen

### KConfig

- kconfig_compiler in libexec installeren
- Nieuwe codegeneratieoptie TranslationDomain=, voor gebruik met TranslationSystem=kde; normaal nodig in bibliotheken.
- Het mogelijk maken om kconfig_compiler te gebruiken vanuit verschillende bronnen

### KCoreAddons

- KDirWatch: maak alleen een verbinding naar FAM indien gevraagd
- Filteren van plug-ins en toepassingen op formfactor toestaan
- Het mogelijk maken om desktoptojson te gebruiken vanuit verschillende bronnen

### KDBusAddons

- Exitwaarden verklaren voor unieke exemplaren

### KDeclarative

- QQC-kloon van KColorButton toevoegen
- Een QmlObject voor elk kdeclarative exemplaar toekennen wanneer mogelijk
- Qt.quit() uit QML-code werkend gemaakt
- Branch 'mart/singleQmlEngineExperiment' samenvoegen
- sizeHint gebaseerd op implicitWidth/height implementeren
- Subklasse van QmlObject met statische engine

### Ondersteuning van KDELibs 4

- KMimeType::Ptr::isNull implementatie repareren.
- Ondersteuning voor KDateTime streaming naar kDebug/qDebug opnieuw inschakelen, voor meer SC
- Juiste vertalingscatalog voor kdebugdialog laden
- Sla de verouderde methoden voor documenteren niet over, zodat mensen de porting hints kunnen lezen

### KDESU

- CMakeLists.txt repareren om KDESU_USE_SUDO_DEFAULT door te geven aan de compilatie zodat het gebruikt wordt door suprocess.cpp

### KDocTools

- K5 docbook sjablonen bijwerken

### KGlobalAccel

- private runtime API wordt geïnstalleerd om toe te staan dat KWin een plug-in voor Wayland levert.
- Terugvallen voor componentFriendlyForAction namen oplossen

### KIconThemes

- Probeer het pictogram niet te tekenen als de grootte ongeldig is

### KItemModels

- Nieuw proxy-model: KRearrangeColumnsProxyModel. Het ondersteunt opnieuw ordenen en kolommen verbergen uit het bronmodel.

### KNotification

- Pixmap-typen in org.kde.StatusNotifierItem.xml repareren
- [ksni] Methode toevoegen om actie bij zijn naam op te halen (bug 349513)

### KPeople

- PersonsModel filtering faciliteiten implementeren

### KPlotting

- KPlotWidget: setAutoDeletePlotObjects toevoegen, geheugenlek in replacePlotObject repareren
- Ontbrekende tickmarks wanneer x0 &gt; 0 repareren.
- KPlotWidget: niet nodig om MinimumSize in te stellen of een nieuwe grootte te geven.

### KTextEditor

- debianchangelog.xml: Debian/Stretch toevoegen, Debian/Buster, Ubuntu-Wily
- Reparatie voor UTF-16 surrogaat paar backspace/delete gedrag.
- QScrollBar de WheelEvents laten afhandelen (bug 340936)
- Patch toepassen uit KWrite devel top update pure basic HL, "Alexander Clay" &lt;tuireann@EpicBasic.org&gt;

### KTextWidgets

- OK-knop in-/uitschakelen gerepareerd

### KWallet Framework

- Het kwallet-query opdrachtregel hulpmiddel geïmporteerd en verbeterd.
- Overschrijven van map-items ondersteunen.

### KXMLGUI

- "KDE Frameworks versie" niet tonen in de KDE dialog 'Info over'

### Plasma Framework

- Maak het donkere theme volledig donker, ook de complementaire groep
- Natuurlijke grootte cachen apart van schaalfactor
- ContainmentView: Niet crashen bij ongeldige corona metagegevens
- AppletQuickItem: KPluginInfo niet ingaan indien niet geldig
- Toevallig lege applet-config pagina's repareren (bug 349250)
- hidpi ondersteunen in de Calendar grid component verbeteren
- KService heeft geldige plug-in-info verifiëren alvorens het te gebruiken
- [calendar] Ga na dat het raster opnieuw wordt gemaakt bij wijziging van thema
- [calendar] Begin weken altijd te tellen vanaf maandag (bug 349044)
- [calendar] Het raster opnieuw maken wanneer instelling voor weeknummers tonen wijzigt
- Een doorzichtig thema wordt nu gebruikt wanneer alleen het verdoezeleffect beschikbaar is (bug 348154)
- Zet applets/versies op een witte lijst voor aparte engine
- Een nieuwe klasse ContainmentView introduceren

### Sonnet

- Sta gebruikt van geaccentueerde spellingcontrole toe in een QPainTextEdit

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
