---
aliases:
- ../../kde-frameworks-5.61.0
date: 2019-08-10
layout: framework
libCount: 70
---
### Baloo

- Link tegen KIOCore in plaats van KIOWidgets in kioslaves
- [IndexCleaner] niet bestaande items in configuratie negeren

### BluezQt

- Crash vanwege de q-pointer die nooit werd geïnitialiseerd repareren
- bluezqt_dbustypes.h niet invoegen uit geïnstalleerde headers

### Breeze pictogrammen

- Pictogram "gebruikers-anderen" toevoegen (bug 407782)
- Zorg dat "edit-none" een symbolische koppeling is naar "dialog-cancel"
- Overbodige en monochrome versies van applications-internet verwijderen
- view-pages-* pictogrammen toevoegen, zoals nodig in Okular voor selectie van pagina-indeling (bug 409082)
- Pijlen rechtsom gebruiken voor _refresh_ en update-* pictogrammen (bug 409914)

### Extra CMake-modules

- android: overschrijven van ANDROID_ARCH en ANDROID_ARCH_ABI als omgevingsvariabelen toestaan
- Gebruikers melden bij niet gebruiken van KDE_INSTALL_USE_QT_SYS_PATHS over prefix.sh
- Een zinniger CMAKE_INSTALL_PREFIX standaard leveren
- Standaard het bouwtype "Debug" maken bij compileren van een git-checkout

### KActivitiesStats

- Datumtekst toevoegen aan KActivities Stats om te filteren op gebeurtenisdatum van hulpbron

### KActivities

- Vorige-/volgende activiteitscode in kactivities-cli vereenvoudigen

### KDE Doxygen hulpmiddelen

- Controleren van mappen voor metainfo.yaml met niet-ascii tekens met Python 2.7 repareren
- Verkeerde padnamen (via repr()) loggen in plaats van geheel crashen
- lijst met gegevensbestanden on-the-fly genereren

### KArchive

- KTar::openArchive: Niet toekennen als bestand twee hoofdmappen heeft
- KZip::openArchive: Niet toekennen bij openen van gebroken bestanden

### KCMUtils

- aan wijzigingen in UI aanpassen in KPageView

### KConfig

- Beveiliging: ondersteuning voor $(...) in configuratie sleutels met marker [$e] verwijderen
- Definitie voor klasse gebruikt in header invoegen

### KCoreAddons

- Functie KFileUtils::suggestName toevoegen om een unieke bestandsnaam te suggereren

### KDeclarative

- Schuifweergave - de ouder niet vullen met de weergave (bug 407643)
- FallbackTapHandler introduceren
- KRun QML proxy: pad/URL verwarring repareren
- Gebeurtenissen in agenda: plug-ins toestaan details van gebeurtenissen te tonen

### KDED

- bureaubladbestand van kded5: geldig type gebruiken (Service) om waarschuwing ui kservice te onderdrukken

### Ondersteuning van KDELibs 4

- Designer-plug-in: consistent "KF5" in groepsnamen &amp; teksten gebruiken
- Gebruik van KPassivePopup niet adverteren

### KDesignerPlugin

- nieuwe KBusyIndicatorWidget uitstallen
- Generatie van designer-plug-in voor KF5WebKit verwijderen

### KDE WebKit

- Voorbeeld van ECMAddQtDesignerPlugin gebruiken in plaats van KF5DesignerPlugin
- Optie om Qt Designer plug-in te bouwen toevoegen (BUILD_DESIGNERPLUGIN, standaard AAN)

### KFileMetaData

- Houd mobipocket-extractor bijgewerkt, maar houd deze uitgeschakeld

### KHolidays

- Publieke vervanging voor vakantiedagen in Rusland toevoegen, voor 2019-2020
- Vakantiedagen in Rusland bijwerken

### KIconThemes

- Herstel "Controleren of groep &lt; LastGroup, omdat KIconEffect UserGroup in elk geval niet behandelt"

### KIO

- Keur suggestName af voor het betere daarvan in KCoreAddons
- Fout met niet kunnen invoeren van map repareren op enige FTP-servers met de Turkse taalcode (bug 409740)

### Kirigami

- Kirigami.AboutPage herschrijven
- Units.toolTipDelay consistent gebruiken in plaats van hard gecodeerde waarden
- De inhoud van de kaart de juiste grootte maken wanneer de kaartgrootte beperkt is
- rimpel verbergen wanneer we niet willen dat items aangeklikt kunnen worden
- zorg dat hendel de willekeurige hoogte van de lade volgt
- [SwipeListItem] Betrek zichtbaarheid van de schuifbalk en vormfactor voor hendel en inline-acties
- Schaling van eenheid voor pictogramgrootte verwijderen voor isMobile
- knop voor terug altijd tonen op lagen &gt; 1
- actie verbergen met submenu's voor meer menu
- standaard positie van ActionToolBar tot Header
- grote z om niet te verschijnen onder dialogen
- Dekking gebruiken om knoppen te verbergen die niet passen
- het scheidingsteken alleen toevoegen wanneer het de breedte vult
- volledig retrocompatible met showNavigationButtons als boolean
- meer granulariteit in globalToolBar.showNavigationButtons

### KItemModels

- David Faure is nu de onderhouder voor KItemModels
- KConcatenateRowsProxyModel: notitie toevoegen dat Qt 5.13 QConcatenateTablesProxyModel levert

### KPackage-framework

- Bied metadata.json aan bij vragen van de pakketmetagegevens
- PackageLoader: de juiste scope voor het KCompressionDevice gebruiken

### KPeople

- declarative: actielijst verversen bij wijziging van persoon
- declarative: niet crashen wanneer de API wordt misbruikt
- personsmodel: phoneNumber toevoegen

### KService

- X-KDE-Wayland-Interfaces bekend maken
- KService gebouwd op Android repareren
- KService: gebroken concept van globale sycoca-database verwijderen
- Zeer gevaarlijke code voor verwijderen verwijderen met kbuildsycoca5 --global
- Oneindige recursie en toekenningen wanneer de sycoca-DB onleesbaar is door gebruiker (bijv. eigenaar root)
- Keur KDBusServiceStarter af. Alle gebruik in kdepim is nu weg, Activatie via DBus is een betere oplossing
- KAutostart toestaan om gemaakt te worden met een absoluut pad

### KTextEditor

- Paginamarges opslaan en laden
- Blijf niet hangen in authenticatie
- Wijzig standaard sneltoets "Invoermodus omschakelen" om geen conflict te hebben met konsolepart (bug 409978)
- Laat sleutelwoord voltooiïngsmodel standaard HideListIfAutomaticInvocation teruggeven
- Minimap: Pak de linker-muisknop-klik niet bij knoppen omhoog/omlaag
- sta tot een reeks accentuering van 1024 toe in plaats van de regel helemaal niet te accentueren als die limiet is bereikt
- invouwen van regels met eindpositie op kolom 0 van een regel repareren (bug 405197)
- Optie toevoegen om enige tekens ook als "automatisch haakje" te behandelen alleen wanneer we een selectie hebben
- Een actie toevoegen om een niet-ingesprongen nieuwe-regel toe te voegen (bug 314395)
- Instelling toevoegen om tekst slepen-en loslaten in/uit te schakelen (standaard aan)

### KUnitConversion

- Binaire gegevenseenheden (bits, kilobytes, kibibytes ... yottabytes) toevoegen

### KWallet Framework

- Initialisatie van xkwalletd naar eerder verplaatsen (bug 410020)
- Migratie-agent van kde4 geheel verwijderen (bug 400462)

### KWayland

- Wayland-protocols gebruiken

### KWidgetsAddons

- concept van kop- en voettekst voor kpageview introduceren
- [Busy Indicator] Laat tijdsduur van QQC2-desktop-style versie overeenkomen
- Een waarschuwingsdialoog met een invouwbare detailssectie
- nieuwe klasse KBusyIndicatorWidget gelijk aan QtQuick's BusyIndicator

### KWindowSystem

- [platforms/xcb] XRES-extensie gebruiken om echte venster-PID te verkrijgen (bug 384837)
- KXMessages weg overzetten uit QWidget

### KXMLGUI

- Zich uitbreidende scheidingen toevoegen als een optie voor aanpassen van werkbalken
- Monochrome actiepictogrammen gebruiken voor knoppen KAboutData
- visibilityChanged connectie verwijderen in het voordeel van bestaande eventFilter

### ModemManagerQt

- Bijwerken van standaard DBus-timeout toestaan op elk interface

### NetworkManagerQt

- apparaat: reapplyConnection() invoegen in het interface

### Plasma Framework

- [ToolButtonStyle] Dezelfde kleurgroep gebruiken voor status erboven zweven
- Kleurenbestand behandelen in installeerprogramma van fake plasmathema
- Plasmathema installeren in locale XDG_DATA_DIR voor pictogramtest
- Tijdsduurindicator van wijzigen toepassen van D22646 naar de QQC2 stijl
- Pakketstructuurplug-ins compileren in verwachte submap
- Accentuering wijzigen naar ButtonFocus
- Uitvoeren van de dialognativetest zonder installeren repareren
- Naar de plug-in van de andere plasmoid zoeken
- [Busy Indicator] Laat tijdsduur van QQC2-desktop-style versie overeenkomen
- Ontbrekende componenten toevoegen in org.kde.plasma.components 3.0
- Pictogrammen voor vernieuwen en opnieuw starten bijwerken om de nieuwe versies van breeze-icons te laten zien (bug 409914)
- itemMouse is niet gedefinieerd in plasma.components 3.0
- clearItems gebruiken wanneer een applet wordt verwijderd
- Crash repareren als switchSize wordt aangepast tijdens initieel instellen
- Cachen van plug-in verbeteren

### Omschrijving

- Phabricator: een nieuwe diff automatisch in de browser openen
- Uitpakken repareren. Patch door Victor Ryzhykh

### QQC2StyleBridge

- Gebroken bewaking repareren die voorkomt dat schuifregelaars voor stijling negatieve waarden krijgen
- Draaisnelheid van indicator voor bezig verlagen
- "Typfout" repareren bij aanmaken van een TextField met focus: true
- [ComboBox] Beleid voor sluiten instellen op klikken buiten in plaats van alleen buiten ouder (bug 408950)
- [SpinBox] renderType instellen (bug 409888)

### Solid

- Zorg er voor dat backends van solid reentrant zijn

### Accentuering van syntaxis

- TypeScript: sleutelwoorden in voorwaardelijke expressies repareren
- Generatie en testpaden van CMake repareren
- Ondersteuning voor extra QML-sleutelwoorden toevoegen geen onderdeel van JavaScript
- Accentuering van cmake bijwerken

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
