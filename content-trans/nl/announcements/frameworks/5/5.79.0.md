---
aliases:
- ../../kde-frameworks-5.79.0
date: 2021-02-13
layout: framework
libCount: 83
qtversion: 5.14
---
### Attica

* Overzetten van QNetworkRequest::FollowRedirectsAttribute naar QNetworkRequest::RedirectPolicyAttribute

### Baloo

* [SearchStore] Afhankelijkheden van bestandssysteem verwijderen uit includeFolder eigenschap
* [FileIndexerConfig] controle op verborgen repareren voor expliciet meegenomen mappen
* [FilteredDirIterator] verborgen bestanden uit QDirIterator aanvragen

### Breeze pictogrammen

* nieuwe telegram-paneelpictogrammen
* Juiste stijl gebruiken voor align-horizontal-left-out pictogrammen (bug 432273)
* Nieuwe kickoff-pictogrammen toevoegen (bug 431883)
* Halve waardering, 100% dekking voor geen waardering, tekstkleur voor waardering in donker thema
* KeePassXC-pictogrammen verwijderen (bug 431593)
* @3x pictogrammen repareren (bug 431475)
* neochat-pictogram toevoegen

### Extra CMake-modules

* GNU_TAR_FOUND alleen inschakelen wanneer --sort=name beschikbaar is
* Generatie van fastlane metagegevens voor een gegeven APK verwijderen
* KDEFrameworksCompilerSettings: -DQT_NO_KEYWORDS en -DQT_NO_FOREACH standaard definiëren
* [KDEGitCommitHooks] kopie van scripts aanmaken in bronmap
* De nieuwe Appstream bestandsextensie ook ondersteunen
* vertalingen ophalen: de URL oplossen alvorens het door te geven aan fetchpo.rb
* Appstream donatie URL's overwegen voor aanmaken van F-Droid metagegevens
* Rechten van scripts repareren (bug 431768)
* ECMQtDeclareLoggingCategory: .categories bestanden in build aanmaken, geen configure
* Functie cmake toevoegen om pre-commit hooks van git te configureren

### Frameworkintegratie

* Vensterdecoraties die niet zijn te verwijderen repareren (bug 414570)

### KDE Doxygen hulpmiddelen

* Ga na dat we geen standaard doxygen lettertype gebruiken
* QDoc rendering verbeteren en een paar bugs in het donkere thema repareren
* Informatie over wie onderhoud doet bijwerken
* Brand nieuw thema consistent met develop.kde.org/docs

### KCalendarCore

* Registratie van MemoryCalendar als een beschouwer opheffen bij verwijderen van een incident
* De recurrenceId gebruiken om het juiste exemplaar te verwijderen
* Notebook-associaties ook wissen bij sluiten van een MemoryCalendar

### KCMUtils

* Modus enkele kolom verzekeren

### KCodecs

* Gebruik van niet-UTF-8 tekenreeksconstanten verwijderen

### KCompletion

* Regressie veroorzaakt door overzetten van operator+ naar operator| repareren

### KConfig

* Code voor opslaan/restore van venstergeometrie opnieuw maken om minder fragiel te zijn
* Herstellen van venstergrootte repareren bij gesloten terwijl gemaximaliseerd (bug 430521)
* KConfig: de component milliseconden van QDateTime behouden

### KCoreAddons

* KFuzzyMatcher voor fuzzy filtering van tekenreeksen toevoegen
* KJob::infoMessage: documenteren dat het argument voor opgemaakte tekst verwijderd zal worden, ongebruikt
* KJobUiDelegate::showErrorMessage: met qWarning() implementeren
* Met X-KDE-PluginInfo-Depends gerepareerde methoden afkeuren
* Van X-KDE-PluginInfo-Depends afhankelijke sleutels laten vallen

### KDeclarative

* Items enkele kolom toestaan
* KeySequenceItem: lege tekenreeks toekennen bij wissen in plaats van ongedefinieerd (bug 432106)
* Dubbelzinnigheid van statussen geselecteerd versus er-boven-zweven voor GridDelegate opheffen (bug 406914)
* Modus Enkel standaard gebruiken

### KFileMetaData

* ffmpegextractor: av_find_default_stream_index gebruiken om videostream te zoeken

### KHolidays

* Vakantiedagen op Mauritius bijgewerkt voor 2021
* Taiwanese vakantiedagen bijgewerkt

### KI18n

* Geen codec voor tekststream zetten bij bouwen tegen Qt6

### KImageFormats

* Gedeelte van NCLX kleurprofielcode vereenvoudigen
* [imagedump] "list MIME type" (-m) optie toevoegen
* Crash bij ongeldige bestanden repareren
* ani: ga na dat riffSizeData de juiste grootte heeft alvorens de quint32_le cast dance te doen
* Plug-in toevoegen voor geanimeerde cursors in Windows (ANI)

### KIO

* Q_LOGGING_CATEGORY-macro gebruiken in plaats van expliciete QLoggingCategory (bug 432406)
* Standaard codec gezet op "US-ASCII" in KIO-toepassingen repareren (bug 432406)
* CopyJob: crash met overslaan/opnieuw proberen repareren (bug 431731)
* KCoreDirLister: niet-overladensignalen van canceled() en completed()
* KFilePreviewGenerator: codebasis moderniseren
* KCoreDirLister: niet-overladensignaal van clear() 
* MultiGetJob: niet-overladensignalen
* FileJob: niet-overladensignaal van close() 
* SkipDialog: signaal result(SkipDialog *_this, int _button) afkeuren
* Vergrendeling repareren bij hernoemen van en bestand uit de eigenschappendialoog (bug 431902)
* addServiceActionsTo en addPluginActionsTo afkeuren
* [KFilePlacesView] dekking voor verborgen items in "alles tonen" verlagen
* mappen niet wijzigen bij openen van url's die niet in een lijst gezet kunnen worden
* Logica van KFileWidget::slotOk aanpassen wanneer in modus bestanden+map
* FileUndoManager: ongedaan maken van kopiëren van lege map repareren
* FileUndoManager: geen bestanden overschrijven bij ongedaan maken
* FileUndoManager: methode undoAvailable() afkeuren
* ExecutableFileOpenDialog: maak het tekstlabel meer generiek
* KProcessRunner: signaal processStarted() slechts eenmaal uitsturen
* "kio_trash: reparatie van de logica wanneer geen limiet is ingesteld" terugdraaien 

### Kirigami

* Niet-symbolisch pictogram voor stopactie gebruiken
* Indrukken van menuknopen op werkbalk correct doen repareren
* Subsectie in plaats van sectie gebruiken
* [controls/BasicListItem]: eigenschap van reserveSpaceForSubtitle toevoegen
* impliciete hoogte van navigatieknop expliciet maken
* Verticale uitlijning van BasicListItem repareren
* [basiclistitem] ga na dat pictogrammen vierkant zijn
* [controls/ListItem]: ingesprongen scheidingsteken voor leidende items verwijderen
* Scheidingsmarge van item in lijst met rechten opnieuw toevoegen wanneer er een leidend item is
* Niet handmatig reverseTwinsChanged aanroepen bij verwijderen van FormLayout (bug 428461)
* [org.kde.desktop/Units] laat tijdsduur overeenkomen met besturing/eenheden
* [Units] veryLongDuration verminderen naar 400ms
* [Units] shortDuration en longDuration verminderen met 50ms
* Gesynthetiseerde muisgebeurtenissen niet als muis beschouwen (bug 431542)
* agressiever implicitHeight gebruiken in plaats van preferredHeight
* Atlas-texturen voor pictogrammen gebruiken
* controls/AbstractApplicationHeader: afgesplitsten verticaal centreren
* [actiontextfield] inline-actiemarges en grootte repareren
* kopgrootte juist bijwerken (bug 429235)
* [controls/OverlaySheet]: Layout.maximumWidth respecteren (bug 431089)
* [controls/PageRouter]: instellingsparameters blootstellen bij dumpen van currentRoutes
* [controls/PageRouter]: topniveauparameters in 'params' eigenschapmap blootstellen
* voorbeeld gerelateerd aan pagerouter verplaatsen in een submap
* Het is mogelijk het venster te verslepen in niet interactieve gebieden
* AbstractApplicationWindow: breed venster gebruiken bij bureaubladsystemen voor alle stijlen
* De scheiding in de lijst met items niet verbergen bij er boven zweven wanneer de achtergrond transparent is
* [controls/ListItemDragHandle] verkeerde ordening in geval van niet schuiven (bug 431214)
* [controls/applicationWindow]: rekening houden met vakbreedte bij berekenen van wideScreen

### KNewStuff

* De KNSQuick paginakop en voet voor Kirigami-ness opnieuw ontwerpen
* Label met getal toevoegen aan waarderingscomponent voor gemakkelijker lezen
* Minimum grootte van QML GHNS dialoogvenster verminderen
* Lichter uiterlijk bij er boven zweven overeen laten komen voor gedelegeerden van KCM-raster
* Ga na dat de minimale breedte voor de QML-dialoog de schermbreedte is of minder
* De indeling van de gedelegeerde van BigPreview repareren
* Valideer items in de cach voor het tonen van de dialoog
* Ondersteuning toevoegen voor kns:/ urls naar het hulpmiddel knewstuff-dialog (bug 430812)
* filecopyworker: bestanden openen vóór lezen/schrijven
* Item op bij te werken resetten wanneer geen inhoud geïdentificeerd is voor bijwerken (bug 430812)
* Crash die af-en-toe gebeurt vanwege het onjuist behouden van een pointer
* Klasse DownloadManager afkeuren
* Eigenschap AcceptHtmlDownloads afkeuren
* Eigenschappen van ChecksumPolicy en SignaturePolicy afkeuren
* Scope-eigenschap afkeuren
* CustomName-eigenschap afkeuren

### KNotification

* NewMenu uitsturen wanneer een nieuw contextmenu is ingesteld (bug 383202)
* KPassivePopup afkeuren
* Ga na dat alle backends naar de meldingen verwijzen voordat ze werk doen
* Zorg dat de app voor meldingvoorbeeld gebouwd kan worden en werkt op Android
* Behandeling van melding-id verplaatsen in KNotification klasse
* Verwijderen van wachtende melding uit wachtrij repareren (bug 423757)

### KPackage-framework

* Eigenaarschap van PackageStructure documenteren bij gebruik van PackageLoader

### KPty

* Genereren van het volledige pad naar kgrantpty in de code voor ! HAVE_OPENPTY repareren

### KQuickCharts

* Een "eerste" methode toevoegen aan ChartDataSource en het in legenda gebruiken (bug 432426)

### KRunner

* Controleren op geselecteerde actie in geval van overeenkomst in informatie
* Lege resultaattekenreeks voor huidige activiteit repareren
* Overladen voor QueryMatch id's afkeuren
* [DBus Runner] afbeelding op afstand testen

### KService

* KPluginInfo::dependencies() afkeuren
* CMake: afhankelijkheden van add_custom_command() specificeren
* Overladen voor KToolInvocation::invokeTerminal expliciet afkeuren
* Methode om KServicePtr te verkrijgen uit standaard terminaltoepassing toevoegen
* KService: method om workingDirectory te zetten toevoegen

### KTextEditor

* [Vi-modus] Beeld niet omschakelen bij wijzigen van hoofd/kleine letters (~ commando) (bug 432056)
* Maximale inspringbreedte verhogen naar 200 (bug 432283)
* ga na dat we de reeksovereenkomst bijwerken bijv. bij ongeldig maken van nu lege reeksen
* Alleen bladwijzertekensfout tonen wanneer in vi-modus (bug 424172)
* [vimode] beweging repareren om item overeen te laten komen dat met één verwijderd is
* Vervangende tekst behouden zolang de snelle zoekbalk niet gesloten is (bug 338111)
* KateBookMarks: codebasis moderniseren
* Alfakanaal niet negeren wanneer er een markering is
* Alfakanaal repareren dat wordt genegeerd bij lezen uit configuratie-interface
* De overeenkomende haakjes in de voorvertoning voorkomen dat ze buiten het beeld vallen
* De overeenkomende haakjes in de voorvertoning voorkomen die soms blijven hangen na omschakelen naar een ander tabblad
* De overeenkomende haakjes in de voorvertoning compacter maken
* Geen overeenkomende haakjes in voorvertoning tonen als dit de cursor bedekt
* Maximaliseer breedte van overeenkomende haakjes in voorvertoning
* De overeenkomende haakjes in de voorvertoning verbergen bij schuiven
* gedupliceerde accentueringsreeksen vermijden die ARGB-renderen afbreken
* de globale KSyntaxHighlighting::Repository als alleen-lezen blootstellen
* Een bug in inspringen corrigeren wanneer de regel "for" of "else" bevat
* Een bug in inspringen corrigeren
* het speciale geval tagLine verwijderen, het leidde tot willekeurige bijwerkfouten in bijv. de
* renderen van regelafbreekmarkering + selectie repareren
* Inspringen repareren voor wanneer bij Enter indrukken en de functieparameter een komma aan het eind heeft
* de kleine speet in de selectiekleur invullen, als het vorige regeleinde in de selectie zit
* code vereenvoudigen + commentaar repareren
* knipfout terugdraaien, teveel code verwijderd voor extra renderingen
* volledige regelselectie neerzetten vermijden, meer in lijn met andere bewerkers
* indenteerder aanpassen aan gewijzigde hl-bestanden
* [Vimode] Commando naar QRegularExpression overzetten
* [Vimode] findPrevWordEnd en findSurroundingBrackets naar QRegularExpression overzetten
* [Vimode] QRegExp::lastIndexIn naar QRegularExpression en QString::lastIndexOf overzetten
* [Vimode] ModeBase::addToNumberUnderCursor naar QRegularExpression overzetten
* [Vimode] eenvoudig gebruik van QRegExp::indexIn overzetten naar QRegularExpression en QString::indexOf
* rgba(r,g,b,aF) gebruiken om het alfakanaal voor exporteren van html te behandelen
* Alfa-kleuren respecteren bij exporteren van html
* Een hulpmethode voor verkrijgen van de kleurnaam juist introduceren
* Ondersteuning van alfa-kleuren: configuratielaag
* markering van gewijzigde regels moet geen huidige regelaccentuering verwijderen
* huidige regelaccentuering ook aanbrengen over pictogramrand
* Alfakanaal voor kleuren van bewerker
* Huidige regelaccentuering heeft een klein gaatje aan het begin voor dynamisch afgebroken regels
* Geen nodeloze berekeningen doen, waarden direct vergelijken
* Huidige regelaccentuering heeft een klein gaatje aan het begin, repareren
* [Vimode] ingevouwen reeks niet overslaan wanneer de beweging aan de binnenkant landt
* Een range-for loop over m_matchingItems gebruiken in plaats van QHash::keys() gebruiken
* normalvimode overzetten naar QRegularExpression
* afhankelijkheden van rechten juist exporteren
* het KSyntaxHighlighting-thema blootstellen
* configChanged toevoegen aan KTextEditor::Editor, ook, voor globale wijzigingen in configuratie
* de configuratiezaken een beetje herordenen
* behandeling van sleutel repareren
* doc/weergave parameters toevoegen aan nieuwe signalen, oude verbindingen repareren
* toegang tot & wijzigen van het huidige thema toestaan via configuratie-interface
* const qualifier verwijderen wanneer LineRanges rondom uitgedeeld wordt
* .toString() gebruiken omdat in QStringView .toInt() ontbreekt in oudere Qt versies
* toLineRange() constexpr inline declareren waar ooit mogelijk
* QStringView versie uit QStringRev hergebruiken, const qualifier verwijderen
* TODO KF6 commentaar uit doxygen commentaar verplaatsen
* Cursor, bereik: fromString(QStringView) overload toevoegen
* "Dynamische regelafbreking met inspringen voor uitlijnen" toestaan om uitgeschakeld te worden (bug 430987)
* LineRange::toString(): '-' teken vermijden, omdat het voor negatieve getallen verwarrend is
* KTextEditor::LineRange in notifyAboutRangeChange() mechanisme gebruiken
* Port tagLines(), checkValidity() en fixLookup() naar LineRanges overzetten
* KTextEditor::LineRange toevoegen
* KateTextBuffer::rangesForLine() impl verplaatsen naar KateTextBlock en onnodige containerconstructies vermijden
* [Vimode] zoeken binnen ingevouwen reeksen repareren (bug 376934)
* [Vimode] opnieuw afspelen van "macro completion" repareren (bug 334032)

### KTextWidgets

* Meer private klassen laten erven uit die van de ouders

### KUnitConversion

* Variabele definiëren alvorens deze te gebruiken

### KWidgetsAddons

* Gebruikmaken van AUTORCC
* Meer private klassen laten erven uit die van de ouders
* Expliciet QStringList invoegen

### KWindowSystem

* Fractionele dekking van gemakshelpers toevoegen
* Invoegen echt repareren
* Invoegen repareren
* xcb: met het actieve scherm zoals gerapporteerd door QX11Info::appScreen() werken

### KXMLGUI

* Invoegen repareren
* Signaal KXMLGUIFactory::shortcutsSaved toevoegen
* De juiste betrokken url van kde get gebruiken (bug 430796)

### Plasma Framework

* [plasmoidheading] bedoelde kleur voor voetteksten gebruiken
* [plasmacomponents3/spinbox] geselecteerde tekstkleur repareren
* widgets>lineedit.svg: foutieve pixel-uitlijnproblemen repareren (bug 432422)
* Inconsistente links en rechts aanvullen in PlasmoidHeading repareren
* Breeze-dark/breeze-light kleuren bijwerken
* "[SpinBox] logische fout in richting van schuiven repareren" teruggedraaid
* [calendar] ontbrekende importnamen repareren
* [ExpandableListItem] uitgevouwen weergave van actielijst het lettertype laten respecteren
* DaysCalendar: overzetten naar PC3/QQC2 waar mogelijk
* Hover-animatie verwijderen uit vlakke knoppen, agenda, items in lijst, knoppen
* Plasmoid-fouten in console dumpen
* Cyclische afhankelijkheid van Units.qml oplossen
* [PlasmaComponents MenuItem] dummy-actie aanmaken wanneer actie wordt vernietigd
* pixmaps niet groter maken dan nodig
* Projectbestanden van JetBrains IDE toevoegen aan genegeerd
* Waarschuwingen over verbindingen repareren
* RESET toevoegen aan eigenschap van globalShortcut (bug 431006)

### Omschrijving

* [nextcloud] configuratie-UI opnieuw bewerken
* Initiële configuratie evalueren
* Maak ListViews aan kdeconnect- en bluetooth-configuratie vast
* Onnodige aan Indeling vast gemaakte eigenschappen verwijderen
* [plugins/nextcloud] Nextcloud-pictogram gebruiken
* [cmake] find_package verplaatsen naar topniveau van CMakeLists.txt

### QQC2StyleBridge

* [combobox] touchpad schuifsnelheid repareren (bug 400258)
* qw kan null zijn
* QQuickWidget ondersteunen (bug 428737)
* venster slepen uit lege gebieden toestaan

### Solid

* CMake: configure_file() gebruiken om noop-incrementeel bouwen te verzekeren
* [Fstab] overlay-aankoppelingen van vastzetter negeren (bug 422385)

### Sonnet

* Geen meerdere opzoekingen doen wanneer een genoeg is

### Accentuering van syntaxis

* ontbrekend verhogen van versie van context.xml toevoegen
* Bestandseinden officieel ondersteunen, zie https://mailman.ntg.nl/pipermail/ntg-context/2020/096906.html
* referentieresultaten bijwerken
* minder levendige operator-kleur voor breeze thema's
* syntaxis voor nieuwe hugo repareren
* syntaxisfouten repareren
* Leesbaarheid van gesolariseerd donker theme verbeteren
* onnodige vangsten met een dynamische regel verwijderen
* bewerking accentuering => bijwerkreferenties samenvoegen
* accentuering van operator samenvoegen
* const overloads voor sommige accessors toevoegen
* Bash, Zsh: cmd repareren;; in een case (bug 430668)
* Atom Light, Breeze Dark/Light: nieuwe kleur voor Operator ; Breeze Light: nieuwe kleur voor ControlFlow
* email.xml: geneste commentaar en escaped tekens detecteren (bug 425345)
* Atom light bijwerken naar gebruik van alfa-kleuren
* remap enige symbool- en operator-stijlen naar dsOperator
* Bash: } in ${!xy*} repareren en meer Parameter Expansion Operator (# in ${#xy} ; !,*,@,[*],[@] in ${!xy*}) (bug 430668)
* Revisies van thema bijwerken
* Kconfig-accentuering bijwerken naar Linux 5.9
* rgba(r,g,b,aF) gebruiken om het alfa-kanaal juist te behandelen voor Qt/Browsers
* rgba niet gebruiken wanneer in themeForPalette
* Zorg ervoor dat rgba gerespecteerd wordt in html en formaat
* Reparaties voor atom-one-dark
* Nog wat meer elementen voor bijwerken van Atom One Dark
* Niet controleren op rgba bij controleren op een beste overeenkomst
* Gebruik van het alfa-kanaal in themakleuren toestaan
* monokai kleuren bijwerken en onjuist blauw repareren
* C++: us-achtervoegsel repareren
* Bash: reparatie #5: $ aan het eind van een tekenreeks met dubbele aanhalingstekens
* VHDL: functie, procedure, type reeks/eenheden en andere verbeteringen repareren
* breeze-dark.theme bijwerken
* Raku: #7: symbolen repareren die beginnen met Q
* quickfix mist contrast voor extensie
* Kleurenschema Oblivion uit GtkSourceView/Pluma/gEdit toevoegen

### ThreadWeaver

* Map-iteratoren repareren bij bouwen tegen Qt6
* Mutexes niet expliciet initialiseren als NonRecursive

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org> Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
