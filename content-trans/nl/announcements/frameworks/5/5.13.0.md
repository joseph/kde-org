---
aliases:
- ../../kde-frameworks-5.13.0
date: 2015-08-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nieuwe frameworks

- KFileMetadata: bibliotheek voor metagegevens van bestand en extractie
- Baloo: bestandsindexering en framework voor zoeken

### Wijzigingen die all frameworks betreffen

- De vereiste Qt versie is omhoog gegaan van 5.2 naar 5.3
- Debuguitvoer is overgezet naar gecategoriseerde uitvoer, voor standaard minder ruis
- Docbook-documentatie is nagezien en bijgewerkt

### Frameworkintegratie

- Crash in bestandsdialoog met alleen mappen gerepareerd
- Niet vertrouwen op options()-&gt;initialDirectory() voor Qt &lt; 5.4

### KDE Doxygen hulpmiddelen

- Manpagina's voor kapidox-scripts en beheerder van informatie voor bijwerken in setup.py

### KBookmarks

- KBookmarkManager: KDirWatch gebruiken in plaats van QFileSystemWatcher om te detecteren of user-places.xbel wordt aangemaakt.

### KCompletion

- Reparaties voor HiDPI voor KLineEdit/KComboBox
- KLineEdit: laat de gebruiker geen tekst verwijderen wanneer de bewerking van regels alleen-lezen is

### KConfig

- Beveel geen gebruik van verouderde API aan
- Genereer geen verouderde code

### KCoreAddons

- Kdelibs4Migration::kdeHome() toevoegen voor zaken niet gedekt door hulpbronnen
- tr() waarschuwing repareren
- KCoreAddons bouwen op Clang+ARM repareren

### KDBusAddons

- KDBusService: documenteer hoe het actieve venster omhoog te brengen, in Activate()

### KDeclarative

- Verouderde oproep KRun::run call repareren
- Zelfde gedrag van MouseArea om coördinaten van gefilterde dochtergebeurtenissen een plaats te geven
- Detecteer dat initieel gezichtspictogram wordt aangemaakt
- Ververs niet het gehele venster wanneer we de plotter weergave maken (bug 348385)
- de userPaths-context juist toevoegen
- Stik niet op een leeg QIconItem

### Ondersteuning van KDELibs 4

- kconfig_compiler_kf5 verplaatst naar libexec, gebruik kreadconfig5 in plaats van de test findExe
- Documenteer de (suboptimale) vervangingen voor KApplication::disableSessionManagement

### KDocTools

- wijzig zin over rapportering van bugs, goedgekeurd door dfaure
- pas Duitse user.entities aan aan en/user.entities
- Werk general.entities bij: wijzig markup voor frameworks + plasma uit toepassing naar productnaam
- en/user.entities bijwerken
- Sjablonen voor boeken en manpagina's bijwerken
- CMAKE_MODULE_PATH in cmake_install.cmake gebruiken
- BUG: 350799 (bug 350799)
- general.entities bijwerken
- Zoeken naar vereiste perl-modules.
- Geef een helper-macro een naamruimte in het geïnstalleerde macros-bestand.
- Sleutelnaam vertalingen aangepast aan standaard vertalingen geleverd door Termcat

### KEmoticons

- Breeze thema installeren
- Kemoticons: maak Breeze-emoticons standaard in plaats van Glass
- Pakket Breeze emoticon gemaakt door Uri Herrera

### KHTML

- Laat KHtml bruikbaar zijn zonder zoeken naar privé afhankelijkheden

### KIconThemes

- Tijdelijke toekenningen van tekenreeksen verwijderen.
- Debugitem in themaboomstructuur verwijderen

### KIdleTime

- Privé headers voor platform-plug-ins zijn geïnstalleerd.

### KIO

- Kill onnodige QUrl-wrappers

### KItemModels

- Nieuw proxy: KExtraColumnsProxyModel, stelt u in staat om kolommen aan een bestaand model toe te voegen.

### KNotification

- De startpositie van Y voor terugvalpop-ups repareren
- Afhankelijkheden verminderen en verplaatsen naar Tier 2
- Vang onbekende meldingen (nullptr deref) (bug 348414)
- Vrijwel onnuttige waarschuwingsberichten verwijderen

### Pakket Framework

- maak van ondertitels, ondertitels ;)
- kpackagetool: uitvoer van niet-latijnse tekst naar stdout repareren

### KPeople

- AllPhoneNumbersProperty toevoegen
- PersonsSortFilterProxyModel nu beschikbaar voor gebruik in QML

### Kross

- krosscore: installeer CamelCase-kop "KrossConfig"
- Python2 tests repareren om samen met PyQt5 uit te voeren

### KService

- kbuildsycoca --global repareren
- KToolInvocation::invokeMailer: bijlage repareren wanneer we meerdere bijlagen hebben

### KTextEditor

- standaard loglevel bewaken voor Qt &lt; 5.4.0, log-cat-naam repareren
- hl voor Xonotic  toevoegen (bug 342265)
- Groovy HL toevoegen (bug 329320)
- J highlighting bijwerken (bug 346386)
- Make compileren met MSVC2015
- minder gebruik van pictogramlader, meer pictogrammen uit pixels repareren
- zoek alle knoppen bij wijzigingen van patronen in-/uitschakelen
- Zoeken &amp; vervangenbalk verbeteren
- nutteloze ruler uit powermodus verwijderen
- meer smalle zoekbalk
- vi: foutief lezen van markType01-vlag repareren
- Juiste kwalificatie gebruiken om basismethode aan te roepen.
- Controles verwijderen, QMetaObject::invokeMethod bewaakt zichzelf daar al tegen.
- HiDPI problemen met kleurkiezers repareren
- coe opschonen: QMetaObject::invokeMethod is veilig voor nullptr.
- meer toelichting
- de manier waarop de interfaces veilig zijn voor null wijzigen
- alleen waarschuwingen en bovenstaand tonen als standaard
- taken uit het verleden verwijderen
- QVarLengthArray gebruiken om de tijdelijke QVector-iteratie op te slaan.
- De hack om groepslabels te laten inspringen verplaatsen naar het moment van constructie.
- Enige serieuze problemen met het KateCompletionModel in modus boomstructuur repareren.
- Gebroken ontwerp van model repareren, die afhankelijk was van Qt 4 gedrag.
- umask-regels volgen bij opslaan van nieuw bestand (bug 343158)
- meson-HL toevoegen
- Omdat Varnish 4.x verschillende wijzigingen in syntaxis vergeleken met Varnish 3.x introduceerde, werden er extra, aparte bestanden voor accentuering van de syntaxis voor Varnish 4 geschreven (varnish4.xml,  varnishtest4.xml).
- HiDPI problemen repareren
- vi-modus: niet crashen als het &lt;c-e&gt; commando wordt uitgevoerd aan het eind van een document. (bug 350299)
- QML multiregel tekenreeksen ondersteunen.
- syntaxis van oors.xml repareren
- CartoCSS hl door Lukas Sommer toevoegen (bug 340756)
- floating point HL repareren, gebruik het ingebouwde Float zoals in C (bug 348843)
- splitsingsrichtingen werden omgedraaid (bug 348845)
- Bug 348317 - [PATCH] Katepart accentuering van syntaxis zou u0123 stijl escapes voor JavaScript moeten herkennen (bug 348317)
- *.cljs toevoegen (bug 349844)
- Het GLSL-accentueringsbestand bijwerken
- standaard kleuren gerepareerd om meer van elkaar te onderscheiden

### KTextWidgets

- Oude accentueringsprogramma verwijderen

### KWallet Framework

- Bouw van Windows repareren
- Toon een waarschuwing met foutcode bij openen van de portefeuille wanneer PAM mislukt
- De backend foutcode teruggeven in plaats van -1 bij mislukken van het openen van een portefeuille
- Maak van "onbekend cipher" van de backend een negatieve code bij teruggeven
- Bewaking voor PAM_KWALLET5_LOGIN voor KWallet5
- Crash wanneer controle van MigrationAgent::isEmptyOldWallet() mislukt repareren
- KWallet kan nu ontgrendeld worden door PAM bij gebruik van de module kwallet-pam

### KWidgetsAddons

- Nieuwe API die QIcon-parameters neemt om de pictogrammen in de tabbladbalk in te stellen
- KCharSelect: unicode-categorie en gebruik van boundingRect voor berekening van breedte repareren
- KCharSelect: celbreedte repareren om inhoud te laten passen
- KMultiTabBar-marges zijn nu OK op HiDPI-schermen
- KRuler: maak niet geïmplementeerde KRuler::setFrameStyle() verouderd, schoon commentaar op
- KEditListWidget: marge verwijderen, zodat het beter uitlijnt met andere widgets

### KWindowSystem

- Harden van NETWM gegevens lezen (bug 350173)
- bewaking voor oudere Qt versies zoals in kio-http
- Privé headers voor platform-plug-ins zijn geïnstalleerd.
- Platformspecifieke delen laden als plug-ins

### KXMLGUI

- Methodegedrag van KShortcutsEditorPrivate::importConfiguration repareren

### Plasma Framework

- Met een knijpgebaar kan nu omgeschakeld worden tussen de verschillende zoomlevels van de agenda's
- toelichting over duplicatie van code in pictogramdialog
- Kleur van groef in schuifregelaar was hard gecodeerd, gewijzigd om kleurschema te gebruiken
- QBENCHMARK gebruiken in plaats van een zwaar beslag op de prestatie van de machine
- Navigatie in de agenda is aanzienlijk verbeterd, biedend een jaar- en decade-overzicht
- PlasmaCore.Dialog heeft nu een eigenschap 'dekking'
- Enige ruimte maken voor het keuzerondje
- Niet de circulaire achtergrond tonen als er een menu is
- Definitie van X-Plasma-NotificationAreaCategory toevoegen
- Meldingen en osd instellen om op alle bureaubladen te tonen
- Nuttige waarschuwing tonen wanneer we geen geldige KPluginInfo kunnen krijgen
- Potentiële eindeloze recursie in PlatformStatus::findLookAndFeelPackage() repareren
- software-updates.svgz naar software.svgz hernoemen

### Sonnet

- In CMake bits toevoegen om bouwen van Voikko plug-in in te schakelen.
- Sonnet::Client factory voor Voikko spellingcontrole implementeren.
- Op Voikko gebaseerd spellingcontrole implementeren (Sonnet::SpellerPlugin)

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
