---
aliases:
- ../announce-applications-16.08-rc
date: 2016-08-05
description: KDE stelt Applications 16.08 Release Candidate beschikbaar.
layout: application
release: applications-16.07.90
title: KDE stelt de Release Candidate van KDE Applicaties 16.08 beschikbaar
---
5 augustus 2016. Vandaag heeft KDE de "release candidate" van de nieuwe versies van KDE Applications vrijgegeven. Met het bevriezen van afhankelijkheden en functies, is het team van KDE nu gefocust op repareren van bugs en verder oppoetsen.

Kijk in de <a href='https://community.kde.org/Applications/16.08_Release_Notes'>uitgavenotities van de gemeenschap</a> voor informatie over nieuwe tarballs, tarballs die nu op KF5 zijn gebaseerd en bekende problemen. Een meer complete aankondiging zal beschikbaar zijn voor de uiteindelijke uitgave

De uitgave KDE Applications 16.08 heeft grondig testen nodig om de kwaliteit en gebruikservaring te handhaven en te verbeteren. Echte gebruikers zijn kritiek in het proces om de hoge kwaliteit van KDE te handhaven, omdat ontwikkelaars eenvoudig niet elke mogelijke configuratie kunnen testen. We rekenen op u om in een vroeg stadium bugs te vinden zodat ze gekraakt kunnen worden voor de uiteindelijke vrijgave. Ga na of u met het team mee kunt doen door de beta te installeren <a href='https://bugs.kde.org/'>en elke bug te rapporteren</a>.
