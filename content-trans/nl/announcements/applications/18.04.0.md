---
aliases:
- ../announce-applications-18.04.0
changelog: true
date: 2018-04-19
description: KDE stelt KDE Applicaties 18.04.0 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 18.04.0 beschikbaar
version: 18.04.0
---
19 april 2018. KDE Applications 18.04.0 is uitgebracht.

We werken voortdurend aan verbetering van de software in onze KDE Application series en we hopen dat u alle nieuwe verbeteringen en reparaties van bugs nuttig vindt!

## Wat is er nieuw in KDE Applicaties 18.04

### Systeem

{{<figure src="/announcements/applications/18.04.0/dolphin1804.png" width="600px" >}}

De eerste belangrijke uitgave in 2018 van <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, de krachtige bestandsbeheerder van KDE, bevat veel verbeteringen aan zijn panelen:

- De secties van panelen van 'Plaatsen' kunnen nu verborgen worden als u de voorkeur geeft aan ze niet tonen en een nieuwe sectie 'Netwerk' is nu beschikbaar om items te bevatten voor locaties op afstand.
- Het paneel 'Terminal' kan vastgezet worden aan elke kant van het venster en als u het probeert te openen zonder dat Konsole is geïnstalleerd, zal Dolphin een waarschuwing geven en u helpen het te installeren.
- HiDPI ondersteuning voor het paneel 'Informatie' is verbeterd.

De mapweergave en de menu's zijn ook bijgewerkt:

- De prullenbakmap toont nu een knop 'Lege prullenbak'.
- Een menu-item 'Doel tonen' is toegevoegd om te helpen bij het zoeken naar symlink-doelen.
- Integratie van git is verbeterd, omdat het contextmenu voor gitmappen nu twee nieuwe acties laten zien voor 'git log' en 'git merge'.

Verdere verbeteringen bevatten:

- Een nieuwe sneltoets is geïntroduceerd die u de optie biedt om de Filterbalk te openen eenvoudig door op de toets slash (/) te drukken.
- U kunt nu foto's sorteren en organiseren op de datum dat ze zijn genomen.
- Het slepen en neerzetten van vele kleine bestanden binnen Dolphin is sneller geworden en gebruikers kunnen nu in bulk gedaan hernoemen ongedaan maken.

{{<figure src="/announcements/applications/18.04.0/konsole1804.png" width="600px" >}}

Om het werken op de opdrachtregel nog plezieriger te maken, kan <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, de toepassing van KDE als terminalemulator, er nu nog leuker uitzien:

- U kunt kleurschema's via KNewStuff downloaden.
- De schuifbalk past zich beter aan aan het actieve kleurschema.
- Standaard wordt de tabbladbalk alleen getoond indoen nodig.

Medewerkers van Konsole stopten hier niet en introduceerden veel nieuw functies:

- Een nieuwe alleen-lezen modus en een om te schakelen profieleigenschap voor kopiëren van tekst als HTML is toegevoegd.
- Onder Wayland ondersteunt Konsole nu het menu slepen-en-loslaten.
- Er zijn een aantal verbeteringen aangevracht met betrekking tot het ZMODEM-protocol: Konsole kan nu de zmodem-upload-indicator B01 behandelen, het zal de voortgang tonen tijdens gegevensoverdracht, De knop Annuleren in de dialoog werkt nu zoals zou moeten en overbrengen van grotere bestanden wordt beter ondersteund door ze in geheugen te laden in 1MB brokken.

Verdere verbeteringen bevatten:

- Schuiven met het muiswiel met libinput is gerepareerd en rollen door shellgeschiedenis bij schuiven met het muiswiel wordt nu voorkomen.
- Zoekopdrachten worden vernieuwd na wijziging van de zoekregels met de optie voor reguliere expressies en bij indrukken van 'Ctrl' + 'Backspace' zal Konsole zijn gedrag overeen laten komen met xterm.
- De sneltoets '--background-mode' is gerepareerd.

### Multimedia

<a href='https://juk.kde.org/'>JuK</a>, de muziekspeler van KDE heeft nu ondersteuning van Wayland. Nieuwe UI-functies omvatten de mogelijkheid om de menubalk te verbergen en een visuele indicatie van de nu spelende track. Bij uitschakeling van verankering in het systeemvak zal JuK niet langer crashen bij de poging om af te sluiten via het pictogram 'afsluiten' in het venster en het gebruikersinterface zal zichtbaar blijven. Bugs met betrekking tot het onverwacht afspelen door JuK bij hervatten uit slaaptoestand in Plasma 5 en behandeling van de afspeellijstkolom zijn ook gerepareerd.

Voor de uitgave 18.04 hebben medewerkers van <a href='https://kdenlive.org/'>Kdenlive</a>, de niet-lineaire videobewerker, van KDE, zich gefocust op onderhouden:

- Van grootte wijzigen van clips verstoort niet langer vervagingen and sleutelframes.
- Gebruikers van schaling op het schem van HiDPI monitoren kunnen genieten van fijnere pictogrammen.
- Een mogelijke crash in opstarten met sommige configuraties is gerepareerd.
- MLT is nu vereist om versie 6.6.0 of later te gebruiken en compatibiliteit is verbeterd.

#### Grafische zaken

{{<figure src="/announcements/applications/18.04.0/gwenview1804.png" width="600px" >}}

In de laatste maanden hebben medewerkers van <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, de afbeeldingsviewer en organizer van KDE, gewerkt aan een serie van verbeteringen. Hoogtepunten omvatten:

- Ondersteuning voor MPRIS controllers is toegevoegd zodat u nu diashows kunt besturen via KDE Connect, uw mediatoetsen op het toetsenbord en de mediaspelerplasmoid.
- Het knoppen voor zweven boven kunnen nu uitgeschakeld worden.
- Het hulpmiddel voor bijsnijden ontving een aantal verbeteringen, zoals zijn instellingen worden nu onthouden bij omschakelen naar een andere afbeelding, de vorm van het selectievak kan nu vergrendeld worden door de toetsen 'Shift' of 'Ctrl' ingedrukt te houden en deze kan ook vergrendeld zijn op de beeldverhouding van de nu getoonde afbeelding.
- In modus volledig-scherm kunt u nu afsluiten met de toets 'Escape' en het kleurpalet zal uw actieve kleurthema weergeven. Als u Gwenview afsluit in deze modus zal deze instellting onthouden worden en de nieuwe sessie zal eveneens in volledig-scherm beginnen.

Attentie tot detail is belangrijk, dus is Gwenview in de volgende gebieden gepolijst:

- Gwenview zal meer gemakkelijk leesbare bestandspaden tonen in de lijst 'Recente mappen', het juiste contextmenu tonen voor items in de lijst 'Recente bestanden' en op de juiste manier alles vergeten wanneer de functie 'Geschiedenis uitschakelen' wordt gebruikt.
- Klikken op een map in de zijbalk biedt schakelen tussen modi bladeren en weergave en onthoudt de laatst gebruikte modus bij schakelen tussen mappen, waarmee snellere navigatie in enorme afbeeldingsverzamelingen mogelijk is.
- Navigatie met het toetsenbord is verder gestroomlijnd door juist aangeven van focus voor toetsenbord in modus bladeren.
- De functie 'In breedte passen' is vervangen door een meer algemene 'Vul'-functie.

{{<figure src="/announcements/applications/18.04.0/gwenviewsettings1804.png" width="600px" >}}

Nog kleinere verbeteringen kunnen vaak de werkmethode van gebruikers plezieriger maken:

- Om consistentie te verbeteren worden SVG afbeeldingen nu vergroot zoals alle andere afbeeldingen bij inschakelen van 'Afbeeldingsweergave → Kleinere afbeeldingen vergroten'.
- Na bewerken van een afbeelding of wijzigingen ongedaan maken, zullen afbeeldingsweergave en miniaturen gesynchroniseerd zijn.
- Bij hernoemen van afbeeldingen zal de bestandsnaamextensie standaard niet geselecteerd zijn en de dialoog 'Verplaatsen/Kopiëren/Koppelen' toont nu standaard de huidige map.
- Veel visuele probleempjes zijn gerepareerd, bijv. in de URL-balk, voor de werkbalk in volledig-scherm en voor de animaties met miniatuurtekstballonnen. Ontbrekende pictogrammen zijn ook toegevoegd.
- Als laatste maar niet de minste, de hoverknop bij volledig-scherm zal direct de afbeelding laten zien in plaats van de inhoud van de map, en de geavanceerde instellingen biedt nu meer controle over de ICC kleurrenderingintent.

#### Kantoor

In <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, de universele documentviewer van KDE, kan rendering van PDF en tekstextractie nu geannuleerd worden als u poppler versie 0.63 of hoger hebt, wat betekent dat als u een complex PDF-bestand hebt en u wijzigt de zoom terwijl het bzig is met renderen het zal dit onmiddellijk annuleren in plaats van te wachten tot het renderen is geëindigd.

U zult verbeterde PDF JavaScript ondersteuning vinden voor AFSimple_Calculate en als u poppler versie 0.64 of hoger hebt, zal Okular PDF JavaScript wijzigen in alleen-lezen status van formulieren.

Beheer van bevestiging van boekingen per e-mail in <a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, de krachtige e-mailclient van KDE, is belangrijk verbeterd met ondersteuning van treinboekingen en een op Wikidata gebaseerde airportdatabase voor het tonen van vluchten met de juiste informatie voor tijdzones. Om de dingen gemakkelijker voor u te maken is een nieuwe extractor geïmplementeerd voor e-mails die geen gestructureerde boekingsdata bevatten.

Verdere verbeteringen bevatten:

- De berichtstructuur kan opnieuw getoond worden met de nieuwe 'Expert' plug-in.
- Er is een plug-in toegevoegd in de Sieve-bewerker voor selecteren van e-mails uit de Akonadi-database.
- Het zoeken van tekst in de bewerker is verbeterd met ondersteuning voor reguliere expressies.

#### Hulpmiddelen

{{<figure src="/announcements/applications/18.04.0/spectacle1804.png" width="600px" >}}

Het gebruikersinterface van <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, het veelzijdige hulpmiddel voor schermafdrukken van KDE, verbeteren was een belangrijk gebied van focus:

- De onderste rij knoppen heeft een revisie ondergaan en toont nu een knop om he Instellingenvenster te openen en een nieuwe knop 'Hulpmiddelen' die methoden laat zien om de laatst gebruikte schermafdrukmap te openen en een schermopnameprogramma te starten.
- De laatst gebruikte opslagmodus wordt nu standaard onthouden.
- De venstergrootte past zich nu aan aan de beeldverhouding van de schermafdruk, wat resulteert in een meer plezierige en ruimte-efficiënte miniatuur schermafdruk.
- He venster Instellingen is aanzienlijk vereenvoudigd.

Bovendien zullen gebruikers in staat zijn om hun werkwijze met deze nieuwe functies te vereenvoudigen:

- Wanneer een specifiek venster is gevangen kan zijn titel automatisch toegevoegd worden aan de naam van het schermafdrukbestand.
- De gebruiker kan nu kiezen of Spectacle automatisch zal afsluiten na elke opslag- of kopieeractie.

Belangrijke reparaties bevatten:

- Slepen en neerzetten naar Chromium-vensters werkt nu zoals verwacht.
- Herhaald gebruik van de sneltoetsen om een schermafdruk op te slaan resulteert niet meer in een meerduidige waarschuwingsdialoog over sneltoetsen.
- Voor rechthoekige schermafdrukken kan de onderrand van de selectie accurater worden bijgesteld.
- Verbeterde betrouwbaarheid van vangen van vensters die raken aan schermranden wanneer compositing is afgeschakeld.

{{<figure src="/announcements/applications/18.04.0/kleopatra1804.gif" width="600px" >}}

Met <a href='https://www.kde.org/applications/utilities/kleopatra/'>Kleopatra</a>, de certificatenbeheerder en universele crypto-GUI van KDE, kunnen sleutels Curve 25519 EdDSA gegenereerd worden wanneer deze gebruikt worden met een recente versie van GnuPG. Een weergave 'Notepad' is toegevoegd voor op tekst gebaseerde crypto-acties en u kunt nu direct in de toepassing ondertekenen/versleutelen en ontcijferen/verifiëren. Onder de weergave 'Details van certificaat' zult u nu een actie exporteren vinden, die u kunt gebruiken om als tekst te kopiëren en te plakken. Nog meer, u kunt het resultaat via de nieuwe weergave 'Notepad' importeren.

In <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, het grafische hulpmiddel voor comprimeren/ decompressie van bestanden van KDE, met ondersteuning voor meerdere formaten, is het nu mogelijk om comprimeren of uitpakken te stoppen terwijl de libzip-backend voor ZIP-archieven wordt gebruikt.

### Toepassingen die meedoen in de vrijgave planning van KDE Applications

De webcamrecorder van KDE <a href='https://userbase.kde.org/Kamoso'>Kamoso</a> en het programma voor reservekopieën <a href='https://www.kde.org/applications/utilities/kbackup/'>KBackup</a> zal nu de vrijgave van Applications volgen. De instant-messenger <a href='https://www.kde.org/applications/internet/kopete/'>Kopete</a> zal ook opnieuw geïntroduceerd worden na te zijn overgebracht naar KDE Frameworks 5.

### Toepassingen gaan naar hun eigen uitgaveplanning

De hex-bewerker <a href='https://community.kde.org/Applications/18.04_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>Okteta</a> zal zijn eigen vrijgaveplanning krijgen na een verzoek van zijn onderhouder.

### Op bugs jagen

Meer dan 170 bugs zijn opgelost in toepassingen inclusief de Kontact Suite, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello en meer!

### Volledige log met wijzigingen
