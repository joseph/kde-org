---
aliases:
- ../announce-applications-15.12.3
changelog: true
date: 2016-03-16
description: KDE stelt KDE Applicaties 15.12.3 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 15.12.3 beschikbaar
version: 15.12.3
---
15 maart 2016. Vandaag heeft KDE de derde stabiele update vrijgegeven voor <a href='../15.12.0'>KDE Applicaties 15.12</a> Deze uitgave bevat alleen bugreparaties en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 15 aangegeven bugreparaties inclusief verbeteringen aan kdepim, akonadi, ark, kblocks, kcalc, ktouch en umbrello, naast anderen.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van KDE Development Platform 4.14.18.
