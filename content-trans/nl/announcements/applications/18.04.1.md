---
aliases:
- ../announce-applications-18.04.1
changelog: true
date: 2018-05-10
description: KDE stelt KDE Applicaties 18.04.1 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 18.04.1 beschikbaar
version: 18.04.1
---
10 mei 2018. Vandaag heeft KDE de eerste stabiele update vrijgegeven voor <a href='../18.04.0'>KDE Applicaties 18.04</a> Deze uitgave bevat alleen bugreparaties en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Ongeveer 20 aangegeven reparaties van bugs, die verbeteringen bevatten aan Kontact, Cantor, Dolphin, Gwenview, JuK, Okular, Umbrello, naast andere.

Verbeteringen bevatten:

- Dubbele items in het plaatsenpaneel van Dolphin veroorzaken niet langer crashes
- Een oude bug met herladen van SVG-bestanden in Gwenview is gerepareerd
- Umbrello's C++ importeren verstaat nu het trefwoord 'explicit'
