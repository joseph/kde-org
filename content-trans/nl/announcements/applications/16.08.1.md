---
aliases:
- ../announce-applications-16.08.1
changelog: true
date: 2016-09-08
description: KDE stelt KDE Applicaties 16.08.1 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 16.08.1 beschikbaar
version: 16.08.1
---
8 september 2016. Vandaag heeft KDE de eerste stabiele update vrijgegeven voor <a href='../16.08.0'>KDE Applicaties 16.08</a> Deze uitgave bevat alleen bugreparaties en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 45 aangegeven bugreparaties inclusief verbeteringen aan kdepim, kate, kdenlive, konsole, marble, kajongg, kopete, umbrello, naast anderen.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van KDE Development Platform 4.14.24.
