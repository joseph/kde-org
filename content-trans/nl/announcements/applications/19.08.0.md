---
aliases:
- ../announce-applications-19.08.0
changelog: true
date: 2019-08-15
description: KDE stelt Applicaties 19.08 beschikbaar.
layout: application
release: applications-19.08.0
title: KDE stelt KDE Applicaties 19.08.0 beschikbaar
version: '19.08'
version_number: 19.08.0
---
{{< peertube "/6443ce38-0a96-4b49-8fc5-a50832ed93ce" >}}

{{% i18n_date %}}

De KDE-gemeenschap is verheugd de uitgave van KDE Applications 19.08 aan te kondigen.

Deze uitgave is onderdeel van het commitment van KDE om voortdurend verbeterde versies van de programma's die we beschikbaar stellen aan onze gebruikers te verbeteren. Nieuwe versies van Applications brengen meer mogelijkheden en beter ontworpen software die de bruikbaarheid en stabiliteit van toepassingen zoals Dolphin, Konsole, Kate, Okular en al uw andere favoriete KDE hulpmiddelen verhogen. Ons doel is om te verzekeren dat u productief blijft en om KDE software gemakkelijker en plezieriger door u te laten gebruiken.

We hopen dat u plezier beleeft aan alle nieuwe verbeteringen die u zal vinden in 19.08!

## Wat is er nieuw in KDE Applicaties 19.08

Meer dan 170 bugs zijn opgelost. Deze reparaties implementeren opnieuw uitgeschakelde functies, normaliseren sneltoetsen en lossen crashes op, waarmee uw toepassingen vriendelijker worden en slimmer werkt en speelt.

### Dolphin

Dolphin is de verkenner van bestanden en mappen van KDE die u nu van overal kunt starten met de nieuwe globale sneltoets <keycap>Meta + E</keycap>. Er is ook een nieuwe functie die rommel op uw bureaublad minimaliseert. Wanneer Dolphin al actief is, als u mappen opent met andere toepassingen, zullen deze mappen openen in een nieuw tabblad in het bestaande venster in plaats van in een nieuw Dolphin-venster. Merk op dat dit gedrag nu standaard aan staat, maar het kan uitgeschakeld worden.

Het informatiepaneel (standaard gelokaliseerd rechts van het hoofdpaneel van Dolphin) is op verschillende manieren verbeterd. U kunt, bijvoorbeeld, kiezen om mediabestanden automatisch af te spelen wanneer u ze accentueert in het hoofdpaneel en u kunt nu de in het paneel getoonden tekst selecteren en kopiëren. Als u, wat het informatiepaneel toont, wilt wijzigen, kunt u dat daar ook in het paneel doen, omdat Dolphin geen apart venster zal openen wanneer u er voor kiest het paneel te configureren.

We hebben ook vele probleempjes en kleine bugs opgelost, zodat uw ervaring met Dolphin gebruiken in het algemeen soepeler is.

{{<figure src="/announcements/applications/19.08.0/dolphin_bookmark.png" alt=`Nieuwe bladwijzerfunctie van Dolphin` caption=`Nieuwe bladwijzerfunctie van Dolphin` width="600px" >}}

### Gwenview

Gwenview is is de afbeeldingsviewer van KDE en in deze uitgave hebben de ontwikkelaars zijn functie voor miniaturen bekijken over het algemeen verbeterd. Gwenview kan nu een \"Modus laag hulpbrongebruik\" gebruiken die lage-resolutie miniaturen laadt (indien beschikbaar). Deze nieuwe modus is veel sneller en meer hulpbron-efficient bij laden van miniaturen voor JPEG afbeeldingen en RAW bestanden. In gevallen wanneer Gwenview geen miniaturen voor een afbeelding kan genereren, toont deze nu een plaatshouderafbeelding in plaats van de miniatuur van de vorige afbeelding te hergebruiken. De problemen die Gwenview had met het tonen van miniaturen uit cameras van Sony en Canon zijn ook opgelost.

Naast wijzigingen in de afdeling miniaturen heeft Gwenview ook een nieuw menu “Delen” geïmplementeerd die het verzenden van afbeeldingen naar verschillende plaatsen biedt en op de juiste manier bestanden laadt en toont in locaties op afstand benaderd met KIO. De nieuwe versie van Gwenview toont ook ruim meer EXIF-metagegevens voor RAW afbeeldingen.

{{<figure src="/announcements/applications/19.08.0/gwenview_share.png" alt=`Nieuw menu “Delen” van Gwenview` caption=`Nieuw menu “Delen” van Gwenview` width="600px" >}}

### Okular

Ontwikkelaars hebben vele verbeteringen geïntroduceerd aan annotaties in Okular, de documentviewer van KDE. Naast de verbeterde UI voor de dialogen voor configuratie van annotatie, zijn aan annotaties aan regels nu verschillende visuele versieringen toegevoegd aan de einden, wat biedt dat ze omgevormd kunnen worden, bijvoorbeeld, in pijlen. Het andere wat u kunt doen met annotaties is ze in een keer uit- en inklappen.

Ondersteuning van EPub-documenten in Okular heeft ook een duw in deze versie gekregen. Okular crasht niet meer bij een poging misvormde ePub-bestanden te laden en zijn prestaties met grote ePub-bestanden is aanzienlijk verbeterd. De lijst met wijzigingen in deze uitgave omvat verbeterde paginaranden en markeringshulpmiddelen in de modus presentatie in modus hoge DPI.

{{<figure src="/announcements/applications/19.08.0/okular_line_end.png" alt=`Hulpmiddel voor annotatieinstellingen van Okular met de nieuwe regeleindeoptie` caption=`Hulpmiddel voor annotatieinstellingen van Okular met de nieuwe regeleindeoptie` width="600px" >}}

### Kate

Met dank aan onze ontwikkelaars zijn drie vervelende bugs geplet in deze versie van de geavanceerde tekstbewerker van KDE. Kate brengt opnieuw zijn bestaande venster naar voren wanneer gevraagd wordt een nieuw document te openen vanuit een andere toepassing. De functie \"Snel openen\" sorteert items op meest recent gebruikt en selecteert vooraf het bovenste item. De derde wijziging is in de functie \"Recente documenten\", die nu werkt wanneer de huidige configuratie is ingesteld om geen individuele instellingen van vensters op te slaan.

### Konsole

De meest te merken wijziging in Konsole, de toepassing terminalemulator van KDE, is de versnelling van de functie tegelen. U kunt nu het hoofdpaneel op elke manier die u wilt, zowel verticaal als horizontaal. De subpanelen kunnen daarna opnieuw opgesplitst worden zoals u dat wil. Deze versie voegt ook de mogelijkheid toe om panelen te verslepen en los te laten, zodat u gemakkelijk de indeling aan uw werkmethode kunt aanpassen.

Daarnaast is het venster Instellingen overhoop gehaald om het helderder en gemakkelijker in gebruik te maken.

{{< video src-webm="/announcements/applications/19.08.0/konsole-tabs.webm" >}}

### Spectacle

Spectacle is de toepassing van KDE voor schermafdrukken en krijgt steeds meer interessante functies met elke nieuwe versie. Deze versie is geen uitzondering, omdat Spectacle nu komt met verschillende nieuwe functies die zijn functionaliteit voor vertragen reguleert. Bij het nemen van een schermafdruk met vertraging, zal Spectacle de resterende tijd tonen zijn venstertitel. Deze informatie is ook zichtbaar in zijn item in de takenbeheerder.

Verder over de functie Vertraging, De knop van Spectacle op de takenbeheerder zal ook een voortgangsbalk tonen, u kunt dus volgen wanneer afdruk genomen zal worden. En, tenslotte, als u Spectacle maximaliseert tijdens het wachten, zult u zien dat de knop “Een nieuwe schermafdruk nemen” is omgedraaid in een knop \"Annuleren\". Deze knop bevat ook een voortgangsbalk, waarmee u de kans hebt om het aftellen te stoppen.

Opslaan van schermafdrukken heeft ook een nieuwe functionaliteit. Wanneer u een schermafdruk hebt opgeslagen toont Spectacle een melding in de toepassing die u biedt om de schermafdruk te openen of de opslagmap.

{{< video src-webm="/announcements/applications/19.08.0/spectacle_progress.webm" >}}

### Kontact

Kontact, de e-mail/agenda/contactpersonen en algemene groupwaresuite van programma's van KDE, brengt Unicode kleur emoji en ondersteuning van Markdown naar de e-mailopsteller. Niet alleen zal de nieuwe versie van KMail uw berichten mooier laten maken, maar, dankzij de integratie met grammatica-controleerders zoals LanguageTool en Grammalecte, zal het u helpen uw tekst te controleren en te corrigeren.

{{<figure src="/announcements/applications/19.08.0/kontact_emoji.png" alt=`Emoji selectieprogramma` caption=`Emoji selectieprogramma` width="600px" >}}

{{<figure src="/announcements/applications/19.08.0/kmail_grammar.png" alt=`Integratie van grammaticacontrole van KMail` caption=`Integratie van grammaticacontrole van KMail` width="600px" >}}

Bij plannen van afspraken worden e-mails met uitnodigingen in KMail niet langer verwijderd na ze te beantwoorden. Het is nu mogelijk een afspraak te verplaatsen van de ene agenda naar een andere in de bewerker van afspraken KOrganizer.

Als laatste maar niet de minste, kan KAddressBook nu SMS-berichten naar contactpersonen verzenden via KDE Connect, wat resulteert in een meer gemakkelijke integratie van uw bureaublad en mobiele apparaten.

### Kdenlive

De nieuwe versie van Kdenlive, de software van KDE voor videobewerking, heeft een nieuwe set toetsenbord-muis-combinaties die zullen helpen om u productiever te maken. U kunt, bijvoorbeeld, de snelheid van een clip in de tijdlijn wijzigen door op CTRL te drukken en dan op de clip te slepen of het miniatuurvoorbeeld van videoclips activeren door Shift in te drukken en de muis over een clipminiatuur te bewegen in de projectbin. Ontwikkelaars hebben ook veel inspanning gestopt in bruikbaarheid door de driepuntsbewerking consistent te maken met andere videobewerkers, die u zeker zult waarderen als u omschakelt naar Kdenlive vanuit een andere bewerker.

{{<figure src="https://cdn.kde.org/screenshots/kdenlive/19-08.png" alt=`Kdenlive 19.08.0` caption=`Kdenlive 19.08.0` width="600px" >}}
