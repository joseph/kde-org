---
version: "5.0.95"
title: "Plasma 5.0.95 Source Information Page"
errata:
    link: https://community.kde.org/Plasma/5.1_Errata
    name: 5.1 Errata
type: info/plasma5
unstable: true
---

This is a beta release of Plasma, featuring Plasma Desktop and
other essential software for your desktop computer.  Details in the <a
href="/announcements/plasma-5.0.95">Plasma 5.0.95 announcement</a>.

For an overview of the differences from Plasma 4 read the initial <a
href="/announcements/plasma5.0/">Plasma 5.0 announcement</a>.
