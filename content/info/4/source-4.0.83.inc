<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdeaccessibility-4.0.83.tar.bz2">kdeaccessibility-4.0.83</a></td><td align="right">6,1MB</td><td><tt>0e288ec272d850c497c453e9ffb7d18f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdeadmin-4.0.83.tar.bz2">kdeadmin-4.0.83</a></td><td align="right">1,8MB</td><td><tt>e25a89113c6698eb618a6b7e7eb016be</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdeartwork-4.0.83.tar.bz2">kdeartwork-4.0.83</a></td><td align="right">41MB</td><td><tt>462a06012368a054649641c202fe590b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdebase-4.0.83.tar.bz2">kdebase-4.0.83</a></td><td align="right">4,2MB</td><td><tt>05d6c1f10fde2f1d6a704d85ca6c02f8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdebase-runtime-4.0.83.tar.bz2">kdebase-runtime-4.0.83</a></td><td align="right">50MB</td><td><tt>68972abac79763675533aaf3ab5f14bd</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdebase-workspace-4.0.83.tar.bz2">kdebase-workspace-4.0.83</a></td><td align="right">32MB</td><td><tt>f7e81d183d10418c5c7dc6a757b9d172</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdebindings-4.0.83.tar.bz2">kdebindings-4.0.83</a></td><td align="right">4,2MB</td><td><tt>96a9e070a9d4b422c265473114cb65fd</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdeedu-4.0.83.tar.bz2">kdeedu-4.0.83</a></td><td align="right">53MB</td><td><tt>88c8e627793b9b2d2512c205d33288f2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdegames-4.0.83.tar.bz2">kdegames-4.0.83</a></td><td align="right">30MB</td><td><tt>8f35e27f3b7e20d7332e7d81a28f4fc7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdegraphics-4.0.83.tar.bz2">kdegraphics-4.0.83</a></td><td align="right">3,3MB</td><td><tt>0ca48930d7f4c688d8ad1d5a2d54cf4e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdelibs-4.0.83.tar.bz2">kdelibs-4.0.83</a></td><td align="right">8,7MB</td><td><tt>2fbea759a4a8a535c2307c7c600e2d0d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdemultimedia-4.0.83.tar.bz2">kdemultimedia-4.0.83</a></td><td align="right">1,4MB</td><td><tt>aaece1acbcad24451ea5a988f5f9fe38</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdenetwork-4.0.83.tar.bz2">kdenetwork-4.0.83</a></td><td align="right">7,1MB</td><td><tt>d5d9c413c45269cbd7ac8a35a02dd0fb</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdepim-4.0.83.tar.bz2">kdepim-4.0.83</a></td><td align="right">13MB</td><td><tt>89d76a005705259b2d7470bf6c4449f9</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdepimlibs-4.0.83.tar.bz2">kdepimlibs-4.0.83</a></td><td align="right">1,9MB</td><td><tt>aa0dccbabfc67172120c6018b7f1b14b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdeplasmoids-4.0.83.tar.bz2">kdeplasmoids-4.0.83</a></td><td align="right">3,9MB</td><td><tt>891de9131043c649de0e129d17581ef1</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdesdk-4.0.83.tar.bz2">kdesdk-4.0.83</a></td><td align="right">4,7MB</td><td><tt>52005ce174b711f90a0d9cb32030627d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdetoys-4.0.83.tar.bz2">kdetoys-4.0.83</a></td><td align="right">1,3MB</td><td><tt>b21e354ea11809519a02995120b4ed96</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdeutils-4.0.83.tar.bz2">kdeutils-4.0.83</a></td><td align="right">2,1MB</td><td><tt>18755511bd09914dafe5e57c55fa13ce</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.83/src/kdewebdev-4.0.83.tar.bz2">kdewebdev-4.0.83</a></td><td align="right">3,3MB</td><td><tt>56dc0b149d1f5f62506e8a1d2e3ea91d</tt></td></tr>
</table>
