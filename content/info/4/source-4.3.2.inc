<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">SHA1&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdeaccessibility-4.3.2.tar.bz2">kdeaccessibility-4.3.2</a></td><td align="right">6.3MB</td><td><tt>f6aa2d3cfa74cb2808e02bd67aec8d58cd61ef30</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdeadmin-4.3.2.tar.bz2">kdeadmin-4.3.2</a></td><td align="right">1.8MB</td><td><tt>fc8978e921f1849b45b269d688b6baf2978555ab</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdeartwork-4.3.2.tar.bz2">kdeartwork-4.3.2</a></td><td align="right">63MB</td><td><tt>c199fae3f37b1a86e46ca2ee7a176d34bf034c5d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdebase-4.3.2.tar.bz2">kdebase-4.3.2</a></td><td align="right">4.1MB</td><td><tt>c9c65dc596c8c48974fbe4099f340371de147fa8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdebase-runtime-4.3.2.tar.bz2">kdebase-runtime-4.3.2</a></td><td align="right">7.0MB</td><td><tt>33bb1da691d1ab75d1fbe5bcac38f105cddab709</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdebase-workspace-4.3.2.tar.bz2">kdebase-workspace-4.3.2</a></td><td align="right">60MB</td><td><tt>3a07f3e64abd324e0b8a3aeab1ae2c13e75ea026</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdebindings-4.3.2.tar.bz2">kdebindings-4.3.2</a></td><td align="right">4.7MB</td><td><tt>72416301dce5362535bd3c09ffd6d2f884409fc4</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdeedu-4.3.2.tar.bz2">kdeedu-4.3.2</a></td><td align="right">56MB</td><td><tt>df274f08242b1dedbfecc5a1d2ce452dcfbb5735</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdegames-4.3.2.tar.bz2">kdegames-4.3.2</a></td><td align="right">49MB</td><td><tt>cc3eee96fdd3aabb7230a675b877fde98a04e35d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdegraphics-4.3.2.tar.bz2">kdegraphics-4.3.2</a></td><td align="right">3.5MB</td><td><tt>142fccc4f6f7d1f059f79e0fc9fd6694ed3a7b0e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdelibs-4.3.2.tar.bz2">kdelibs-4.3.2</a></td><td align="right">11MB</td><td><tt>37f8dd8275bffbcb0e01f9f133a20856329dfa0c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdelibs-experimental-4.3.2.tar.bz2">kdelibs-experimental-4.3.2</a></td><td align="right">28KB</td><td><tt>79e34cff9fdcca8fc0e06a0bf524b08abf815705</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdemultimedia-4.3.2.tar.bz2">kdemultimedia-4.3.2</a></td><td align="right">1.6MB</td><td><tt>22fe17528685799e03f1caf583c7e7b021f2ddd7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdenetwork-4.3.2.tar.bz2">kdenetwork-4.3.2</a></td><td align="right">7.1MB</td><td><tt>7b384e11b551c9d1113cc39d318ca1b9d7712864</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdepim-4.3.2.tar.bz2">kdepim-4.3.2</a></td><td align="right">11MB</td><td><tt>0de9c7f1c4544f7520128016a6e06f10e6ed1173</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdepimlibs-4.3.2.tar.bz2">kdepimlibs-4.3.2</a></td><td align="right">1.8MB</td><td><tt>1d48b9619e04da121e6233bfb081377afa7c327a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdepim-runtime-4.3.2.tar.bz2">kdepim-runtime-4.3.2</a></td><td align="right">728KB</td><td><tt>2061f4b2845728a97aef6b64f679cbb8f3a50d91</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdeplasma-addons-4.3.2.tar.bz2">kdeplasma-addons-4.3.2</a></td><td align="right">1.4MB</td><td><tt>ecbb3ebe64eccf41a567bdf98e741b14a05536f4</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdesdk-4.3.2.tar.bz2">kdesdk-4.3.2</a></td><td align="right">5.4MB</td><td><tt>eabd284791cd4315b743389bddb13848c47bf287</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdetoys-4.3.2.tar.bz2">kdetoys-4.3.2</a></td><td align="right">1.3MB</td><td><tt>7557b1c808a6378beb450d16293cb743c34a1fb7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdeutils-4.3.2.tar.bz2">kdeutils-4.3.2</a></td><td align="right">2.5MB</td><td><tt>16a4193d847e67abce215056c6eab2cbb348a1c8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/kdewebdev-4.3.2.tar.bz2">kdewebdev-4.3.2</a></td><td align="right">2.5MB</td><td><tt>e44a2e311bad8adc18ac4cc7dfd9942839a2dab3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.2/src/oxygen-icons-4.3.2.tar.bz2">oxygen-icons-4.3.2</a></td><td align="right">115MB</td><td><tt>8257c799ac8e5a3b284c0048b6ce821e1e25647c</tt></td></tr>
</table>
