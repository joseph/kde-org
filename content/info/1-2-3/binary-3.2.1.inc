<ul>

<!-- ASP LINUX -->
<li><a href="http://www.asplinux.ru/">ASP Linux</a>
<!--  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/ASPLinux/README">README</a>) -->
  :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/ASPLinux/9.2/noarch/">Icon and Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      9.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/ASPLinux/9.2/i386/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>

<!-- CONECTIVA LINUX -->
<!--
<li><a href="http://www.conectiva.com/">Conectiva Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/Conectiva/README">README</a>
  /
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/Conectiva/LEIAME">LEIAME</a>
  ) :
  <ul type="disc">
    <li>
      9.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/Conectiva/cl9/conectiva/RPMS.kdeorg/">Intel
      i386</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/Conectiva/cl9/SRPMS.kdeorg/">SRPMs</a>
    </li>
    <li>
      8.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/Conectiva/cl8/conectiva/RPMS.kdeorg/">Intel
      i386</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/Conectiva/cl8/SRPMS.kdeorg/">SRPMs</a>
    </li>
     <li>
      7.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/Conectiva/cl7/conectiva/RPMS.kdeorg/">Intel
      i386</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/Conectiva/cl7/SRPMS.kdeorg/">SRPMs</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--  DEBIAN -->
<!--
<li><a href="http://www.debian.org/">Debian</a>
  (<a href="http://download.kde.org/stable/3.2.1/Debian/README">README</a>) :
    <ul type="disc">
      <li>
         Debian stable (woody) (Intel i386, IBM PowerPC, DEC Alpha) : <tt>deb http://download.kde.org/stable/3.2.1/Debian stable main</tt>
      </li>
    </ul>
  <p />
</li>
-->

<!--   FREEBSD -->
<!--
<li><a href="http://www.freebsd.org/">FreeBSD</a>
  (<a href="http://download.kde.org/stable/3.2.1/FreeBSD/README">README</a>)
  <p />
</li>
-->

<!--   MAC OS X (FINK PROJECT) -->
<!--
<li>
  <a href="http://www.apple.com/macosx/">Mac OS</a> /
  <a href="http://www.opendarwin.org/">OpenDarwin</a>
  (downloads via the <a href="http://fink.sourceforge.net/">Fink</a> project):
  <ul type="disc">
    <li>
      <a href="http://fink.sourceforge.net/news/kde.php">X</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   MANDRAKE LINUX -->
<!--
<li>
  <a href="http://www.linux-mandrake.com/">Mandrake Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/Mandrake/README">README</a>):
  <ul type="disc">
    <li>
      8.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/Mandrake/8.2/">Intel
      i586</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   REDHAT LINUX -->
<li>
  <a href="http://www.redhat.com/">Red Hat</a>
  :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/RedHat/Fedora/noarch/">Logo and Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      Fedora: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/RedHat/Fedora/i386/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>

<!-- SLACKWARE LINUX -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution):
   <ul type="disc">
     <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/contrib/Slackware/noarch/">Language
        packages</a> (all versions and architectures)
     </li>
    <li>
       9.1: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/contrib/Slackware/9.1/">Intel i486</a>
     </li>
   </ul>
  <p />
</li>

<!-- AIX -->
<!--
<li>
  <a href="http://www-1.ibm.com/servers/aix/">IBM AIX</a> (Not IBM endorsed):
   <ul type="disc">
     <li>
       5.1+: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/AIX/5.1/">PowerPC</a>
     </li>
   </ul>
  <p />
</li>
-->

<!-- SOLARIS -->
<!--
<li>
  <a href="http://www.sun.com/solaris/">SUN Solaris</a> (Unofficial contribution):
  <ul type="disc">
    <li>
      8.0: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/contrib/Solaris/8.0/x86/">Intel x86</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   SUSE LINUX -->
<li>
  <a href="http://www.suse.com/">SuSE Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/SuSE/README">README</a>)
  :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/SuSE/noarch/">Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      9.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/SuSE/ix86/9.0/">Intel
      i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/SuSE/x86_64/9.0/">AMD x86-64</a>
    </li>
    <li>
      8.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/SuSE/ix86/8.2/">Intel i586</a>
    </li>
    <li>
      8.1:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/SuSE/ix86/8.1/">Intel
      i586</a>
    </li>
  </ul>
  <p />
</li>

<!-- TURBO LINUX -->
<!--
<li>
    <a href="http://www.turbolinux.com/">Turbolinux</a>
    (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/Turbo/Readme.txt">README</a>):
    <ul type="disc">
      <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/Turbo/noarch/">Language
        packages</a> (all versions and architectures)</li>
      <li>
       8.0:
       <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/Turbo/8/i586/">Intel i586</a>
      </li>
      <li>
       7.0:
       <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/Turbo/7/i586/">Intel i586</a>
      </li>
    </ul>
  <p />
</li>
-->

<!--   TRU64 -->
<!--
<li>
  <a href="http://www.tru64unix.compaq.com/">Tru64</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/contrib/Tru64/README.Tru64">README</a>):
  <ul type="disc">
    <li>
      4.0d, e, f and g and 5.x:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/contrib/Tru64/">Compaq Alpha</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   YELLOWDOG LINUX -->
<!--
  <a href="http://www.yellowdoglinux.com/">YellowDog</a>:
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/YellowDog">2.2</a>
-->

<!-- YOPER LINUX -->
<!--
<li>
  <a href="http://www.yoper.com/">Yoper</a>:
  <ul type="disc">
    <li>
      1.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/Yoper/">Intel i686 tgz</a>
    </li>
  </ul>
  <p />
</li>
-->

<!-- VECTOR LINUX -->
<!--
<li>
  <a href="http://www.vectorlinux.com/">VECTORLINUX</a>:
  (<a href="http://download.kde.org/stable/3.2.1/contrib/VectorLinux/3.0/README">README</a>):
  <ul type="disc">
    <li>
      3.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/contrib/VectorLinux/3.2/">Intel i386</a>
    </li>
    <li>
      3.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/contrib/VectorLinux/3.0/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>
-->

</ul>
