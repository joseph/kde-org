<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.2/src/kdeaccessibility-3.80.2.tar.bz2">kdeaccessibility-3.80.2</a></td><td align="right">7.4MB</td><td><tt>b7b95562be738d8db5e3795ef8238c87</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.2/src/kdeaddons-3.80.2.tar.bz2">kdeaddons-3.80.2</a></td><td align="right">885KB</td><td><tt>9f79db6ab7d4309bcb169a371ed96782</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.2/src/kdeadmin-3.80.2.tar.bz2">kdeadmin-3.80.2</a></td><td align="right">1.4MB</td><td><tt>e0e9fe7d8d45380c44a4456d64c80805</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.2/src/kdeartwork-3.80.2.tar.bz2">kdeartwork-3.80.2</a></td><td align="right">15MB</td><td><tt>a9467cfbde312fb4607ff6a03e12288a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.2/src/kdebase-3.80.2.tar.bz2">kdebase-3.80.2</a></td><td align="right">18MB</td><td><tt>063e75a101191c6cf64e7514cb29fdd6</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.2/src/kdeedu-3.80.2.tar.bz2">kdeedu-3.80.2</a></td><td align="right">26MB</td><td><tt>cfaacb380ebff1ed87c561cd68c09a12</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.2/src/kdegames-3.80.2.tar.bz2">kdegames-3.80.2</a></td><td align="right">18MB</td><td><tt>6f8012afb060d4db497af27110c45055</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.2/src/kdegraphics-3.80.2.tar.bz2">kdegraphics-3.80.2</a></td><td align="right">4.9MB</td><td><tt>b387e378e8737b20b8e91b21e67920c6</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.2/src/kdelibs-3.80.2.tar.bz2">kdelibs-3.80.2</a></td><td align="right">13MB</td><td><tt>697cb962328c21b69ccf81c810030fce</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.2/src/kdemultimedia-3.80.2.tar.bz2">kdemultimedia-3.80.2</a></td><td align="right">4.0MB</td><td><tt>80a4555b28eeb2ea5d02fd252c546cd9</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.2/src/kdenetwork-3.80.2.tar.bz2">kdenetwork-3.80.2</a></td><td align="right">7.0MB</td><td><tt>07b61b2de9a50a9da59449043d5379a2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.2/src/kdepim-3.80.2.tar.bz2">kdepim-3.80.2</a></td><td align="right">8.3MB</td><td><tt>44b123e24579d306446f4fd35549b575</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.2/src/kdepimlibs-3.80.2.tar.bz2">kdepimlibs-3.80.2</a></td><td align="right">1.1MB</td><td><tt>286332dd42f9cba2897b9a9f5fd47240</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.2/src/kdesdk-3.80.2.tar.bz2">kdesdk-3.80.2</a></td><td align="right">4.0MB</td><td><tt>eb4b7a482a3e18e9a4e0f613fdea8826</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.2/src/kdetoys-3.80.2.tar.bz2">kdetoys-3.80.2</a></td><td align="right">2.2MB</td><td><tt>6dcad952551752d23c1a6884f71fb986</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.2/src/kdeutils-3.80.2.tar.bz2">kdeutils-3.80.2</a></td><td align="right">2.1MB</td><td><tt>35ac2c3249d284565bcabee141674cc3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.2/src/kdewebdev-3.80.2.tar.bz2">kdewebdev-3.80.2</a></td><td align="right">4.4MB</td><td><tt>82d5ddf903e1af819ad4cf11bd5ebc98</tt></td></tr>
</table>
