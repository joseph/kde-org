<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">MD5&nbsp;Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/arts-1.3.1.tar.bz2">arts-1.3.1</a></td>
   <td align="right">952kB</td>
   <td><tt>26554e4c47d9a52bc20a07e39d32a731</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/kdeaccessibility-3.3.1.tar.bz2">kdeaccessibility-3.3.1</a></td>
   <td align="right">1.6MB</td>
   <td><tt>b5154d0c0232c93308b8e7bb7d83e6c9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/kdeaddons-3.3.1.tar.bz2">kdeaddons-3.3.1</a></td>
   <td align="right">1.9MB</td>
   <td><tt>69ec1deff7ae2d0c3602bb141ed4c546</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/kdeadmin-3.3.1.tar.bz2">kdeadmin-3.3.1</a></td>
   <td align="right">1.9MB</td>
   <td><tt>76b52fefed929f25978d003965ad9ee7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/kdeartwork-3.3.1.tar.bz2">kdeartwork-3.3.1</a></td>
   <td align="right">17MB</td>
   <td><tt>b983255aabdc6aadb2b9f66145d5ecfb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/kdebase-3.3.1.tar.bz2">kdebase-3.3.1</a></td>
   <td align="right">19MB</td>
   <td><tt>dd0d9707296f2be143c28a8be21b6e24</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/kdebindings-3.3.1.tar.bz2">kdebindings-3.3.1</a></td>
   <td align="right">7.3MB</td>
   <td><tt>fd64783a92e66a2405b9e25f7322c222</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/kdeedu-3.3.1.tar.bz2">kdeedu-3.3.1</a></td>
   <td align="right">21MB</td>
   <td><tt>757e51db2b7c6cf831b59ef7f1304a2c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/kdegames-3.3.1.tar.bz2">kdegames-3.3.1</a></td>
   <td align="right">9.3MB</td>
   <td><tt>a105e16aab1fccbb24e6bd24d6345e7a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/kdegraphics-3.3.1.tar.bz2">kdegraphics-3.3.1</a></td>
   <td align="right">6.6MB</td>
   <td><tt>18f063ec6fad27b304ae97cf4b480140</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/kde-i18n-3.3.1.tar.bz2">kde-i18n-3.3.1</a></td>
   <td align="right">181MB</td>
   <td><tt>34c43a57b4e8c8f2fea0622ba14ff471</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/kdelibs-3.3.1.tar.bz2">kdelibs-3.3.1</a></td>
   <td align="right">15MB</td>
   <td><tt>8636c93405b20eceadb12af5c5483508</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/kdemultimedia-3.3.1.tar.bz2">kdemultimedia-3.3.1</a></td>
   <td align="right">5.6MB</td>
   <td><tt>4a3462ba7b37e0057eadd24541908f95</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/kdenetwork-3.3.1.tar.bz2">kdenetwork-3.3.1</a></td>
   <td align="right">7.1MB</td>
   <td><tt>4b35a9e23ef555b47e9423082856ca80</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/kdepim-3.3.1.tar.bz2">kdepim-3.3.1</a></td>
   <td align="right">10MB</td>
   <td><tt>5aed63aed6b7c1ad9232cb30aa124a40</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/kdesdk-3.3.1.tar.bz2">kdesdk-3.3.1</a></td>
   <td align="right">4.6MB</td>
   <td><tt>475c72c3010560d6bb4d355fbce9596c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/kdetoys-3.3.1.tar.bz2">kdetoys-3.3.1</a></td>
   <td align="right">3.1MB</td>
   <td><tt>ef76385dd7856ef964102fddf1936450</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/kdeutils-3.3.1.tar.bz2">kdeutils-3.3.1</a></td>
   <td align="right">2.6MB</td>
   <td><tt>07e45ca924cc52ce5565bb0bf3f6fa6a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/kdevelop-3.1.1.tar.bz2">kdevelop-3.1.1</a></td>
   <td align="right">8.0MB</td>
   <td><tt>93224b9552ae6e5ab5c0c7e13943f9d3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.1/src/kdewebdev-3.3.1.tar.bz2">kdewebdev-3.3.1</a></td>
   <td align="right">5.0MB</td>
   <td><tt>b4178f3cadaa33befb6a0f98fbbb99c1</tt></td>
</tr>

</table>
