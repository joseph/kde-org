    <table border="0" cellpadding="4" cellspacing="0">
      <tr valign="top">
        <th align="left">Location</th>

        <th align="left">Size</th>

        <th align="left">MD5&nbsp;Sum</th>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.3/src/arts-1.1.3.tar.bz2">arts-1.1.3</a></td>

        <td align="right">965KB</td>

        <td><tt>f04eb6ef387d63741561ab6623696322</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.3/src/kde-i18n-3.1.3.tar.bz2">kde-i18n-3.1.3</a></td>

        <td align="right">144MB</td>

        <td><tt>dd7fcd22d3b90cdb024623e627ab87b6</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.3/src/kdeaddons-3.1.3.tar.bz2">kdeaddons-3.1.3</a></td>

        <td align="right">1.1MB</td>

        <td><tt>d1835674847b1c94c16b508fedc2d85c</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.3/src/kdeadmin-3.1.3.tar.bz2">kdeadmin-3.1.3</a></td>

        <td align="right">1.5MB</td>

        <td><tt>e733972693e1f9196a11cc90953a93d8</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.3/src/kdeartwork-3.1.3.tar.bz2">kdeartwork-3.1.3</a></td>

        <td align="right">14MB</td>

        <td><tt>a68f5b45060777b5d81952ec188db749</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.3/src/kdebase-3.1.3.tar.bz2">kdebase-3.1.3</a></td>

        <td align="right">15MB</td>

        <td><tt>d11514ebed619de18869d95e2d110951</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.3/src/kdebindings-3.1.3.tar.bz2">kdebindings-3.1.3</a></td>

        <td align="right">5.9MB</td>

        <td><tt>3c932da16560136940afaf2391782cde</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.3/src/kdeedu-3.1.3.tar.bz2">kdeedu-3.1.3</a></td>

        <td align="right">20MB</td>

        <td><tt>02996b08a1c2a2aa044406480c0eb8a7</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.3/src/kdegames-3.1.3.tar.bz2">kdegames-3.1.3</a></td>

        <td align="right">8.2MB</td>

        <td><tt>98151556b1556e7e39930ab12bdeace2</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.3/src/kdegraphics-3.1.3.tar.bz2">kdegraphics-3.1.3</a></td>

        <td align="right">4.4MB</td>

        <td><tt>14afcd5713481b19a5a9908e522445de</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.3/src/kdelibs-3.1.3a.tar.bz2">kdelibs-3.1.3a</a></td>

        <td align="right">11MB</td>

        <td><tt>d05854938dcbda2e571f7f1ba51cdda7</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.3/src/kdemultimedia-3.1.3.tar.bz2">kdemultimedia-3.1.3</a></td>

        <td align="right">5.7MB</td>

        <td><tt>da1e05b8284976359cab32f0abf4b87b</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.3/src/kdenetwork-3.1.3.tar.bz2">kdenetwork-3.1.3</a></td>

        <td align="right">4.8MB</td>

        <td><tt>18b1a274575eddf053dd6e72ad5554c7</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.3/src/kdepim-3.1.3.tar.bz2">kdepim-3.1.3</a></td>

        <td align="right">3.2MB</td>

        <td><tt>7a556df5e5ae4c00c8bea4cd8f9c1f3e</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.3/src/kdesdk-3.1.3.tar.bz2">kdesdk-3.1.3</a></td>

        <td align="right">2.1MB</td>

        <td><tt>427586c481c0e503c197ff2d5aa3b81c</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.3/src/kdetoys-3.1.3.tar.bz2">kdetoys-3.1.3</a></td>

        <td align="right">1.8MB</td>

        <td><tt>2d3a41ece1a87f6cf1bc1cfddae30da1</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.3/src/kdeutils-3.1.3.tar.bz2">kdeutils-3.1.3</a></td>

        <td align="right">1.4MB</td>

        <td><tt>21f3fa1d110a8cf8cb1a140745f90211</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.3/src/quanta-3.1.3.tar.bz2">quanta-3.1.3</a></td>

        <td align="right">2.8MB</td>

        <td><tt>0971ec23ed4f9e87b66ead4e4e18eff7</tt></td>
      </tr>
    </table>

