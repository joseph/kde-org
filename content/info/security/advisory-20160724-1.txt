KDE Project Security Advisory
=============================

Title:           karchive: KNewstuff downloads can install files outside the extraction directory.
Risk Rating:     Important
CVE:             CVE-2016-6232
Platforms:       Linux / Mac / Windows
Versions:        karchive < 5.24
Author:          David Faure faure@kde.org
Date:            24 July 2016

Overview
========

A maliciously crafted archive (.zip or .tar.bz2) with "../" in the file paths
could be offered for download via the KNewStuff framework (e.g. on www.kde-
look.org), and upon extraction would install files anywhere in the user's home
directory.

Proof of concept
================

For testing, an example of a malicious archive can be found at
http://www.davidfaure.fr/kde/tar_relative_path_outside_archive.tar.bz2

Impact
======

Users can unwillingly install files like a modified .bashrc, or a new .desktop
file associated to a common MIME type and executing a malicious command.

Workaround
==========

Users should not install anything via KNewStuff until KDE Frameworks 5.24,
or should at least inspect downloaded archives to make sure they don't contain
relative paths containing "../".

Solution
========

KArchive 5.24, released as part of KDE Frameworks 5.24, forbids archive
extraction from installing files outside the extraction directory.

Alternatively, commit 0cb243f in karchive.git can be applied to previous
releases.

Thanks to Andreas Cord-Landwehr for finding this issue and fixing it.

