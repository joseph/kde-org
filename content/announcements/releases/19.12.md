---
layout: page
publishDate: 2019-12-12 13:00:00
summary: Over 120 individual programs plus dozens of programmer libraries and feature
  plugins are released simultaneously as part of KDE's release service.
title: 19.12 Releases
type: announcement
---

December 12, 2019. Over 120 individual programs plus dozens of programmer libraries and feature plugins are released simultaneously as part of KDE's release service.

Today they all get new feature release sources.

Distro and app store packagers should update their pre-release channels to check for issues.

+ [KDE's December Apps Update](https://www.kde.org/announcements/releases/2019-12-apps-update) for new features
+ [19.12 release notes](https://community.kde.org/Releases/19.12_Release_Notes) for information on tarballs and known issues.
+ [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
+ [19.12 source info page](https://kde.org/info/releases-19.12.0)
+ [19.12 full changelog](https://kde.org/announcements/changelog-releases.php?version=19.12.0)

## Press Contacts

For more information send us an email: [press@kde.org](mailto:press@kde.org).
