---
aliases:
- ../announce-3.5.8
custom_about: true
date: '2007-10-16'
title: KDE 3.5.8 tillkännagivande
---

<h3 align="center">
   KDE-projektet släpper åttonde översättnings- och serviceversionen av den 
   ledande fria skrivbordsmiljön.
</h3>

<p align="justify">
  KDE 3.5.8 innehåller översättningar till 65 språk samt förbättringar i 
  KDE PIM och andra applikationer.
</p>

<p align="justify">
  <a href="/">KDE-projektet</a>
  tillkännager idag KDE 3.5.8, en serviceversion av senaste generationen av den
  mest avancerade och kraftfulla skrivbordsmiljön för GNU/Linux och andra UNIX.
  KDE innehåller nu översättningar till 65 språk, vilket gör den tillgänglig för
  fler människor än de flesta proprietära mjukvaror och kan enkelt utökas till
  fler språk av de som vill bidra till det öppna källkodsprojektet.
</p>

<p align="justify">
    Även om utvecklarnas fokus just nu ligger på att färdigställa KDE 4.0, så är 
    fortfarande den stabila 3.5-serien skrivbordsmiljön den som används än så länge. 
    Den är bevisat stbil och väl understödd. KDE 3.5.8 har därför 
    vidareutvecklats med hundratals buggrättningar för att förbättra 
    användarnas upplevelse.
    Huvudpunkterna i KDE 3.5.8 är:
        
  <ul>
      <li>
      Ett antal buggrättningar i KHTML.
      Det är framför allt hanteringen av HTTP anslutningar som har 
      förbättrats, men 3.5.8 innehåller även utökat stöd för vissa CSS
      finesser för att bli mer kompatibla med standarder.
      </li>
      <li>
      I kdegraphics har KPDF (KDE's pdf-visare) och ritprogrammet Kolourpaint
      fått en hel del rättningar.
      </li>
      <li>
      KDE PIM sviten har, som vanligt fått en del stabilitetsfixar, inklusive
      KDE's epostklient KMail och organiseraren KOrganizer. Även en del andra
      smårättningar har gjorts.
      </li>
  </ul>
 
</p>

<p align="justify">
  För en mer detaljerad lista över förbättringarna sedan
  <a href="/announcements/announce-3.5.7">KDE 3.5.7</a>
  från den 22e maj 2007 se
  <a href="/announcements/changelogs/changelog3_5_7to3_5_8">KDE 3.5.8 Ändringslogg</a>.
</p>

<p align="justify">
  KDE 3.5.8 levereras med en grundläggande skrivbordsmiljö och femton tilläggspaket
  (PIM, administration,  Nätverk, utbildning, hjälpmedel, multimedia, spel,
  grafik, webbutveckling med mera). KDE's prisbelönta verktyg och
  applikationer är tillgängliga på <strong>65 språk</strong>.
</p>

<h4>
  Distributioner med KDE
</h4>
<p align="justify">
  De flesta Linuxdistributioner och UNIX inkluderar inte nya versioner av KDE
  omedelbart men kommer att integrera KDE 3.5.8 in sin nästa version. Besök 
  <a href="/distributions">denna lista</a>
  för att se vilka distributioner som inkluderar KDE.
</p>

<h4>
  OM KDE-PROJEKTET
</h4>
<p align="justify">
  KDE-projektet består av hundratals utvecklare, översättare, grafiker
  och andra medverkande över hela världen som samarbetar via
  Internet. Gemenskapen skapar och distribuerar fritt en stabil,
  integrerad och fri skrivbords- och kontorsmiljö. KDE tillhandahåller
  en flexibel, komponentbaserad, nätverkstransparent arkitektur och
  kraftfulla utvecklingsverktyg, som tillsammans erbjuder en enastående
  utvecklingsplattform.
</p>

<p align="justify">
  KDE, som är baserat på Qt-teknologi från Trolltech, är ett levande bevis på
  att utvecklingsmodellen av programvara med öppen källkod enligt "basarstilen"
  kan resultera i förstklassig teknologi likvärdig eller överlägsen den den mest
  komplexa slutna programvara.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Varumärkesnotiser</em>
  KDEs och K Desktop Environments logotyp är av KDE e.V. registrerade varumärken.

Linux är ett av Linus Torvalds registrerat varumärke.

UNIX är ett av The Open Group registrerat varumärke i USA och andra länder.

Alla andra varumärken och rättigheter refererade i detta dokument tillhör respektive ägare.
</font>

</p>

<hr />
