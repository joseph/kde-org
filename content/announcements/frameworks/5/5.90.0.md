---
qtversion: 5.15.2
date: 2022-01-08
layout: framework
libCount: 83
---


### Baloo

* [kioslaves/tags] Set proper display name for root entry (bug 416389)

### BluezQt

* Expose missing services to QML

### Breeze Icons

* Support old cmake
* Add places-book, -library, -comic icons
* Include "*@*" in the icon_files list for installation
* Make FM and system settings icons color-aware
* Improve installation of light fallback icons

### Extra CMake Modules

* Add support for finding Poppler's Qt6 library
* Add support for finding Qt6 QML modules
* Add missing prefix to version-less install directory aliases
* FindInotify.cmake: add target support
* Define version-less install targets in KDEInstallDirs5.cmake

### KArchive

* Fix printf conversion specifiers

### KAuth

* Use version-less and non-deprecated data install dir variable
* ActionReply: remove operator<< and operator>>

### KBookmarks

* [kbookmarksmanager] Don't recreate static QRegularExpression in findByAddress

### KCalendarCore

* ICalFormat: reduce calls to dtStart()/dtEnd(), use the local vars
* Fix timezone information being lost when creating events

### KCMUtils

* Wrap deprecated KDelarative::ConfigModule::aboutData method call in deprecation wrappers
* Deprecate KPluginSelector class
* Allow KCMUtils to build without deprecated KCoreAddons methods
* Update showNavigationButtons to use enum type
* Fix PluginPage deprecation docs

### KConfig

* kconfig_compiler/CMakeLists.txt - use CMAKE_CROSSCOMPILING for the check
* Allow kreadconfig/kwriteconfig to access the root group

### KConfigWidgets

* Fix conditions used in cmake.in config file
* [kcolorschememodel] Read .colors files from assets on Android
* Sync default colors from Breeze Light color scheme
* kcommandbar: Don't warn if action is separator
* [kcmodule] Deprecate aboutData and componentData
* [KCommandBar] Fix resetting m_hasActionsWithIcons

### KCoreAddons

* Introduce K_PLUGIN_CLASS macro for creating plugin factory without metadata
* Don't hardcode kservicestypes5 as search path
* Put QT_VERSION check around Kdelibs4Migration class
* FindProcstat: Fix CMake warning and use an imported target
* Don't put the plugins in a "/plugins" sub-dir in the builddir
* KPluginFactory: report errors from QPluginLoader
* KF5CoreAddonsMacros: Replace "." with "_" for KPLUGINFACTORY_PLUGIN_CLASS_INTERNAL_NAME compile definition
* Clarify deprecation docs of KPluginLoader::factory
* Write value of QT_MAJOR_VERSION in cmake config file

### KDBusAddons

* Fix conditions used in cmake.in config file
* API docs: improve docs for D-Bus activation and startup id handling

### KDeclarative

* Drop lib prefix when building for Windows
* Follow method name changes in kglobalaccel
* Use "frameless" style for grid and scroll view QtQuick KCMs by default
* Detect overlapping shortcuts

### KDELibs 4 Support

* Remove ugly white frame from beautiful Latvian flag

### KDocTools

* Don't compare signed with unsigned int

### KDE GUI Addons

* Add KIconUtils::addOverlays() overload to allow passing QIcon and QStringList (bug 447573)
* KSystemclipboard: add a nullcheck (bug 447060)
* WaylandClipboard: Do not emit change upon the offer for our own source

### KHolidays #

* holiday_us_en-us - add Juneteenth

### KI18n

* API dox: fix KUIT tags examples to use xi18n* calls

### KIconThemes

* KIconColors: add Complement and Contrast
* KIconColors: add .ColorScheme-ActiveText

### KIO

* RenameDialog: Don't show size/dates if we don't know them
* Add PreviewJob::availableThumbnailerPlugins method
* kdirmodel: Return "unknown" icon when the icon is null (bug 447573)
* Finish PolKit integration
* Fix hidden NTFS mountpoints when /etc/mtab is a regular file
* KFileItemActions: use different string for 'open folder with'
* Deprecate Scheduler::setJobPriority
* Fix desktop link modification dialog
* Do not access service past its prime (bug 446539)
* [KFilePlacesModel] Drop redundant "entry" name from context menu
* [knewfilemenu] Hide progress info for stat jobs
* [KPropertiesDialog] Fix porting bug to KLazyLocalizedString
* [KFilePlacesView] Drop redundant "entry" name from context menu
* [kfilepropertiesdialog] Don't show UI for default apps on Windows
* Fix warning when showing preview whose parent is /
* Revert "Do not create thumbnails when it requires to copy the file to /tmp"
* Port from KPluginMetaData::readTranslatedValue to KJsonUtils::readTranslatedValue
* [OpenUrlJob] Don't just open `.part` files
* De-duplicate service menus from KServiceTypeTrader and KFileUtils results
* KCoreDirLister: in slotEntries pre-sort new items added and add them more efficiently

### Kirigami

* WheelHandler: Improve consistency with scrolling in Qt Widgets, add more properties
* Fix navigation buttons on layers
* FormLayout: Switch an instance of let to var
* ShadowedRectangle: Add renderType option
* AboutPage: show spinner feedback while loading remote icon
* AboutPage: Only mess with the URL when we are dealing with KDE products (bug 444554)
* AboutItem: don't multiply sizes by devicePixelRatio
* AboutItem: Fix incorrect usage of height: and width: inside layouts
* OverlaySheet: Modify anchors on tall headers imperatively
* globaltoolbar: Use strict === equality in JavaScript
* PageRowGlobalToolBarUI: don't animate opacity, take two (bug 417636)

### KItemModels

* Support numeric sort and filter roles next to role names
* Forward removeRow(s) to QML

### KJobWidgets

* KUIServerV2JobTracker: Add "transient" property support

### KNewStuff

* Drop lib prefix when building for Windows (bug 446950)
* Remove defunct manual khotnewstuff_upload test
* Revert "Adapt build system for building against qt6" (commited in bad
* Revert "Add volker fix  about cmake variable" (commited in bad branch)
* Fix i18n* functions for knewstuff-dialog not existing
* Move KNS3::Action class to new KNSWidgets submodule
* New class: KNS3::Action

### KNotification

* Use the org.freedesktop.Notifications.ActivationToken signal
* Unify WITH_KWINDOWSYSTEM and HAVE_KWINDOWSYSTEM build vars
* Offer API to support xdg_activation_v1

### KPackage Framework

* Add a service type property definition for NoDisplay

### KParts

* partviewer test: Add assertion to make sure we load the plugin factory successful
* Increase KF_DISABLE_DEPRECATED_BEFORE_AND_AT to latest released frameworks version
* Port deprecated KFileItemActions::associatedApplications method call
* Port deprecated KService::instantiatePlugin method call
* Call KPluginFactory::create overload without plugin keyword
* Deprecate KParts::Plugin class
* Port KPart template away from deprecated KPluginLoader
* Port from KPluginLoader::findPlugins to KPluginMetaData::findPlugins

### KRunner

* Do not require Plasma when building without deprecations
* dbusrunner: Set parent for matches to the current runner
* Allow runners to opt-out storage of entry to history
* RunerContext: Allow runners to update the query string (bug 433636)

### KService

* Wrap KServiceTypeTrader methods to create instances also in KCOREADDONS visibility guard
* Do not use toLower on desktopEntryName
* Expand deprecation docs for KServiceTypeTrader
* Deprecate KServiceTypeTrader class
* Emit deprecation warning for KServiceType class
* Deprecate KPluginInfo in favor of KPluginMetaData

### KTextEditor

* Add a formatting commit to ignore-list
* try to fix behavior for vimode on completion (bug 444883)
* Change build system to make building against qt6
* Apply word filter on async completion models (bug 444883)
* Validates the input method attributes received from input method (bug 443977)
* Remove unused exporting of SwapFile class symbols
* Fix cursor position after completion tail restore
* Color current indentation line differently
* Vimode-keyparser: Make some functions more efficient
* Use KTextEditor::Range by value

### KUnitConversion

* Prepare KUnitConversion::UnitCategory to become non-virtual in KF6
* Prepare for KUnitConversion::Unit to become non-virtual in KF6
* Fix cache file removal in valuetest
* Fix cyclic reference between Unit and UnitCategory

### KWallet Framework

* Add desktop file for kwalletd
* Properly apply KAboutData
* Fix notifyrc name

### KWayland

* Make linux/input.h a hard dependency

### KWidgetsAddons

* Fix year format in heading of KDatePicker
* Localise numbers displayed in KDatePicker
* [KMessageWidget] Ignore resize event when doing `animatedShow()`

### KWindowSystem

* Avoid using QByteArray::operator[] for the null terminator (bug 434557)
* Avoid creating and leaking QWindows

### KXMLGUI

* Fix nested @ expansion in CMake config file
* [KToolBar] Add actions from delayed toolbutton menus to context menu
* Do not steal all keys from shortcut

### Plasma Framework

* plasmoidheading: make corners consistent, improve top line color
* PC3 ItemDelegate: Actually use ItemDelegate type
* Add a destroy method to the view
* PC3 ProgressBar: fix fill going OOB when indeterminate state ends (bug 428955)
* Fix background corners and PC3 ToolTip style (bug 442745)
* fix availableScreenRect for applets/containments (bug 445495)
* widgets/tasks.svgz: Use more saturated colors for the focus and attention states (bug 434821)
* When adding a new applet to a desktop containment, add it to the center
* wallpaperinterface: Update "contextualActions" after clearing actions (bug 446195)

### Purpose

* [imgur] Improve grammar of upload notification text
* [imgur] Show deletion url in notification (bug 441566)
* [imgur] Copy link to clipboard and show notification (bug 437347)
* Don't build bluetooth plugin on non-Linux
* Unbreak the Nextcloud plugin
* Add Twitter URL plugin

### QQC2StyleBridge

* TextFieldContextMenu: Fix menu not opening
* Add A SpinBox test

### Solid

* udisks backend: don't assume /etc/mtab is not present
* Remove trailing \x00 from string returned by Q6File::decodeName()

### Sonnet

* Don't access an out of bounds index into a QString

### Syntax Highlighting

* Systemd unit: update to systemd v250
* Separate dynamic StringDetect rule to avoid copies in StringDetect::doMath()
* Automatically replace StringDetect to DetectChar, Detect2Chars or AnyChar when possible
* Very basic support for [[link]] and [[link][desc]]
* support implicit link in normal text
* SQL and SQL (PostgreSQL): nested comments support (bug 442449)
* GnuPlot: fix a lot of issue (bug 442181)
* PHP: add readonly, never and some functions/classes/constants of php-8.1
* Bash and Zsh: support of ${!2} (bug 440360)
* Bash: more unixcommands (GNU coreutils and some others) (bug 439211)
* Fix language specification comments
* Rename MIME type text/x-objcpp-src => text/x-objc++src
* Add Homuncuius.theme
* Remove rawhtml, not needed
* Add grammar for RETRO Forth

### ThreadWeaver

* Use fetchAndStoreOrdered() instead of fetchAndAddOrdered()
* Fix invalid lambda argument for use in std::for_each

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
