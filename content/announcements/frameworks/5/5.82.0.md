---
aliases:
- ../../kde-frameworks-5.82.0
qtversion: 5.15
date: 2021-05-08
layout: framework
libCount: 83
---


### Baloo

* Fix baloo_file autostart with systemd
* Move baloo_file, baloo_file_extractor to libexec
* Properly shutdown extractor process
* Port extractorprocess to WorkerPipe/ControllerPipe helper classes (bug 435521)
* [XAttrIndexer] Avoid unnecessary reindexing on XAttr changes
* Cleanup/extend mimetypes for Archive document type
* Mark Kindle mobipocket variant as type Document
* Mark various comicbook variants as "Document"
* Do not crash when a dir is move/delete is detected by kinotify (bug 435143)
* When adding a folder to configuration, normalize the path semantically (bug 434919)
* Expose IndexerState enum to QML
* [DocumentUrlDB] Remove bogus child sanity check

### Breeze Icons

* New Sidebar Show/Hide Icons
* Revert "Added branches with leaves to Kmymoney icon"
* Add financial-budget icon
* Don't attempt to validate icons when cross-compiling

### Extra CMake Modules

* ECMGenerateExportHeader: add macros for enumerator deprecation warning
* Fix static Windows builds by not setting /NODEFAULTLIB:libcmt /DEFAULTLIB:msvcrt
* Fix installation of qm files from po files in the build directory
* Document the advanced APK packaging options
* Add a way to pass additional arguments to androiddeployqt

### KArchive

* Fix cmake detection for tar+zstd
* Implement KZstdFilter

### KCalendarCore

* Don't export UTC timezone as TZID=UTC

### KCMUtils

* [KCModuleProxy] Refactor loadModule() for simplicity
* Remove ComponentsDialog class, not used anywhere
* Deprecate unused KSettingsWidgetAdaptor class

### KConfig

* Make the docs state that KConfig::reparseConfiguration() calls sync() if needed
* Use new version-controlled enumerator deprecation warning macros
* Deprecate KDesktopFile::readDevice()

### KConfigWidgets

* Use new version-controlled enumerator deprecation warning macros
* Un-overload KConfigDialogManager::settingsChanged() signals

### KContacts

* Call ki18n_install() unconditionally

### KCoreAddons

* KPluginMetaData::instanciate to print the error when it fails
* Call ecm_install_po_files_as_qm() unconditionally
* Add SMB2 and CIFS magic numbers
* KFuzzyMatcher update

### KDBusAddons

* Remove obsolete license file
* Relicense file to LGPL-2.0-or-later

### KDeclarative

* Add the ContextualHelpButton from some kcms in this import
* [GridDelegate] Show inline controls for active item too
* Make GridView KCMs handle keyboard focus in an accessible manner

### KFileMetaData

* Use new version-controlled enumerator deprecation warning macros

### KGlobalAccel

* Use kstart5 to start the processes, when available
* Remove duplicated code to start a process
* Prevent kglobalaccel5 getting activated on non-Plasma systems (bug 430691)
* [runtime] Avoid service restarts when the X connection is unexpectedly removed

### KDE GUI Addons

* Port from deprecated QFontMetrics::charWidth() to horizontalAdvance()
* Add KIconUtils::addOverlays() overload to replace KDE::icon()

### KHolidays #

* add holidays for srilanka in sinhala
* Replace moon phase calculation code

### KHTML

* Remove usage of the register keyword
* Let gperf generate C++ code

### KIconThemes

* Use deprecation macros for KIconLoader::alphaBlending()
* Use new version-controlled enumerator deprecation warning macro
* Deprecate KIconLoader::Panel in KF6

### KImageFormats

* Support building with OpenEXR 3
* xcf: fix new[]/delete mismatch, as detected by ASAN
* ani: convert +1 to -1 so we don't do a potential integer overflow

### KIO

* kcm/webshortcuts: Show icons for web providers
* KShortUriFilter: Use https:// instead of http:// as default protocol
* KFilePlacesView: Make icon palette match the widget palette
* Use new version-controlled enumerator deprecation warning macros
* Deprecate KFileItemActions::preferredOpenWithAction
* Port KFileItemActions insertOpenWithActionsTo away from trader constraints
* Always compile dataprotocol ioslave to be run "in-process on the client side"
* KFileItemActions: Deprecate desktop file plugin loading of KFileItemActions
* Proxy KCM: Show a warning when a KIO-specific setting is selected
* Don't forward declaration when we already include files
* Make the openOrExecute dialog modal (bug 430396)
* ChmodJob: port to AskUserActionInterface
* Deprecate the remnants of FSDevice-related code
* Use QStyle::PixelMetric() instead of hardcoding the window frame width
* kfileitemactions: Keep KAbstractFileItemActionPlugin reusable during lifetime of object
* Add error signal to KAbstractFileItemActionPlugin
* Remove KDevicePropsPlugin from KPropertiesDialog
* KDirOperator: show the progress bar above the horizontal scrollbar (bug 194017)
* kio_trash: replace KDiskFreeSpaceInfo with QStorageInfo
* FileProtocol: port from KDiskFreeSpaceInfo to QStorageInfo
* When changing requestMimeTypeWhileListing to on, clear cached items
* Add an option to allow slaves to determine mime type when using KIO::listDir
* CommandLauncherJob: add setProcessEnvironment(QProcessEnvironment)
* OpenUrlJob: don't execute .exe files (via WINE) if running executables is forbidden
* Fix assert in the kioslave executable when putting a slave on hold
* When comparing KFilItem's, also compare user and group
* KPropertiesDialog: call abortApplying() in all plugins where needed
* Core/Gui split for KUriFilterPlugin
* Simplify User-Agent code to remove KService/KParts dependency
* [KFilePlacesItem] Avoid calling KMountPoint::findByPath()
* Fix memory leak in kpasswdserver, caught by ASAN
* Add KCoreDirLister::jobError signal
* Add error handling to CommandLauncherJob

### Kirigami

* SearchField: Stop timer if the user accepted manually
* Icon: make sure we don't call a nullptr reply (bug 423183)
* [org.kde.desktop/Units] Use FontMetrics for fontMetrics
* Add KDE Store avatar and profile links to AboutPage
* Fix broken static build
* Static builds: Let qmlimportscanner find kirigami
* [OverlaySheet] Fix click to close (bug 435523)
* [BasicListItem] Don't unconditionally apply leading and trailing padding (bug 435784)
* Fix PageRow.replace() when PageRow is empty
* Fix deployment of static builds on Android
* Embrace the usage of categorised debugging methods
* [ListSectionHeader] Unset deprecated supportsMouseEvents: property
* [ListSectionHeader] use named Kirigami import
* controls/SizeGroup: shield against nullptr qmlContext (bug 435593)
* Fix richTextLabel for the initial value of CJK style label
* Switch some arbitrary numbers to the humanMoment unit
* Add a note about humanMoment not being for animation durations
* ColumnView: do not enable animation in setCurrentIndex
* Do not force active focus in ColumnView::replaceItem()
* Rewrite columnView::replaceItem()
* PageRow: use columnView.replaceItem()
* PageRow: split pagesLogic.initAndInsertPage into several functions
* Implement ColumnView::replaceItem
* Revert "disable animations during replace"
* Fixes restoremode for binding
* Only send one accepted signal when activating the clear action

### KJobWidgets

* KUiServerV2JobTracker: Send initial job state along

### KJS

* Remove usage of register keyword

### KNewStuff

* Deprecate quickengine changedEntries & related methods
* Do not show error when installation is canceled (bug 436355)
* NewStuff Dialog: Forward entryEvent signal
* NewStuff Button: Forward entryEvent signal
* Make QML GHNS dialog a tiny bit wider (bug 435547)
* ensure .desktop files are executable

### KNotification

* Allow to build KNotifications without QtWidgets on Android
* Remove the taskbar and external process backends on Android
* Don't install the notification plugin servicetype file on Android

### KPackage Framework

* Remove kpluginindex.json caching

### KParts

* Deprecate Part::iconLoader
* Use new version-controlled enumerator deprecation warning macros

### KPeople

* Make DBus dependency on Windows and Mac optional

### KQuickCharts

* Trigger dataChanged when a Chart's colorSource's data changes
* Fix LegendModel requesting not exising items from sources
* Correctly signal that ShortNameChanged when updating
* Drop item creation from Legend and rename it to LegendLayout
* Rework LegendDelegate to be a Control
* Add a Logging item for easier deprecation messages in QML
* Add preferredWidth to LegendAttached
* Use decorations.Legend as central item for Controls Legend
* Add a Legend item to decorations
* Add a dataChanged signal to Chart
* Convert AxisLabels to use ItemBuilder
* Introduce ItemBuilder as a generic C++ Repeater equivalent

### KRunner

* Allow runners to prevent duplicate results (bug 406292)
* RunnerManager: Do not set the enabledCategories by default
* querymatch: Add property to display text as multiline string (bug 395376)
* runnermanager: Deprecate singleModeAdvertisedRunnerIds and runnerName
* runnermanager: Allow loading runners in single runner mode if they are disabled (bug 435050)
* RunnerManager: Deprecate single runner mode related methods
* Allow single runners to be specified only using the launchQuery overload
* dbus runner: Fix interface definition (bug 433549)
* runnermanager: Remove logic for deferred runs
* querymatch: Deprecate unused getter & setter for mimeType

### KService

* Deprecate KMimeTypeTrader
* Deprecate KPluginTrader
* Properly deprecate some KToolInvocation methods
* Include X-KDE-Wayland-VirtualKeyboard

### KTextEditor

* Fix memory leaks
* Make KTextWidgets dependency explicit
* Fix dragging when folding stuff is around (bug 435500)
* When there is no selection, disable selection-only search
* Use a MovingRange for remembering the selection search range
* Fix incremental selection-only search (bug 327408)
* Attempt fix crash on dragging (bug 435500)
* Always do resizing and limit width to 1/2 of screen
* Make argumentHintTree border less bright and more like Kate::LSPTooltip
* Make completionWidget border less bright and more like Kate::LSPTooltip
* fix memory corruption on 'Close the file, discard..' (bug 433180)

### KWayland

* Add VRR to output device and management
* Use new version-controlled enumerator deprecation warning macros
* Add support for overscan
* Bump plasma-wayland-protocols dependency to 1.2.1
* Bump required PlasmaWaylandProtocols

### KWidgetsAddons

* KPageDialog: document automagical connections
* Enable Unicode support in QRegularExpression where needed
* Remove pointless tooltip for yes and no KStandardGuiItems (bug 424658)
* KPageDialog: make it clearer that a button box is added by default

### KWindowSystem

* Deprecate present windows and highlight windows apis
* Deprecate WId-based effects API
* Use new version-controlled enumerator deprecation warning macros
* Deprecate KWindowEffects::windowSizes

### KXMLGUI

* Fix initial window size calculations
* Don't build with KGlobalAccel on Windows, Mac, and Android

### ModemManagerQt

* Port ModemManagerQt away from QMap::unite

### Plasma Framework

* Add up to 5 event indicators to the DayDelegate
* Port Plasma::Dialog to QWindow-based effects API
* Fix PC3.GroupBox.color
* ContainmentInterface: Set transient parent of submenues (bug 417378)
* Make the size of the task manager margins as big as the panel ones (bug 434324)
* Redesign the Plasma Calendar applet
* [Wayland] Ensure pinned popup end up on the panel layer (bug 400317)
* [ExpandableListItem] Fix determination of the number of list items
* Fix month and year not getting updated after reseting date to today
* Fix PC3 TabButton alignment
* Port to singleton Theme
* Fix elide of TabButtons (bug 434945)
* [widgets/listitem] Increase left/right margins to match top/bottom margins

### Purpose

* FileItemActions: Use new signal to display errors

### QQC2StyleBridge

* Draw icon in QQStyleItem (bug 433433)
* [CheckBox] Rewrite to be nearly identical to RadioButton (bug 435934)
* [RadioButton] Correct typo
* Keep context menu above other popups, use dismiss() (bug 428631)

### Solid

* Do not search for DBus on Windows & Mac

### Sonnet

* Fix assert when bringing up the contextmenu after starting a compose seyquence

### Syntax Highlighting

* debchangelog: add Impish Indri
* remove text/plain from Godot
* systemd unit: merge some RegExpr rules
* systemd unit: increase version number
* systemd unit: update to systemd v248
* add support for Godot's gd-scripts
* Bash/Zsh: (#5) fix SubShell context that starts as an arithmetic evaluation ; add regions for all SubShell
* Doxygen: fix Word style when the word is at the end of line

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
