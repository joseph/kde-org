---
qtversion: 5.15.2
date: 2022-08-14
layout: framework
libCount: 83
---


### Attica

* Pass the HTTP status code as the MetaData status code
* Properly detect failed jobs

### Breeze Icons

* Add mimetypes/{16,22,32,64}/application-x-msdownload, which is used for DLLs
* Remove monochrome versions of help-related app icons (bug 456737)

### Extra CMake Modules

* kde-modules/KDEGitCommitHooks.cmake - handle non-toplevel project

### KActivities

* Utilize std::optional instead of custom impl
* Remove debug message for Consumer destructor

### KArchive

* k7zip: Merge two functions to the constructor

### KCalendarCore

* Deprecate deletion tracking support in CalFormat::fromString
* Add plugin loader for KCalendarCore::CalendarPlugin

### KCMUtils

* follow symlinks for search of .desktop files
* tools/kcmmetadatagenerator: Print out critical warning if file could not be opened
* Make CMake import target of kcmdesktopfilegenerator relocatable
* Add utility cmake method to generate KCM metadata desktop files

### KCompletion

* KCompletion: clean-up private class
* KCompletion*: general clean-up

### KConfig

* Don't inherit from containers

### KConfigWidgets

* [KCommandBar] Improve position and size
* follow symlinks during search for help
* Avoid tracking dangling KConfigDialogManager (bug 456630)

### KCoreAddons

* Deprecate KMessage system
* KUserGroup: fix data race by porting from getgrgid to getgrgid_r
* KUserGroup: centralize calls to getgrgid(), at least for constructors
* KUser: fix data race by porting to getpwuid_r and getpwnam_r
* KUser: use member initialization, call getpwuid in a single place
* Add KRuntimePlatform as alternative for KDeclarative::runtimePlatform
* Bump shared-mime-info to 1.8
* make kprocess a more proper qprocess
* new ksandbox function to start processes
* Adapt libexec paths for KF6

### KDeclarative

* GridDelegate: Improve accessibility for inline buttons

### KDELibs 4 Support

* Remove unused X-KDE-ModuleType
* Convert file to UTF-8

### KDocTools

* Add Han Young to the contributors list

### KFileMetaData

* Add PNG extractor
* dump tool: Print extractor name

### KGlobalAccel

* GlobalShortcutsRegistry: don't use a QHash for a couple of elements

### KDE GUI Addons

* QtWaylandClient is required when building with Wayland support

### KHolidays #

* Use non-deprecated API unconditionally everywhere
* Implement deprecated API by calling the new one
* Make rawHolidays() with category filter also work with seasonal holidays
* Actually implement HolidayRegion::rawHolidays()
* Actually include seasonal holidays in rawHolidaysWithAstroSeasons()
* Add methods to check for polar days/nights

### KIconThemes

* Do not use QIcon::setThemeName to set system theme

### KImageFormats

* Use right type on enums
* PSD: Improve alpha detection (bug 182496)
* PSD: LAB support

### KIO

* ScopedProcessRunner: Set scope name according to X-KDE-AliasFor
* Template: have KIO worker bundle name match full lingo
* Use QDateTime::fromSecsSinceEpoch instead of fromMSecsSinceEpoch(1000 *
* KFileItem: save a useless multiplication
* KRecentDocument: better prevent duplicate bookmark for same href (bug 456046)
* KRecentDocument: Improve compat with Gtk File Picker (bug 456046)
* Port kio_remote to WorkerBase
* FileUndoManager: use emitRenaming where appropriate
* FileUndoManager: fix job description when undoing a batch-renaming (bug 437510)
* fix x-kde-protocol(s) filters for json based kpropertiesdialog plugins
* Deprecate AskUserActionInterface::Sorry
* Fix WidgetsAskUserActionHandler::requestUserMessageBox's "Don't ask again"
* [KUrlNavigatorToggleButton] Load accept icon on demand
* KPropertiesDialog: Handle non-existing file type in default handler label
* KPropertiesDialog: Update enabled state for default handler label
* KUrlNavigatorButton: use a struct instead of QPair
* KFileItemActions: use a struct instead of QPair
* kio_http: Use a struct instead of QPair
* HostInfo: Use a struct instead of QPair
* KFileItemActions: Add icon to "Open With..." action
* port sandbox detection to ksandbox
* [kfileitemactions] Pass parentWidget to JobUIDelegate
* Rename view action and set its icon
* Fix crash in DropJob (bug 454747)
* Fix thumbnailer result for parent mime types being overwritten (bug 453480)
* Port kio_help to WorkerBase
* [knewfilemenu] Deprecate setViewShowsHiddenFiles
* KDirOperator: only show preview on mouse hover
* KImageFilePreview: if no current preview, don't show last one on resize (bug 434912)
* KImageFilePreview: only create QTimeLine member if needed
* [knewfilemenu] Replace setPopupFiles with setWorkingDirectory
* [KFilePlacesView] Fix condition for pointIsTeardownAction
* Switch template to KIO worker
* [BatchRenameJob] Add job description and report periodically
* Adapt libexec search path to KF6
* KFileWidget: remove location history dummy-entry related code (bug 452994)
* Fix logic error in Qt6 offline detection

### Kirigami

* CardsGridView: enable Home/End key navigation
* CardsListView: enable Home/End key navigation
* Don't use an alias and expose directly the children property
* Rework CardsGridView leftMargin calculation
* Use custom header for setting window
* Deprecated view headers
* Fix floating action button icon alignment when there is no main action
* OverlaySheet: add workaround for Qt horizontal scrollview bug (bug 457223)
* Also handle QML url value types in PageRow
* Try to depend on the newer QQC2 version possible (bug 451240)
* workaround on items without implicit size
* libkirigami/units: Fix wrong implementation of property setter and getter of veryShortDuration
* fix horizontal alignment for titleLayot
* Improve Kirigami.Chip description and use case
* PromptDialog: Change its content label to selectable counterpart
* Add icon to UrlButton menu
* Improve action name in UrlButton menu
* SizeGroup: Hide implementations details (static list operations) as private

### KJobWidgets

* ui-server: Fix crash by only re-registering live KJobs (bug 450325)

### KJS

* Convert file to UTF-8

### KJSEmbed

* kjscmd5 is nongui

### KNewStuff

* [kmoretools] Don't lowercase the desktop entry name (bug 417575)
* Fixup 6d889ae0: move newlines out of translated string
* Remove unneeded close button from window footer
* Add a "try again later" message for transient errors

### KNotification

* Remove notificationplugin service type
* Drop lib prefix when building for Windows (MinGW)
* use ksandbox instead of hardcoding sandbox detection

### KPackage Framework

* Fix listing of KPackage structures (bug 456726)
* README.md add dot at end of sentence

### KPeople

* Don't install service type definitions

### KPty

* add the ability to disable ctty set up

### KQuickCharts

* Remove arcconfig
* BarChart: de-duplicate some code
* Remove an unused method

### KRunner

* Add note about alternative to Plasma::RunnerContext::setSingleRunnerQueryMode
* templates/runnerpython: Add hint for multiline property
* Fix Qt6 build

### KService

* Add property def for X-Flatpak key
* Define HAVE_MMAP to 0 instead of undefining it (bug 456780)
* KSycocaDict: don't inherit from containers
* use ksandbox to determine if we are inside a flatpak

### KTextEditor

* fix crash (bug 457392)
* Pass parent widget to print preview dialog
* fix crash in insertText
* Make views for read-only themes read-only instead of disabling them
* Cleanup: Remove unused members from KateStyleTreeWidget
* Cleanup: use switch in defaultStyleName
* Make defaultStyleCount reusable
* Silence compiler warning when compiling without KAUTH
* avoid crash on caret drawing if no view (bug 456508)
* avoid accessing view pointer during printing (bug 456508)
* remove unused library

### KTextWidgets

* Fix heading level line break handling with Qt 6.3

### KWallet Framework

* Remove duplicate header between cpp/h
* Do not try to rename label twice in entryRenamed()
* Do not create EntryLocation with empty key
* Introduce Secret Service API

### KWayland

* Unset wl_display global filter upon filtered display destruction

### KWidgetsAddons

* [KMessageDialog] Emit notification sound just like KMessageBox
* Make OK button configurable in KMessageBox::error/detailedError
* Deprecate KMessage*::sorry
* Remove declaration of unimplemented sorryWId(buttonOk) overload
* Deprecate unsupported & unused KMessageBox::PlainCaption flag
* Deprecate KMessageBox::about()
* Add edit-clear-history icon to the Clear List action

### KWindowSystem

* Doc: Update WM spec URL

### KXMLGUI

* [KShortcutsDialog] Make it possible to reload shortcut schemes
* [KShortcutsDialog] Make it possible to add a custom edit action
* [KShortcutSchemesEditor] Fix bug introduced by refactoring in commit de0790fe
* Fix crash of KMix in Legacy Tray
* [KShortcutsDialog] Add icons for actions
* KKeySequenceWidget: don't use a QHash to hold a few elements

### Plasma Framework

* Update blur and other window effects when the dialog changes size (bug 457053)
* Fix generated import versions for QtQuick Controls with Qt 6
* ExpandableListItem: read `Accessible.description` from action
* ExpandableListItem: give expand button a name and a tooltip
* ExpandableListItem: add accessible properties
* PC3/ToolTip: Sync to qq2-desktop-style: Copy delay & timeout implementations
* PC3/ToolTip: Sync to qq2-desktop-style: Port to QtQuick.Layouts
* PC3/ToolTip: Sync to qq2-desktop-style: Wrap text on label
* PlaceholderMessage: use strict Javascript equality
* Scope DialogShadows lifetime to application (bug 443706)
* Use non-deprecated URL interceptors access with Qt 6
* QMenuProxy::transientParent add null checks

### Prison

* Try lower ECC levels if we exceed the maximum QR code content size
* prison-datamatrix unused local variable
* Support ZXing 1.4.0

### QQC2StyleBridge

* Install style as a regular QML for Qt6
* ToolTip: Sync to PC3: Strip out ampersands
* ProgressBar,Slider: Adapt great precision to the harsh reality (bug 455339)
* Fix QQC2 MenuBar theme (bug 456729)
* properly reset the cursorshape (bug 456911)
* Use QApplication::font with respective widget class
* Wrap checkbox label
* ToolTip: wrap text for long tooltips
* TextArea,TextField: Remove not referenced id from background style item
* TextField: Disable Undo for password fields, including its context menu
* Remove dynamic QQC2 version selection
* Fix: url types (like icon.source) are QVariant without length property
* Nit: reformat long expressions
* Menu: Fix wrong pixel metric being requested for verticalPadding
* ProgressBar: Drop layout direction hack

### Solid

* [UDisksDeviceBackend] Port introspect to QXmlStreamReader
* [UDisksDeviceBackend] Remove pointless QDBusInterface
* udev/cpuinfo_arm: add more Apple part IDs
* udev/cpuinfo_arm: change Apple part formatting
* Drop lib prefix when building for Windows (MinGW)

### Sonnet

* Port to ECMQmlModule
* Drop lib prefix when building for Windows (MinGW)
* fix obsolete string

### Syntax Highlighting

* Jam: add block comments
* Bash: add globany in Option ; add comment in [[ ... ]] and other fixes for [[ ... ]]
* Zsh: add comment in [[ ... ]] and other fixes for [[ ... ]]
* add CommonJS module and ECMAScript module extension for JS and TS
* Add tool for matching lexers
* Fix code regex and bump files version
* Another bunch of languages hand mapped between minted and KDE
* Fix some issues with code colouring when syntax was broken
* Fix embedded lua code highlighting
* Fix for lstlisting bug which prevented proper recognition of language
* Fix regression for lstlisting
* Add support for multiple minted and lstlistings languages
* add rockspec file to Lua
* Markdown: use zsh syntax for zsh code
* Zsh: fix variable and string in a glob qualifier
* Scheme: fix combined prefix number
* add basic support of aspx and ashx (bug 437220)
* Batch: fix many issues and add tests
* R: add := operator (bug 450484)
* Lua: new styles (Special Variable and Self Variable), remove hard color and fix special keywords in BlockComment
* Disable spellcheck in systemd unit Files for non text Data
* Disable spellcheck in SGML Files for non text Data
* Disable spellcheck in Perl Files for non text Data
* Disable spellcheck in git-rebase-todo Files for Comment Data
* Disable spellcheck in fstab Files, except Comment Data
* Disable spellcheck in .desktop Files, except Comment Data
* Disable spellcheck in XML (Debug) Files, except Comment Data
* Enable spellcheck for Comment Data in XML Files
* Disable spellcheck in Bash Files for non text Data
* Disable spellcheck in INI Files, except Comment Data
* Simplify code by leveraging QMetaEnum::fromType
* Use QMetaEnum to find number of available text styles
* Introduce ThemeData::textStyle to share code
* cleanup: share code to read Format colors from style or theme
* Update atom-one-light.theme revision number
* Fix atom-one-light.theme diff file colors
* Fix atom-one-dark.theme diff colors

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
