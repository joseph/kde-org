---
aliases:
- ../../plasma-5.12.4
changelog: 5.12.3-5.12.4
date: 2018-03-27
layout: plasma
youtube: xha6DJ_v1E4
figure:
  src: /announcements/plasma/5/5.12.0/plasma-5.12.png
  class: text-center mt-4
asBugfix: true
---

* Fix pixelated icon scaling for HiDPI screens. <a href="https://commits.kde.org/kmenuedit/e8e3c0f8e4a122ffc13386ab55b408aba2d29e14">Commit.</a> Fixes bug <a href="https://bugs.kde.org/390737">#390737</a>. Phabricator Code review <a href="https://phabricator.kde.org/D11631">D11631</a>
* Discover: Simplify the tasks view. <a href="https://commits.kde.org/discover/1d7b96ec879eb59107b915e52f31941d7480ef0e">Commit.</a> Fixes bug <a href="https://bugs.kde.org/391760">#391760</a>
* Move to KRunner's second results item with a single keypress. <a href="https://commits.kde.org/plasma-workspace/b8ffa755ed2df6bf6a47b9268e7da93355a4d856">Commit.</a> Fixes bug <a href="https://bugs.kde.org/392197">#392197</a>. Phabricator Code review <a href="https://phabricator.kde.org/D11611">D11611</a>
