---
date: 2022-11-08
changelog: 5.26.2-5.26.3
layout: plasma
video: false
asBugfix: true
draft: false
---

+ System Settings: Fix kcms inside a category always being indented. [Commit.](http://commits.kde.org/systemsettings/aa828df30208294960d8245acd9d3ba37d11e5ba) 
+ Fix panel configuration to properly limit maximum panel size to half height. [Commit.](http://commits.kde.org/plasma-desktop/97fcbfa54e682a4f4274aae59fdfe4e344e61d5a) 
+ KWin Screencast: Fix inverted screencast on OpenGLES and memfd. [Commit.](http://commits.kde.org/kwin/6fd95cc1c5ebce989e4cfeb5dc44276fcb950d94) 
