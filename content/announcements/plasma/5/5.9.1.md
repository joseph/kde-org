---
aliases:
- ../../plasma-5.9.1
changelog: 5.9.0-5.9.1
date: 2017-02-07
layout: plasma
youtube: lm0sqqVcotA
figure:
  src: /announcements/plasma/5/5.9.0/plasma-5.9.png
  class: text-center mt-4
asBugfix: true
---

- Fix i18n extraction: xgettext doesn't recognize single quotes. <a href="https://commits.kde.org/plasma-desktop/8c174b9c1e0b1b1be141eb9280ca260886f0e2cb">Commit.</a>
- Set wallpaper type in SDDM config. <a href="https://commits.kde.org/sddm-kcm/19e83b28161783d570bde2ced692a8b5f2236693">Commit.</a> Fixes bug <a href="https://bugs.kde.org/370521">#370521</a>
