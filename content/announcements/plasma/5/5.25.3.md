---
date: 2022-07-12
changelog: 5.25.2-5.25.3
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Kcms/colors: Properly apply tinting to the window titlebar. [Commit.](http://commits.kde.org/plasma-workspace/9f46232368583ef97d617782ee29f9eabd20eda8) Fixes bug [#455395](https://bugs.kde.org/455395). Fixes bug [#454047](https://bugs.kde.org/454047)
+ Kcms/lookandfeel: Set all defaults when saving the default package. [Commit.](http://commits.kde.org/plasma-workspace/0093ea7db5ad53f2b775c6d1b0a0b59c2bb1b564) Fixes bug [#456275](https://bugs.kde.org/456275)
+ Ship kconf update script to clean animation factor from kwinrc. [Commit.](http://commits.kde.org/kwin/832e46864e62f9c99147c1621353c8149ed2b2b8)
