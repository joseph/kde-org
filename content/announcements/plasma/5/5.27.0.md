---
title: "Plasma 5.27"
subtitle: "The 'KDE 💖 Free Software' Edition"
description: "Plasma 5.27 is out and brings massive improvements to the desktop and all its tools. Another work of love from the KDE devs and contributors."
date: "2023-02-14"
images:
 - /announcements/plasma/5/5.27.0/fullscreen_with_apps.png
layout: plasma-5.27
scssFiles:
- /scss/plasma-5-27.scss
authors:
- SPDX-FileCopyrightText: 2023 Paul Brown <paul.brown@kde.org>
- SPDX-FileCopyrightText: 2023 Carl Schwan <carl@carlschwan.eu>
- SPDX-FileCopyrightText: 2023 Aron Kovacs <aronkvh@gmail.com>
- SPDX-FileCopyrightText: 2023 Nate Graham <nate@kde.org>
- SPDX-FileCopyrightText: 2023 Aniqa Khokhar <aniqa.khokhar@kde.org>
SPDX-License-Identifier: CC-BY-4.0
highlights:
  title: Highlights
  items:
  - header: Discover Apps
    text: Discover helps you find the apps you need
    hl_video:
      poster: https://cdn.kde.org/promo/Announcements/Plasma/5.27/discover_hl_720p.png
      mp4: https://cdn.kde.org/promo/Announcements/Plasma/5.27/discover_hl_720p.mp4
      webm: https://cdn.kde.org/promo/Announcements/Plasma/5.27/discover_hl_720p.webm
    hl_class: overview
  - header:  Guided Set up
    text: Get up and running with Konqi
    hl_video:
      poster: https://cdn.kde.org/promo/Announcements/Plasma/5.27/wizard_hl_720p.png
      mp4: https://cdn.kde.org/promo/Announcements/Plasma/5.27/wizard_hl_720p.mp4
      webm: https://cdn.kde.org/promo/Announcements/Plasma/5.27/wizard_hl_720p.webm
    hl_class: hl-krunner
  - header: Window Tiling
    text: Organize your windows like a pro
    hl_video:
      poster: https://cdn.kde.org/promo/Announcements/Plasma/5.27/tiling_hl_720p.png
      mp4: https://cdn.kde.org/promo/Announcements/Plasma/5.27/tiling_hl_720p.mp4
      webm: https://cdn.kde.org/promo/Announcements/Plasma/5.27/tiling_hl_720p.webm
    hl_class: fingerprint
draft: false
---

{{< container >}}

Today is [&#x1F496; Free Software Day](https://fsfe.org/news/2023/news-20230104-01.en.html) and what better way to celebrate than with a brand new version of Plasma! 

{{< img src="happy_booth.jpg" alt="The KDE team expressing their ❤ for Free Software" img_class="max-width-800" >}}

Plasma 5.27 brings exciting new improvements to your desktop, and the first thing you'll notice when firing up Plasma is our new Konqi-powered wizard which will guide you through setting up the desktop.

Other big new features include a window tiling system, a more stylish app theme, cleaner and more usable tools, and widgets that give you more control over your machine.

Additionally, Plasma 5.27 is a Long Term Support version with tons of stability work and bugfixes, so you can feel the warm and stable &#x1F498; from the KDE community forever... Or at least until the next LTS rolls around in 2024!

**Read on to find out more about Plasma 5.27!**

---
If you enjoy Plasma 5.27, you can express your &#x1F496; for Free Software too! Support our work using the donation box on this page. Your generosity will help us continue to produce &#x1F498;-infused free software for the whole world.

{{< /container >}}

{{< highlight-grid >}}

{{< container >}}

## What's New

### Pretty in Plasma

The first thing you'll notice after installing Plasma is the easy-to-follow welcome wizard. It can help you connect to the Internet, learn about Plasma's features and how to tweak settings, install new software, and more.

{{< img src="plasma-welcome.png" alt="Konqi guides you through Plasma's set up" img_class="max-width-800" >}}

Speaking of features, check out the new tiling system: it will allow you to set up custom tile layouts and resize adjacent tiled windows simultaneously. Activate it in *System Settings* > *Workspace Behavior* > *Desktop Effects* and then you can tile a window dragging it while holding down the <kbd>Shift</kbd> key. To create custom tile layouts, hold down the <kbd>Meta</kbd> ("<kbd>Windows</kbd>") key, and then press <kbd>T</kbd>.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.27/tiling.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.27/tiling.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.27/tiling.png" fig_class="mx-auto max-width-800" >}}

Note that this feature is still in its infancy and not designed to completely replicate all the features of a more mature tiling window manager yet. Nevertheless, a boon for sure for those who like their windows tidy and organized!

Every new version of Plasma brings visual enhancements that make it more attractive. Apart from the spectacular wallpaper by Andy Betts, Breeze-themed windows now have a subtle outline around them, which not only looks classy, but also helps keep dark-themed windows from blending into one another. Another theme-related tweak is a nice full-screen blend effect when manually changing Plasma themes.

### [System Settings](https://userbase.kde.org/System_Settings)

KDE's designers have been hard at work reducing the number of pages in Plasma's *System Settings* utility and moving smaller options in with other settings. Such is the case of the configuration for the animation when apps are launching, which now lives on the *Cursors* page. Additionally, the *Highlight Changed Settings* button has been moved to the hamburger menu for a cleaner look.

In a similar vein, all global volume settings have been moved into the System Settings *Audio Volume* page, and the Audio Volume widget no longer has a separate settings page of its own. Clicking its *configure* button now takes you to the page in System Settings.

The settings for touch-enabled devices such as touchscreens and drawing tablets have also been improved and expanded.

### [Discover](https://apps.kde.org/discover/)

Discover is Plasma's app store/software manager. In Plasma 5.27, Discover now has a brand-new homepage design with dynamically-updating categories that show popular apps, plus a new set of featured apps that showcase the best of KDE.

Discover also makes it easier to find things: if it can't find a match for the search term in the current category, it will now offer to search in all categories, just in case.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.27/discoversearch.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.27/discoversearch.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.27/discoversearch.png" fig_class="mx-auto max-width-800" >}}

And for those lucky owners of Valve's Steam Deck gaming console, Discover can now perform system updates from within the desktop.

### [KRunner](https://userbase.kde.org/Plasma/Krunner)

KRunner is Plasma's seek-and-run utility, originally designed to let you run apps or commands from your desktop. Over the years, KRunner has grown much more functionality, including full desktop search, unit and currency exchange rate conversions, dictionary definitions, calculator features, and it can even show graphical representations of mathematical functions. Just press <kbd>Alt</kbd> + <kbd>Space</kbd> and start typing.

In Plasma 5.27, KRunner can now show you the current time in other locations! Just type "time", a space, and the name of a country, major city, or even a time zone code (like "UTC").

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.27/timeKRunner.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.27/timeKRunner.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.27/timeKRunner.png" fig_class="mx-auto max-width-800" >}}

KRunner has also gotten a lot smarter about its results in general. Over the years there have been complaints about some results not being the top item when they should have been. Devs have tackled the issue and that problem is now solved: The most relevant results now appear first. In addition, if KRunner can't find what you're looking for on your own machine, it will now offer a web search.

KRunner is not only powerful, but also easy to use! Type "define", a space, and a word, and then KRunner will show a dictionary definition of the word. Click on the definition and Plasma will notify you that the definition has been copied to the clipboard, ready to be pasted and used elsewhere.

### Panel, Tray & Widgets

Widgets provide you with tools integrated into the desktop that make using Plasma a joy to use, and are always being expanded with more features to cover a wider variety of needs.

For example, the *Digital Clock* widget can now show the Hebrew calendar in its calendar view. And the *Media Player* widget is now touch-sensitive and lets you change the volume and playback position just by swiping up and down or left and right (respectively).

{{< img src="hebrew-calendar.jpg" alt="Showing the Hebrew calendar in the digital clock pop-up." img_class="max-width-800" >}}

The *Color Picker* has had quite a few improvements this time around, including the possibility of displaying up to 9 preview color circles (up from 1 in prior versions of the widget). You can also drag an image onto it to calculate that image's average color. Finally, left-clicking on any color will show that it has been copied to the clipboard.

{{< img src="colorpicker.png" alt="Display up to 9 colors in the Color Picker widget preview." img_class="max-width-800" >}}

When setting up a VPN, the *Networks* widget will now intelligently detect when any support packages are missing and offer to install them for you, making it fast and easy to proceed.

And finally, monitoring your system using widgets has gotten easier. The *Bluetooth* widget shows the battery status of connected devices when you hover the cursor over it; the *System Monitor* (both the widget and the app) can detect monitor power usage for NVIDIA GPUs; and widget tooltips now inform you when middle-clicking or scrolling on a widget will change something. For example, middle-clicking the *Networks* widget will put your device into airplane mode, and middle-clicking the *Audio Volume* widget will mute all audio, while scrolling over it will raise or lower the volume.

{{< /container >}}

{{< diagonal-box color="purple" logo="/images/wayland.png" alt="Wayland logo" >}}

### Wayland

Migrating Plasma to the new Wayland display server technology has been no mean feat. But despite how hard the work has been, it is paying off, as Wayland opens many new ways to interact with your desktop. Plasma 5.27's Wayland support is better than ever, with many bug fixes and reliability improvements throughout!

Additionally, artists will be happy to know that design apps like Krita can now be aware of pen tilt and rotation on drawing tablets. Plasma on Wayland also supports high resolution scroll wheels for smoother scrolling through long views.

Plasma on Wayland has also gained support for the *Global Shortcuts* portal. This allows apps on Wayland to offer a standardized user interface for setting and editing global shortcuts.

Finally, Plasma on Wayland is now smarter about setting appropriate scale factors for your screens automatically, so you don't have to.

{{< /diagonal-box >}}

{{< container class="text-center" >}}

### Plasma for Power Users

Plasma offers ease of use, but also raw power. The following are tips and tricks that you can use in Plasma 5.27 to increase your productivity:

* **Multi-monitor overhaul:** Those of you who use multiple monitors should benefit greatly from a major overhaul of how Plasma handles them. Arrangements will now be more robust, without the chance of panels and desktops getting lost after monitors are unplugged or re-arranged.
* **Use global shortcuts to run terminal commands:** The *Shortcuts* page in the *System Settings* utility now lets you configure keyboard shortcuts not only for apps, but also to launch terminal commands and scripts.
* **Activate "Do Not Disturb" mode from the command line:** If you spend most of the time in the terminal and are being overwhelmed with notifications, just type `kde-inhibit --notifications` and Plasma will enter "Do Not Disturb" mode.
* **Send windows to Activities:** Do you use Activities to compartmentalize your private work, public life, and play time? If so, now you can move or copy windows to one, some or all of your Activities by right-clicking on the titlebar and choosing where you want it to go.
* **Save power with one keystroke:** While on the lock screen, hit the <kbd>Esc</kbd> key to turn off the screen and save some power.
* **Customized menu entries**. If you like customizing how apps launch, KDE's *Menu Editor* has always let you set environment variables when opening your apps, but now it's much easier to do so, as Plasma 5.27 gives the editor a specific text box for exactly that purpose.

{{< /container >}}

{{< container >}}

... And there's much more going on. To see the full list of changes, [check out the changelog for Plasma 5.27](/announcements/changelogs/plasma/5/5.26.5-5.27.0).

{{< /container >}}
