---
date: 2023-04-04
changelog: 5.27.3-5.27.4
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Breeze KStyle: make painted arrows more scalable, fix RTL delay menu arrows. [Commit.](http://commits.kde.org/breeze/a15812125c61ae531e029b5afc646c332758a074)
+ Discover Rpm-ostree: Improve handling of externally started transactions. [Commit.](http://commits.kde.org/discover/1aa5ac965e41500eca19c31f414eeb5d61384fe5)
+ Plasma Audio Volume Control Applet: add missing function for "Show virtual devices" menu item. [Commit.](http://commits.kde.org/plasma-pa/1e4d0d567a72d444dff1f90bb9c1712212d6e07e) Fixes bug [#465996](https://bugs.kde.org/465996)

