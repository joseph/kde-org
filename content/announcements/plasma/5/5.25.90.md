---
#scssFiles:
#  - /scss/plasma-5-25.scss
authors:
  - SPDX-FileCopyrightText: 2022 Jonathan Esk-Riddell <jr@jriddell.org>
SPDX-License-Identifier: CC-BY-4.0
date: 2022-09-14
changelog: 5.25.5-5.25.90
layout: plasma
title: Plasma 5.26 Beta
draft: false
---

{{< figure src="plasma-5.26-beta.png" alt="Plasma 5.26 Beta"  >}}

Today we are bringing you the preview version of KDE's Plasma 5.26 release. Plasma 5.26 Beta is aimed at testers, developers, and bug-hunters.  As well as our lightweight and feature rich Linux Desktop this release adds a Bigscreen version of Plasma for use on televisions.

To help KDE developers iron out bugs and solve issues, install Plasma 5.26 Beta and test run the features listed below. Please report bugs to our [bug tracker](https://bugs.kde.org).

The final version of Plasma 5.26 will become available for the general public on the 11th of October.

**DISCLAIMER:** This release contains untested and unstable software. It is highly recommended you **do not use this version in a production environment** and do not use it as your daily work environment. You risk crashes and loss of data.

See below the most noteworthy changes that need testing:

## Plasma Bigscreen

<a href="https://plasma-bigscreen.org">Plasma Bigscreen</a> is a user interface for your television.

{{< figure src="https://cdn.kde.org/screenshots/plasma-bigscreen/plasma-bigscreen.png" alt="Plasma Bigscreen" >}}

## Aura Browser

Browser for a fully immersed Big Screen experience allowing you to navigate the world wide web using just your remote control.

{{< figure src="https://cdn.kde.org/screenshots/aura-browser/aura-browser.png" alt="Aura Browser"  >}}

## Plank Player

Multimedia Player for playing local files on Plasma Bigscreen

{{< figure src="https://cdn.kde.org/screenshots/plank-player/plank-player.png" alt="Plank Player"  >}}

## Plasma Remotecontrollers

Translate various input device events into keyboard and pointer events for your bigscreen television.

## KPipewire

Components relating to Flatpak 'pipewire' use in Plasma.

## Discover

- Discover now lets you choose the frequency with which it notifies you about new updates
- Discover now displays content ratings for apps
- Discover now lets you change the name used for submitting a review
- Discover now has a “Share” button on each app’s details page

## Plasmoids

- The pop-ups of Plasma widgets in the panel are now resizable from their edges and corners just like normal windows, and they remember the sizes you set, too!
- Quite a lot of Plasma widgets have gained improved accessibility characteristics, after using them with a screen reader

## Kickoff App Menu

- You can now click on a letter header in Kickoff’s “All Applications” view to be taken to a view where you can choose a letter and be zoomed right to the apps that start with that letter
- Kickoff now has a new non-default “Compact” mode that lets you see more items at the time.
- When using a horizontal panel, Kickoff can now be configured to display text and/or remove the icon

# System Settings

- It’s now easier to preview wallpapers: just click on them and the desktop will change to show you what the wallpaper would look like
- Plasma now supports wallpapers with different images displayed when using a light color scheme vs a dark color scheme
- Animated images can now be used as wallpapers, either standalone, or even as a part of a slideshow
- Support for keyboard navigation in more applets
- Typing in the Overview effect now filters windows when there are any matching the search text, in addition to doing a KRunner search when no open windows match the search text
- Support for re-binding the buttons of a multi-button mouse
- System Settings’ Night Color page now lets you use a map to choose a manual location
- On System Settings’ Night Color page, you can now set a day color in addition to a night color

## Wayland Support

- In the Plasma Wayland session, it’s now possible to adjust how a graphics tablet’s input area maps to your screen coordinates
- It's possible to select if apps will be scaled by the compositor or by themselves to avoid having blurry apps on Wayland
- In the Plasma Wayland session, it’s now possible to disable middle-click paste


Remember to check and see if [your bug has already been reported](https://bugs.kde.org) before reporting a new one.
