---
date: 2023-02-02
appCount: 120
image: true
layout: gear
---
+ dolphin: Don't recurse into symlinks when counting directory contents ([Commit](http://commits.kde.org/dolphin/491068a4405f93ce66d4f49fa5ba5dee29e9546b), fixes bug [#434125](https://bugs.kde.org/434125))
+ kdeconnect: Fix ssh authentication using pubkey on recent openssh versions ([Commit](http://commits.kde.org/kdeconnect-kde/a7c17468f0bf16e4ed87c093dac0b77749c61d50), fixes bug [#443155](https://bugs.kde.org/443155))
+ libkdegames: Fix finding knewstuff themes ([Commit](http://commits.kde.org/libkdegames/fae6ef9b8785fdbdc8518000a5865d88026c7148), fixes bug [#464288](https://bugs.kde.org/464288))
