---
date: 2022-11-03
appCount: 120
image: true
layout: gear
---
+ dolphin: Fix opening unnecessary new windows ([Commit](https://invent.kde.org/system/dolphin/-/commit/348794eff8235f46b1f21d5e4b6fe0fe2b06ca57), fixes bug [#440663](https://bugs.kde.org/440663))
+ konsole: Fix a crash when extending the selection ([Commit](https://invent.kde.org/utilities/konsole/-/commit/1d30c7d75c1ee95a73f8240d1747c77f2683f60d), fixes bug [#398320](https://bugs.kde.org/398320) and bug [#458822](https://bugs.kde.org/458822))
