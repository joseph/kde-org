------------------------------------------------------------------------
r853937 | gateau | 2008-08-28 15:24:43 +0200 (Thu, 28 Aug 2008) | 4 lines

Generate thumbnails when coming back from view mode.
BUG: 169393
Backported https://svn.kde.org/home/kde/trunk/KDE/kdegraphics@853923

------------------------------------------------------------------------
r853938 | gateau | 2008-08-28 15:24:48 +0200 (Thu, 28 Aug 2008) | 2 lines

Bumped version number.

------------------------------------------------------------------------
r853974 | pino | 2008-08-28 16:59:26 +0200 (Thu, 28 Aug 2008) | 2 lines

win32: set that libspectre is found only when both library and header were found

------------------------------------------------------------------------
r854233 | scripty | 2008-08-29 09:56:31 +0200 (Fri, 29 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r854602 | aacid | 2008-08-29 23:56:38 +0200 (Fri, 29 Aug 2008) | 2 lines

 Approved i18n change, add context to Value: for HSV

------------------------------------------------------------------------
r854903 | lueck | 2008-08-30 17:11:09 +0200 (Sat, 30 Aug 2008) | 1 line

documentation backport fron trunk
------------------------------------------------------------------------
r854933 | aacid | 2008-08-30 19:08:56 +0200 (Sat, 30 Aug 2008) | 10 lines

Backport r854927 | aacid | 2008-08-30 18:42:03 +0200 (Sat, 30 Aug 2008) | 7 lines

So i score another kioslave:
 * Add a != 0 to the strcmp comparison, makes code more clear to my eyes
 * Fix port duplication check and improve comment
 * KUrl does not like : in the host part so change it by ___ hoping gphoto2 does not have any port that has ___ inside

This is most probably the CORRECT fix but it works for me and my camera


------------------------------------------------------------------------
r855384 | ereslibre | 2008-08-31 18:10:03 +0200 (Sun, 31 Aug 2008) | 2 lines

Backport rev 855382

------------------------------------------------------------------------
r855847 | pino | 2008-09-01 17:37:19 +0200 (Mon, 01 Sep 2008) | 2 lines

Backport: consider the orientation of TIFF documents.

------------------------------------------------------------------------
r855908 | pino | 2008-09-01 19:42:23 +0200 (Mon, 01 Sep 2008) | 4 lines

Backport: use the size as specified by the images, not a fake one.

CCBUG: 169592

------------------------------------------------------------------------
r856940 | gateau | 2008-09-04 10:40:13 +0200 (Thu, 04 Sep 2008) | 4 lines

Do not rely on mOriginalTime == 0 to determine if KDE_stat failed:
It can be 0 if the image date is the unix epoch.
Backported https://svn.kde.org/home/kde/trunk/KDE/kdegraphics@856617

------------------------------------------------------------------------
r857805 | cgilles | 2008-09-06 15:14:19 +0200 (Sat, 06 Sep 2008) | 2 lines

sync with trunk

------------------------------------------------------------------------
r857806 | cgilles | 2008-09-06 15:15:26 +0200 (Sat, 06 Sep 2008) | 2 lines

sync with trunk

------------------------------------------------------------------------
r857809 | cgilles | 2008-09-06 15:17:11 +0200 (Sat, 06 Sep 2008) | 2 lines

update

------------------------------------------------------------------------
r858435 | cgilles | 2008-09-08 08:15:01 +0200 (Mon, 08 Sep 2008) | 2 lines

sync with trunk

------------------------------------------------------------------------
r858925 | cgilles | 2008-09-09 08:05:40 +0200 (Tue, 09 Sep 2008) | 2 lines

sync with trunk

------------------------------------------------------------------------
r858931 | cgilles | 2008-09-09 08:31:49 +0200 (Tue, 09 Sep 2008) | 2 lines

sync with trunk

------------------------------------------------------------------------
r858982 | cgilles | 2008-09-09 12:04:49 +0200 (Tue, 09 Sep 2008) | 2 lines

sync with trunk

------------------------------------------------------------------------
r859354 | scripty | 2008-09-10 08:47:15 +0200 (Wed, 10 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r859446 | pino | 2008-09-10 13:33:21 +0200 (Wed, 10 Sep 2008) | 2 lines

Make "fit to width" the default zoom level for new documents.

------------------------------------------------------------------------
r862405 | berger | 2008-09-18 21:28:30 +0200 (Thu, 18 Sep 2008) | 2 lines

fix compilation in koffice's kdcraw based importer: allow to create DcrawSettingsWidget with only a QWidget as a parameter

------------------------------------------------------------------------
r863236 | pino | 2008-09-21 16:51:14 +0200 (Sun, 21 Sep 2008) | 2 lines

backport: read-only text fields should be read-only, not disabled

------------------------------------------------------------------------
r863256 | pino | 2008-09-21 17:59:31 +0200 (Sun, 21 Sep 2008) | 2 lines

do not use the specified widget as parent for the Document, but just keep it

------------------------------------------------------------------------
r863714 | pino | 2008-09-23 00:52:15 +0200 (Tue, 23 Sep 2008) | 5 lines

- make sure to properly unregister all the observers before the Document gets destroyed
- keep ownership of the "export as" menu, and check before manipulating its child widgets: avoid crashes in case the part widget is destroyed before the part is
both the problems discovered and debugged with the precious help of Armin Berres, thanks!
CCMAIL: trigger@space-based.de

------------------------------------------------------------------------
r863717 | pino | 2008-09-23 01:02:54 +0200 (Tue, 23 Sep 2008) | 2 lines

bump versions

------------------------------------------------------------------------
