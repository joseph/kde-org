------------------------------------------------------------------------
r1059600 | adawit | 2009-12-07 01:20:40 +0000 (Mon, 07 Dec 2009) | 2 lines

Connect to the correct signals...

------------------------------------------------------------------------
r1061542 | scripty | 2009-12-12 04:07:00 +0000 (Sat, 12 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1064488 | mpyne | 2009-12-21 03:15:29 +0000 (Mon, 21 Dec 2009) | 13 lines

Backport the PlayerManager in JuK decoupling to KDE SC 4.3 branch.

This is an invasive patch, but that's due more to the amount of tentacles this singleton
had than due to any large changes. The net effect is that it's easier to control the
shutdown deletion order, which should reduce crash-on-shutdown bugs which are still being
experienced.

This has been in trunk since 26 November with no (extra) problems noted.

Thanks to Nicolas for the reminder.

CCMAIL:nlecureuil@mandriva.com

------------------------------------------------------------------------
r1067642 | scripty | 2009-12-30 04:20:06 +0000 (Wed, 30 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1068822 | scripty | 2010-01-02 03:59:06 +0000 (Sat, 02 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1072380 | scripty | 2010-01-10 04:11:01 +0000 (Sun, 10 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1076372 | scripty | 2010-01-18 04:09:19 +0000 (Mon, 18 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
