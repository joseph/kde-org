------------------------------------------------------------------------
r961882 | mueller | 2009-04-30 21:11:38 +0000 (Thu, 30 Apr 2009) | 2 lines

update version number for 4.2.3

------------------------------------------------------------------------
r961957 | rpedersen | 2009-05-01 09:05:13 +0000 (Fri, 01 May 2009) | 2 lines

backport trunk r960802 and r961953

------------------------------------------------------------------------
r962273 | darioandres | 2009-05-01 22:16:43 +0000 (Fri, 01 May 2009) | 10 lines

Backport to 4.2branch of:
SVN commit 962271 by darioandres:

When moving an action from Available to Current, set the focus on the new added
option
So, if the user adds more actions they will be place under the first added on.
(instead of putting them at the bottom)

CCBUG: 134555

------------------------------------------------------------------------
r962452 | orlovich | 2009-05-02 14:03:05 +0000 (Sat, 02 May 2009) | 6 lines

Split dom tree versionning into multiple counters, so we can do 
less needless collection cache flushing -- and fix not doing it when needed 
for getElementsByClassName. Also, ClassNodeList doesn't justify having 
its own file, but I think the entire NodeListImpl family is now large enough...
(And this should be compile-time neutral). 

------------------------------------------------------------------------
r962475 | orlovich | 2009-05-02 14:09:09 +0000 (Sat, 02 May 2009) | 2 lines

Need this too.. But these should probably move to ElementImpl anyway... 

------------------------------------------------------------------------
r962478 | orlovich | 2009-05-02 14:11:39 +0000 (Sat, 02 May 2009) | 2 lines

... This part is kind of important, too. 

------------------------------------------------------------------------
r962585 | ossi | 2009-05-02 15:37:03 +0000 (Sat, 02 May 2009) | 7 lines

don't drop changes of binary data

somebody give me a brown paper bag - the not-actually-modified check
used strcmp for the byte array comparison ...

BUG: 190464

------------------------------------------------------------------------
r962646 | orlovich | 2009-05-02 19:29:31 +0000 (Sat, 02 May 2009) | 8 lines

Implement DOM3 core compareDocumentPosition; seems to be needed by GWT...

This passes the cases in the not-yet-commited domts update that don't trigger 
bugs in XML/entity notation handling (once I fixed a problem in their generation)

CCBUG:129398


------------------------------------------------------------------------
r963449 | dfaure | 2009-05-04 18:53:46 +0000 (Mon, 04 May 2009) | 2 lines

Fix strcasestr detection for glibc-2.9

------------------------------------------------------------------------
r963518 | ilic | 2009-05-04 20:44:41 +0000 (Mon, 04 May 2009) | 2 lines

Temporarely discontinuing language-sensitivity of number formats. (bport: 963498)

------------------------------------------------------------------------
r963519 | ilic | 2009-05-04 20:45:25 +0000 (Mon, 04 May 2009) | 2 lines

Temporarely discontinuing language sensitivity of number formats (2). (bport: 963498)

------------------------------------------------------------------------
r963522 | lueck | 2009-05-04 20:48:49 +0000 (Mon, 04 May 2009) | 2 lines

add missing i18n calls to make the ssl error messages translatable, makes 14 new messages for branch 4.2
CCMAIL:kde-i18n-doc@kde.org
------------------------------------------------------------------------
r963546 | ilic | 2009-05-04 21:35:19 +0000 (Mon, 04 May 2009) | 1 line

Fixes build problem on OpenSolaris (patch by tropikhajma). (bport: 963545)
------------------------------------------------------------------------
r963583 | orlovich | 2009-05-04 22:31:27 +0000 (Mon, 04 May 2009) | 6 lines

Make sure to encode form elements in tree order; some web frameworks rely on that 
for DND stuff. Fixes the essence of bug #158039, but there is a funky issue with 
list bullets that that still has.

CCBUG:158039

------------------------------------------------------------------------
r963699 | thanngo | 2009-05-05 09:25:07 +0000 (Tue, 05 May 2009) | 3 lines

fix a gcc4.4 build error


------------------------------------------------------------------------
r964115 | cfeck | 2009-05-06 01:13:50 +0000 (Wed, 06 May 2009) | 6 lines

Add tab order to input fields, respect sizeHint() (backport r964114)

Will be fixed in KDE 4.2.4

CCBUG: 186371

------------------------------------------------------------------------
r964131 | orlovich | 2009-05-06 05:00:59 +0000 (Wed, 06 May 2009) | 2 lines

Don't needlessly clear selection when an another app grabs it not the expected behavior in KDE.

------------------------------------------------------------------------
r964148 | aseigo | 2009-05-06 07:27:38 +0000 (Wed, 06 May 2009) | 2 lines

proper glob

------------------------------------------------------------------------
r964199 | ogoffart | 2009-05-06 10:17:29 +0000 (Wed, 06 May 2009) | 2 lines

Backort 964197 :  Fix broken signal-slot connection

------------------------------------------------------------------------
r965817 | ehamberg | 2009-05-09 21:12:42 +0000 (Sat, 09 May 2009) | 5 lines

backport of r906814:

replace static_cast by the null ptr constructor

BUG: 192170
------------------------------------------------------------------------
r965818 | ehamberg | 2009-05-09 21:12:45 +0000 (Sat, 09 May 2009) | 9 lines

backport of r961949:

don't give focus to wrong view

don't use an anonymous single shot timer for managing the closing of the
command line editor. KateCmdLineEdit::hideLineEdit() will give focus to
it's m_view, which after changing views will be the wrong one.
instead, keep a qtimer pointer and connect the view's focusout signal to
the timer's stop slot.
------------------------------------------------------------------------
r966155 | orlovich | 2009-05-10 14:17:43 +0000 (Sun, 10 May 2009) | 13 lines

Make sure to clip date value to the valid range in all paths.
This prevents freezes on 32-bit as we don't end up with magnitudes
so large that there is not enough precision to represent the year 
accurately.

It prevents crashes on 64-bit as we don't end up with time_t's corresponding to 
date's with a year  > 2^31 - 1, on which localtime/gmtime return 0.
Thanks to Zahl for info and for verifying this help on 64-bit..

BUG:189373



------------------------------------------------------------------------
r966269 | ehamberg | 2009-05-10 20:36:35 +0000 (Sun, 10 May 2009) | 3 lines

backport of r966260:

fix erroneous behaviour when pasting text that contains several lines but wasn't yanked in a linewise manner
------------------------------------------------------------------------
r966295 | darioandres | 2009-05-10 21:49:24 +0000 (Sun, 10 May 2009) | 7 lines

Backport to 4.2branch of:
SVN commit 966294 by darioandres:

Launch the bug report wizard using HTTPS as default

CCBUG: 163799

------------------------------------------------------------------------
r966335 | darioandres | 2009-05-10 23:53:00 +0000 (Sun, 10 May 2009) | 9 lines

Backport to 4.2branch of:
SVN commit 966333 by darioandres:

Fix the broken behaviour of KRestrictedLine
(this needs a better implementation)

CCBUG: 161012


------------------------------------------------------------------------
r966486 | scripty | 2009-05-11 09:03:47 +0000 (Mon, 11 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r967095 | winterz | 2009-05-12 14:15:17 +0000 (Tue, 12 May 2009) | 4 lines

merge SVN revision 966931 from till on Tue May 12 10:03:49 2009 UTC
restore missing braces in KWindowSystemPrivate::activate() that causes D-Bus hangs.


------------------------------------------------------------------------
r967263 | darioandres | 2009-05-13 03:46:49 +0000 (Wed, 13 May 2009) | 8 lines

Backport of 4.2branch of:
SVN commit 967261 by darioandres:

Do not call the find/findNext/replace actions when their shortcuts are pressed
but the findReplaceEnabled option was explicity disabled

CCBUG: 192529

------------------------------------------------------------------------
r967961 | reed | 2009-05-14 15:32:26 +0000 (Thu, 14 May 2009) | 5 lines

from Sho_ (one of the yakuake devs):

11:13 < Sho_> there's another osx-related kwindowsystem todo we noticed a while back: KWindowSystem::compositingActive(), which yakuake and other apps use to decide whether to paint for an ARGB visual, always returns false on Mac
11:14 < Sho_> when it should probably always return true since afaik you can't switch off quartz compositor on os x anyway

------------------------------------------------------------------------
r968021 | habacker | 2009-05-14 18:43:32 +0000 (Thu, 14 May 2009) | 1 line

backported 947698
------------------------------------------------------------------------
r968405 | osterfeld | 2009-05-15 16:13:23 +0000 (Fri, 15 May 2009) | 2 lines

fix build on OS X, now in sync with trunk

------------------------------------------------------------------------
r968439 | osterfeld | 2009-05-15 18:21:54 +0000 (Fri, 15 May 2009) | 1 line

build on OS X, following trunk
------------------------------------------------------------------------
r968564 | scripty | 2009-05-16 07:59:20 +0000 (Sat, 16 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r968648 | zwabel | 2009-05-16 13:26:28 +0000 (Sat, 16 May 2009) | 3 lines

Backport r968646:
Add some more mutex locking in some places where it's needed. This should fix some possible crashes due to multithreading in KDevelop.

------------------------------------------------------------------------
r969195 | orlovich | 2009-05-17 16:34:59 +0000 (Sun, 17 May 2009) | 9 lines

Proper fix for what the tail padding stuff was trying to accomplish:
once we get to end of input, keep feeding flex a single buffer with nulls,
rather than let it run past the end. The original flex driver does something 
similar.

Thanks to Andrew Coles for providing a simple testcase so I could understand the issue.



------------------------------------------------------------------------
r969255 | zwabel | 2009-05-17 19:32:35 +0000 (Sun, 17 May 2009) | 2 lines

Backport r969254: Prevent possible deadlock

------------------------------------------------------------------------
r969286 | darioandres | 2009-05-17 21:33:51 +0000 (Sun, 17 May 2009) | 8 lines

Backport to 4.2branch of:
SVN commit 969284 by darioandres:

Do not assign m_codec if QTextCodec::codecForName(name)
returns NULL. This will cause crashes.

CCBUG: 178789

------------------------------------------------------------------------
r969331 | zwabel | 2009-05-17 23:55:22 +0000 (Sun, 17 May 2009) | 4 lines

Backport r969330:
Lock and unlock the smart-mutex again in each call to editStart and editEnd, no matter whether it is recursive or not.
We can do that, since the mutex is recursive. This fixes a possible crash: During code-completion, the smart-mutex is released when external functions are called. When those functions triggered an edit again through public API, the edit did not re-lock the smart-mutex, thus accessing internal data without thread-safe protection.

------------------------------------------------------------------------
r969733 | jferrer | 2009-05-18 19:33:56 +0000 (Mon, 18 May 2009) | 2 lines

Add a new translator for documentation (Rafael Carreras)

------------------------------------------------------------------------
r969763 | ilic | 2009-05-18 20:55:18 +0000 (Mon, 18 May 2009) | 1 line

Introducing proper static data. (bport: 963716)
------------------------------------------------------------------------
r969771 | jferrer | 2009-05-18 21:07:48 +0000 (Mon, 18 May 2009) | 3 lines

Update email address for Rafael Carreras
CCMAIL: rcarreras@caliu.cat

------------------------------------------------------------------------
r969775 | dfaure | 2009-05-18 21:17:31 +0000 (Mon, 18 May 2009) | 6 lines

* Add a mutex around the KCatalog code, that is playing with env vars (setting+resetting), as needed by gettext().
BUG: 191122

* Fix isApplicationTranslatedInto() being false wrongly in apps that use setMainCatalog()
 (e.g. in korgac it would say "nope, because no korgac.po" even though it's using korganizer.po)

------------------------------------------------------------------------
r970309 | darioandres | 2009-05-20 00:04:30 +0000 (Wed, 20 May 2009) | 8 lines

Backport to 4.2branch of:
SVN commit 970308 by darioandres:

Prevent a crash when opening the editor for shorcuts in another group
different of the one which is currently open.

CCBUG: 181024

------------------------------------------------------------------------
r970821 | darioandres | 2009-05-20 21:43:03 +0000 (Wed, 20 May 2009) | 7 lines

Backport to 4.2branch:

Allow the file dialog to save a file using the absolute path on the
location text edit. Review by dfaure

BUG: 180208

------------------------------------------------------------------------
r970853 | dfaure | 2009-05-21 01:27:01 +0000 (Thu, 21 May 2009) | 3 lines

shared-mime-info says "note that files with high-bit-set characters should still be treated as text since these can appear in UTF-8 text, unlike control characters" and that was the intent of the code; but the 127-255 range was treated as signed, so negative, so < 32 -- so text files with accents in them weren't recognized as text.
BUG: 188355

------------------------------------------------------------------------
r970871 | scripty | 2009-05-21 07:45:25 +0000 (Thu, 21 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r971545 | orlovich | 2009-05-22 17:21:39 +0000 (Fri, 22 May 2009) | 3 lines

Backport vtokarev's r971542, which fixes a whole 
bunch of crashers related to inline box tree.

------------------------------------------------------------------------
r971572 | ahartmetz | 2009-05-22 19:07:16 +0000 (Fri, 22 May 2009) | 1 line

backport the bugfix part from 952940 in trunk: store a rule to ignore all ignored errors, not only the newly ignored errors. the old incomplete rule would be replaced with a new incomplete rule and this would happen every time the respective site was visited...
------------------------------------------------------------------------
r971638 | dfaure | 2009-05-23 02:08:05 +0000 (Sat, 23 May 2009) | 2 lines

backport: fix porting bug

------------------------------------------------------------------------
r971641 | dfaure | 2009-05-23 02:30:23 +0000 (Sat, 23 May 2009) | 3 lines

backport additional tests for deleteDirectory() -- but no breakage in branch, it came from
the trunk-only recursive-deletion-in-kio_file speedup. This is why I don't backport speedups :-)

------------------------------------------------------------------------
r971645 | ggarand | 2009-05-23 02:44:09 +0000 (Sat, 23 May 2009) | 11 lines

automatically merged revision 965861:
When computing percentage height, extend the recurse-to-containing-block
-when-size-is-auto quirk to apply to all objects, and not only to replaced
elements.

Opera was the last browser to support the same level of strictness as khtml
on this matter, but they finally gave up in 9.6x serie.

So we have to add this CSS violation in quirk mode to remain compatible ;(

BUG: 158592
------------------------------------------------------------------------
r971646 | ggarand | 2009-05-23 02:44:52 +0000 (Sat, 23 May 2009) | 9 lines

automatically merged revision 965863:
introduce a fuse for smooth scrolling, that will check if it missed the
deadline many times in a row.

If it is the case and the 'when efficient' setting is in use,
smooth scrolling will be disabled until the next page load.

This helps on pages where clueless, braindead web developers do expensive
javascript computations and other heavy DOM changes at each scroll event.
------------------------------------------------------------------------
r971647 | ggarand | 2009-05-23 02:45:28 +0000 (Sat, 23 May 2009) | 2 lines

automatically merged revision 969348:
fix an invalid read.
------------------------------------------------------------------------
r971648 | ggarand | 2009-05-23 02:47:15 +0000 (Sat, 23 May 2009) | 6 lines

automatically merged revision 969352:
don't let our internal proxy style override any preexisting proxy
style - rather internally proxy the external widget's internal proxy style.
(say that fast)

BUG: 192663
------------------------------------------------------------------------
r971649 | ggarand | 2009-05-23 02:48:20 +0000 (Sat, 23 May 2009) | 5 lines

automatically merged revision 969349:
Window::open arguments may also be splitted on syntactically meaningful
whitespace - not just on commas.

Be compatible.
------------------------------------------------------------------------
r971650 | ggarand | 2009-05-23 02:50:51 +0000 (Sat, 23 May 2009) | 4 lines

automatically merged revision 969354:
include floats in computation of visible flow Region.

BUG: 172693
------------------------------------------------------------------------
r971651 | ggarand | 2009-05-23 02:53:32 +0000 (Sat, 23 May 2009) | 5 lines

automatically merged revision 971234:
don't allow mix of percents and integers in functional-style colour
declarations since it is explicitely disallowed by CSS 2.1

BUG: 193434
------------------------------------------------------------------------
r971652 | ggarand | 2009-05-23 02:54:29 +0000 (Sat, 23 May 2009) | 5 lines

automatically merged revision 971235:
don't allow special aligment values to percolate through tables even
in strict mode, as it isn't Mozilla-compatible.

BUG: 193093
------------------------------------------------------------------------
r971656 | ggarand | 2009-05-23 02:56:39 +0000 (Sat, 23 May 2009) | 3 lines

automatically merged revision 971236:
fix buffer flickering to black for some opacity values nearing 1.0.
probably a Qt bug - don't have time to investigate.
------------------------------------------------------------------------
r971657 | ggarand | 2009-05-23 02:57:51 +0000 (Sat, 23 May 2009) | 5 lines

automatically merged revision 971237:
adapt to change in soft-hyphen rendering strategy in Qt 4
diagnosed by Allan.

BUG: 145335
------------------------------------------------------------------------
r972312 | orlovich | 2009-05-24 15:00:55 +0000 (Sun, 24 May 2009) | 6 lines

Revert r964131 for 4.2.4 --- it triggers a weird bug where selection would get cancelled 
after you finish making it some of the time. Will look for a proper fix for trunk,
but this should be safer for stable.

CCBUG:193400

------------------------------------------------------------------------
r972839 | dfaure | 2009-05-25 20:03:01 +0000 (Mon, 25 May 2009) | 2 lines

Fix KDebug bug: it wasn't possible to deactivate area 0.

------------------------------------------------------------------------
r972882 | darioandres | 2009-05-25 21:51:22 +0000 (Mon, 25 May 2009) | 12 lines

Backport to 4.2branch of:

SVN commit 972880 by darioandres:

When the user chooses to toggle the "Show Hidden Files" states, the treeview is
reload
but the current selection is missing. This patch re-selects the previous
selected URL

CCBUG: 161323


------------------------------------------------------------------------
r973365 | cfeck | 2009-05-26 22:51:13 +0000 (Tue, 26 May 2009) | 4 lines

Do not save configuration when configGroup == 0 (backport r911008)

BUG: 194126

------------------------------------------------------------------------
r973449 | scripty | 2009-05-27 08:44:56 +0000 (Wed, 27 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r973813 | dfaure | 2009-05-27 21:00:34 +0000 (Wed, 27 May 2009) | 2 lines

backport crash fix when starting an app without a sycoca available, and the app uses ksycoca from a timer.

------------------------------------------------------------------------
r973828 | cfeck | 2009-05-27 22:01:16 +0000 (Wed, 27 May 2009) | 2 lines

Do not leave wild QPixmap pointers in frames cache (backport r973827)

------------------------------------------------------------------------
r974223 | mueller | 2009-05-28 19:13:00 +0000 (Thu, 28 May 2009) | 2 lines

4.2.4 preparations

------------------------------------------------------------------------
