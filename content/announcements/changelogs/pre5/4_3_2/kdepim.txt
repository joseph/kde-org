------------------------------------------------------------------------
r1017649 | djarvie | 2009-08-31 03:14:00 +0000 (Mon, 31 Aug 2009) | 2 lines

Use KDE system settings to determine default working days in the week.

------------------------------------------------------------------------
r1017920 | mkoller | 2009-08-31 21:43:59 +0000 (Mon, 31 Aug 2009) | 7 lines

Backport r1017919 by mkoller from trunk to the 4.3 branch:

CCBUG: 205448

find a better compromise between search speed and responseness of the ui (empirical result)


------------------------------------------------------------------------
r1019073 | winterz | 2009-09-02 19:14:28 +0000 (Wed, 02 Sep 2009) | 11 lines

Backport r1019064 by winterz from trunk to the 4.3 branch:

show UTC incidences in the local time zone, which is especially useful
for Kolab users who see all their incidences in UTC.

Based on a patch from Steffen Hanikel. Thanks!

CCBUG: 204059
MERGE: e35,e4,4.3


------------------------------------------------------------------------
r1019113 | mkoller | 2009-09-02 21:45:13 +0000 (Wed, 02 Sep 2009) | 6 lines


Backport r1019109 by mkoller from trunk to the 4.3 branch:

Use KDE's locale/calendar settings for the Group Header in aggregations


------------------------------------------------------------------------
r1019119 | winterz | 2009-09-02 22:02:42 +0000 (Wed, 02 Sep 2009) | 7 lines

Backport r1019115 by winterz from trunk to the 4.3 branch:

probable crash fix when autocompleting LDAP address.
CCBUG: 206024
MERGE: e35,e4,4.3


------------------------------------------------------------------------
r1019177 | scripty | 2009-09-03 04:11:28 +0000 (Thu, 03 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1019358 | winterz | 2009-09-03 11:40:30 +0000 (Thu, 03 Sep 2009) | 4 lines

Backport r1017761 by thiago from trunk to the 4.3 branch:

Don't crash if currentItem is 0

------------------------------------------------------------------------
r1019504 | lueck | 2009-09-03 18:53:01 +0000 (Thu, 03 Sep 2009) | 1 line

pim doc backport for 4.3
------------------------------------------------------------------------
r1019521 | lueck | 2009-09-03 19:42:47 +0000 (Thu, 03 Sep 2009) | 1 line

missed pim doc backport for 4.3
------------------------------------------------------------------------
r1019562 | lueck | 2009-09-03 21:37:42 +0000 (Thu, 03 Sep 2009) | 1 line

revert as requested by David Jarvie
------------------------------------------------------------------------
r1019888 | lueck | 2009-09-04 18:19:50 +0000 (Fri, 04 Sep 2009) | 1 line

add to backport for kdepim
------------------------------------------------------------------------
r1019912 | lueck | 2009-09-04 19:20:45 +0000 (Fri, 04 Sep 2009) | 2 lines

fix doc update, the stuff about Shift-Delete deleting withouta confirmation prompt is already commited. Thanks to David Jarvie for keeping an eye on me
CCMAIL:djarvie@kde.org
------------------------------------------------------------------------
r1019990 | mkoller | 2009-09-04 23:27:15 +0000 (Fri, 04 Sep 2009) | 5 lines

Backport bug 191455

Replace dir-delimiters with _ char onm saving attachments


------------------------------------------------------------------------
r1020117 | winterz | 2009-09-05 10:30:35 +0000 (Sat, 05 Sep 2009) | 7 lines

Backport r1020115 by winterz from trunk to the 4.3 branch:

crash guard
CCBUG: 206306
MERGE: 4.3


------------------------------------------------------------------------
r1020261 | mkoller | 2009-09-05 21:30:19 +0000 (Sat, 05 Sep 2009) | 6 lines

backport of bugfix for #196053
Fix crash in precommand execution (hopefully).
The crash was probably due to the eventloop object got destroyed in its own loop.
catch 0-pointer instead of assert. Happens when kmail exits while precommand runs


------------------------------------------------------------------------
r1020455 | mkoller | 2009-09-06 11:24:52 +0000 (Sun, 06 Sep 2009) | 6 lines

backport of 1017452 by mkoller of bug 160990

When scrolling one page, use 80% of the available height of the current
view (not the height of the complete widget including the message structure viewer)


------------------------------------------------------------------------
r1020457 | mkoller | 2009-09-06 11:26:54 +0000 (Sun, 06 Sep 2009) | 5 lines

Backport r1017140 by mkoller from trunk to the 4.3 branch:

remove accelerator marker when selecting one of many mail addresses on dropping a vcard


------------------------------------------------------------------------
r1020458 | mkoller | 2009-09-06 11:28:18 +0000 (Sun, 06 Sep 2009) | 5 lines

Backport r1016921 by mkoller from trunk to the 4.3 branch:

When compressing an attachment, mark the mail as modified.


------------------------------------------------------------------------
r1020460 | mkoller | 2009-09-06 11:30:30 +0000 (Sun, 06 Sep 2009) | 7 lines

Backport r1016778 by mkoller from trunk to the 4.3 branch:

CCBUG: 204532

Show clear button in text input field


------------------------------------------------------------------------
r1020461 | mkoller | 2009-09-06 11:33:42 +0000 (Sun, 06 Sep 2009) | 5 lines

backport of 1016754 by mkoller for bug 181794

if the aggregation is defined without grouping only show the root decoration when
threading is enabled

------------------------------------------------------------------------
r1020464 | mkoller | 2009-09-06 11:39:18 +0000 (Sun, 06 Sep 2009) | 7 lines

Backport r1016744 by mkoller from trunk to the 4.3 branch:

CCBUG: 205018

obey the current icons size when setting the column width for the first three columns


------------------------------------------------------------------------
r1020465 | mkoller | 2009-09-06 11:40:09 +0000 (Sun, 06 Sep 2009) | 8 lines

Backport r1017340 by mkoller from trunk to the 4.3 branch:

CCBUG: 140353

try to find the annoyance-filter executable in the default installation
path but also in a system path


------------------------------------------------------------------------
r1020525 | tmcguire | 2009-09-06 14:13:50 +0000 (Sun, 06 Sep 2009) | 8 lines

Backport r1014138 by tmcguire from trunk to the 4.3 branch:

Fix the problem that the forward menu was empty, because the action list was
plugged before createGUI() was called.
Also, clean up a bit, for example don't set the shortcuts twice.

BUG: 206377

------------------------------------------------------------------------
r1020526 | tmcguire | 2009-09-06 14:15:27 +0000 (Sun, 06 Sep 2009) | 7 lines

Backport r1020484 by tmcguire from trunk to the 4.3 branch:

Don't crash when importing two messages and the first import failed, because then
the folder was not created yet, and KMail tried to add the message to a non-existant
folder.


------------------------------------------------------------------------
r1020527 | tmcguire | 2009-09-06 14:16:24 +0000 (Sun, 06 Sep 2009) | 4 lines

Backport r1020504 by tmcguire from trunk to the 4.3 branch:

Argh, code duplication! Fix the import crash here as well.

------------------------------------------------------------------------
r1020537 | tmcguire | 2009-09-06 14:48:07 +0000 (Sun, 06 Sep 2009) | 1 line

version++
------------------------------------------------------------------------
r1020587 | mkoller | 2009-09-06 17:20:48 +0000 (Sun, 06 Sep 2009) | 5 lines

backport of r1020581 for bug 143237

Check for an attachment not only in the first BodyPart but in the Message itself if it has no first BodyPart.


------------------------------------------------------------------------
r1020593 | mkoller | 2009-09-06 17:33:17 +0000 (Sun, 06 Sep 2009) | 10 lines

Backport r1020592 by mkoller from trunk to the 4.3 branch:

CCBUG: 185167

make sure the "save as" dialog does not strip the filename part after the last "."
thinking it's an extension.

Also, factor out a fileName cleaning function


------------------------------------------------------------------------
r1020609 | mkoller | 2009-09-06 18:30:01 +0000 (Sun, 06 Sep 2009) | 7 lines

Backport r1020607 by mkoller from trunk to the 4.3 branch:

CCBUG: 95665

store the "dont ask again" flag in the global config, so that kontact/kmail share it


------------------------------------------------------------------------
r1020612 | mkoller | 2009-09-06 18:38:09 +0000 (Sun, 06 Sep 2009) | 6 lines

Backport r1020610 by mkoller from trunk to the 4.3 branch:

store the "dont ask again" flag in the global config, so that kontact/kmail
share it


------------------------------------------------------------------------
r1020613 | mkoller | 2009-09-06 18:43:32 +0000 (Sun, 06 Sep 2009) | 2 lines

Revert last change to not have update problems with config settings

------------------------------------------------------------------------
r1020871 | winterz | 2009-09-07 12:25:25 +0000 (Mon, 07 Sep 2009) | 7 lines

Backport r1020870 by winterz from trunk to the 4.3 branch:

crash guard
CCBUG: 206625
MERGE: 4.3


------------------------------------------------------------------------
r1021563 | tmcguire | 2009-09-09 14:22:31 +0000 (Wed, 09 Sep 2009) | 9 lines

Backport r1020615 by tmcguire from trunk to the 4.3 branch:

Don't get the full message when deleting it, just the KMsgBase.

This could fix a giant memory leak reported by Jaime.
Or this could just explode into my face.
With the message storage system, you never know.


------------------------------------------------------------------------
r1021565 | tmcguire | 2009-09-09 14:24:25 +0000 (Wed, 09 Sep 2009) | 21 lines

Backport r1021515 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1015875 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1015875 | tmcguire | 2009-08-26 15:49:42 +0200 (Wed, 26 Aug 2009) | 10 lines
  
  Merged revisions 1015834 via svnmerge from 
  svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1015834 | tmcguire | 2009-08-26 14:24:04 +0200 (Wed, 26 Aug 2009) | 3 lines
    
    Fix Till's todo: Also take local folders into account when collecting the resource folders.
    Fixes kolab/issue3792.
  ........
................


------------------------------------------------------------------------
r1021566 | tmcguire | 2009-09-09 14:25:55 +0000 (Wed, 09 Sep 2009) | 23 lines

Backport r1021536 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1016356 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1016356 | winterz | 2009-08-27 17:32:40 +0200 (Thu, 27 Aug 2009) | 12 lines
  
  Merged revisions 1016353 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1016353 | winterz | 2009-08-27 11:26:36 -0400 (Thu, 27 Aug 2009) | 5 lines
    
    provide a bit more left-padding when printing
    kolab/issue3254
    
    MERGE: e4,trunk,4.3
  ........
................


------------------------------------------------------------------------
r1021567 | tmcguire | 2009-09-09 14:27:28 +0000 (Wed, 09 Sep 2009) | 22 lines

Backport r1021540 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1016435 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1016435 | winterz | 2009-08-27 22:01:47 +0200 (Thu, 27 Aug 2009) | 11 lines
  
  Merged revisions 1016428 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1016428 | winterz | 2009-08-27 15:56:44 -0400 (Thu, 27 Aug 2009) | 4 lines
    
    ensure that recent addresses don't get double-double-quoted
    kolab/issue3812
    MERGE: e4,trunk,4.3
  ........
................


------------------------------------------------------------------------
r1021571 | tmcguire | 2009-09-09 14:29:08 +0000 (Wed, 09 Sep 2009) | 23 lines

Backport r1021534 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1015955 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1015955 | winterz | 2009-08-26 19:01:33 +0200 (Wed, 26 Aug 2009) | 12 lines
  
  Merged revisions 1015950 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1015950 | winterz | 2009-08-26 12:52:41 -0400 (Wed, 26 Aug 2009) | 5 lines
    
    make "Go to Today" work properly when in Work Week view.
    kolab/issue3671
    
    MERGE: e4,trunk,4.3
  ........
................


------------------------------------------------------------------------
r1021572 | tmcguire | 2009-09-09 14:31:15 +0000 (Wed, 09 Sep 2009) | 23 lines

Backport r1021553 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1019831 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1019831 | winterz | 2009-09-04 16:59:28 +0200 (Fri, 04 Sep 2009) | 12 lines
  
  Merged revisions 1019828 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1019828 | winterz | 2009-09-04 10:49:59 -0400 (Fri, 04 Sep 2009) | 5 lines
    
    make sure authorization dialogs are shown on top.
    fixes kolab/issue3799
    
    MERGE:e4,trunk,4.3
  ........
................


------------------------------------------------------------------------
r1022069 | tnyblom | 2009-09-10 17:45:39 +0000 (Thu, 10 Sep 2009) | 2 lines

Backport crash guard from trunk

------------------------------------------------------------------------
r1022098 | winterz | 2009-09-10 19:53:59 +0000 (Thu, 10 Sep 2009) | 4 lines

apply Urs' patch that refines the export of addresses from/to GMX.
Should be in trunk already.
CCMAIL: tschenturs@gmx.ch

------------------------------------------------------------------------
r1022456 | mkoller | 2009-09-11 20:04:35 +0000 (Fri, 11 Sep 2009) | 7 lines

Backport r1022455 by mkoller from trunk to the 4.3 branch:

CCBUG: 207028

fix display of unread message count in right-to-left language


------------------------------------------------------------------------
r1022567 | scripty | 2009-09-12 03:06:23 +0000 (Sat, 12 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1022893 | winterz | 2009-09-13 12:46:08 +0000 (Sun, 13 Sep 2009) | 6 lines

Backport r1022892 by winterz from trunk to the 4.3 branch:

fix setting wordwrap on KUrlLabels
MERGE: 4.3


------------------------------------------------------------------------
r1022963 | tmcguire | 2009-09-13 16:18:14 +0000 (Sun, 13 Sep 2009) | 25 lines

Backport r1022953 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1021531 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1021531 | winterz | 2009-09-09 15:19:32 +0200 (Wed, 09 Sep 2009) | 14 lines
  
  Merged revisions 1021458 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1021458 | tmcguire | 2009-09-09 05:16:29 -0400 (Wed, 09 Sep 2009) | 7 lines
    
    After adding a locally added message to the server, don't put it in the deletion map.
    
    Should fix kolab/issue3833.
    
    However, there are some stale deletion maps because of this bug, I need to think of a clever
    way to get rid of them...
  ........
................


------------------------------------------------------------------------
r1022964 | tmcguire | 2009-09-13 16:18:57 +0000 (Sun, 13 Sep 2009) | 22 lines

Backport r1022954 by tmcguire from trunk to the 4.3 branch:

Merged revisions 1021533 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1021533 | winterz | 2009-09-09 15:21:33 +0200 (Wed, 09 Sep 2009) | 12 lines
  
  Merged revisions 1021464 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1021464 | tmcguire | 2009-09-09 05:37:52 -0400 (Wed, 09 Sep 2009) | 5 lines
    
    When there is nothing to delete, clear the deletion map, as it might have stale  entries
    resulting from kolab/issue3833.
    
    Part of kolab/issue3833
  ........
................


------------------------------------------------------------------------
r1023466 | wstephens | 2009-09-14 19:19:51 +0000 (Mon, 14 Sep 2009) | 8 lines

Backport r1023461 - fix restore of always show matched 'download later'
message setting (https://bugzilla.novell.com/show_bug.cgi?id=471760).

Thomas, I forgot to CC you on the original commit, CCing you know in
case you want this for a branch.

CCMAIL: mcguire@kde.org

------------------------------------------------------------------------
r1023576 | winterz | 2009-09-14 23:24:44 +0000 (Mon, 14 Sep 2009) | 5 lines

Backport r1023565 by tmcguire from trunk to the 4.3 branch:

Return the correct timespec for UTC and floating.
Now all day events in KOrganizer don't disappear anymore after editing.

------------------------------------------------------------------------
r1023689 | dfaure | 2009-09-15 09:09:08 +0000 (Tue, 15 Sep 2009) | 3 lines

You must call plugActionList again in slotNewToolbarConfig, otherwise the action lists disappear.
BUG: 207299

------------------------------------------------------------------------
r1023897 | winterz | 2009-09-15 14:13:00 +0000 (Tue, 15 Sep 2009) | 2 lines

revert mistaken commits

------------------------------------------------------------------------
r1024448 | tmcguire | 2009-09-16 16:14:36 +0000 (Wed, 16 Sep 2009) | 1 line

Backport the fixes for the incorrect iterator porting from trunk to the 4.3 branch.
------------------------------------------------------------------------
r1024449 | scripty | 2009-09-16 16:14:42 +0000 (Wed, 16 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1024451 | tmcguire | 2009-09-16 16:15:50 +0000 (Wed, 16 Sep 2009) | 20 lines

Backport r1024429 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1022284 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1022284 | winterz | 2009-09-11 13:19:22 +0200 (Fri, 11 Sep 2009) | 9 lines
  
  Merged revisions 1022235 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1022235 | mutz | 2009-09-11 04:40:28 -0400 (Fri, 11 Sep 2009) | 1 line
    
    Don't leave a signTestNode lying around if we've already written out a deferred decryption block. Fixes regression kolab/issue3577.
  ........
................


------------------------------------------------------------------------
r1024458 | tmcguire | 2009-09-16 16:17:02 +0000 (Wed, 16 Sep 2009) | 24 lines

Backport r1024425 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1021557 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1021557 | winterz | 2009-09-09 16:07:16 +0200 (Wed, 09 Sep 2009) | 13 lines
  
  Merged revisions 1021405 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1021405 | mutz | 2009-09-09 02:10:05 -0400 (Wed, 09 Sep 2009) | 5 lines
    
    certmanager/lib/ui/messagebox.cpp: make saved auditlog valid HTML
    
    Before, we were writing out the QtML that we have in the QTextEdit. Now, we
    keep a pristine copy of the audit log as received from the backend, and wrap
    it in <html><head><title>{caption}</title></head><body>...</body></html>
  ........
................


------------------------------------------------------------------------
r1024907 | dfaure | 2009-09-17 14:44:39 +0000 (Thu, 17 Sep 2009) | 4 lines

Add null-check in setTitle too (r990396 was about tabWidthForMaxChars).
Fix will be in 4.3.2.
BUG: 185597

------------------------------------------------------------------------
r1025325 | cgiboudeaux | 2009-09-18 12:06:27 +0000 (Fri, 18 Sep 2009) | 5 lines

Fix an include issue when building kdepim branch vs. kdelibs trunk + 4.3/experimental

MERGE: none


------------------------------------------------------------------------
r1026315 | tstaerk | 2009-09-21 12:43:04 +0000 (Mon, 21 Sep 2009) | 4 lines

Allow handing over a parameter to work on a specific iCalendar file.
Backport of 1026234.
CCBUGS:194865

------------------------------------------------------------------------
r1026391 | winterz | 2009-09-21 18:09:27 +0000 (Mon, 21 Sep 2009) | 4 lines

Backport r1023692 by tmcguire from trunk to the 4.3 branch:

Remove unneeded code, this is done in setTimeEditorsEnabled() already, and even correctly (obeying allday setting)

------------------------------------------------------------------------
r1026392 | winterz | 2009-09-21 18:09:54 +0000 (Mon, 21 Sep 2009) | 4 lines

Backport r1023693 by tmcguire from trunk to the 4.3 branch:

Didn't want to remove this in r1023692.

------------------------------------------------------------------------
r1026393 | winterz | 2009-09-21 18:10:23 +0000 (Mon, 21 Sep 2009) | 7 lines

Backport r1023697 by tmcguire from trunk to the 4.3 branch:

Set the correct time zone when setting the defaults.
This and my previous commits have the effect that when creating all day events, the time
zone is correctly shown as "Floating".


------------------------------------------------------------------------
r1026400 | winterz | 2009-09-21 18:26:14 +0000 (Mon, 21 Sep 2009) | 10 lines

Backport r1026321 by winterz from trunk to the 4.3 branch:

use qobject_cast instead of dynamic_cast where possible
check for invalid start/end dates
check for incidence=0
in general, a bunch of little changes for guarding against crashes in Month view.
CCBUG: 197507
MERGE: 4.3


------------------------------------------------------------------------
r1026401 | winterz | 2009-09-21 18:26:40 +0000 (Mon, 21 Sep 2009) | 6 lines

Backport r1026322 by winterz from trunk to the 4.3 branch:

guard against showing a popup for an invalid date.
MERGE: 4.3


------------------------------------------------------------------------
r1027230 | tmcguire | 2009-09-23 16:14:58 +0000 (Wed, 23 Sep 2009) | 6 lines

Backport r1026498 by tmcguire from trunk to the 4.3 branch:

Use BroadcastStatus to set the status message, this fixes the problem of an oversized
progress bar in the status bar when fetching feeds in Kontact.


------------------------------------------------------------------------
r1027231 | tmcguire | 2009-09-23 16:16:15 +0000 (Wed, 23 Sep 2009) | 24 lines

Backport r1024426 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1021558 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1021558 | winterz | 2009-09-09 16:14:02 +0200 (Wed, 09 Sep 2009) | 13 lines
  
  Merged revisions 1021421 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1021421 | mutz | 2009-09-09 02:38:07 -0400 (Wed, 09 Sep 2009) | 5 lines
    
    QGpgMEJob: fix reads past-the-end in get_auditlog_as_html
    
    In Qt3, QByteArrays are not '\0'-delimited, so we need to pass the QByteArray
    size explicitly into QString::fromUtf8(). This removes the garbage at the end
    of the audit log.
  ........
................


------------------------------------------------------------------------
r1027233 | tmcguire | 2009-09-23 16:21:33 +0000 (Wed, 23 Sep 2009) | 30 lines

Backport r1027225 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1024389 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1024389 | winterz | 2009-09-16 16:06:47 +0200 (Wed, 16 Sep 2009) | 19 lines
  
  Merged revisions 1024159 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1024159 | tmcguire | 2009-09-16 04:44:29 -0400 (Wed, 16 Sep 2009) | 12 lines
    
    When renaming a folder, make sure the reference count is correct again, i.e. open
    the folder in the syncing code, as that closes the folder later.
    
    THis fixes some issues with the message list, as the folder got closed behind
    its back.
    
    Don't clear the selected message after renaming a folder as well.
    
    Also, remove some annoying debug output when syncing.
    
    Fixes kolab/issue3853
  ........
................


------------------------------------------------------------------------
r1027236 | tmcguire | 2009-09-23 16:22:15 +0000 (Wed, 23 Sep 2009) | 22 lines

Backport r1027226 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1024393 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1024393 | winterz | 2009-09-16 16:13:29 +0200 (Wed, 16 Sep 2009) | 11 lines
  
  Merged revisions 1024288 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1024288 | tmcguire | 2009-09-16 06:29:52 -0400 (Wed, 16 Sep 2009) | 4 lines
    
    Don't overwrite custom folder types. Treat the folder as a mail folder instead.
    
    kolab/issue2069
  ........
................


------------------------------------------------------------------------
r1027237 | tmcguire | 2009-09-23 16:23:05 +0000 (Wed, 23 Sep 2009) | 21 lines

Backport r1027228 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1025188 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1025188 | tmcguire | 2009-09-18 10:48:53 +0200 (Fri, 18 Sep 2009) | 10 lines
  
  Merged revisions 1025159 via svnmerge from 
  svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1025159 | tmcguire | 2009-09-18 10:22:08 +0200 (Fri, 18 Sep 2009) | 3 lines
    
    Fix regression that the shared seen flag was not saved for the inbox.
    kolab/issue3830
  ........
................


------------------------------------------------------------------------
r1027308 | winterz | 2009-09-23 20:04:18 +0000 (Wed, 23 Sep 2009) | 6 lines

Backport r1027261 by tmcguire from trunk to the 4.3 branch:

Better also set the start and end time spec for floating events.
MERGE: 4.3, e4


------------------------------------------------------------------------
r1027350 | winterz | 2009-09-23 21:29:43 +0000 (Wed, 23 Sep 2009) | 2 lines

increase the patch level for next week's 4.3.2 tagging.

------------------------------------------------------------------------
r1027454 | skelly | 2009-09-24 08:37:49 +0000 (Thu, 24 Sep 2009) | 3 lines

Fix tests in a gui-less environment


------------------------------------------------------------------------
r1028614 | winterz | 2009-09-27 18:25:51 +0000 (Sun, 27 Sep 2009) | 4 lines

backport SVN commit 1028613 by winterz:

don't show events that are already over. such events cannot be considered "upcoming".

------------------------------------------------------------------------
r1028862 | winterz | 2009-09-28 11:13:36 +0000 (Mon, 28 Sep 2009) | 7 lines

revert SVN commit 1028613 by winterz:
don't show events that are already over. such events cannot be considered "upcoming".

Sorry, I forgot we are in a mini-freeze.
Put this back after the mini-freeze is over for 4.3.3.


------------------------------------------------------------------------
r1028929 | djarvie | 2009-09-28 14:12:22 +0000 (Mon, 28 Sep 2009) | 2 lines

Bug 208521: Provide 'any time' option when deferring date-only alarms

------------------------------------------------------------------------
r1029329 | reed | 2009-09-29 14:51:38 +0000 (Tue, 29 Sep 2009) | 1 line

backport r1017719 from trunk (fix kalarm moc generation on !X11)
------------------------------------------------------------------------
r1029535 | scripty | 2009-09-30 03:08:36 +0000 (Wed, 30 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
