2008-01-05 18:13 +0000 [r757714]  mlaurent

	* branches/KDE/4.0/kdenetwork/filesharing/advanced/nfs/nfsfile.cpp,
	  branches/KDE/4.0/kdenetwork/filesharing/advanced/kcm_sambaconf/sambafile.cpp:
	  Backport: kdesu is not in standard path

2008-01-05 18:18 +0000 [r757716]  mlaurent

	* branches/KDE/4.0/kdenetwork/filesharing/advanced/propsdlgplugin/propertiespage.cpp:
	  An other fix for kdesu

2008-01-05 19:57 +0000 [r757747]  taupter

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/avdevice/videodevice.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/libkopete/avdevice/videodevice.h:
	  Backport of trunk's SVN commit 757376.

2008-01-05 20:17 +0000 [r757754]  mlaurent

	* branches/KDE/4.0/kdenetwork/kppp/pppdata.cpp: Backport: fix crash

2008-01-07 02:23 +0000 [r758163]  mattr

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/kopetepassword.cpp:
	  Fix bug 141016 for KDE 4.0.1 Backport from trunk CCBUG: 141016

2008-01-08 08:29 +0000 [r758553]  taupter

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/avdevice/videodevicepool.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/libkopete/avdevice/videodevice.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/kopete/config/avdevice/avdeviceconfig.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/libkopete/avdevice/videodevice.h:
	  Fixed image saturation problem with uvc devices. Image controls
	  now update accordingly when loaded. Added new pixel format
	  detection code.

2008-01-08 17:10 +0000 [r758651]  mueller

	* branches/KDE/4.0/kdenetwork/kopete/protocols/jabber/libiris/iris/xmpp-im/xmpp_vcard.cpp:
	  stop crashing in sigill

2008-01-09 19:25 +0000 [r758991]  reitelbach

	* branches/KDE/4.0/kdebase/apps/doc/dolphin/index.docbook,
	  branches/KDE/4.0/kdebase/workspace/solid/solidshell/main.cpp,
	  branches/KDE/4.0/kdegraphics/kolourpaint/pixmapfx/kpPixmapFX_ScreenDepth.cpp,
	  branches/KDE/4.0/kdenetwork/kget/transfer-plugins/kio/transferKio.cpp,
	  branches/KDE/4.0/kdegames/kblackbox/main.cpp: some collected i18n
	  typo fixes that did not make it into KDE 4.0 due to the message
	  freeze.

2008-01-10 22:30 +0000 [r759600]  uwolfer

	* branches/KDE/4.0/kdenetwork/kget/ui/transfersview.cpp,
	  branches/KDE/4.0/kdenetwork/kget/ui/transfersview.h: Backport:
	  SVN commit 758972 by jgoday: Fix expandable details widget when a
	  transfer is moved

2008-01-12 20:49 +0000 [r760507]  lappelhans

	* branches/KDE/4.0/kdenetwork/kget/ui/transfersview.cpp,
	  branches/KDE/4.0/kdenetwork/kget/ui/transfersview.h: Close
	  expandableDetails when row is removed...

2008-01-12 21:14 +0000 [r760518]  mlaurent

	* branches/KDE/4.0/kdenetwork/knewsticker/settingsdialog.cpp,
	  branches/KDE/4.0/kdenetwork/krdc/config/preferencesdialog.cpp,
	  branches/KDE/4.0/kdenetwork/kget/conf/preferencesdialog.cpp,
	  branches/KDE/4.0/kdenetwork/krfb/manageinvitationsdialog.cpp:
	  Backport: Fix help button

2008-01-13 14:31 +0000 [r760838]  ilic

	* branches/KDE/4.0/kdenetwork/kget/transfer-plugins/bittorrent/advanceddetails/iwfilelistmodel.cpp,
	  branches/KDE/4.0/kdenetwork/kget/transfer-plugins/bittorrent/advanceddetails/iwfiletreemodel.cpp:
	  i18n fixes.

2008-01-14 17:09 +0000 [r761362]  wstephens

	* branches/KDE/4.0/kdenetwork/kopete/protocols/gadu/kopete_gadu.desktop,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/messenger/kopete_messenger.desktop,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/meanwhile/kopete_meanwhile.desktop,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/qq/kopete_qq.desktop,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/sms/kopete_sms.desktop,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/testbed/kopete_testbed.desktop,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/winpopup/kopete_wp.desktop,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/groupwise/kopete_groupwise.desktop,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/irc/kopete_irc.desktop,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/jabber/kopete_jabber.desktop,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/msn/kopete_msn.desktop,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/telepathy/kopete_telepathy.desktop,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/yahoo/kopete_yahoo.desktop:
	  Remove erroneously translated Category fields since scripty is
	  incapable of doing it. This was causing the empty account list in
	  the add account wizard in Kopete. BUG: 155256

2008-01-14 17:17 +0000 [r761367]  wstephens

	* branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/aim/kopete_aim.desktop,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/icq/kopete_icq.desktop:
	  Do r761365 here too CCBUG: 155256

2008-01-14 19:18 +0000 [r761400]  uwolfer

	* branches/KDE/4.0/kdenetwork/kget/transfer-plugins/multisegmentkio/multisegkio.cpp:
	  Backport: SVN commit 761292 by mvaldes: Fix remove the .part
	  extension when the original file exits #155720

2008-01-17 22:36 +0000 [r762818]  uwolfer

	* branches/KDE/4.0/kdenetwork/krdc/mainwindow.cpp: Backport: SVN
	  commit 762816 by uwolfer: Fix off-by-one errors when start page
	  has been disabled. #156041

2008-01-19 14:58 +0000 [r763471]  uwolfer

	* branches/KDE/4.0/kdenetwork/kget/plasma/applet/plasma-kget.cpp:
	  Backport: SVN commit 763466 by lappelhans: Remember plasmoid-size
	  during theme-change #155400

2008-01-22 02:07 +0000 [r764558]  mattr

	* branches/KDE/4.0/kdenetwork/kopete/protocols/jabber/libiris/iris/include/xmpp_xdata.h:
	  Backport adding the export to the Field inner class

2008-01-22 19:44 +0000 [r764886-764883]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/kopete/chatwindow/chatview.cpp:
	  Backport commit 764853: Filter status changes if "Show events in
	  chat window" is set.

	* branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/icq/icqaccount.cpp:
	  Backport commit 764857: Show correct XStatus for invisible
	  action.

	* branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/liboscar/userdetails.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/icq/icqcontact.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/icq/icqaccount.cpp:
	  Backport commit 764879. XtrazStatusSpecified should be only used
	  for user details merge. Fixes wrong contact status if XStatus was
	  used before.

2008-01-23 21:12 +0000 [r765328-765327]  raabe

	* branches/KDE/4.0/kdenetwork/knewsticker/settingsdialog.cpp:
	  automatically merged revision 765024: - When opening the settings
	  dialog, make sure that the right page of the widget stack is
	  shown depending on the currently configured view mode.

	* branches/KDE/4.0/kdenetwork/knewsticker/itemviews.h,
	  branches/KDE/4.0/kdenetwork/knewsticker/itemviews.cpp:
	  automatically merged revision 765033: - Avoid crash when
	  switching from paged view to scrolling view while the paged view
	  is exchanging the item. We deleted the PagingItemView (which
	  eventually deleted all the HyperlinkItem objects) but didn't stop
	  the move in/out animations, so the timer accessed a dangling
	  pointer.

2008-01-23 23:20 +0000 [r765419]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/kopete/chatwindow/chatmessagepart.cpp:
	  Backport fix for bug 155170: Chat window always scrolls to the
	  bottom when a message is received CCBUG: 155170

2008-01-23 23:23 +0000 [r765422]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/kopete/chatwindow/kopetechatwindow.cpp:
	  Backport fix for bug 155237 and 155172: Autoscrolling in
	  unfocused tabs is not working CCBUG: 155237 CCBUG: 155172

2008-01-24 10:32 +0000 [r765573-765572]  taupter

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/avdevice/bayer.cpp
	  (added),
	  branches/KDE/4.0/kdenetwork/kopete/libkopete/avdevice/videodevice.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/libkopete/avdevice/sonix_compress.cpp
	  (added),
	  branches/KDE/4.0/kdenetwork/kopete/libkopete/avdevice/videodevice.h,
	  branches/KDE/4.0/kdenetwork/kopete/libkopete/avdevice/videoinput.h,
	  branches/KDE/4.0/kdenetwork/kopete/libkopete/avdevice/CMakeLists.txt:
	  Backported commit 765567 from trunk

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/avdevice/sonix_compress.h
	  (added),
	  branches/KDE/4.0/kdenetwork/kopete/libkopete/avdevice/bayer.h
	  (added): Commiting forgotten files.

2008-01-24 18:36 +0000 [r765807]  lappelhans

	* branches/KDE/4.0/kdenetwork/kget/core/kget.cpp: Backport from
	  rev.765802

2008-01-24 21:19 +0000 [r765910]  cconnell

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/kopetemessage.cpp:
	  Backport revision 758764 BUG:155216

2008-01-25 21:14 +0000 [r766437]  uwolfer

	* branches/KDE/4.0/kdenetwork/kget/mainwindow.cpp: Backport: SVN
	  commit 766435 by uwolfer: do not quit the app when it has been
	  minimized to system tray and a new transfer dialog gets opened
	  and closed again #156486

2008-01-26 20:16 +0000 [r766865]  porten

	* branches/KDE/4.0/kdenetwork/kppp/general.cpp: Removed bogus buddy
	  association. Must have been introduced during the port to
	  KIntNumInput.

2008-01-27 16:18 +0000 [r767246]  mattr

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/kopeteversion.h:
	  bump version for KDE 4.0.1

2008-01-28 21:13 +0000 [r767828]  uwolfer

	* branches/KDE/4.0/kdenetwork/kget/main.cpp: bump version

2008-01-29 07:26 +0000 [r768038]  mlaurent

	* branches/KDE/4.0/kdenetwork/kopete/kopete/config/appearance/tooltipeditwidget.ui:
	  Backport: wordwrap to avoid to have big dialogbox

2008-01-29 07:31 +0000 [r768040]  mlaurent

	* branches/KDE/4.0/kdenetwork/kopete/kopete/addaccountwizard/addaccountwizard.cpp:
	  Backport: Fix help button

2008-01-29 07:43 +0000 [r768042]  mlaurent

	* branches/KDE/4.0/kdenetwork/kopete/protocols/gadu/ui/gaduregisteraccountui.ui:
	  Backport: Add an other wordwrap

2008-01-29 07:55 +0000 [r768045]  mlaurent

	* branches/KDE/4.0/kdenetwork/kopete/plugins/alias/aliaspreferences.cpp:
	  Backport: add parent to dialogbox Update button when we delete
	  alias

2008-01-29 08:04 +0000 [r768048]  mlaurent

	* branches/KDE/4.0/kdenetwork/kopete/plugins/highlight/highlightpreferences.cpp:
	  Backport: disable when any item is selected

2008-01-29 08:12 +0000 [r768051]  mlaurent

	* branches/KDE/4.0/kdenetwork/kopete/plugins/privacy/privacypreferences.cpp:
	  Backport: Give the real parent

2008-01-29 08:18 +0000 [r768055]  mlaurent

	* branches/KDE/4.0/kdenetwork/kopete/plugins/privacy/contactselectorwidget.cpp:
	  Backport: Don't crash when metacontact is null

2008-01-29 08:27 +0000 [r768060]  mlaurent

	* branches/KDE/4.0/kdenetwork/kopete/protocols/msn/ui/msneditaccountwidget.cpp:
	  Backport: Don't give an error when we canceled dialogbox

2008-01-29 12:09 +0000 [r768107]  mlaurent

	* branches/KDE/4.0/kdenetwork/kopete/plugins/highlight/highlightpreferences.cpp:
	  Backport: port code

2008-01-29 12:22 +0000 [r768109]  mlaurent

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/ui/addressbookselectordialog.cpp:
	  Backport: Add an entry for help button (it's important that help
	  button works)

2008-01-29 22:39 +0000 [r768380]  uwolfer

	* branches/KDE/4.0/kdenetwork/kget/ui/droptarget.cpp: Backport: SVN
	  commit 768379 by uwolfer: correct way to hide the drop target
	  from the task list #156075

