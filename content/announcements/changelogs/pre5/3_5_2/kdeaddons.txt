2006-01-24 06:18 +0000 [r501864]  coolo

	* branches/KDE/3.5/kdeaddons/kate/snippets/katesnippets.desktop:
	  fix encoding

2006-02-02 17:21 +0000 [r504959]  henrique

	* branches/KDE/3.5/kdeaddons/konq-plugins/kuick/kmetamenu.h,
	  branches/KDE/3.5/kdeaddons/konq-plugins/kuick/kmetamenu.cpp: *
	  Disable the 'Contact' entry if there are no contacts. Patch by
	  Thomas McGuire <Thomas.McGuire@gmx.net> BUG: 84922

2006-02-03 19:21 +0000 [r505382]  jriddell

	* branches/KDE/3.5/kdeaddons/debian (removed): Remove debian
	  directory, now at
	  http://svn.debian.org/wsvn/pkg-kde/trunk/packages/kdeaddons
	  svn://svn.debian.org/pkg-kde/trunk/packages/kdeaddons

2006-02-15 00:45 +0000 [r509535]  annma

	* branches/KDE/3.5/kdeaddons/doc/konq-plugins/fsview/index.docbook:
	  fix 119975 in 3.5 branch first error was already corrected

2006-02-15 02:50 +0000 [r509548]  annma

	* branches/KDE/3.5/kdeaddons/doc/kate-plugins/insertcommand.docbook,
	  branches/KDE/3.5/kdeaddons/doc/kate-plugins/insertcommand.png
	  (added),
	  branches/KDE/3.5/kdeaddons/doc/kate-plugins/configure_insertcommand.png
	  (added): fix 111994 in 3.5 branch - add missing screenshot + add
	  a configure plugin screenshot and fix inaccuracies was fixed in
	  trunk (revision 509547) BUG=111994

2006-02-15 18:01 +0000 [r509853]  dhaumann

	* branches/KDE/3.5/kdeaddons/kate/tabbarextension/plugin_katetabbarextension.cpp:
	  fix: wrong modified state BUG: 121027

2006-02-15 23:27 +0000 [r509978]  annma

	* branches/KDE/3.5/kdeaddons/doc/kate-plugins/filetemplates.docbook:
	  remove empty <para> tag

2006-02-25 01:44 +0000 [r513330]  charis

	* branches/KDE/3.5/kdeaddons/konq-plugins/arkplugin/arkplugin.cpp:
	  Don't show the compress menu when we have selected the "." folder
	  BUG: 115595

2006-02-26 20:15 +0000 [r513884]  lueck

	* branches/KDE/3.5/kdeaddons/doc/konq-plugins/imgallery/index.docbook,
	  branches/KDE/3.5/kdeaddons/doc/kate-plugins/xmltools.docbook,
	  branches/KDE/3.5/kdeaddons/doc/konq-plugins/imgallery/look.png,
	  branches/KDE/3.5/kdeaddons/doc/konq-plugins/imgallery/thumbnails.png,
	  branches/KDE/3.5/kdeaddons/doc/konq-plugins/index.docbook,
	  branches/KDE/3.5/kdeaddons/doc/konq-plugins/khtmlsettings/index.docbook,
	  branches/KDE/3.5/kdeaddons/doc/kate-plugins/filetemplates.docbook,
	  branches/KDE/3.5/kdeaddons/doc/kate-plugins/xmlcheck.docbook,
	  branches/KDE/3.5/kdeaddons/doc/konq-plugins/fsview/index.docbook,
	  branches/KDE/3.5/kdeaddons/doc/konq-plugins/babel/cr16-action-babelfish.png
	  (removed), branches/KDE/3.5/kdeaddons/doc/konq-plugins/smbmounter
	  (removed),
	  branches/KDE/3.5/kdeaddons/doc/konq-plugins/babel/index.docbook,
	  branches/KDE/3.5/kdeaddons/doc/kate-plugins/insertcommand.docbook,
	  branches/KDE/3.5/kdeaddons/doc/konq-plugins/validators/index.docbook,
	  branches/KDE/3.5/kdeaddons/doc/kate-plugins/index.docbook,
	  branches/KDE/3.5/kdeaddons/doc/konq-plugins/imgallery/folders.png,
	  branches/KDE/3.5/kdeaddons/doc/konq-plugins/domtreeviewer/index.docbook:
	  updated kdeaddons docbooks for KDE 3.5.2
	  CCMAIL:kde-doc-english@kde.org

2006-02-27 15:36 +0000 [r514207]  mueller

	* branches/KDE/3.5/kdeaddons/noatun-plugins/blurscope/scopedisplayer.cpp:
	  fix undefined operation

2006-02-28 13:23 +0000 [r514439]  lukas

	* branches/KDE/3.5/kdeaddons/konq-plugins/kimgalleryplugin/imgalleryplugin.cpp:
	  fix broken comments file BUG: 102646

2006-03-05 16:06 +0000 [r515980]  reitelbach

	* branches/KDE/3.5/kdeaddons/kate/Makefile.am: Compile all plugins!
	  (insertcommand, make and helloworld were missing) Thanks to
	  Burkhard Lück for figuring this out :) BUG:122102

2006-03-05 18:37 +0000 [r516028]  lueck

	* branches/KDE/3.5/kdeaddons/kate/insertcommand/Makefile.am: fix
	  for untranslatable string BUG:122103 CCMAIL:kde-i18n-doc@kde.org

2006-03-08 12:24 +0000 [r516728]  charis

	* branches/KDE/3.5/kdeaddons/konq-plugins/arkplugin/arkplugin.cpp:
	  KMimeType returns .zip .ZIP .rar and .RAR. We should select zip
	  and rar as the extensions instead of the capitals counterparts
	  BUG: 122640

2006-03-08 23:09 +0000 [r516867]  charis

	* branches/KDE/3.5/kdeaddons/konq-plugins/arkplugin/arkplugin.cpp:
	  Better implementation. Don't be sure that KMimeType will always
	  return the same results BUG: 122640 CCMAIL: thiago@kde.org

2006-03-09 02:03 +0000 [r516886]  fedemar

	* branches/KDE/3.5/kdeaddons/konq-plugins/arkplugin/arkplugin.cpp:
	  Don't show the popup menu if the protocol doesn't support writing
	  (e.g. http)

2006-03-09 21:26 +0000 [r517083]  fedemar

	* branches/KDE/3.5/kdeaddons/konq-plugins/arkplugin/arkplugin.cpp:
	  Show only the "Extract To..." item if the protocol doesn't
	  support writing.

2006-03-12 01:18 +0000 [r517743]  charis

	* branches/KDE/3.5/kdeaddons/konq-plugins/arkplugin/arkplugin.cpp:
	  Do not let the user compress multiple file from the context menu
	  as .gz .bz2. He should use real archives BUG: 123175

2006-03-12 14:12 +0000 [r517890]  bram

	* branches/KDE/3.5/kdeaddons/konq-plugins/sidebar/metabar/src/metabarwidget.cpp:
	  Fix untranslated string CCMAIL:kde-i18n-doc@kde.org
	  CCMAIL:kde-i18n-nl@kde.org

2006-03-12 19:50 +0000 [r517990]  bram

	* branches/KDE/3.5/kdeaddons/konq-plugins/sidebar/metabar/src/defaultplugin.cpp:
	  More untranslated strings. I quickly read the source for more
	  eventual untranslated string, but couldn't find any more.
	  CCMAIL:kde-i18n-doc@kde.org

2006-03-17 21:34 +0000 [r519776]  coolo

	* branches/KDE/3.5/kdeaddons/kdeaddons.lsm: tagging 3.5.2

