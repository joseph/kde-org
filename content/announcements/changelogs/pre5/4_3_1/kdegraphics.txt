------------------------------------------------------------------------
r1005983 | pino | 2009-08-02 18:52:42 +0000 (Sun, 02 Aug 2009) | 4 lines

make the gs kprocess collecct its output; disable an error message box (not doable in threads)
will be in kde 4.3.1
CCBUG: 192405

------------------------------------------------------------------------
r1007319 | gateau | 2009-08-05 14:17:18 +0000 (Wed, 05 Aug 2009) | 1 line

Bumped version numbers
------------------------------------------------------------------------
r1007320 | gateau | 2009-08-05 14:17:23 +0000 (Wed, 05 Aug 2009) | 1 line

Consider we always have a current album.
------------------------------------------------------------------------
r1007938 | lunakl | 2009-08-06 14:46:32 +0000 (Thu, 06 Aug 2009) | 7 lines

Backport r1007936:
WX11BypassWM for the region window, it's a special-purpose window
and shouldn't be managed by KWin at all (r555388 and I mean it).
Otherwise KWin can enforce a different geometry (bnc#528731).
Also grab keyboard and mouse, to accompany this.


------------------------------------------------------------------------
r1008701 | scripty | 2009-08-08 03:20:40 +0000 (Sat, 08 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1009823 | aacid | 2009-08-10 22:29:58 +0000 (Mon, 10 Aug 2009) | 4 lines

Backport r1009822 | aacid | 2009-08-11 00:28:54 +0200 (Tue, 11 Aug 2009) | 3 lines

Do not start a search if it's already running

------------------------------------------------------------------------
r1010218 | aacid | 2009-08-11 22:05:20 +0000 (Tue, 11 Aug 2009) | 7 lines

Backport r1010216 | aacid | 2009-08-12 00:04:01 +0200 (Wed, 12 Aug 2009) | 5 lines

If KMimeType::findByUrl(url) fails because for example we are doing
  kolourpaint http://www.kde.org/announcements/4.3/images/kde430-inspired.png
use KMimeType::findByPath(tempFile) so that it works, probably this is not totally
correct but WorksForMe and given kolourpaint has no maintainer i'm commiting it

------------------------------------------------------------------------
r1011224 | scripty | 2009-08-14 03:20:57 +0000 (Fri, 14 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1013164 | scripty | 2009-08-19 03:21:06 +0000 (Wed, 19 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1013401 | sars | 2009-08-19 19:30:02 +0000 (Wed, 19 Aug 2009) | 3 lines

Do not discard the selection if it spans the whole width _or_ height but not both. (now in 4.3)

CCBUG:202979
------------------------------------------------------------------------
r1014385 | sars | 2009-08-22 17:58:15 +0000 (Sat, 22 Aug 2009) | 1 line

Backport bug fix to enable the preview resolution to be set on backends that use combobox for the resolution.
------------------------------------------------------------------------
r1015189 | aacid | 2009-08-24 20:36:08 +0000 (Mon, 24 Aug 2009) | 2 lines

Backport aacid * r1015188 okular/trunk/KDE/kdegraphics/okular/generators/spectre/okularGhostview.desktop: add a InitialPreference bigger than karbon since we are better than karbon on previewing ps

------------------------------------------------------------------------
r1016034 | gateau | 2009-08-26 20:43:10 +0000 (Wed, 26 Aug 2009) | 1 line

Bumped version number
------------------------------------------------------------------------
r1016038 | gateau | 2009-08-26 20:52:18 +0000 (Wed, 26 Aug 2009) | 3 lines

Really quit when all images have been saved.

BUG: 199531
------------------------------------------------------------------------
r1016039 | gateau | 2009-08-26 20:52:23 +0000 (Wed, 26 Aug 2009) | 1 line

Updated
------------------------------------------------------------------------
r1016200 | scripty | 2009-08-27 03:36:40 +0000 (Thu, 27 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1016238 | sars | 2009-08-27 08:59:30 +0000 (Thu, 27 Aug 2009) | 1 line

Back-port the preview speed up bug fix from trunk (revision 992872)
------------------------------------------------------------------------
r1016249 | pino | 2009-08-27 09:46:10 +0000 (Thu, 27 Aug 2009) | 2 lines

bump version to 0.9.1

------------------------------------------------------------------------
r1016257 | pino | 2009-08-27 10:16:19 +0000 (Thu, 27 Aug 2009) | 3 lines

temporarly force 72x72 DPI
CCBUG: 204864

------------------------------------------------------------------------
