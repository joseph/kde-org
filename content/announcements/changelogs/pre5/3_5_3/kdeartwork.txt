2006-03-25 23:57 +0000 [r522559]  nickell

	* branches/KDE/3.5/kdeartwork/kwin-styles/smooth-blend/client/smoothblend.cc:
	  Update the mask when resizing or when a show event occurs

2006-04-03 00:12 +0000 [r525838]  tyrerj

	* branches/KDE/3.5/kdeartwork/pics/hicolor/hi16-action-tab_new_bg.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/index.theme,
	  branches/KDE/3.5/kdeartwork/pics/hicolor/hi16-action-tab_new.png
	  (added),
	  branches/KDE/3.5/kdeartwork/pics/hicolor/hi16-action-tab_remove.png
	  (added),
	  branches/KDE/3.5/kdeartwork/pics/hicolor/hi16-action-tab_remove_other.png
	  (added): Adding new hicolor icons: hi16-action-tab_new_bg
	  hi16-action-tab_remove_other hi16-action-tab_remove
	  hi16-action-tab_new Removing inherits from KDEClassic so that it
	  will use HiColor.

2006-04-30 16:58 +0000 [r535874]  tyrerj

	* branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/16x16/actions/tab_new.png,
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/16x16/actions/tab_remove.png,
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/16x16/actions/tab_new_bg.png:
	  Updating KDEClassic icons "tab_*"

2006-04-30 18:50 +0000 [r535936]  tyrerj

	* branches/KDE/3.5/kdeartwork/pics/hicolor/hi32-action-tab_new.png
	  (added),
	  branches/KDE/3.5/kdeartwork/pics/hicolor/hi32-action-tab_remove.png
	  (added),
	  branches/KDE/3.5/kdeartwork/pics/hicolor/hi32-action-tab_remove_other.png
	  (added),
	  branches/KDE/3.5/kdeartwork/pics/hicolor/hi32-action-tab_new_bg.png
	  (added): Adding missing 32x32 "tab_*" icons in HiColor

2006-04-30 19:57 +0000 [r535957]  tyrerj

	* branches/KDE/3.5/kdeartwork/pics/hicolor/hi22-action-view_fit_height.png
	  (added),
	  branches/KDE/3.5/kdeartwork/pics/hicolor/hi32-action-view_fit_height.png
	  (added),
	  branches/KDE/3.5/kdeartwork/pics/hicolor/hi16-action-view_fit_height.png
	  (added): Adding missing icon "view_fit_height" icons in HiColor

2006-05-05 05:07 +0000 [r537544]  tyrerj

	* branches/KDE/3.5/kdeartwork/pics/hicolor/hi22-action-tab_new.png
	  (added),
	  branches/KDE/3.5/kdeartwork/pics/hicolor/hi22-action-tab_remove.png
	  (added),
	  branches/KDE/3.5/kdeartwork/pics/hicolor/hi22-action-tab_remove_other.png
	  (added),
	  branches/KDE/3.5/kdeartwork/pics/hicolor/hi22-action-tab_new_bg.png
	  (added): Adding missing 22x22 "tab_*" icons in HiColor

2006-05-07 18:32 +0000 [r538392]  tyrerj

	* branches/KDE/3.5/kdeartwork/pics/hicolor/hi22-action-tab_duplicate.png
	  (added),
	  branches/KDE/3.5/kdeartwork/pics/hicolor/hi32-action-tab_duplicate.png
	  (added),
	  branches/KDE/3.5/kdeartwork/pics/hicolor/hi22-action-tab_breakoff.png
	  (added),
	  branches/KDE/3.5/kdeartwork/pics/hicolor/hi32-action-tab_breakoff.png
	  (added): Adding missing 22x22 and 32x32 "tab_*" icons in HiColor

2006-05-07 20:05 +0000 [r538403]  jriddell

	* branches/KDE/3.5/kdeartwork/pics (removed): Remove hicolour
	  icons. KDE does not ship hicolour icons, they are for third part
	  applications. If adding icons to KDE add them to crystalsvg or
	  kdeclassic

2006-05-12 23:50 +0000 [r540314]  tyrerj

	* branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/32x32/actions/tab_new.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/32x32/actions/tab_duplicate.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/22x22/actions/tab_remove.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/22x22/actions/tab_breakoff.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/32x32/actions/tab_remove.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/22x22/actions/tab_new_bg.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/32x32/actions/tab_breakoff.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/32x32/actions/tab_new_bg.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/22x22/actions/tab_new.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/22x22/actions/tab_duplicate.png
	  (added): Adding missing sizes of "tab_*" icons in KDEClassic

2006-05-22 04:15 +0000 [r543449-543448]  howells

	* branches/KDE/3.5/kdeartwork/kwin-styles/glow/glowclient.cpp: kill
	  a compiler warning

	* branches/KDE/3.5/kdeartwork/kscreensaver/kdesavers/rotation.h:
	  fix compile on solaris. sorry for the delay BUG: 105159

2006-05-23 10:34 +0000 [r543997]  binner

	* branches/KDE/3.5/kdevelop/configure.in.in,
	  branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kontact/src/main.cpp,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdepim/akregator/src/aboutdata.h,
	  branches/KDE/3.5/kdepim/kmail/kmversion.h,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdepim/kaddressbook/kabcore.cpp,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdeutils/kcalc/version.h,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version for KDE
	  3.5.3

