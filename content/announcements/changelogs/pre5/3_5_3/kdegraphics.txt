2006-03-20 20:03 +0000 [r520779]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp:
	  Use UGooString for dates Fixes bug 123938 Will be present in KDE
	  3.5.3 BUGS: 123938

2006-03-23 21:23 +0000 [r521921]  mm

	* branches/KDE/3.5/kdegraphics/kamera/configure.in.in: also check
	  for gphoto2-port-config and use it (bug and patch 124159)

2006-03-24 13:55 +0000 [r522113]  woebbe

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Makefile.am: fixed
	  INCLUDES

2006-03-30 23:58 +0000 [r524707]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/src/imlibwidget.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickio.h (removed),
	  branches/KDE/3.5/kdegraphics/kuickshow/src/filecache.cpp (added),
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imlibwidget.h,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickdata.h,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickfile.cpp (added),
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickshow.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/filecache.h (added),
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickimage.cpp
	  (added),
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imagewindow.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickfile.h (added),
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickshow.h,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickimage.h (added),
	  branches/KDE/3.5/kdegraphics/kuickshow/src,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imagewindow.h,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/main.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/Makefile.am,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/printing.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/version.h,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickio.cpp (removed),
	  branches/KDE/3.5/kdegraphics/kuickshow/src/printing.h: - fixed
	  one of the most long-standing bugs: non-local browsing (i.e. you
	  can browse remote directories like local ones now, flipping
	  through the images with PageUp/Down etc., including pre-loading
	  images) BUG: 74088 BUG: 93543 BUG: 120724 Browsing through /media
	  should work now, but can be improved by not "downloading" from
	  there. Framework to do this is there, though. CCBUG: 120724 -
	  also allow saving to remote URLs - fixed image not being
	  completely maximized when opening image in fullscreen mode BUG:
	  80805 - performance improvement - layed groundwork for fixing
	  #48812 and #101986 CCBUG: 48812 CCBUG: 101986 - make sure all
	  temporary files get deleted BUG: 74323 #103875 is already fixed
	  in KDE 3.5.2, forgot to close the report BUG: 103875

2006-03-31 00:03 +0000 [r524709]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/src/imlibwidget.h: removed
	  unused member

2006-03-31 00:11 +0000 [r524711]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/ChangeLog: updated

2006-03-31 10:48 +0000 [r524828]  woebbe

	* branches/KDE/3.5/kdegraphics/kuickshow/src/kuickfile.h: -pedantic

2006-04-01 11:18 +0000 [r525229]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/XRef.cc: Comparing
	  int with uints is a very bad idea. Fixes
	  http://www.produkte.panasonic.de/doc/u0000/x70/userguide/x70_userguide_1_d_-.pdf

2006-04-03 11:46 +0000 [r525940]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/src/imlibwidget.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imdata.h,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickimage.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imagewindow.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/version.h,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickimage.h,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imdata.cpp: provide
	  antialiasing image resizing (currently defaulting to use it,
	  GUI-option will follow) BUG: 108887 BUG: 58521

2006-04-05 18:15 +0000 [r526835]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/JBIG2Stream.cc: Some
	  fix for JBIG2 PDF stolen from poppler ;-) Fixes
	  http://ia311040.us.archive.org/~rkumar/test1_opt.pdf for instance

2006-04-12 09:23 +0000 [r528970]  coolo

	* branches/KDE/3.5/kdegraphics/kamera/kioslave/kamera.cpp:
	  ForwardingSlave simply invents mimetypes if there are none (for
	  whatever reason). So give them a mimetype so konqueror is not
	  confused between application/octet-stream and S_IFDIR

2006-04-12 09:31 +0000 [r528975]  mm

	* branches/KDE/3.5/kdegraphics/kamera/kioslave/kamera.cpp: more
	  suse 10.1 fixes, mostly for better "usb:xxx,yyy" handling.

2006-04-18 00:17 +0000 [r530899]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kpdf/ui/pageview.cpp: consistency:
	  use KCursor over QCursor everywhere, not just in some places

2006-04-21 02:14 +0000 [r532087]  tyrerj

	* branches/KDE/3.5/kdegraphics/kpdf/part.cpp: Minor change to use
	  icon: "show_side_panel".

2006-04-22 05:22 +0000 [r532483]  mpyne

	* branches/KDE/3.5/kdegraphics/kamera/kioslave/kamera.cpp:
	  Suspicious code fixes, use delete [] on char[].

2006-04-22 05:28 +0000 [r532485-532484]  mpyne

	* branches/KDE/3.5/kdegraphics/kiconedit/kicongrid.cpp: Suspicious
	  code fixes. Use delete[] on int[] arrays. kiconedit.cpp also had
	  a flag for the kdither_32_to_8 stuff at line 2018. However, n is
	  clipped to 8 (The sequence is n = 1,2,4,8) so I don't think that
	  there is really a problem. Someone else might want to look
	  though.

	* branches/KDE/3.5/kdegraphics/kghostview/kgvdocument.cpp:
	  Suspicious code fixes. Open files after checking to see whether
	  we will use them so that we don't leak file handles. Even better
	  would be to use that nifty QFile class from Qt, but whatever.

2006-04-22 05:48 +0000 [r532486]  mpyne

	* branches/KDE/3.5/kdegraphics/kpovmodeler/pmspheresweep.cpp,
	  branches/KDE/3.5/kdegraphics/kpovmodeler/pmpovrayparser.cpp,
	  branches/KDE/3.5/kdegraphics/kpovmodeler/pmvariant.cpp:
	  Suspicious code fixes for kpovmodeler. ',' && foo() is always
	  true, so reorganize parentheses to match the intent of the tests.
	  Initialize i to a value in pmspheresweep.cpp. Set the success
	  flag for all cases in pmvariant.cpp I did not touch
	  pmdockwidget.cpp:2474 as I'm not sure if the loop should break or
	  not.

2006-04-22 06:04 +0000 [r532487]  mpyne

	* branches/KDE/3.5/kdegraphics/kfile-plugins/gif/gif-info.c: More
	  suspicious code fixes. Fix a file handle leak, and use of
	  uninitialized data.

2006-04-22 06:12 +0000 [r532489]  mpyne

	* branches/KDE/3.5/kdegraphics/kmrml/kmrml/lib/kmrml_config.cpp:
	  More suspicious code fixes. In this case two char* are
	  inadvertently compared rather than the string (which probably
	  worked since they were the same strings in the common case when
	  comparing against localhost).

2006-04-22 16:07 +0000 [r532710]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kooka/kookaview.cpp: trying to
	  understand the logic and fix the "Re: Suspicious code in
	  kdegraphics-3.5.2" CCMAIL: freitag@suse.de

2006-04-22 16:14 +0000 [r532716]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kooka/scanpackager.cpp: remove
	  unnecessary check for QDragMoveEvent being 0L ("Re: Suspicious
	  code in kdegraphics-3.5.2")

2006-04-22 16:32 +0000 [r532728]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kooka/scanpackager.cpp: more fixing
	  of "Re: Suspicious code in kdegraphics-3.5.2" CCMAIL:
	  freitag@suse.de

2006-04-22 16:42 +0000 [r532733]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/src/kuickshow.cpp: check
	  for steps == 0, even though that never happens "Re: Suspicious
	  code in kdegraphics-3.5.2"

2006-04-22 16:49 +0000 [r532737]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/src/defaultswidget.cpp:
	  removed superfluous check for null pointer "Re: Suspicious code
	  in kdegraphics-3.5.2"

2006-04-22 17:04 +0000 [r532751-532748]  pfeiffer

	* branches/KDE/3.5/kdegraphics/libkscan/img_canvas.cpp: added
	  fall-through comments "Re: Suspicious code in kdegraphics-3.5.2"

	* branches/KDE/3.5/kdegraphics/kfile-plugins/jpeg/exif.cpp: moved
	  one break to the place where it was supposed to be "Re:
	  Suspicious code in kdegraphics-3.5.2"

2006-04-23 15:36 +0000 [r533073]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/GfxState.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/JPXStream.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/ui/pageview.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp:
	  some improvements from the "suspicious code mail" in k-c-d

2006-04-28 18:03 +0000 [r535156]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/shell/shell.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/part.cpp: enable the print
	  action when opening a file though drag and drop BUGS: 126406

2006-04-29 15:24 +0000 [r535464]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/JBIG2Stream.cc: fix
	  memory leak. More at
	  https://bugs.freedesktop.org/show_bug.cgi?id=6765

2006-04-29 18:34 +0000 [r535543]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/src/generalwidget.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/generalwidget.h,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickfile.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/version.h,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imdata.cpp: GUI: make
	  smooth scaling configurable (adds one new message) make use of
	  KIO::NetAccess::mostLocalURL() so as to avoid "downloading" files
	  from e.g. /media

2006-05-01 23:46 +0000 [r536395]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/README,
	  branches/KDE/3.5/kdegraphics/kolourpaint/VERSION: up ver for KDE
	  3.5.2, 14 minutes before freeze - coolo, you made me late for
	  class :P

2006-05-03 18:14 +0000 [r537010]  coolo

	* branches/KDE/3.5/kdegraphics/kviewshell/plugins/djvu/libdjvu/JB2Image.cpp:
	  avoid crash (CID 1869)

2006-05-03 19:57 +0000 [r537052]  aacid

	* branches/KDE/3.5/kdegraphics/kghostview/kghostview_part.desktop
	  (added), branches/KDE/3.5/kdegraphics/kghostview/Makefile.am,
	  branches/KDE/3.5/kdegraphics/kghostview/kghostview.desktop: Fix
	  KGhostView .desktop files BUGS: 126488

2006-05-05 20:48 +0000 [r537798]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Function.cc: quick
	  fix for 126760, you get the same (or it seems to me) image that
	  with acroread so may not be THAT bad BUGS: 126760

2006-05-07 06:15 +0000 [r538159]  coolo

	* branches/KDE/3.5/kdegraphics/kghostview/kgvdocument.cpp: avoid
	  crash (CID 1771)

2006-05-14 16:28 +0000 [r540804]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/part.cpp: fix code, it was
	  behaving correctly just by good luck, thanks antlarr for showing
	  the light CCMAIL: larrosa@kde.org

2006-05-20 14:24 +0000 [r542840]  woebbe

	* branches/KDE/3.5/kdegraphics/ksvg/impl/libs/libtext2path/src/GlyphTracer.h,
	  branches/KDE/3.5/kdegraphics/ksvg/plugin/backends/libart/GlyphTracerLibart.cpp,
	  branches/KDE/3.5/kdegraphics/ksvg/configure.in.in,
	  branches/KDE/3.5/kdegraphics/ksvg/impl/libs/libtext2path/src/GlyphTracer.cpp:
	  compile with FreeType 2.2.x. please test, if it compiles with
	  older FreeType versions.

2006-05-20 16:50 +0000 [r542927]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/src/kuickshow.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickshow.h: fix
	  remote browsing of http-urls ;) BUG: 127684

2006-05-20 17:11 +0000 [r542933]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/src/kuickfile.cpp: fix
	  showing of non-existing images :-) two error dialogs will be
	  shown (one by KIO, one by kuickshow), but I guess we can live
	  with that for now BUG: 127685

2006-05-20 18:00 +0000 [r542944]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/src/kuickfile.cpp: guard a
	  kdWarning()

2006-05-20 18:25 +0000 [r542953]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/shell/main.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/VERSION: 0.5.3

2006-05-21 15:08 +0000 [r543265]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/src/filecache.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickshow.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/filecache.h: be nice
	  and ask a singleton to delete itself

2006-05-23 10:34 +0000 [r543997]  binner

	* branches/KDE/3.5/kdevelop/configure.in.in,
	  branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kontact/src/main.cpp,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdepim/akregator/src/aboutdata.h,
	  branches/KDE/3.5/kdepim/kmail/kmversion.h,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdepim/kaddressbook/kabcore.cpp,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdeutils/kcalc/version.h,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version for KDE
	  3.5.3

