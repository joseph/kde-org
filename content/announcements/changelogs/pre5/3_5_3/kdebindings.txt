2006-03-21 13:49 +0000 [r521003]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib.kdevprj (removed),
	  branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/lib/Qt/qtruby.rb,
	  branches/KDE/3.5/kdebindings/qtruby/qtruby-only.patch (removed),
	  branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/smokeruby.h,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * Added various
	  explicit calls to method_missing() for methods which are defined
	  in Kernel and Object, such as 'exec', 'select', 'format' etc.
	  Otherwise, the corresponding methods in the Smoke library were
	  not being invoked correctly. * Removed a virtual destructor
	  compile warning * Removed obsolete qtruby-only.patch and kdevelop
	  project file

2006-03-21 17:44 +0000 [r521143]  rdale

	* branches/KDE/3.5/kdebindings/dcopjava/binding/org/kde/DCOP/Makefile.am:
	  * Fix problem when srcdir != builddir CCMAIL:
	  kde-bindings@kde.org

2006-03-22 12:38 +0000 [r521367]  rdale

	* branches/KDE/3.5/kdebindings/korundum/ChangeLog,
	  branches/KDE/3.5/kdebindings/korundum/rubylib/korundum/lib/KDE/korundum.rb:
	  * Added various explicit calls to method_missing() for methods
	  which are defined in Kernel and Object, such as 'exec', 'select',
	  'format' etc. Otherwise, the corresponding methods in the Smoke
	  library were not being invoked correctly.

2006-03-29 16:25 +0000 [r524058]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * Added a
	  KDE::KonsolePart class for when a konsolePart KPart is
	  dynamically loaded.

2006-03-29 16:34 +0000 [r524061]  rdale

	* branches/KDE/3.5/kdebindings/korundum/rubylib/korundum/Korundum.cpp,
	  branches/KDE/3.5/kdebindings/korundum/ChangeLog,
	  branches/KDE/3.5/kdebindings/korundum/rubylib/korundum/lib/KDE/korundum.rb:
	  * Added a KDE::KonsolePart class for when a konsolePart KPart is
	  dynamically loaded. It adds the five methods from the
	  ExtTerminalInterface to the KParts::ReadOnlyPart for interacting
	  with the konsolePart. * Example usage: factory =
	  KDE::LibLoader.self().factory("libkonsolepart") if ! factory.nil?
	  @konsole = factory.create(self) end @konsole.autoStartShell =
	  true @konsole.startProgram("/bin/echo", ["echo", "-n"])
	  @konsole.showShellInDir( Qt::Dir.home.path ) @konsole.sendInput(
	  "puts 'hello there konsole'" ) * Fixes problem reported by Ranjan
	  Ghosh CCMAIL: rgpublic@gmx.net

2006-03-29 19:45 +0000 [r524124]  rdale

	* branches/KDE/3.5/kdebindings/korundum/rubylib/korundum/Korundum.cpp:
	  * Special case KonsolePart.metaObject() as it isn't in the Smoke
	  library. Otherwise, KParts::ReadOnlyPart.metaObject() would be
	  called.

2006-03-29 20:16 +0000 [r524140]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * Don't special
	  case open() in virtual method callbacks, because all the Qt
	  classes with open methods now have explicit open() method calls.
	  So it isn't possible that Kernel.open() will be called wrongly
	  anymore.

2006-04-17 18:48 +0000 [r530830]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/lib/Qt/qtruby.rb,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * Made :foobar a
	  synonym for 'foobar()' for slots and signals * Added some calls
	  for Qt::Event.type methods to force them to go via
	  method_missing()

2006-04-20 14:45 +0000 [r531876]  rdale

	* branches/KDE/3.5/kdebindings/korundum/rubylib/examples/kludgeror.rb:
	  * Fixed non-working kludgeror

2006-04-20 14:52 +0000 [r531881]  rdale

	* branches/KDE/3.5/kdebindings/korundum/rubylib/examples/xmlgui.rb:
	  * Fixed non-working xmlgui.rb app

2006-05-03 20:05 +0000 [r537057]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * The qt_emit()
	  and qt_invoke() methods are overriden by the QtRuby runtime. When
	  they are called the runtime looks for a Ruby slot or signal
	  matching the call, and invokes it if found. If a Ruby version
	  wasn't found for a signal however, the qt_invoke() method was
	  being called in the Smoke library instead of qt_emit(). This
	  occasionally caused a crash in code using KDE::HTMLPart.

2006-05-07 22:07 +0000 [r538419]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/lib/Qt/qtruby.rb,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * An an type of
	  Q_UINT16 wasn't working with QtRuby. Fixes problem reported by
	  maelclerambault. CCBUGS: 126928

2006-05-14 18:25 +0000 [r540825]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/lib/Qt/qtruby.rb,
	  branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * When an unknown
	  C++ class is found, a corresponding Ruby class is now is now
	  created. For instance, if a KPresenter KPart is loaded, a
	  KDE::PresenterDoc class is created. * It is now possible to set
	  and get properties without needing to use Qt::Object.property()
	  and Qt::Object.setProperty(). For instance: factory =
	  KDE::LibLoader.self().factory("libkpresenterpart") @kpresenter =
	  factory.create(self) p @kpresenter.presentationDuration
	  @kpresenter.presentationDuration = true * A Qt::Variant.to_ruby
	  method has been added which returns a Ruby value corresponding to
	  the current value of a Qt::Variant * A boolean Qt::Variant can
	  now be constructed with a Qt::Variant.new(true) call, and a dummy
	  second arg (as for C++) is no longer needed.

2006-05-15 09:35 +0000 [r540985]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/lib/Qt/qtruby.rb,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * Removed the
	  customized version of Kernel.exec, Kernel.open etc as they aren't
	  needed anymore.

2006-05-16 09:08 +0000 [r541434]  rdale

	* branches/KDE/3.5/kdebindings/korundum/rubylib/korundum/Korundum.cpp,
	  branches/KDE/3.5/kdebindings/korundum/ChangeLog,
	  branches/KDE/3.5/kdebindings/korundum/rubylib/korundum/lib/KDE/korundum.rb:
	  * Fixed regression causing KDE::UniqueApplication.exec to not
	  work * Removed the konsole_part_metaobject() C function as it
	  isn't needed anymore

2006-05-20 20:24 +0000 [r542990]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/lib/Qt/qtruby.rb,
	  branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * Fix regression
	  for dynamic class creation via QMetaObject info causing a crash.
	  * A list of properties is now returned via
	  Qt::MetaObject.propertyNames for a Qt::Object with properties for
	  the inspect and pretty_print methods.

2006-05-23 10:34 +0000 [r543997]  binner

	* branches/KDE/3.5/kdevelop/configure.in.in,
	  branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kontact/src/main.cpp,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdepim/akregator/src/aboutdata.h,
	  branches/KDE/3.5/kdepim/kmail/kmversion.h,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdepim/kaddressbook/kabcore.cpp,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdeutils/kcalc/version.h,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version for KDE
	  3.5.3

