2008-04-03 14:48 +0000 [r793291]  mueller

	* branches/KDE/4.0/kdeutils/ark/app/ark.desktop: add
	  application/x-zip-compressed

2008-04-25 23:01 +0000 [r801212]  pino

	* branches/KDE/4.0/kdeutils/sweeper/sweeper.cpp,
	  branches/KDE/4.0/kdeutils/sweeper/privacyfunctions.h,
	  branches/KDE/4.0/kdeutils/sweeper/privacyaction.h: backport three
	  fixes from trunk: - load the configuration of the checked items
	  at startup, and save it when closing - activate the auto-saving
	  of the main window settings - add the information about the
	  configuration key in each privacy action, and store the info with
	  that key; the keys are the same as used in kde3

2008-04-29 15:38 +0000 [r802473]  mueller

	* branches/KDE/4.0/kdeutils/kdf/disklist.cpp: backport 802472

