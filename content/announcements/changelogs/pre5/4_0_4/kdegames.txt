2008-04-01 22:45 +0000 [r792733]  tyrerj

	* branches/KDE/4.0/kdegames/kpat/background.svg,
	  branches/KDE/4.0/kdegames/kpat/dealer.cpp: CCBUG:159492 This
	  patch is a necessary condition to fix the problem. But, it may
	  not be sufficent. Also replacing "background.svg" file with one
	  which has been optimized.

2008-04-13 13:00 +0000 [r796361]  mueller

	* branches/KDE/4.0/kdegames/ksudoku/src/gui/views/CMakeLists.txt,
	  branches/KDE/4.0/kdegames/ksudoku/src/gui/CMakeLists.txt,
	  branches/KDE/4.0/kdegames/ksudoku/src/logic/CMakeLists.txt: merge
	  r793349 to fix build with cmake 2.6

2008-04-28 18:47 +0000 [r802177-802171]  milliams

	* branches/KDE/4.0/kdegames/libkdegames/highscore/kscoredialog.cpp:
	  Backport 801819 Set login name or fullname as default username in
	  KScoreDialog if no other name is available

	* branches/KDE/4.0/kdegames/libkdegames/highscore/kscoredialog.cpp:
	  Backport 802125 Allow score <= 0.

	* branches/KDE/4.0/kdegames/libkdegames/highscore/kscoredialog.cpp:
	  Backport 802167 * Prefill the scoredialog with the last used
	  name, if any * Use the correct buttons (Cancel and OK, when a new
	  score in being inserted).

