---
title: Plasma 5.21.5 complete changelog
version: 5.21.5
hidden: true
plasma: true
type: fulllog
---
{{< details title="Bluedevil" href="https://commits.kde.org/bluedevil" >}}
+ [sendfile] Update org.kde.bluedevilsendfile.desktop. [Commit.](http://commits.kde.org/bluedevil/449af5d6dd0bab9c269ec00ce48a4edeafb4f14f) 
+ [kio] Add trailing slash to device url. [Commit.](http://commits.kde.org/bluedevil/ba9fbb6bbdadc67ccfbe953cdf2f0f0e8a609cb2) Fixes bug [#409179](https://bugs.kde.org/409179)
{{< /details >}}

{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Fix SplitterProxy not clearing when above another QSplitterHandle. [Commit.](http://commits.kde.org/breeze/6c3a46e0f1abb697fb0900f21196cafcbd8ee37c) Fixes bug [#431921](https://bugs.kde.org/431921)
+ Fix logic error in SplitterProxy::setEnabled. [Commit.](http://commits.kde.org/breeze/b2615c3dd74ac77b91cf31c49c1589f82752f4ac) 
{{< /details >}}

{{< details title="breeze-gtk" href="https://commits.kde.org/breeze-gtk" >}}
+ Use Breeze-style arrows everywhere. [Commit.](http://commits.kde.org/breeze-gtk/0d370bdfa5a3f9fd250d49a4ac4ba19efa25fa5f) 
+ Gtk3, gtk4: make comboboxes look more true to breeze qstyle. [Commit.](http://commits.kde.org/breeze-gtk/ebcaca8d5983427e15d9e69c9ce17ed03e87e545) 
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Add missing Qt5Concurrent to target_link_libraries. [Commit.](http://commits.kde.org/discover/3c578beeb5d3748d9fdacae4ace42fe6098d9184) 
+ Fwupd: Make sure we initalise the device defaults. [Commit.](http://commits.kde.org/discover/e039091932dc607f15fdfded875dc487ecd6949c) Fixes bug [#435785](https://bugs.kde.org/435785)
+ Pk: Only clear notifier results when they're successful. [Commit.](http://commits.kde.org/discover/4f627c7566d5b729adb78c7d7b7c629f51f41917) 
+ Pk: Fix dependencies view appearing. [Commit.](http://commits.kde.org/discover/74406978e6014572defa4bbb92e9634871ca3d64) 
+ Pk: Clear the offline updates results after displaying them. [Commit.](http://commits.kde.org/discover/4b53275c369fb745ace8fd2170343d9f516f58b4) 
+ Only wrap flatpak.h includes in extern "C" with older versions. [Commit.](http://commits.kde.org/discover/12f83062b01d5ba946e6f98503b72d4e58721aa4) 
{{< /details >}}

{{< details title="Dr Konqi" href="https://commits.kde.org/drkonqi" >}}
+ Move bugz api from qurlquery to a custom container. [Commit.](http://commits.kde.org/drkonqi/7e02852def51c91ed64d525a999d798952786fc7) Fixes bug [#435442](https://bugs.kde.org/435442)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Effects: Properly reset present times in coverswitch and flipswitch effects. [Commit.](http://commits.kde.org/kwin/170a436aaccdbd7a69bd2516ad921545d3454f14) Fixes bug [#433471](https://bugs.kde.org/433471)
+ Fix crash when stopping PipeWire streaming. [Commit.](http://commits.kde.org/kwin/3a51749f09a95e37cc225c4c3f920925fc1de64b) 
+ Lockscreen: also activate lock screen windows. [Commit.](http://commits.kde.org/kwin/a8a4c9a881ecd57327b0c35f723292772f4537f3) Fixes bug [#427882](https://bugs.kde.org/427882)
+ Wayland: Check workspace position after creating decoration. [Commit.](http://commits.kde.org/kwin/e7d53f0848498958aa8dccf3bb515283ba07a5f8) Fixes bug [#432326](https://bugs.kde.org/432326)
+ Platforms/drm: Fix crash in EglGbmBackend::presentOnOutput(). [Commit.](http://commits.kde.org/kwin/5a57eeafc840e97e14b4ed465a60f02dae57d6d1) 
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Do not leak the created QQuickItems. [Commit.](http://commits.kde.org/libksysguard/c5a71b1b9641e31b73b9f82f58ca98a4d7181a4f) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Fix renaming shortcut for files selected via selection button and popup. [Commit.](http://commits.kde.org/plasma-desktop/db0121941fe3d766c9d3a98fec3c67618a979a64) Fixes bug [#425436](https://bugs.kde.org/425436)
+ Calculate cursor position relative to top-left corner of current screen. [Commit.](http://commits.kde.org/plasma-desktop/2b7b0c5419455a16425fc06e25cce2cd1f434121) Fixes bug [#436216](https://bugs.kde.org/436216)
+ Set a transientParent for the FolderView ContextMenu. [Commit.](http://commits.kde.org/plasma-desktop/30f74282fbb4252f18786737765a83dcae3d8b88) See bug [#417378](https://bugs.kde.org/417378)
+ Fix: unneeded keyboardConfig.save(). [Commit.](http://commits.kde.org/plasma-desktop/9b3f1333979d8ad8973f8ee253a25157767a5e86) 
+ Kcms/users: elide text that can't fit in users list. [Commit.](http://commits.kde.org/plasma-desktop/2e8fbfbcbd22728363c5d139bd808f9feffe77d6) Fixes bug [#435700](https://bugs.kde.org/435700)
{{< /details >}}

{{< details title="Plasma Firewall" href="https://commits.kde.org/plasma-firewall" >}}
+ [kcm/core/rule] Add missing include to make bsd happy. [Commit.](http://commits.kde.org/plasma-firewall/54f844cc0ab2cfaf545decb9fb5546d8d5f4dd89) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Forward openconnect usergroup. [Commit.](http://commits.kde.org/plasma-nm/aa872eca0575af615ca918acbb1c8e743d1074d5) Fixes bug [#435561](https://bugs.kde.org/435561)
+ Applet: delay model updates on expanded password field. [Commit.](http://commits.kde.org/plasma-nm/64dc6234b6980172bb53084c16a0e4e693d6011a) Fixes bug [#435430](https://bugs.kde.org/435430)
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Increment volume by percent instead of a constant volumeStep. [Commit.](http://commits.kde.org/plasma-pa/26994d67552d21ce41f641de473be2b7b3509834) Fixes bug [#434769](https://bugs.kde.org/434769). Fixes bug [#435522](https://bugs.kde.org/435522)
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ Also append fixedColumns  on initial load. [Commit.](http://commits.kde.org/plasma-systemmonitor/d804571bb08c0a29a16fa788c2e8572f533e2a19) 
+ Remove RowLayout from KillDialog DialogButtonBox. [Commit.](http://commits.kde.org/plasma-systemmonitor/5d0c9551ab7e0a5b2f1e97f1453871e970e3bb3a) Fixes bug [#435192](https://bugs.kde.org/435192)
+ Cleanup old controller if FaceLoader gets destroyed. [Commit.](http://commits.kde.org/plasma-systemmonitor/f151d1a1af683a2d1a50a0a2c524a23e5738d129) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ [applets/digital-clock] Fix "both pointSize and pixelSize defined" warning. [Commit.](http://commits.kde.org/plasma-workspace/2b1009b7bd9f8153576022199a5d4ee1fc4f0ef5) 
+ [applets/digitalclock] Fix timezone filter. [Commit.](http://commits.kde.org/plasma-workspace/07156230590f4d4ae5af1852104e28653a76da8f) 
+ Add a -1 to make the mouse input not redirect to a margin pixel. [Commit.](http://commits.kde.org/plasma-workspace/40c7390b85855b0a8685888a1bb30c9b3e81c23d) Fixes bug [#413736](https://bugs.kde.org/413736)
+ Fix crash on drag-and-drop over panel. [Commit.](http://commits.kde.org/plasma-workspace/9e986f8c85ad79e75ed43c1af751ded44afbbc5b) Fixes bug [#398440](https://bugs.kde.org/398440)
+ [Notifications] Don't generate previews of zero size. [Commit.](http://commits.kde.org/plasma-workspace/18273cd8fae51cf66546f5e4ade51f527ab80e50) See bug [#430862](https://bugs.kde.org/430862)
+ Krunner: Always set cursor position to end when focusing search field. [Commit.](http://commits.kde.org/plasma-workspace/f5503e9a565390d264b2cfd28a48d71a66c51bce) Fixes bug [#435604](https://bugs.kde.org/435604)
+ Don't extract QML as Java. [Commit.](http://commits.kde.org/plasma-workspace/23a830244bebab8637dc0c42271674f3410fb957) 
{{< /details >}}

{{< details title="SDDM KCM" href="https://commits.kde.org/sddm-kcm" >}}
+ Raise the maximum allowed UID to 60513. [Commit.](http://commits.kde.org/sddm-kcm/36978d75efe014609ecbce720c461a4463bcd498) 
{{< /details >}}

