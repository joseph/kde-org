---
title: Plasma 5.22.5 complete changelog
version: 5.22.5
hidden: true
plasma: true
type: fulllog
---
{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Kstyle: consider activeSubControls when deciding arrow colour. [Commit.](http://commits.kde.org/breeze/37f43577ee2b1a095a6791c9dd7d2f6a3dfa7852) Fixes bug [#434884](https://bugs.kde.org/434884)
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Show actual shortcut for Refresh action's tooltip. [Commit.](http://commits.kde.org/discover/4c753725aaaa177601231593543a6c3a9119ec62) Fixes bug [#438916](https://bugs.kde.org/438916)
{{< /details >}}

{{< details title="KDE GTK Config" href="https://commits.kde.org/kde-gtk-config" >}}
+ Don't apply window_decorations.css to non-Breeze themes. [Commit.](http://commits.kde.org/kde-gtk-config/41b4edf6e0065adccf9ab1aa45746d34e951a5d4) 
+ Make sure to actually commit GSettings changes. [Commit.](http://commits.kde.org/kde-gtk-config/9e55858a64808b8a56511126662a10de462d71ed) 
{{< /details >}}

{{< details title="ksystemstats" href="https://commits.kde.org/ksystemstats" >}}
+ Fix handling of IPV6 addresses. [Commit.](http://commits.kde.org/ksystemstats/f8523a922f3f6032aa7e06a4dcbebd9f94065c9d) Fixes bug [#436296](https://bugs.kde.org/436296)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Fix drag and drop raise with Xwayland windows. [Commit.](http://commits.kde.org/kwin/37c9d43b93b299fc20b57fb1717805aaa65a572e) Fixes bug [#440534](https://bugs.kde.org/440534)
+ Platforms/drm: check wl_eglstream buffers before attaching. [Commit.](http://commits.kde.org/kwin/9056eccd6241223b36d899357680acd52ac68804) Fixes bug [#440852](https://bugs.kde.org/440852)
+ Platforms/drm: fix gpu removal. [Commit.](http://commits.kde.org/kwin/5611c905cea800ee57ba5690c3ec75b086258612) See bug [#441372](https://bugs.kde.org/441372)
+ Kcm/kwindecoration: Fix window thumbnail sizes. [Commit.](http://commits.kde.org/kwin/99b603af9ca9e3736d28675d58aeaad0d7268e9e) 
+ Platforms/drm: consider KWIN_DRM_DEVICES for hotplugged gpus. [Commit.](http://commits.kde.org/kwin/a30f6cc9cb0d0640b95244c9a9d17ac7d63e841e) 
+ Platforms/drm: fix crashing debug operator. [Commit.](http://commits.kde.org/kwin/855bdbd34c0bcc9709b3e86b80db3fd1294069ba) 
+ Copy shape region to Deleted. [Commit.](http://commits.kde.org/kwin/ad62f7c3b7ba60182ae42d1573dae8fd57560ad7) Fixes bug [#440001](https://bugs.kde.org/440001). Fixes bug [#438458](https://bugs.kde.org/438458). Fixes bug [#435378](https://bugs.kde.org/435378)
+ Avoid discarding previous pixmap. [Commit.](http://commits.kde.org/kwin/f432ba7821cea2302d040c8bfc7e3cb7d9540874) Fixes bug [#439689](https://bugs.kde.org/439689)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ [emojier] Set KLocalizedContext. [Commit.](http://commits.kde.org/plasma-desktop/aa0bf7f144a671be24e58310fdcf44eed8cfd880) See bug [#441097](https://bugs.kde.org/441097)
+ [emojier] Register QAbstractItemModel to QML. [Commit.](http://commits.kde.org/plasma-desktop/ce3c7fdef0f824e68b5f662f43dbf38f7eeb9998) 
+ [containments/desktop] Stop hardcoding applet handle tooltip position. [Commit.](http://commits.kde.org/plasma-desktop/6e37179bbc76407111ad6d0ced44ab18fe9d0c44) Fixes bug [#425636](https://bugs.kde.org/425636)
+ [containments/panel] Fix panel applet configuration with touch. [Commit.](http://commits.kde.org/plasma-desktop/78bd7883be916ff92fb6d4fc4ca18e32f4ff9e8d) Fixes bug [#439918](https://bugs.kde.org/439918)
+ [desktoppackage/alternatives] Don't overflow with long text. [Commit.](http://commits.kde.org/plasma-desktop/9d3af571e68f699f60d1fd559b5c815b6d1ead8c) Fixes bug [#439098](https://bugs.kde.org/439098)
+ [applets/taskmanager] Don't make pinned app tooltips interactive. [Commit.](http://commits.kde.org/plasma-desktop/2f6e5b1fd406e7a42224196eb282f6fb475895dd) Fixes bug [#439094](https://bugs.kde.org/439094)
+ [containments/desktop] Fix applet overlay icon size with touch interaction. [Commit.](http://commits.kde.org/plasma-desktop/29cf0fd997b4ce642b4292237f5a194db87f2f27) 
+ Use the correct panel prefixes from the theme. [Commit.](http://commits.kde.org/plasma-desktop/1c711c1ee865c357b0cb893496013839c68e05ba) 
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ Fix page export. [Commit.](http://commits.kde.org/plasma-systemmonitor/d13ed9565150bba454036a99382de381a4620ebe) Fixes bug [#440551](https://bugs.kde.org/440551)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ [kcms/icons] Fix i18n usage. [Commit.](http://commits.kde.org/plasma-workspace/76df30e4c144c7853e030680057e76c294523c2a) 
+ [kcms/icons] Clip ListView in popup. [Commit.](http://commits.kde.org/plasma-workspace/fc88eee220fa5eda7d76cb9bccdd2f90ca0a0f8b) 
+ [applets/digitalclock] Let long timezones list scroll. [Commit.](http://commits.kde.org/plasma-workspace/97320f2f74f5a275f4dc8ccd3ac765955af058f9) Fixes bug [#439147](https://bugs.kde.org/439147)
+ [applets/digital-clock] Fix header in RTL mode. [Commit.](http://commits.kde.org/plasma-workspace/f32b3221a9286b12d340bf5478d7971b08ca39b0) Fixes bug [#438083](https://bugs.kde.org/438083)
{{< /details >}}

