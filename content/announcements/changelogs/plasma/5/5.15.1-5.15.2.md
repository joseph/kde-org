---
aliases:
- /announcements/plasma-5.15.1-5.15.2-changelog
hidden: true
plasma: true
title: Plasma 5.15.2 Complete Changelog
type: fulllog
version: 5.15.2
---

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Provide a better package icon. <a href='https://commits.kde.org/discover/ae18ea936184ececc9548f53bce5eb80ddec9c89'>Commit.</a>
- Mark all radio buttons as such. <a href='https://commits.kde.org/discover/668e91785d6028019e3da08da9a46204f712611f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404400'>#404400</a>
- Make sure we connect the action to its bridge. <a href='https://commits.kde.org/discover/bc628e5e8ef0df3e1dcad604d9d4af16f0363d23'>Commit.</a>
- Remove connectivity flash at start. <a href='https://commits.kde.org/discover/c1772e61adb8b790882fce80b55a2f66c98affe0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404610'>#404610</a>
- [Updates page] Try our best to not elide version strings. <a href='https://commits.kde.org/discover/d2d33a20b0e08e12f54c46d1918aa2160c92bd81'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404624'>#404624</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19210'>D19210</a>

### <a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a>

- Check whether the list of entries is empty before accessing it. <a href='https://commits.kde.org/kde-gtk-config/ed7b28ccc77531b3f66ed3a6a83057dfa890e9d2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401002'>#401002</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- [PotD] Port mostly to QQC2. <a href='https://commits.kde.org/kdeplasma-addons/dd5a234771ab0d4f4f03b91d43c21b2a4ac5caed'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404248'>#404248</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18975'>D18975</a>

### <a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a>

- Only enable the module help button if the module actually has help. <a href='https://commits.kde.org/kinfocenter/6677f21d96bedfb03b79ac1b6f8b912482821905'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19188'>D19188</a>
- The "Module Help" button gets enabled when help is available. <a href='https://commits.kde.org/kinfocenter/585b92af8f0e415ffb8c2cb6a4712a8fe01fbbc4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392597'>#392597</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19187'>D19187</a>
- [about-distro] let distributions choose VERSION_ID or VERSION. <a href='https://commits.kde.org/kinfocenter/99f10f1e5580f373600478746016c796ac55e3f9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19226'>D19226</a>
- About System can now use os-release's LOGO configuration. <a href='https://commits.kde.org/kinfocenter/d5b56b448c90295fce3809c465631bdbc515adeb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19181'>D19181</a>
- About System now displays "variant" information from os-release. <a href='https://commits.kde.org/kinfocenter/affa7b1139b47e3747a2ce0807f461c0d730b617'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19182'>D19182</a>
- [about-distro] update osrelease parser for latest values in spec. <a href='https://commits.kde.org/kinfocenter/d1ce16a6e558a88c7d9409bceb6d7b1e8a1c71a3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19160'>D19160</a>
- [about-distro] run absolute paths through qicon intead of qpixmap. <a href='https://commits.kde.org/kinfocenter/e7e44a7af825c7db2c86399940b3d260a730cdf5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19033'>D19033</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Don't destroy DecorationRenderer in setup/finish compositing. <a href='https://commits.kde.org/kwin/9a68cbd9b110b529021c128ea6650ce6774f462a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18921'>D18921</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- [Kickoff] Don't respond to window resizing/tiling shortcuts. <a href='https://commits.kde.org/plasma-desktop/7a33e1130612238ec6031a8c6ecc675a6133a172'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402614'>#402614</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19023'>D19023</a>
- [Kickoff] remove hint state. <a href='https://commits.kde.org/plasma-desktop/1312ab1cb4fee5a4b844559955ba594f729cfab2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19154'>D19154</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- [weather] envcan: Fix typo in lowercase icon lookup string. <a href='https://commits.kde.org/plasma-workspace/58e6cc6e119f7b149d623058cc50a5ad247a8c5f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19176'>D19176</a>
- Don't show entries with NoDisplay=true with the applauncher containmentaction. <a href='https://commits.kde.org/plasma-workspace/7816978b51204b82471359a8c0960a13710d489f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19157'>D19157</a>
- Don't show entries with NoDisplay=true with the applauncher containmentaction. <a href='https://commits.kde.org/plasma-workspace/8192bd3aec27a77bb1ab903bb928ffe0a2f4c517'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19157'>D19157</a>
- Improve screen filtering for global menu applet. <a href='https://commits.kde.org/plasma-workspace/f6bf54808f32390009fdf6cdbb52627670be733a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404500'>#404500</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19126'>D19126</a>

### <a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a>

- Fix selection of multiple files. <a href='https://commits.kde.org/xdg-desktop-portal-kde/8ef6eb3f7a950327262881a5f3b21fac1d3064c6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404739'>#404739</a>
- Properly convert filename to string from bytearray. <a href='https://commits.kde.org/xdg-desktop-portal-kde/27651ce217568e0292e3161cd2da1754b4961b5e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404445'>#404445</a>